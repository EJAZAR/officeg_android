var Config = new require("../config");
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var uuid = require('uuid');
var db = new require('../helpers/db');
var MessageService = new require('../helpers/message');
var chatManager = require('../controllers/chatController');
var utils = require("../helpers/utils");
var Crypt = require("../helpers/crypt");

var https = require('https');
var _ = require('underscore');

//var authenticateModel = require("../models/authenticateModel");
/*
const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(Config.DB, neo4j.auth.basic(Config.DB_USER, Config.DB_PASS));
const session = driver.session();
*/
var relationship = exports;

async function createUser(user) {
    var fid = utils.createFid();
    user.data = Crypt.encryptPerson(user.data);
    if(user.data.countrycode == undefined || user.data.countrycode == null)
        user.data.countrycode = '';
    console.log('createUser user = ' + JSON.stringify(user));
    var result = await db.execute("CREATE(person:Person {fid:{fid}, isowner:true, countrycode:{cc}, created_date:{ts}, updated_date:{ts}, mobilenumber:{number}, status: {status}, isowner:{isowner}, ownerfid:{ownerfid}, displayname:{displayname}, firstname:{firstname}, lastname:{lastname}, dob:{dob}, gender:{gender}, doi:{doi}, isalive:{isalive}, deceaseddate:{deceaseddate}, profileimage:{profileimage}, familynotification:{familynotification}}) RETURN person",
    {
        status:user.status,
        ts: new Date().getTime(),
        fid: fid,
        isowner:user.isowner,
        ownerfid:fid,
        number: user.data.mobilenumber,
        cc: user.data.countrycode,
        displayname:user.data.displayname,
        firstname:user.data.firstname,
        lastname:user.data.lastname,
        dob:user.data.dob,
        gender:user.data.gender,
        doi:user.data.doi,
        isalive:user.data.isalive,
        deceaseddate:user.data.deceaseddate,
        profileimage:user.data.profileimage,
        familynotification:'true'
    });
    console.log('createUser result = ' + JSON.stringify(result));

    user.data.dob = Crypt.decryptField(user.data.dob);
    user.data.deceaseddate = Crypt.decryptField(user.data.deceaseddate);
    user.data.displayname = Crypt.decryptField(user.data.displayname); // this value will be used for connecting relationship

    if(user.data.dob != undefined && user.data.dob != "" && user.data.dob != null) {
        var eventid = user.data.dob.substring(0, 5).replace('/', '_');
        try {
            await db.execute("MERGE(event:Event{eventid:{eventid}}) ON CREATE SET event.eventid = {eventid}",{eventid:eventid});
            await db.execute("MATCH (event:Event{eventid:{eventid}}) , (person:Person{fid:{fid}})  CREATE UNIQUE (person)-[r:EVENT_ON{type:{type}}]->(event)", {fid:fid, eventid:eventid, type:"birthday"});
        }catch(error) {
            console.log(error);
        }
    }
    if(user.data.deceaseddate != undefined && user.data.deceaseddate != "" && user.data.deceaseddate != null) {
        var eventid = user.data.deceaseddate.substring(0, 5).replace('/', '_');
        try {
            await db.execute("MERGE(event:Event{eventid:{eventid}}) ON CREATE SET event.eventid = {eventid}",{eventid:eventid});
            await db.execute("MATCH (event:Event{eventid:{eventid}}) , (person:Person{fid:{fid}})  CREATE UNIQUE (person)-[r:EVENT_ON{type:{type}}]->(event)", {fid:fid, eventid:eventid, type:"deathday"});
        }catch(error) {
            console.log(error);
        }
    }

    return result;
}

async function createRelationship(destinationUser, relationshiptype, message, mode) {
   
    console.log("Create relation data : " + JSON.stringify(destinationUser)+" and "+relationshiptype+" and "+message+" and "+ mode +" and "+ utils.getOppositeRelation(relationshiptype, destinationUser.gender));

    if(message != undefined && message != '' && message != null){
    	var result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) WHERE NOT (source)-[:IS_RELATED_TO]-(destination) WITH source, destination CREATE(notification:Notification {type:1, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}, srcfid:source.fid, desfid:destination.fid, relation:{srcreltype} }) CREATE (source)-[:IS_RELATED_TO{reltype:{srcreltype}, status:{status}, created_date:{ts}, uid:{uid}, displayname:{dn}}]->(destination) CREATE (destination)-[:IS_RELATED_TO{reltype:{desreltype},status:{status}, created_date:{ts}, uid:{uid}, displayname:source.displayname}]->(source) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) return notification;"
        , {
            srcfid: destinationUser.newfid,
            desfid: destinationUser.ownerfid,
            ts:new Date().getTime(),
            srcreltype:relationshiptype,
            desreltype:utils.getOppositeRelation(relationshiptype, destinationUser.gender),
            dn:Crypt.encryptField(destinationUser.displayname),
            desc:"Connection Request",
            status: (destinationUser.type == "3")? "approved" : "pending",
            msg:message,
            mode : mode,
            uid: uuid.v4()
        });
	} else {
    	var result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) WHERE NOT (source)-[:IS_RELATED_TO]-(destination) WITH source, destination CREATE (source)-[:IS_RELATED_TO{reltype:{srcreltype}, status:{status}, created_date:{ts}, uid:{uid}, displayname:{dn}}]->(destination) CREATE (destination)-[:IS_RELATED_TO{reltype:{desreltype},status:{status}, created_date:{ts}, uid:{uid}, displayname:source.displayname}]->(source) return source;"
        , {
            srcfid: destinationUser.newfid,
            desfid: destinationUser.ownerfid,
            ts:new Date().getTime(),
            srcreltype:relationshiptype,
            desreltype:utils.getOppositeRelation(relationshiptype, destinationUser.gender),
            dn:Crypt.encryptField(destinationUser.displayname),
            desc:"Connection Request",
            status: (destinationUser.type == "3")? "approved" : "pending",
            mode : mode,
            uid: uuid.v4()
        });
	}
    console.log('result = '+ JSON.stringify(result));
    return result;
}

async function createfgdataRelationship(destinationUser, relationshiptype, message, mode) {
    console.log('oppositerelation 3 = ' + relationshiptype + ' and ' + destinationUser.gender + ' and ' + utils.getOppositeRelation(relationshiptype, destinationUser.gender));
    console.log("Create relation data : " + JSON.stringify(destinationUser)+" and "+relationshiptype+" and "+message+" and "+ mode +" and "+ utils.getOppositeRelation(relationshiptype, destinationUser.gender));
    var result = await db.execute("MATCH(source:Rep{fid:{srcfid}}), (destination:Person{fid:{desfid}}) WHERE NOT (source)-[:IS_CREATED]-(destination) WITH source, destination CREATE(notification:Notification {type:1, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}, srcfid:source.fid, desfid:destination.fid, relation:{srcreltype} }) CREATE (source)-[:IS_CREATED{reltype:{srcreltype}, status:{status}, created_date:{ts}, uid:{uid}, displayname:{dn}, familyname:{familyname}}]->(destination) CREATE (destination)-[:CREATED_BY{reltype:{desreltype},status:{status}, created_date:{ts}, uid:{uid}, displayname:source.displayname}]->(source) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) return notification;"
        , {
            srcfid: destinationUser.ownerfid,
            desfid: destinationUser.newfid,
            ts:new Date().getTime(),
            srcreltype:relationshiptype,
            familyname: destinationUser.familyname,
            desreltype:utils.getOppositeRelation(relationshiptype, destinationUser.gender),
            dn:Crypt.encryptField(destinationUser.displayname),
            desc:"Connection Request",
            status: (destinationUser.type == "3")? "approved" : "pending",
            msg:message,
            mode : mode,
            uid: uuid.v4()
        });
        console.log('result = '+ JSON.stringify(result));
    return result;
}

relationship.fgdatacreatefamily = async function (request, response) {
    try {
        var data = request.body;
        console.log("fgdatacreatefamily request = " + JSON.stringify(data));
        //var key = data.secret_key;
        var relmobilenumber = data.mobilenumber;
        var countrycode = data.countrycode;
        var isowner = true;
        var key = "mobilenumber"; // This will destination person whom to be connected with
        var value = relmobilenumber;
        
        var sourceAttr = "fid";
        var sourceValue = data.ownerfid; // fid will be always coming with the left hand side person for whom the new relation to be attached.

        var relationshiptype = data.relationship;

        var isNewUser = !isowner;
        var result = null;
        
        // Query the requesting user details as user will be owner for new user
        var rep = null;
        var repQuery = await db.execute("MATCH(person:Rep) WHERE person.key={value} RETURN person", { value: data.secret_key});
        console.log("fgdatacreatefamily isowner = " + isowner);
        try {
            rep = repQuery.records[0]._fields[0].properties;
        }
        catch(e) {
            // This should not be come here
        }
        
        var newfid = data.fid;

        if(relmobilenumber != undefined && relmobilenumber != '' && relmobilenumber != null){
            console.log("Checking mobile number");
            result = await db.execute("MATCH(person:Person) WHERE person.mobilenumber = {value} RETURN person", { value: relmobilenumber});
            console.log("person result = " + JSON.stringify(result));
            if(!_.isEmpty(result.records)) {
		        console.log("person not empty");
                var relationship_ic = await db.execute("MATCH (person1:Person{mobilenumber:{value}})<-[ic:IS_CREATED]-(rep1:Rep{key:{key}}) return ic", {value: relmobilenumber, key: data.secret_key});
		        if(!_.isEmpty(relationship_ic.records)) {
		            console.log("relationship_ic not empty");
        	        response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
            		return;
	            } 
            } else{
		        console.log("User does not exists. Creating it...");
    		    result = await createUser({data:data, status:'approved', isowner:true, ownerfid:rep.fid});        
    		    console.log("else user result = " + JSON.stringify(result));
            }
        } else {
	        console.log("User does not exists. Creating it...");
    	    result = await createUser({data:data, status:'approved', isowner:true, ownerfid:rep.fid});        
    	    console.log("else user result = " + JSON.stringify(result));
        }
	
        const record = result.records[0];
        const person = record._fields[0].properties;
        // Get the person fid whom we need to connect the relationship
        newfid = person.fid;
        console.log("Connecting person fid is: " + newfid);

        var requestingPerson = rep;

        requestingPerson.displayname = Crypt.decryptField(requestingPerson.displayname);
        requestingPerson.gender = Crypt.decryptField(requestingPerson.gender);
        person.displayname = Crypt.decryptField(person.displayname);

        var message = "";
        var mode = "sms";
        var need2SendNotification = false;

        console.log("Creating relationship...");
        var result = await createfgdataRelationship({mobilenumber : relmobilenumber, displayname: data.displayname, fid:data.fid, type:data.type, ownerfid:data.ownerfid, newfid:newfid, gender:requestingPerson.gender, familyname:data.familyname}, relationshiptype, message, mode);
        if(_.isEmpty(result.records)) {
        	response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
        	return;
		} else {
        	if(need2SendNotification){
            	chatManager.sendNotificationMessage(person, {
                	reltype:relationshiptype,
	                type:"1",
    	            fid: "" + requestingPerson.fid,
        	        senderid:requestingPerson.fid,
            	    profileimage:Crypt.decryptField(requestingPerson.profileimage),
                	id:db.int(result.records[0]._fields[0].identity).toString(),
	                mode:"pushservice",
    	            message:message,
        	        datetime:new Date().getTime(),
            	    relationpath:""
	            }, message);
    	    }

        	if(data.type == "3") {
//            	relationship.sendRecommendedConnections(newfid, data.ownerfid, utils.getOppositeRelation(relationshiptype, requestingPerson.gender));
	        }
    	    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true, fid:person.fid} });
        }
    }
    catch (err) {
        console.log("Failed in fgdatacreatefamily : " + err);
        response.json(new ERROR(ErrorCode.FAILED_TO_CONNECT_PROFILE));
    }
}

relationship.fgdataconnectfamily = async function (request, response) {
    try {
        var data = request.body;
        //var key = data.secret_key;
        var relmobilenumber = data.mobilenumber;
        var countrycode = data.countrycode;
        var isowner = true;
        var key = "mobilenumber"; // This will destination person whom to be connected with
        var value = relmobilenumber;
        console.log("fgdataconnectfamily : " + JSON.stringify(data));

        var sourceAttr = "fid";
        var sourceValue = data.fid; // fid will be always coming with the left hand side person for whom the new relation to be attached.

        key = "fid";
        value = data.fid;
        sourceValue = data.fid;

        var relationshiptype = data.relationship;

        var isNewUser = !isowner;

        isowner = false;
        // Query the requesting user details as user will be owner for new user

        var newfid = data.fid;

        isNewUser = true;
        console.log('fgdataconnectfamily type = ' + data.type);

        if(data.type == "1") { // connect with phone
            key = "mobilenumber";
            value = relmobilenumber
        }
        else if(data.type == "2") { // connect with fid
            key = "fid";
            value = data.fid;
        } else if(data.type == "3") { // connect with fid
            if(relmobilenumber != undefined && relmobilenumber != '' && relmobilenumber != null){
                console.log("Checking mobile number");
                var result = await db.execute("MATCH(person:Person) WHERE person.mobilenumber = {value} RETURN person", { value: relmobilenumber});
                console.log("result " + JSON.stringify(result));
                if(!_.isEmpty(result.records)) {
                    console.log("result not empty");
                    response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
                    return;
                }
            }
        }
        console.log('fgdataconnectfamily type 2 = ' + data.type);

        var result = null;

        if(data.type == "1" || data.type == "2") {
            console.log("Checking presence to connect : " + key + " and value  : " + value);
            result = await db.execute("MATCH(person:Person) WHERE person." + key + " = {value} RETURN person", { value: value});
            if(_.isEmpty(result.records)) {
                console.log("result empty");
                response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
                return;
            }
        } else {
            console.log("User does not exists. Creating it...");
            if(isowner) {
                result = await createUser({data:data, status:'invited', isowner:true, ownerfid:''});
            }
            else {
                result = await createUser({data:data, status:'approved', isowner:false, ownerfid:data.fid});
            }
        }

        const record = result.records[0];
        const person = record._fields[0].properties;
        // Get the person fid whom we need to connect the relationship
        newfid = person.fid;
        console.log("Connecting person fid : " + newfid);

        console.log("MATCH(person:Person) WHERE person." + sourceAttr + " = " + sourceValue +" RETURN person");
        var temp = await db.execute("MATCH(person:Person) WHERE person." + sourceAttr + " = {value} RETURN person", { value: sourceValue });
        var requestingPerson = temp.records[0]._fields[0].properties;

        requestingPerson.displayname = Crypt.decryptField(requestingPerson.displayname);
        requestingPerson.gender = Crypt.decryptField(requestingPerson.gender);
        person.displayname = Crypt.decryptField(person.displayname);

        var message = "";
        var mode = "sms";
        var need2SendNotification = false;

//         if(isowner) {
//             console.log(isNewUser + ' and ' + person.status);
//             if(isNewUser  || person.status == 'invited') {
//             	console.log('oppositerelation 1 = ' + utils.getOppositeRelation(relationshiptype, requestingPerson.gender));
//                 message = "Your " + utils.getOppositeRelation(relationshiptype, requestingPerson.gender) + " " + requestingPerson.displayname + " wants to connect with you, please download FamilyG (http://bit.ly/2z9RSTU) to connect";
//                 //message = requestingPerson.displayname + " want to connect with you using FamilyG. Please download the app (http://bit.ly/2z9RSTU) to connect with your " + relationshiptype + ".";
// 				console.log('connect = '+countrycode + ' and ' + relmobilenumber);
//                 //MessageService.sendSMS(countrycode, relmobilenumber, message);
//                 message = requestingPerson.displayname + " is requesting to connect with you as " + relationshiptype;
//             }
//             else {
//                 mode = "pushservice"
//                 message = requestingPerson.displayname + " is requesting to connect with you as " + relationshiptype;
// 
//                 need2SendNotification = true;
//                 
//                 //MessageService.sendPushNotification("FamilyG", message, {reltype:relationshiptype, type:"connection", fid: "" + requestingPerson.fid }, person.devicetoken);
//             }
//         }

        if(data.type == "1" || data.type == "2") {
            newfid = data.ownerfid;
            data.fid = person.fid;
        }
        console.log("Creating relationship..." + data.fid + " and " + newfid + " and " + data.type);
        var ownerfid = data.fid;
        if(data.type == '3'){
        	var tempid = ownerfid;
        	ownerfid = newfid;
        	newfid = tempid;
        } else if(data.type == '1' || data.type == '2'){
			var ownerresult = await db.execute("MATCH(person:Person) WHERE person.fid = {value} RETURN person", { value: data.ownerfid});
            if(!_.isEmpty(ownerresult.records)) {
				requestingPerson.gender = Crypt.decryptField(ownerresult.records[0]._fields[0].properties.gender);
            }
        }

        var result = await createRelationship({mobilenumber : relmobilenumber, displayname: data.displayname, fid:data.fid, type:"3", ownerfid:ownerfid, newfid:newfid, gender:requestingPerson.gender}, relationshiptype, message, mode);
        if(_.isEmpty(result.records)) {
        	response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
		} else {
//        	if(need2SendNotification){ 
//             	chatManager.sendNotificationMessage(person, {
//                 	reltype:relationshiptype,
// 	                type:"1",
//     	            fid: "" + requestingPerson.fid,
//         	        senderid:requestingPerson.fid,
//             	    profileimage:Crypt.decryptField(requestingPerson.profileimage),
//                 	id:db.int(result.records[0]._fields[0].identity).toString(),
// 	                mode:"pushservice",
//     	            message:message,
//         	        datetime:new Date().getTime(),
//             	    relationpath:""
// 	            }, message);
//     	    }

    	    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true, fid:person.fid} });
        }
    }
    catch (err) {
        console.log("Failed in fgdataconnectfamily : " + err);
        response.json(new ERROR(ErrorCode.FAILED_TO_CONNECT_PROFILE));
    }
}

relationship.disconnect = async function (request, response) {
    try {
        var temp = await db.execute("MATCH(a:Person{fid:{srcfid}})-[r:IS_RELATED_TO]-(b:Person{fid:{desfid}}) DETACH DELETE r RETURN a, b", request.body);
        if(!_.isEmpty(temp.records)) {
            
            var message = Crypt.decryptField(temp.records[0]._fields[0].properties.displayname) + " has removed your relationship.";

            var result = await db.execute("MATCH (a:Person{fid:{srcfid}}),(b:Person{fid:{desfid}}) where b.familynotification='true' CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (a)-[:SENT]->(notification) CREATE (notification)-[:TO]->(b) return notification", {srcfid:request.body.srcfid, desfid: request.body.desfid, desc:"confirmation", ts:new Date().getTime(), msg:message, mode : "pushservice"  });
        if(!_.isEmpty(result.records)) {

            chatManager.sendNotificationMessage(temp.records[0]._fields[1].properties,  {
                type:"2", 
                senderid: temp.records[0]._fields[0].properties.fid, 
                message:message, 
                id:db.int(result.records[0]._fields[0].identity).toString(),
                profileimage:Crypt.decryptField(temp.records[0]._fields[0].properties.profileimage), 
                mode:"pushservice" ,
				datetime:new Date().getTime(), 
            }, message);
		}            
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });

        }
        else
            response.json(new ERROR(ErrorCode.FAILED_TO_DISCONNECT_PROFILE));
    }
    catch (err) {
        console.log("Failed in disconnecting profile : " + err);
        response.json(new ERROR(ErrorCode.FAILED_TO_DISCONNECT_PROFILE));
    }
}

relationship.fgdatadisconnect = async function (request, response) {
    try {
        var temp = await db.execute("MATCH(a:Person{fid:{srcfid}})-[r:IS_RELATED_TO]-(b:Person{fid:{desfid}}) DETACH DELETE r RETURN a, b", request.body);
        if(!_.isEmpty(temp.records)) {
        	response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
        }
        else
            response.json(new ERROR(ErrorCode.FAILED_TO_DISCONNECT_PROFILE));
    }
    catch (err) {
        console.log("Failed in disconnecting profile : " + err);
        response.json(new ERROR(ErrorCode.FAILED_TO_DISCONNECT_PROFILE));
    }
}

/*
relationship.connectWithFID = function (request, response) {
    var registrationToken = "dCiGeBZ9H74:APA91bEwEeiYXMxEM_DDc3o4NjYnYt9brkp6lttYADHk4CzxddDA6lGpaBbyu_Zj7LJp8FGoXODcWoyjfhxqGSirNCLQXBq53epE7yFWvjjfZkXbX8w2ut1uxeOrJljLN9WKEILpC0sX";

    // See the "Defining the message payload" section below for details
    // on how to define a message payload.
    var payload = {
        notification: {
            title: "FamilyG Test",
            body: "You have new connection request."
        },
        data: {
            score: "850",
            time: "2:45"
        }
    };

    admin.messaging().sendToDevice(registrationToken, payload)
  .then(function (response) {
      // See the MessagingDevicesResponse reference documentation for
      // the contents of response.
      console.log("Successfully sent message:", response);
  })
  .catch(function (error) {
      console.log("Error sending message:", error);
  });
    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: true });
}

relationship.createWithoutPhone = function (request, response) {

}*/

relationship.getfamiles = async function (request, response) {
    try {
        var key=request.query.secret_key;
        var fid=request.query.fid;  
        var myrelations=[];
        if(fid == null || fid == undefined || fid == ""){
            var result=await db.execute("MATCH (a:Rep {key:{key}})-[rs:IS_CREATED]->(b) return a,rs,b", {key:key});
            if (!_.isEmpty(result.records)){
                try {
                    result.records.forEach(async function (record) {
                        try {
                            var srcPerson = Crypt.decryptPerson(record.get(0).properties);
                            var relationship = record.get(1).properties;
                            var destPerson = Crypt.decryptPerson(record.get(2).properties);

                            myrelations.push({fid:destPerson.fid,firstname:destPerson.firstname,lastname:destPerson.lastname,displayname:Crypt.decryptField(relationship.displayname),profileimage:destPerson.profileimage,status:relationship.status, gender:destPerson.gender, isfav:true, uid:relationship.uid, isfirstdegree:true, isowner:destPerson.isowner, ownerfid:destPerson.ownerfid, relationtype:relationship.reltype, mobilenumber:destPerson.mobilenumber, dob:destPerson.dob, deceaseddate:destPerson.deceaseddate, isalive:destPerson.isalive, familyname:relationship.familyname});

                        }
                        catch(e) {
                            console.log("Error in getting relation for key : " + e);
                        }
                    });
                }
                catch (E) { }
            }
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: myrelations }); 
        }
//        else if(fid != null){ 
//             var result=await db.execute("MATCH (a:Person {fid:{fid}})-[rs:IS_RELATED_TO]->(b) return a,rs,b",{fid:fid});
//             if (!_.isEmpty(result.records)){
//                 try {
// 
//                     for(var cindex = 0; cindex < result.records.length; cindex++) {
//                         //result.records.forEach(async function (record) {
//                         var record = result.records[cindex];
//                         var srcPerson = Crypt.decryptPerson(record._fields[0].properties);
//                         var relationship=record._fields[1].properties;
//                         var destPerson = Crypt.decryptPerson(record._fields[2].properties);
//                         if(relationship.status != "rejected") {
//                             var favresult=await db.execute("MATCH (a:Person {key:{key}})-[rs:IS_FAVOURITE_TO|IS_RELATED_TO]->(b:Person{fid:{fid}}) WHERE rs.status= 'approved' return a,rs,b", {key:key, fid: destPerson.fid});
//                             // Take the person displayname not personal relationship name when user traverse through tree
//                             var displayname = destPerson.displayname;
//                             if(srcPerson.key == key) // If the user is logged in user then provide person relationship name
//                                 displayname = Crypt.decryptField(relationship.displayname);
//                             myrelations.push({fid:destPerson.fid, firstname:destPerson.firstname, lastname:destPerson.lastname, displayname:displayname, profileimage:destPerson.profileimage,status:relationship.status, gender:destPerson.gender, isfav:!_.isEmpty(favresult.records), uid:"", isfirstdegree:false , isowner:destPerson.isowner, ownerfid:destPerson.ownerfid, relationtype:relationship.reltype, mobilenumber:destPerson.mobilenumber, dob:destPerson.dob, deceaseddate:destPerson.deceaseddate, isalive:destPerson.isalive});
//                         }
//                     }
//                 }
//                 catch (E) { console.log("Error in getting relation : " + E);}
//             }
//             response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: myrelations }); 
//         }
    }
    catch(e) {
        console.log("Failed to retrieve the relation : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_RELATION));
    }
   
}

relationship.getfirstdegreerelations = async function (request, response) {
    try {
        var key=request.query.secret_key;
        var fid=request.query.fid;  
        var myrelations=[];
            console.log('getfirstdegreerelations request = ' + JSON.stringify(request.query));
            var result=await db.execute("MATCH (a:Person {fid:{fid}})-[rs:IS_RELATED_TO]->(b) return a,rs,b", {fid:fid});
            if (!_.isEmpty(result.records)){
                try {
                    console.log('getfirstdegreerelations = result ' + JSON.stringify(result));
                    result.records.forEach(async function (record) {
                        try {
                            var srcPerson = Crypt.decryptPerson(record.get(0).properties);
                            var relationship = record.get(1).properties;
                            var destPerson = Crypt.decryptPerson(record.get(2).properties);
                            console.log('getfirstdegreerelations destPerson =  ' + JSON.stringify(destPerson));

                            myrelations.push({fid:destPerson.fid,firstname:destPerson.firstname,lastname:destPerson.lastname,displayname:destPerson.displayname,profileimage:destPerson.profileimage,status:relationship.status, gender:destPerson.gender, isfav:true, uid:relationship.uid, isfirstdegree:true, isowner:destPerson.isowner, ownerfid:destPerson.ownerfid, relationtype:relationship.reltype, mobilenumber:destPerson.mobilenumber, dob:destPerson.dob, deceaseddate:destPerson.deceaseddate, isalive:destPerson.isalive});

                        }
                        catch(e) {
                            console.log("Error in getting relation for key : " + e);
                        }
                    });
                }
                catch (E) { }
            }
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: myrelations }); 
        
    }
    catch(e) {
        console.log("Failed to retrieve the relation : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_RELATION));
    }
   
}

relationship.sendNotificationToFavourites = async function(key, value, message, skipkey, skipvalue) {
    var result = await db.execute("MATCH(d:Person{key:{skipvalue}}) MATCH (a:Person)-[r]->(b:Person) where (type(r) = 'IS_RELATED_TO' OR type(r) = 'IS_FAVOURITE_TO') AND r.status = 'approved' AND (a." + key + "= {value} OR a."+ skipkey + "= {skipvalue}) AND (b.familynotification='true' OR b." + key + "= {value})  WITH distinct b, d  CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE UNIQUE (d)-[:SENT]->(notification) CREATE UNIQUE (notification)-[:TO]->(b) RETURN distinct b, notification,d",{value:value, skipvalue:skipvalue, desc:"information",ts:new Date().getTime(),  msg:message,  mode : "pushservice"});

	console.log('sendNotificationToFavourites = ' + JSON.stringify(result));

    result.records.forEach( async function (record) {
        var destinPerson = record.get(0).properties;
        var sourcePerson = record.get(2).properties;
        chatManager.sendNotificationMessage(destinPerson, {
            reltype:'', 
            type:"2", 
            fid: '',
            profileimage:Crypt.decryptField(sourcePerson.profileimage),
            id:db.int(record.get(0).identity).toString(),
            mode:'pushservice',
            message:message,
            datetime:new Date().getTime(),
            senderid:sourcePerson.fid
        }, message);

        //db.execute("MATCH (a:Person{fid:{fid}}),(b:Person{key:{key}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a)", {key:sourcePerson.key, fid: destinPerson.fid, desc:"information", ts:new Date().getTime(), msg:message, mode : "pushservice"  });

    });
    
}

relationship.sendTestMessage = async function(request, response) {
    var result = await db.execute("MATCH (a:Person{mobilenumber:{value}}) RETURN a",{value:request.query.mobilenumber});

    result.records.forEach( async function (record) {
        var destinPerson = record.get(0).properties;
        
        chatManager.sendNotificationMessage(destinPerson, {reltype:'', type:"2", fid: '' }, "Test message");

        //db.execute("MATCH (a:Person{fid:{fid}}),(b:Person{key:{key}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a)", {key:sourcePerson.key, fid: destinPerson.fid, desc:"information", ts:new Date().getTime(), msg:message, mode : "pushservice"  });

    });
    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:true });
}

relationship.sendRecommendedConnections = async function(srcfid, desfid, relationType) {
    try {
        console.log("Sending recommended relations for srcfid " + srcfid + ", desfid : " + desfid + " rel " + relationType);
        var result = await db.execute("MATCH(a:Person{fid:{desfid}})-[r:IS_RELATED_TO]->(b:Person), (c:Person{fid:{srcfid}}) WHERE NOT (c)-[:IS_RELATED_TO]->(b) and r.status='approved' RETURN c, b, r, a", {srcfid:srcfid, desfid:desfid});
        console.log('sendRecommendedConnections result : ' + JSON.stringify(result));
        
        result.records.forEach( async function (record) {
            try {
                var relation = record.get(2).properties;
                var person = record.get(1).properties;
                var destin = record.get(3).properties;
                var source = record.get(0).properties;
                if(person.fid == source.fid)
                    return;

                var rel = null;
                switch(relationType) {
                    case "FATHER":
                    case "MOTHER":
                        if(relation.reltype == "SON")
                            rel = "BROTHER";
                        else if(relation.reltype == "DAUGHTER")
                            rel = "SISTER";
                        else if(relation.reltype == "HUSBAND")
                            rel = "FATHER";
                        else if(relation.reltype == "WIFE")
                            rel = "MOTHER";
                        break;
                    case "SON":
                    case "DAUGHTER":
                        if(relation.reltype == "FATHER")
                            rel = "HUSBAND";
                        else if(relation.reltype == "MOTHER")
                            rel = "WIFE";
                        else if(relation.reltype == "BROTHER")
                            rel = "SON";
                        else if(relation.reltype == "SISTER")
                            rel = "DAUGHTER";
                        break;
                    case "BROTHER":
                    case "SISTER":
                        if(relation.reltype == "FATHER")
                            rel = "FATHER";
                        else if(relation.reltype == "MOTHER")
                            rel = "MOTHER";
                        else if(relation.reltype == "BROTHER")
                            rel = "BROTHER";
                        else if(relation.reltype == "SISTER")
                            rel = "SISTER";
                        break;
                    case "WIFE":
                    case "HUSBAND":
                        if(relation.reltype == "SON")
                            rel = "SON";
                        else if(relation.reltype == "DAUGHTER")
                            rel = "DAUGHTER";
                        break;
                    default:
                        rel = null;
                        break;
                }
                if(rel != null) {
                    
                    var approvingPersonFid = source.fid;
                    var approvingPersonDeviceToken = source;
                    var message =  Crypt.decryptField(person.displayname) + " is " + relation.reltype + " to " + Crypt.decryptField(destin.displayname) + ". Do you want to connect as " + rel + "?";

                    if(!source.isowner) {
                        console.log("Approving person fid : " + source.ownerfid);
                        var approvingPerson = await db.execute("MATCH(person:Person) WHERE person.fid={value} RETURN person", { value: source.ownerfid});
                        var aperson = approvingPerson.records[0]._fields[0].properties;
                        approvingPersonFid = aperson.fid;
                        approvingPersonDeviceToken = aperson;

                        message =  Crypt.decryptField(person.displayname) + " is " + relation.reltype + " to " + Crypt.decryptField(destin.displayname) + ". May be " + Crypt.decryptField(person.displayname) + " is " + rel + " for " + Crypt.decryptField(source.displayname) + ".";
                    }

                    
                    console.log("Suggesting : " + message);
                    
                    var noti_exist = await db.execute("MATCH (notification:Notification{type:5,srcfid:{srcfid}, desfid:{desfid}, relation:{rel}}) RETURN notification;", {
                        srcfid:source.fid, 
                        desfid:person.fid, 
                        aperfid:approvingPersonFid,
                        msg:message,
                        ts:new Date().getTime(), 
                        desc:'suggestion', 
                        rel:rel});

                if(_.isEmpty(noti_exist.records)) {
                    console.log("Suggesting : notification not exist");

                    var noti_result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}), (approver:Person{fid:{aperfid}}) WITH source, destination, approver CREATE(notification:Notification {type:5, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:'pushservice', srcfid:{srcfid}, desfid:{desfid}, relation:{rel}})  CREATE (destination)-[:SENT]->(notification) CREATE (notification)-[:TO]->(approver) RETURN notification;", {
                        srcfid:source.fid, 
                        desfid:person.fid, 
                        aperfid:approvingPersonFid,
                        msg:message, 
                        ts:new Date().getTime(), 
                        desc:'suggestion', 
                        rel:rel});

                    
                    chatManager.sendNotificationMessage(approvingPersonDeviceToken, {
                        reltype:rel, 
                        type:"5", 
                        fid: person.fid ,
                        senderid:person.fid,
                        profileimage:Crypt.decryptField(person.profileimage),
                        id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                        mode:'pushservice',
                        message:message,
                        datetime:new Date().getTime(),
                    }, message);
                }
                }
                
            }
            catch(e) {
                console.log("Error " + e);
            }
        });
    }
    catch(e) {
        console.log("Error : " + e);
    }
}

relationship.deleteNotification = async function(request, response) {
    try {
        var input = request.query;
        if(input.type != undefined) {
    		console.log('deleteNotification = ' + input.type);
    		if(input.type == "single") {
	        	var result = await db.execute("match(b) where id(b) = " + input.notification_id + " SET b.active = 0 return b");
	        }else{
    	    	var result = await db.execute("MATCH (a)-[rs:SENT]-(b)-[rt:TO]-(c:Person {key: {key}}) WHERE b.type = 2 SET b.active = 0  RETURN b",{key:input.secret_key});
        	}
        }else{
        	    var result = await db.execute("match(b) where id(b) = " + input.notification_id + " SET b.active = 0 return b");
        }
    }
    catch(e) {
        console.log("Error in deleting notification : " + e);
    }

    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{staus:true} });
}

relationship.connectWithRecommendation = async function(data) { 
    try {
        console.log("Connecting with recommendations... for id " + data.notification_id);

        var result = await db.execute("match(b) where id(b) = " + data.notification_id + " SET b.active = 0 return b");
        var notification = result.records[0]._fields[0].properties; // get the notification node where srcfid and desfid is stored.

        result = await db.execute("MATCH (source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) RETURN source, destination",{srcfid:notification.srcfid, desfid:notification.desfid});

        var srcPerson = result.records[0]._fields[0].properties;
        var desPerson = result.records[0]._fields[1].properties;
        var oppositeRelationType = utils.getOppositeRelation(notification.relation, Crypt.decryptField(srcPerson.gender));


        var message = Crypt.decryptField(srcPerson.displayname) + "  is requesting to connect with " + ((desPerson.isowner) ? "you" : Crypt.decryptField(desPerson.displayname) ) + " as " + oppositeRelationType;
			
        result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}), (approver:Person{fid:{afid}}) WHERE NOT (source)-[:IS_RELATED_TO]-(destination) WITH source, destination, approver CREATE(notification:Notification {type:1, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}, srcfid:source.fid, desfid:destination.fid}) CREATE (source)-[:IS_RELATED_TO{reltype:{srcreltype}, status:{status}, created_date:{ts}, uid:{uid}, displayname:{dn}}]->(destination) CREATE (destination)-[:IS_RELATED_TO{reltype:{desreltype},status:{status}, created_date:{ts}, uid:{uid}, displayname:source.displayname}]->(source) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(approver) RETURN notification,approver;"
        , {
            srcfid: notification.srcfid,
            desfid: notification.desfid,
            afid:(desPerson.isowner ? notification.desfid : desPerson.ownerfid),
            ts:new Date().getTime(),
            srcreltype:notification.relation,
            desreltype:oppositeRelationType,
            dn:desPerson.displayname,
            desc:"Connection Request",
            status: "pending",
            msg:message,
            mode : "pushservice",
            uid: uuid.v4()
        });
        console.log("Sending notification to " + desPerson.devicetoken);
        chatManager.sendNotificationMessage(result.records[0]._fields[1].properties, {
            reltype:notification.relation, 
            type:"1", 
            fid: desPerson.fid ,
            senderid:srcPerson.fid,
            profileimage:Crypt.decryptField(srcPerson.profileimage),
            id:db.int(result.records[0]._fields[0].identity).toString(),
            mode:'pushservice',
            message:message,
            datetime:new Date().getTime(),
        }, message);

    }
    catch(e) {
        console.log("Error " + e);
    }
}

relationship.approve = async function (request, response) {
    try {
        var fid=request.query.fid;
        var key=request.query.secret_key;
        var type = request.query.type;
        var notification_id = parseInt(request.query.notification_id);

        var result = await db.execute("match(b) where id(b) = " + notification_id + " return b");
        var notification = result.records[0]._fields[0].properties; // get the notification node where srcfid and desfid is stored.
        
        if(type == "5") {
            relationship.connectWithRecommendation(request.query);
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{staus:true} }); 
            return ;
        }

        var relationtype = (type == "3") ? "IS_FAVOURITE_TO" : "IS_RELATED_TO";
        console.log("Request for approval with type " + relationtype + " " + type + " key : " + key + " fid : "+ fid);
        var result=await db.execute("MATCH (a)-[rs:"+ relationtype + "]->(b) where a.fid={srcfid} and b.fid={desfid} RETURN a,rs,b",{srcfid:notification.srcfid,desfid:notification.desfid});
        result.records.forEach( async function (record) {
            try {
                var sourcePerson = record.get(0).properties;
                var relatedTo = record.get(1).properties;
                var destinPerson = record.get(2).properties;
                console.log("approve = " + relatedTo.status.toLowerCase());

                if(relatedTo.status.toLowerCase() == "approved"){
                    response.json(new ERROR(ErrorCode.REQUEST_ALREADY_APPROVED));
                }
                else if(relatedTo.status.toLowerCase() == "pending"){
					var res, res1, relationshipType = request.query.relationship;
					console.log("approve = " + relationshipType);

                    if(relationshipType != undefined && relationshipType != '' && relationshipType != null){

                    // Source to destination
    	                res=await db.execute("MERGE(a:Person{fid:{srcfid}})-[rs:" + relationtype + "]->(b:Person{fid:{desfid}}) ON MATCH SET rs.status = 'approved', rs.reltype = '" + relationshipType + "' return a, rs, b",{srcfid:notification.srcfid, desfid:notification.desfid});
                    // Destination to source
            	        res1=await db.execute("MERGE(a:Person{fid:{srcfid}})<-[rs:" + relationtype + "]-(b:Person{fid:{desfid}}) ON MATCH SET rs.status = 'approved', rs.reltype = '" + utils.getOppositeRelation(relationshipType, Crypt.decryptField(sourcePerson.gender)) + "' return a, rs, b",{srcfid:notification.srcfid, desfid:notification.desfid});
                	    //console.log("Sending PN to : " + destinPerson.devicetoken);
					} else{

                    	// Source to destination
	                    res=await db.execute("MERGE(a:Person{fid:{srcfid}})-[rs:" + relationtype + "]->(b:Person{fid:{desfid}}) ON MATCH SET rs.status = 'approved' return a, rs, b",{srcfid:notification.srcfid,desfid:notification.desfid});
    	                // Destination to source
        	            res1=await db.execute("MERGE(a:Person{fid:{srcfid}})<-[rs:" + relationtype + "]-(b:Person{fid:{desfid}}) ON MATCH SET rs.status = 'approved' return a, rs, b",{srcfid:notification.srcfid,desfid:notification.desfid});
            	        //console.log("Sending PN to : " + destinPerson.devicetoken);

					}
                    var message = Crypt.decryptField(res1.records[0]._fields[2].properties.displayname) + " and " + Crypt.decryptField(res1.records[0]._fields[0].properties.displayname) + " are now connected as " + ((type== "3") ? "favourites" : res1.records[0]._fields[1].properties.reltype);
					console.log("approve = " + message);

                    if(type == "3") {
                        var noti_result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) WITH source, destination CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}, srcfid:source.fid, desfid:destination.fid})  CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) CREATE (notification)-[:TO]->(source) RETURN notification;"
                            , {
                                srcfid: sourcePerson.fid,
                                desfid: destinPerson.fid,
                               
                                ts:new Date().getTime(),
                               
                                desc:"Connection Request",
                                status: "pending",
                                msg:message,
                                mode : "pushservice",
                                uid: uuid.v4()
                            });
                        chatManager.sendNotificationMessage(destinPerson, {
                            reltype:res1.records[0]._fields[1].properties.reltype, 
                            type:"2", 
                            fid: sourcePerson.fid ,
                            senderid:sourcePerson.fid,
                            profileimage:Crypt.decryptField(sourcePerson.profileimage),
                            id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                            mode:'pushservice',
                            message:message,
                            datetime:new Date().getTime(),
                        }, message);
                        chatManager.sendNotificationMessage(sourcePerson, {
                            reltype:res1.records[0]._fields[1].properties.reltype, 
                            type:"2", 
                            fid: sourcePerson.fid,
                            senderid:destinPerson.fid,
                            profileimage:Crypt.decryptField(destinPerson.profileimage),
                            id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                            mode:'pushservice',
                            message:message,
                            datetime:new Date().getTime(),
                        }, message);
                    }

                    db.execute("MATCH (a:Person{fid:{fid}})-[s:SENT]->(n:Notification)-[t:TO]->(b:Person{key:{key}}) WHERE n.type = 1 or n.type = 3 SET n.active = 0", {key:key, fid: fid});
                    //db.execute("MATCH (a:Person{fid:{fid}}),(b:Person{key:{key}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a)", {key:key, fid: fid, desc:"confirmation", ts:new Date().getTime(), msg:message, mode : "pushservice"  });
                    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{staus:true} }); 

                    message = Crypt.decryptField(res1.records[0]._fields[1].properties.displayname) + " connected with " + Crypt.decryptField(res1.records[0]._fields[2].properties.displayname) + " as " + res1.records[0]._fields[1].properties.reltype;

                    if(type == "1") {
					    console.log("approve = " + fid + " and " + message + " and " + key);
                        relationship.sendNotificationToFavourites("fid", fid, message, "key", key);
                        //relationship.sendNotificationToFavourites("key", key, message, "fid", fid);
                    }
                    console.log("Is owner for des : " + destinPerson.isowner + " and for src : " + sourcePerson.isowner );
                    if((destinPerson.isowner == "true" || destinPerson.isowner ) && (sourcePerson.isowner == "true" || sourcePerson.isowner) && relationtype == "IS_RELATED_TO" ) {
                        relationship.sendRecommendedConnections(sourcePerson.fid, destinPerson.fid,  res.records[0]._fields[1].properties.reltype);
                        relationship.sendRecommendedConnections(destinPerson.fid, sourcePerson.fid,  res1.records[0]._fields[1].properties.reltype);
                    }
                }
            }
            catch(e){
                 console.log("Failed to approve the relation : " + e);
                response.json(new ERROR(ErrorCode.FAILED_TO_APPROVE));
            }
        });
    }
    catch(e) {
        console.log("Failed to approve the relation : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_APPROVE));
    }
}

relationship.reject = async function (request, response) {
    try {
        var fid=request.query.fid;
        var key=request.query.secret_key;
        var type = request.query.type;
        var notification_id = parseInt(request.query.notification_id);

        var result = await db.execute("match(b) where id(b) = " + notification_id + " return b");
        var notification = result.records[0]._fields[0].properties; // get the notification node where srcfid and desfid is stored.

        var relationtype = (type == "3") ? "IS_FAVOURITE_TO" : "IS_RELATED_TO";
        console.log("Request for reject with type " + relationtype + " " + type + " key : " + key + " fid : "+ fid);

        var result=await db.execute("MATCH (a:Person {fid: {srcfid}})-[rs:" + relationtype + "]->(b:Person {fid: {desfid}}) RETURN a,rs,b",{srcfid:notification.srcfid,desfid:notification.desfid});
        result.records.forEach( async function (record) {
            var  sourcePerson = record.get(0).properties;
            var relatedTo = record.get(1).properties;
            var destinPerson = record.get(2).properties;
            if(relatedTo.status == "approved"){
                response.json(new ERROR(ErrorCode.REQUEST_ALREADY_APPROVED));
            }
            else if(relatedTo.status.toLowerCase() == "pending"){
                // Source to destination
                var res=await db.execute("MATCH(a:Person{fid:{srcfid}})-[rs:" + relationtype + "]->(b:Person{fid:{desfid}}) DELETE rs",{srcfid:notification.srcfid,desfid:notification.desfid});
                // Destination to source
                var res1=await db.execute("MATCH (a:Person{fid:{srcfid}})<-[rs:" + relationtype + "]-(b:Person{fid:{desfid}})  DELETE rs return a, rs, b",{srcfid:notification.srcfid,desfid:notification.desfid});
                var message = "Your request to connect with " + Crypt.decryptField(destinPerson.displayname) + " as " +  ((type== "3") ? "favourite" : relatedTo.reltype) + " is rejected.";
               
                db.execute("MATCH (a:Person{fid:{fid}})-[s:SENT]->(n:Notification)-[t:TO]->(b:Person{key:{key}}) WHERE n.type = 1 or n.type = 3 SET n.active = 0", {key:key, fid: fid});
                var noti_result = await db.execute("MATCH (a:Person{fid:{srcfid}}),(b:Person{fid:{desfid}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a) RETURN notification", {srcfid:notification.srcfid,desfid:notification.desfid, desc:"confirmation", ts:new Date().getTime(), msg:message, mode : "pushservice"  });

                chatManager.sendNotificationMessage(sourcePerson, {
                    reltype:relatedTo.reltype, 
                    type:"2", 
                    fid: sourcePerson.fid ,
                    senderid:destinPerson.fid,
                    profileimage:Crypt.decryptField(destinPerson.profileimage),
                    id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                    mode:'pushservice',
                    message:message,
                    datetime:new Date().getTime(),
                }, message);

                response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{status:true} });
            }
        });       
    }
    catch(e) {
        console.log("Failed to reject the relation : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_REJECT));
    }
}

relationship.getnotifications = async function (request, response) {
    try {
        var key = request.query.secret_key;
        var type = request.query.type == "1" ? " AND b.type = 1" :  "";
        var result = await db.execute("MATCH (a)-[rs:SENT]-(b)-[rt:TO]-(c:Person {key: {key}}) WHERE b.active = 1 " + type + " RETURN a, b,c", {key:key});
        var notifications = [];
        console.log("Received notifications of " + result.records.length + "for key : " +key);
        for(var index=0; index< result.records.length; index++) {
            var record = result.records[index];
            //result.records.forEach(function (record) {
            var sourcePerson = record._fields[0].properties;
            var notification = record._fields[1].properties;
            var destinPerson = record._fields[2].properties;

            notifications.push({type: notification.type.low, message:notification.message, senderid:sourcePerson.fid, datetime:notification.created_date, mode: notification.mode, profileimage:Crypt.decryptField(sourcePerson.profileimage), id:db.int(record._fields[1].identity).toString(), relationpath:notification.relationpath, mergefid:notification.mergefid , anotherfid:notification.anotherfid, relation:notification.relation });
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: notifications }); 
    }
    catch(e) {
        console.log("Failed to retrieve notifications : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_NOTIFICATIONS));
    }
}



relationship.viewProfile = async function (request, response) {
    try {
        var value=request.query.secret_key;
        var fid=request.query.fid;
        var prop="key";
        var personprofile={};
        if(fid != null){
            prop="fid";
            value=request.query.fid;
        }
        var result = await db.execute("MATCH (a:Person{" + prop + ":{value}}) RETURN a", {value:value});
        if(!_.isEmpty(result.records)) {
            result.records.forEach(function (record) {
                var person = Crypt.decryptPerson(record.get(0).properties);
                personprofile={fid:person.fid, firstname:person.firstname, lastname:person.lastname,profileimage:person.profileimage,mobilenumber:person.mobilenumber, gender:person.gender,countrycode:person.countrycode,dob:person.dob,displayname:person.displayname, doi : person.doi, deceaseddate:person.deceaseddate, ownerfid:person.ownerfid, isalive:person.isalive, isowner:person.isowner};
                response.json({iserror: false, errorcode: ErrorCode.SUCCESS, result:personprofile});
            });
        }
        else
            response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_PROFILE));
    }
    catch(e) {
        console.log("Failed to retrieve profile info : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_PROFILE));
    }
}

relationship.addFavourite = async function (request, response) {
    try {
        var key=request.query.secret_key;
        var fid=request.query.fid;
        var result = await db.execute("MATCH (a:Person{key:{key}}),(b:Person{fid:{fid}}) CREATE UNIQUE (a)-[rs:IS_FAVOURITE_TO]->(b) SET rs.status= {status}, rs.uid=a.fid + '-'+b.fid RETURN a,b,rs", {key:key,fid:fid,status:'pending'});

        if(result.records[0]._fields[2].properties.requestedTime != undefined ) {
            var requestedTime = db.int(result.records[0]._fields[2].properties.requestedTime);
            if((new Date().getTime() - requestedTime) < (1000 * 60 * 5)) {
                response.json({ iserror: true, errorcode: ErrorCode.SUCCESS, result:null, message:"A request has been already sent for approval." });
                return;
            }
        }
        
        await db.execute("MATCH (a:Person{key:{key}}),(b:Person{fid:{fid}}) CREATE UNIQUE (a)-[rs:IS_FAVOURITE_TO]->(b) SET rs.status= {status}, rs.uid=a.fid + '-'+b.fid, rs.requestedTime={time} RETURN a,b,rs", {key:key,fid:fid,status:'pending', time:new Date().getTime()});

        await db.execute("MATCH (a:Person{key:{key}}),(b:Person{fid:{fid}}) CREATE UNIQUE (a)<-[rs:IS_FAVOURITE_TO]-(b) SET rs.status= {status}, rs.uid=a.fid + '-'+b.fid RETURN a,b,rs", {key:key,fid:fid,status:'pending'});
    
        var message = Crypt.decryptField(result.records[0]._fields[0].properties.displayname) + " is requesting to add you as favourite.";

        var noti_result = await db.execute("MATCH (a:Person{fid:{fid}}),(b:Person{key:{key}}) CREATE(notification:Notification {type:3, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}, srcfid:{srcfid}, desfid:{desfid}, relationpath:{relationpath}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a) return notification", {key:key, fid: fid, desc:"confirmation", ts:new Date().getTime(), msg:message, mode : "pushservice", srcfid:result.records[0]._fields[0].properties.fid, desfid:result.records[0]._fields[1].properties.fid, relationpath:request.query.relationpath  });

        chatManager.sendNotificationMessage(result.records[0]._fields[1].properties,  {
            type:"3", 
            fid: result.records[0]._fields[0].properties.fid ,
            senderid:result.records[0]._fields[0].properties.fid,
			profileimage:Crypt.decryptField(result.records[0]._fields[0].properties.profileimage),
            id:db.int(noti_result.records[0]._fields[0].identity).toString(),
            mode:'pushservice',
            message:message,
            datetime:new Date().getTime(),
        }, message);
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{status:true} });
    }
    catch(error) {
        console.log("Error in add to fav : " + error);
        response.json(new ERROR(ErrorCode.FAILED_TO_CONNECT_PROFILE));
    }
}

relationship.removeFavourite = async function (request, response) {
    try {
        var key=request.query.secret_key;
        var fid=request.query.fid;
        var result = await db.execute("MATCH (a:Person{key:{key}})-[rs:IS_FAVOURITE_TO]-(b:Person{fid:{fid}}) delete rs return a,b", {key:key,fid:fid});

        var message = Crypt.decryptField(result.records[0]._fields[0].properties.displayname) + " has removed you from favourite list.";

        var noti_result = await db.execute("MATCH (a:Person{fid:{fid}}),(b:Person{key:{key}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:{mode}})  CREATE (b)-[:SENT]->(notification) CREATE (notification)-[:TO]->(a) return notification", {key:key, fid: fid, desc:"confirmation", ts:new Date().getTime(), msg:message, mode : "pushservice"  });

        chatManager.sendNotificationMessage(result.records[0]._fields[1].properties,  {
            type:"2", 
            fid: result.records[0]._fields[0].properties.fid ,
            senderid:result.records[0]._fields[0].properties.fid,
            profileimage:Crypt.decryptField(result.records[0]._fields[0].properties.profileimage),
            id:db.int(noti_result.records[0]._fields[0].identity).toString(),
            mode:'pushservice',
            message:message,
            datetime:new Date().getTime(),
        }, message);

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{status:true} });
    }
    catch(e) {
        console.log("Error in remove from fav : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_DISCONNECT_PROFILE));
    }

}

relationship.getFavouriteList = async function (request, response) {
    try {
        var key=request.query.secret_key;
        var favouriteList=[];
        var directRelationResult = await db.execute("MATCH (a:Person{key: {key}})-[rs:IS_RELATED_TO]->(b) WHERE rs.status = 'approved' AND NOT(a)-[:BLOCKED]-(b) RETURN b, rs", {key:key});

        if (!_.isEmpty(directRelationResult.records)){
            try {
            	console.log('directRelationResult = ' + JSON.stringify(directRelationResult));
                directRelationResult.records.forEach(function (record) {
                    var favourite=Crypt.decryptPerson(record.get(0).properties);
                    var relationship = record.get(1).properties;
                    favouriteList.push({fid:favourite.fid, firstname:favourite.firstname, lastname:favourite.lastname, displayname:Crypt.decryptField(relationship.displayname), profileimage:favourite.profileimage, uid:relationship.uid, mobilenumber:favourite.mobilenumber});
                });
           
            }
            catch (E) { }
        }   

        var result = await db.execute("MATCH (a:Person{key: {key}})-[rs:IS_FAVOURITE_TO]->(b) WHERE rs.status = 'approved' AND NOT (a)-[:BLOCKED]-(b) RETURN b, rs", {key:key});
        if (!_.isEmpty(result.records)){
            try {
                result.records.forEach(function (record) {
                    var favourite=Crypt.decryptPerson(record.get(0).properties);
                    var relationship = record.get(1).properties;
                    favouriteList.push({fid:favourite.fid, firstname:favourite.firstname, lastname:favourite.lastname, displayname:favourite.displayname, profileimage:favourite.profileimage, uid:relationship.uid,mobilenumber:favourite.mobilenumber});
                });
            }
            catch (E) { }
        }   
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: favouriteList }); 
    }
    catch(e) {
        console.log("Failed to retrieve fav list " + e);
        response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_CONNECT_PROFILE, result: favouriteList }); 
    }
}

relationship.blockUser = async function(request, response) {
    try {
        await db.execute("MATCH (a:Person{key:{secret_key}}), (b:Person{fid:{fid}}) CREATE UNIQUE (a)-[rs:BLOCKED]->(b) RETURN rs", request.body);
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} }); 
    }
    catch(e){
        console.log("Failed to block the user " + e);
    }
}

relationship.unblockUser = async function(request, response) {
    try {
        await db.execute("MATCH(a:Person{key:{secret_key}})-[rs:BLOCKED]->(b:Person{fid:{fid}}) DELETE rs", request.body);
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} }); 
    }
    catch(e){
    }
}

relationship.getBlockedUsers = async function(request, response) {
    try {
        var result = await db.execute("MATCH(a:Person{key:{secret_key}})-[rs:BLOCKED]->(b:Person) return b ", request.body);
        var blockedUsers = [];
        if(!_.isEmpty(result.records)){
            for(var index = 0; index < result.records.length; index++ ) {
                var person = result.records[index]._fields[0].properties;
                blockedUsers.push({displayname:Crypt.decryptField(person.displayname), mobilenumber:person.mobilenumber, fid:person.fid, profileimage: Crypt.decryptField(person.profileimage)});
            }
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: blockedUsers }); 
    }
    catch(e){
    }
}

async function getUsers(fid, generations, level, maxlevel) {
    if(level > maxlevel) // Check for the current level greater than expected doi than stop processing
        return ;
    var user = {
        parents : [],
        children: [],
        sibling:[]
    };
    var result = await db.execute("MATCH(a:Person{fid:{fid}})-[r:IS_RELATED_TO]-(b) WHERE r.status = 'approved' RETURN a,r,b", {fid:fid});
    if(!_.isEmpty(result.records)){
        var sourcePerson = result.records[0]._fields[0].properties;

        for(var index = 0; index < result.records.length; index++ ) {
            var relationship = result.records[0]._fields[1].properties;
            var destPerson = result.records[0]._fields[2].properties;

            if(relation.reltype == "FATHER" || relation.reltype == "MOTHER") {
                user.parents
            }
            else if(relation.reltype == "SON" || relation.reltype == "DAUGHTER") {
                genIndex--;
            }
            else if(relation.reltype == "HUSBAND" || relation.reltype == "WIFE") {
                    
            }
            else if(relation.reltype == "BROTHER" || relation.reltype == "SISTER") {
                    
            }

        }
    }
}

relationship.merge = async function (request, response) {
    try {

            var mergefid=request.body.mergefid;
            var anotherfid=request.body.anotherfid;
            var secret_key=request.body.secret_key;
            var mode=request.body.mode;
            console.log('mergefid = ' + mergefid + 'anotherfid = ' + anotherfid + 'secret_key = ' + secret_key + 'mode = ' + mode);

            if(mode == 'request'){
                var duplicateCheck = await db.execute("MATCH (mergeOwner:Person{key:{secret_key}})-[r:SENT]->(noti:Notification{mergefid:{mergefid},anotherfid:{anotherfid}}) RETURN noti", { secret_key: secret_key, mergefid: mergefid, anotherfid: anotherfid });
                console.log(JSON.stringify(duplicateCheck));
                if(!_.isEmpty(duplicateCheck.records)){
                    response.json(new ERROR(ErrorCode.DUPLICATE_MERGE_REQUEST));
                }
                else{
                    var fourPersons = await db.execute("MATCH (mergeOwner:Person{key:{secret_key}}),(mergePerson:Person{ownerfid:mergeOwner.fid, fid:{mergefid}}),(anotherPerson:Person{fid:{anotherfid}}),(anotherOwner:Person{fid:anotherPerson.ownerfid}) RETURN mergeOwner, mergePerson, anotherPerson, anotherOwner", { secret_key: secret_key, mergefid: mergefid, anotherfid: anotherfid });
                        //console.log(JSON.stringify(ownerANDmergeperson));

                    var mergeowner = fourPersons.records[0]._fields[0].properties;
                    var mergeperson = fourPersons.records[0]._fields[1].properties;
                    var anotherperson = fourPersons.records[0]._fields[2].properties;
                    var anotherowner = fourPersons.records[0]._fields[3].properties;

                    var message =  Crypt.decryptField(mergeowner.displayname) + " is requesting to merge " +  Crypt.decryptField(mergeperson.displayname)  + "(fgid:" + mergeperson.fid + ") with " + Crypt.decryptField(anotherperson.displayname) + "(fgid:" + anotherperson.fid + "), Since both are same. Do you agree?";

                    console.log("Suggesting : " + message);
                    var noti_result = await db.execute("MATCH(source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) CREATE(notification:Notification {type:6, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:'pushservice', srcfid:{srcfid}, desfid:{desfid}, mergefid:{mergefid}, anotherfid:{anotherfid}}) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) RETURN notification;", {
                        srcfid:mergeowner.fid,
                        desfid:anotherowner.fid,
                        mergefid: mergefid,
                        anotherfid: anotherfid,
                        msg:message,
                        ts:new Date().getTime(),
                        desc:'suggestion'});

                    chatManager.sendNotificationMessage(anotherowner, {
                        type:"6",
                        mergefid: mergefid,
                        senderid:mergeowner.fid,
                        anotherfid: anotherfid,
                        profileimage:Crypt.decryptField(mergeowner.profileimage),
                        id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                        mode:'pushservice',
                        message:message,
                        datetime:new Date().getTime(),
                    }, message);
                }
            }else{

                var newfid = utils.createFid();
                var fourPersons = await db.execute("MATCH (anotherOwner:Person{key:{secret_key}}),(anotherPerson:Person{ownerfid:anotherOwner.fid, fid:{anotherfid}}),(mergePerson:Person{fid:{mergefid}}),(mergeOwner:Person{fid:mergePerson.ownerfid}) RETURN mergeOwner, mergePerson, anotherPerson, anotherOwner", { secret_key: secret_key, mergefid: mergefid, anotherfid: anotherfid });
                    console.log(JSON.stringify(fourPersons));

                var mergeowner = fourPersons.records[0]._fields[0].properties;
                var mergeperson = fourPersons.records[0]._fields[1].properties;
                var anotherperson = fourPersons.records[0]._fields[2].properties;
                var anotherowner = fourPersons.records[0]._fields[3].properties;

                console.log('mergefid = ' + mergefid + 'anotherfid = ' + anotherfid + 'newfid = ' + newfid);
                await db.execute("MATCH (a:Person{fid:{mergefid}}) create (b:Person) set b = a, b.fid = {newfid} RETURN b", {mergefid:mergefid, newfid:newfid});

                await db.execute("MATCH (p:Person)<-[r1:IS_RELATED_TO]-(q:Person{fid:{mergefid}}), (r:Person{fid:{newfid}}) merge (p)<-[r2:IS_RELATED_TO]-(r) set r2 = r1", {mergefid:mergefid, newfid:newfid});
                await db.execute("MATCH (p:Person)<-[r1:IS_RELATED_TO]-(q:Person{fid:{anotherfid}}), (r:Person{fid:{newfid}}) merge (p)<-[r2:IS_RELATED_TO]-(r) set r2 = r1", {anotherfid:anotherfid, newfid:newfid});

                await db.execute("MATCH (p:Person)-[r1:IS_RELATED_TO]->(q:Person{fid:{mergefid}}), (r:Person{fid:{newfid}}) merge (p)-[r2:IS_RELATED_TO]->(r) set r2 = r1", {mergefid:mergefid, newfid:newfid});
                await db.execute("MATCH (p:Person)-[r1:IS_RELATED_TO]->(q:Person{fid:{anotherfid}}), (r:Person{fid:{newfid}}) merge (p)-[r2:IS_RELATED_TO]->(r) set r2 = r1", {anotherfid:anotherfid, newfid:newfid});

                await db.execute("match (p:Notification)-[r1:TO]->(q:Person{fid:{mergefid}}), (r:Person{fid:{newfid}}) merge (p)-[r2:TO]->(r) set r2 = r1 detach delete q", {mergefid:mergefid, newfid:newfid});
                await db.execute("match (p:Notification)-[r1:TO]->(q:Person{fid:{anotherfid}}), (r:Person{fid:{newfid}}) merge (p)-[r2:TO]->(r) set r2 = r1 detach delete q", {anotherfid:anotherfid, newfid:newfid});

                var message =  "Merging " +  Crypt.decryptField(mergeperson.displayname)  + "(fgid:" + mergeperson.fid + ") with " + Crypt.decryptField(anotherperson.displayname) + "(fgid:" + anotherperson.fid + ") is successful and new fgid is " + newfid;

                console.log("Merge : " + message);

                var noti_result = await db.execute("MATCH (source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:'pushservice', desfid:{desfid}}) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) RETURN notification;", {
                    srcfid:mergeowner.fid,
                    desfid:anotherowner.fid,
                    msg:message,
                    ts:new Date().getTime(),
                    desc:'information'});

                chatManager.sendNotificationMessage(anotherowner, {
                    type:"2",
                    id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                    mode:'pushservice',
                    message:message,
                    datetime:new Date().getTime(),
                }, message);

                    noti_result = await db.execute("MATCH (source:Person{fid:{srcfid}}), (destination:Person{fid:{desfid}}) CREATE(notification:Notification {type:2, typedesc:{desc}, created_date:{ts}, message:{msg}, active:1, mode:'pushservice', desfid:{desfid}}) CREATE (source)-[:SENT]->(notification) CREATE (notification)-[:TO]->(destination) RETURN notification;", {
                    srcfid:anotherowner.fid,
                    desfid:mergeowner.fid,
                    msg:message,
                    ts:new Date().getTime(),
                    desc:'information'});

                chatManager.sendNotificationMessage(mergeowner, {
                    type:"2",
                    id:db.int(noti_result.records[0]._fields[0].identity).toString(),
                    mode:'pushservice',
                    message:message,
                    datetime:new Date().getTime(),
                }, message);

            }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{status:true} });
    }
    catch(e) {
        console.log("Error to merge : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_MERGE));
    }

}

function User(id) {
    this.id = id;
    this.parents = [];
    this.children = [];
    this.siblings = [];
    this.spouses = [];

    this.add = function(relationship, personA) {
        var relation = relationship.properties.reltype;
        var id = db.int(personA.identity).toString();
        var user = new User(id);
        var source = null;
        var destin = null;
        var gen = 0; // same generation
        if(relation == "FATHER" || relation == "MOTHER") {
            source = this.parents;
            destin = user.children;
            gen = 1;
        }
        else if(relation == "BROTHER" || relation == "SISTER") {
            source = this.siblings;
            destin = user.siblings;
        }
        else if(relation == "WIFE" || relation == "HUSBAND") {
            source = this.spouses;
            destin = user.spouses;
        }
        else if(relation == "SON" || relation == "DAUGHTER") {
            source = this.children;
            destin = user.parents;
            gen = -1;
        }

        if(source != null) {
            if(source.indexOf(id) == -1)
                source.push(id);
        }
        if(destin != null) {
            if(destin.indexOf(this.id) == -1)
                destin.push(this.id);
        }

        return {gen: gen, person:user};
    }
}

function UserAndGeneration(doi) {
    this.users = {};
    this.generations = new Array(doi * 2 +1);
    this.doi = doi;
    this.init = function(person) {
        this.generations[this.doi] = [new User(db.int(person.identity).toString())];
        var temp = Crypt.decryptPerson(person.properties);
        this.users[db.int(person.identity)] = {
            firstname:temp.firstname,
            lastname:temp.lastname,
            displayname:temp.displayname,
            profileimage:temp.profileimage,
            fid : temp.fid,
            gender:temp.gender,
            gen:[this.doi],
            isprocessed:false,
            processed_level:0
        };
    }
    this.add = function(relationship, personA, personB) {
        var id = db.int(personB.identity).toString();
        if(this.users[id] == undefined) {
            var temp = Crypt.decryptPerson(personB.properties);
            this.users[id] = {
                firstname:temp.firstname,
                lastname:temp.lastname,
                displayname:temp.displayname,
                profileimage:temp.profileimage,
                fid : temp.fid,
                gender:temp.gender,
                gen :[],
                isprocessed:false,
                processed_level:0
            };
        }
        var genIndex = this.doi;
        //for(var ri = 0; ri < relationships.length; ri++ ) {
            //var relationship = relationships[relationships.length - 1];
        var startNodeId = db.int(personA.identity).toString();
        var endNodeId = db.int(personB.identity).toString();

        var userDetail = this.users[startNodeId];
        if(userDetail == undefined) // this condition should not occur
            return;

        console.log("userDetail : " + JSON.stringify(userDetail));
        var userGenerations = userDetail.gen.sort(function(a,b){ return b-a;});
        console.log("userGenerations : " + JSON.stringify(userGenerations));

        var userMapping = this.generations[userGenerations[0]].find(function(user) {
        	console.log(user.id +" and "+ startNodeId);
            return user.id == startNodeId;
        });
    	console.log("UserAndGeneration : " + JSON.stringify(userMapping));
        console.log("UserAndGeneration this : " + JSON.stringify(this));
        if(userMapping != null) {
            var newuser = userMapping.add(relationship, personB);
            console.log("UserAndGeneration newuser : " + JSON.stringify(newuser));
            if(this.generations[userGenerations[0] + newuser.gen] == null)
                this.generations[userGenerations[0] + newuser.gen] = [];
            var user = this.generations[userGenerations[0] + newuser.gen].find(function(user) {
                return user.id == newuser.person.id;
            });

            if(user == null)
                this.generations[userGenerations[0] + newuser.gen].push(newuser.person);
            if(this.users[id].gen.indexOf(userGenerations[0] + newuser.gen) == -1)
                this.users[id].gen.push(userGenerations[0] + newuser.gen);

        }
        console.log("UserAndGeneration this : " + JSON.stringify(this));
    }
}

relationship.getRelationship = async function(usersAndGenerations, fg_id, cur_level, max_level) {

    if(cur_level > max_level) {
        console.log("Reached max level for relationship : " + fg_id + " with cur level : ["+ cur_level + "]" );
        return ;
    }
    console.log("Getting relationship for fg id : " + fg_id + " with cur level : ["+ cur_level + "]" );
    var result = await db.execute("MATCH (a:Person{fid:{fg_id}})-[rel:IS_RELATED_TO]->(b:Person) WHERE rel.status = 'approved' RETURN distinct a, rel, b", {fg_id:fg_id});
    if(!_.isEmpty(result.records)){
        for(var index = 0; index < result.records.length; index++ ) {
            var relation = result.records[index]._fields[1];
            console.log("fg id : " + fg_id + " => User : " + Crypt.decryptField(result.records[index]._fields[2].properties.displayname) + " with relation '" + relation.properties.reltype + "'" );

            await usersAndGenerations.add(relation, result.records[index]._fields[0], result.records[index]._fields[2]);
            var user = usersAndGenerations.users[result.records[index]._fields[2].identity];
            console.log("getRelationship user : " + JSON.stringify(user) +" and "+ (cur_level + 1 < user.processed_level) + " and "+ (cur_level + 1 < max_level));
            if((!user.isprocessed && (cur_level + 1 < max_level)) || (cur_level + 1 < user.processed_level) ) {
                //console.log("Processing for " + result.records[index]._fields[2].properties.displayname + " with fid : " +  result.records[index]._fields[2].properties.fid );
                user.isprocessed = true;
                user.processed_level = cur_level + 1;

                await relationship.getRelationship(usersAndGenerations, result.records[index]._fields[2].properties.fid,cur_level + 1,  max_level);
            }
            //await relationship.getRelationship(usersAndGenerations, result.records[index]._fields[2].properties.fid,cur_level + 1,  max_level);
        }
    }
    else {
        console.log("No relationship for fg id : " + fg_id + " with cur level : ["+ cur_level + "]" );
    }
}

relationship.getPedigreeReport = async function(request, response) {
    try {
        var input = request.query;
        if(input.doi == "" || input.doi == null || input.doi == undefined)
            input.doi = 3;
        if(input.secret_key == "" || input.secret_key == null || input.secret_key == undefined)
            input.secret_key = "";
        var doi = parseInt(input.doi);
        console.log("Querying");
        var result = await db.execute("MATCH (a:Person{key:{value}}) RETURN distinct a", {value:input.secret_key});
        var usersAndGenerations = new UserAndGeneration(doi);
        console.log("usersAndGenerations = " + JSON.stringify(usersAndGenerations));

        if(!_.isEmpty(result.records)){
            usersAndGenerations.init(result.records[0]._fields[0]);
            console.log("usersAndGenerations.init = " + JSON.stringify(usersAndGenerations));
            usersAndGenerations.users[result.records[0]._fields[0].identity].isprocessed = true;
            await relationship.getRelationship(usersAndGenerations, result.records[0]._fields[0].properties.fid, 0, input.doi);
        }

        var fs = require('fs');
        var content = "" + fs.readFileSync('app/reports/pedigree.html');
        content = content.replace("USER_LIST_JSON_REPLACE_BY_SERVER", JSON.stringify(usersAndGenerations));

        response.contentType("text/html");
        response.write(content);
        response.end();
        return ;

        var phantom = require('phantom');
        var path = require('path');

        var log = console.log;
        log = function() {};
        var instance = await phantom.create();
        var page = await instance.createPage();
        //page.property('onCallback', function(data) {
            //console.log("On call back" + JSON.stringify(data));
        //});
        page.property('content', content);
        var filename = uuid.v4();
        var pdf = await page.render('reports/'+filename + '.pdf');
        response.contentType("application/pdf");
        //console.log(path.resolve(__dirname + '/../../' + filename  + '.pdf'))
        response.sendFile(path.resolve(__dirname + '/../../reports/' + filename  + '.pdf'));

        instance.exit();
    }
    catch(e) {
        console.log("Error in report : " + e);
        response.json({});
    }
}
relationship.getPedigreeUsers = async function(request, response) {
    try {
        /*var phantom = require('phantom');

        phantom.create().then(function(ph) {
            ph.createPage().then(function(page) {
                page.open("http://www.google.com").then(function(status) {
                    page.render('google.pdf').then(function() {
                        console.log('Page Rendered');
                        ph.exit();
                    });
                });
            });
        });*/

        var doi = parseInt(request.query.doi);
        console.log("Quering all users within given degree : " + doi);
        var result = await db.execute("MATCH (a:Person{fid:'8a2137'})-[rels:IS_RELATED_TO*1.." + request.query.doi + "]->(b:Person) WHERE ALL (rel in rels WHERE rel.status = 'approved') RETURN distinct a, rels, b ORDER BY LENGTH(rels)", request.query);
        console.log("Quering all users completed.");
        var generations = new UserAndGeneration(doi);

        if(!_.isEmpty(result.records)){


            generations.init(result.records[0]._fields[0]);
            //var sourcePerson = result.records[0]._fields[0].properties;
            //pedigreeUsers[db.int(result.records[0]._fields[0].identity)] = sourcePerson;
            //generations[current] = new Array();
            //generations[current].push( db.int(result.records[0]._fields[0].identity).toString());
            console.log("Total records : " + result.records.length);
            for(var index = 0; index < result.records.length; index++ ) {
                //console.log("Node ID : " + db.int(result.records[index]._fields[2].identity));
                var relationships = result.records[index]._fields[1];
                generations.add(relationships, result.records[index]._fields[2]);
                /*console.log("=============================================================");
                var genIndex = current;
                for(var ri = 0; ri < relationships.length; ri++ ) {

                    console.log("Start : " + db.int(relationships[ri].start) + ",end : " + db.int(relationships[ri].end));
                    var relationship = relationships[ri];
                    var relation = relationship.properties;


                    if(relation.reltype == "FATHER" || relation.reltype == "MOTHER") {
                        genIndex++;
                    }
                    else if(relation.reltype == "SON" || relation.reltype == "DAUGHTER") {
                        genIndex--;
                    }
                    else if(relation.reltype == "HUSBAND" || relation.reltype == "WIFE") {

                    }
                    else if(relation.reltype == "BROTHER" || relation.reltype == "SISTER") {

                    }

                    if(generations[genIndex] == undefined)
                        generations[genIndex] = new Array();
                    if(generations[genIndex].indexOf(db.int(relationship.end).toString()) == -1)
                        generations[genIndex].push(db.int(relationship.end).toString());

                }


                var person = result.records[index]._fields[2].properties;
                pedigreeUsers[db.int(result.records[index]._fields[2].identity)] = {displayname:person.displayname, mobilenumber:person.mobilenumber, fid:person.fid, profileimage: person.profileimage};
                */
            }
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: generations });
    }
    catch(e) {
        console.log("Failed in pedigree view : " + e)
        response.json(new ERROR(ErrorCode.FAILED_TO_UPDATE_PROFILE));
    }
}


relationship.sendTestSMS = async function(request, response) {
    try {
        MessageService.sendSMS("91", '9945667681', 'Test Message');
    }
    catch(e) {

    }
}

relationship.ancestry = async function(request, response) {
    try {
        var key=request.query.secret_key;
        var doi=request.query.doi;
        console.log("ancestry = "+doi+" "+key);

        var ancestrymembersresult = await db.execute("MATCH (a:Person{key:{key}})-[rels:IS_RELATED_TO*1.."+ doi + "]->(b:Person) return distinct a, b ",{key:key});
        var ancestrymembers = [];
        var ancestrymembersfid = [];
        if(!_.isEmpty(ancestrymembersresult.records)) {

            for(var j=0;j<ancestrymembersresult.records.length;j++){
                var destPersons=ancestrymembersresult.records[j]._fields[1].properties;
                var tempMember = {};
                tempMember.fid = destPersons.fid;
                tempMember.person = destPersons;
                ancestrymembers.push(tempMember);
                ancestrymembersfid.push(tempMember.fid);
            }
            var destPersons=ancestrymembersresult.records[0]._fields[0].properties;

            if(ancestrymembersfid.indexOf(destPersons.fid)==-1){
                var tempMember = {};
                tempMember.fid = destPersons.fid;
                tempMember.person = destPersons;
                ancestrymembers.push(tempMember);
                ancestrymembersfid.push(tempMember.fid);
            }
        }
            //console.log("ancestry2 = "+ JSON.stringify(ancestrymembersfid));

        var ancmembersrelationresult = await db.execute("MATCH (a:Person)-[r:IS_RELATED_TO]->(b:Person) WHERE a.fid IN {ids1} and b.fid IN {ids2} return distinct a,r,b",{ids1:ancestrymembersfid,ids2:ancestrymembersfid});

            var ancestrymembersrelation=[];
            if(!_.isEmpty(ancmembersrelationresult.records)) {

                for(var k = 0; k < ancestrymembers.length; k++ ) {

                    var singleperson={};
                    singleperson.fid=ancestrymembers[k].fid;
                    singleperson.name=Crypt.decryptField(ancestrymembers[k].person.displayname);

                        for(var index = 0; index < ancmembersrelationresult.records.length; index++ ) {

                            var srcPerson=ancmembersrelationresult.records[index].get(0).properties;
                            var relationship=ancmembersrelationresult.records[index].get(1).properties;
                            var destPerson=ancmembersrelationresult.records[index].get(2).properties;
                            //console.log("ancestry5 = "+ singleperson.fid +" and "+ srcPerson.fid+" and "+ destPerson.fid+" and "+ relationship.reltype);

                            if(singleperson.fid == srcPerson.fid){

                                var reltype=relationship.reltype;

                                if(singleperson[reltype] != undefined && singleperson[reltype] != "" && singleperson[reltype]!= null){
                                    singleperson[reltype] = singleperson[reltype] + ", " + destPerson.fid;
                                }else{
                                    singleperson[reltype] = destPerson.fid;
                                }
                                //console.log("ancestrymembersrelation = "+ JSON.stringify(sourcePerson));
                            }
                        }
                        ancestrymembersrelation.push(singleperson);
                }
            }
                //console.log("ancestrymembersrelation = "+ JSON.stringify(ancestrymembersrelation));
                response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: ancestrymembersrelation });
    }
    catch(e) {
        console.log("ancestry error = "+ e);
        response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_CONNECT_PROFILE, result: e });
    }
}

relationship.getPedigreeTree = async function(request, response) {
    try {
        
       var users = await relationship.treeancestry(request, response);

        var fs = require('fs');
        var content = "" + fs.readFileSync('app/reports/pedigreetree.html');
        content = content.replace("USER_LIST_JSON_REPLACE_BY_SERVER", JSON.stringify(users));

        response.contentType("text/html");
        response.write(content);
        response.end();
        return;

        var phantom = require('phantom');
        var path = require('path');

        var log = console.log;
        log = function() {};
        var instance = await phantom.create();
        var page = await instance.createPage();
        //page.property('onCallback', function(data) {
            //console.log("On call back" + JSON.stringify(data));
        //});
        page.property('content', content);
        var filename = uuid.v4();
        var pdf = await page.render('reports/'+filename + '.pdf');
        response.contentType("application/pdf");
        //console.log(path.resolve(__dirname + '/../../' + filename  + '.pdf'))
        response.sendFile(path.resolve(__dirname + '/../../reports/' + filename  + '.pdf'));

        instance.exit();
    }
    catch(e) {
        console.log("Error in report : " + e);
        response.json({});
    }
}

relationship.tree = async function(request, response) {
    try {
        var input = request.query;
        if(input.doi == "" || input.doi == null || input.doi == undefined)
            input.doi = 3;
        if(input.secret_key == "" || input.secret_key == null || input.secret_key == undefined)
            input.secret_key = "";
        var doi = parseInt(input.doi);
        console.log("Querying");
        var result = await db.execute("MATCH (a:Person{key:{value}}) RETURN distinct a", {value:input.secret_key});
        console.log("Query completed");
        var generations = new UserAndGeneration(doi);

        if(!_.isEmpty(result.records)){
            generations.init(result.records[0]._fields[0]);
            generations.users[result.records[0]._fields[0].identity].isprocessed = true;
            await relationship.getRelationship(generations, result.records[0]._fields[0].properties.fid, 0, input.doi);
        }


        var newGeneration={};
		for(var i in generations.generations)
		{
			var innnerArray = generations.generations[i];
			for(var j in innnerArray)
			{
				var generationObject = innnerArray[j];
				newGeneration[generationObject.id]=generationObject;
			}
		}
        generations.center_id = result.records[0]._fields[0].identity.low + "";
        generations.first_id = generations.generations[doi * 2][0].id;
        generations.generations = newGeneration;

		response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: generations });
    }
    catch(e) {
        console.log("Error in tree : " + e);
        response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_GET_TREE, result: e });
    }
}

relationship.fgdatatreeancestry = async function(request, response) {
    try {
        var input = request.query;
        if(input.doi == "" || input.doi == null || input.doi == undefined)
            input.doi = 3;
        var doi = parseInt(input.doi);
        doi = doi + 1;
        console.log("fgdatatreeancestry input = " + JSON.stringify(input));
	    var result;
        if(input.fid != "" && input.fid != null && input.fid != undefined) {
	        result = await db.execute("MATCH (a:Person{fid:{value}}) RETURN distinct a", {value:input.fid});
        } else {
	        result = await db.execute("MATCH (a:Person{mobilenumber:{value}}) RETURN distinct a", {value:input.secret_mobile_number});        
        }
        console.log("Query completed");
        var generations = new UserAndGeneration(doi);

        if(!_.isEmpty(result.records)){
            generations.init(result.records[0]._fields[0]);
            generations.users[result.records[0]._fields[0].identity].isprocessed = true;
            await relationship.getRelationship(generations, result.records[0]._fields[0].properties.fid, 0, doi);
        }else{
//         	if(input.mobilenumber == "" || input.mobilenumber == null || input.mobilenumber == undefined)
//             input.mobilenumber = "";
// 	        result = await db.execute("MATCH (a:Person{mobilenumber:{value}}) RETURN distinct a", {value:input.mobilenumber});
// 		    console.log("treeancestry else result = " + JSON.stringify(result));
//         	generations = new UserAndGeneration(doi);
// 			if(!_.isEmpty(result.records)){
// 	            generations.init(result.records[0]._fields[0]);
//             	generations.users[result.records[0]._fields[0].identity].isprocessed = true;
//             	await relationship.getRelationship(generations, result.records[0]._fields[0].properties.fid, 0, input.doi);
//         	}
        }

        var newGeneration=[];
		for(var i in generations.generations){
			var innnerArray = generations.generations[i];

			for(var j in innnerArray){
				var generationObject1 = innnerArray[j];
			    innnerArray[j]["level"] = generations.users[generationObject1.id].gen[0];
			    innnerArray[j]["name"]  = generations.users[generationObject1.id].displayname;
			}

			console.log("innnerArray1 = "+ JSON.stringify(innnerArray));
			for(var k in innnerArray){
				for(var l in innnerArray){
					var diff = l-k;
					if(diff >1 || diff<-1){
					console.log("innnerArray k and l = "+ k +" and " + l + " and " + JSON.stringify(innnerArray[k]) +" and "+ JSON.stringify(innnerArray[l]));

					if(innnerArray[l] != undefined && innnerArray[k] != undefined)
					{
					console.log("innnerArray[k]1 = "+ innnerArray[k]["id"] +" and "+ innnerArray[l]["spouses"]);
					if (innnerArray[k]["id"] == innnerArray[l]["spouses"]){
						var tempInt = parseInt(k)+1;
						innnerArray = moveJsonObject(innnerArray,l , tempInt );
						console.log("innnerArray check inner = "+ l +" and " + tempInt + " and " +   JSON.stringify(innnerArray));
						console.log("innnerArray[k]2 = "+ innnerArray[k]["id"] +" and "+ innnerArray[tempInt]["spouses"]);
					}
					}
					}
				}
			}
			
// 			innnerArray = sortByKey(innnerArray, 'spouses');
//  		innnerArray = sortByKey(innnerArray, 'id', 'spouses');
			console.log("innnerArray2 = "+ JSON.stringify(innnerArray));

			for(var j in innnerArray){
				var generationObject = innnerArray[j];
			    generationObject["level"] = generations.users[generationObject.id].gen[0];
			    // generationObject["name"] = generations.users[generationObject.id].displayname +" , "+generationObject.id;
			    generationObject["name"] = generations.users[generationObject.id].displayname;
				newGeneration.push(generationObject);
			}
		}
		
        generations.center_id = result.records[0]._fields[0].identity.low + "";
        generations.generations = newGeneration;
		console.log('treeancestry = ' + input.mode);
        if(input.mode != undefined && input.mode != null && input.mode == "api") {
    		response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: newGeneration });
		}

		return newGeneration;
    }
    catch(e) {
        console.log("Error in tree : " + e);
        response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_GET_TREE, result: e });
    }
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function sortByKey(array, key1, key2) {
    return array.sort(function(a, b) {
        var x = a[key1]; var y = b[key2];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function moveJsonObject(arr, old_index, new_index) {
    while (old_index < 0) {
        old_index += arr.length;
    }
    while (new_index < 0) {
        new_index += arr.length;
    }
    if (new_index >= arr.length) {
        var k = new_index - arr.length;
        while ((k--) + 1) {
            arr.push(undefined);
        }
    }
     arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
   return arr;
}

relationship.findrelationship = async function(request, response) {
    try {
    	
    	var secret_key=request.query.secret_key;
        var relationfid=request.query.relationfid;
    	console.log('findrelationship = ' + relationfid + ' and ' + secret_key);

        var result = await db.execute("MATCH p=shortestPath((a:Person{key:{secret_key}})-[:IS_RELATED_TO*1..3]->(b:Person{fid:{relationfid}})) RETURN p", {secret_key:secret_key, relationfid:relationfid});
        var blockedUsers = [];
	    console.log(JSON.stringify(result));
        if(!_.isEmpty(result.records)){
//	 	       for(var index = 0; index < result.records.length; index++ ) {
//                 var person = result.records[index]._fields[0].properties;
//                 blockedUsers.push({displayname:Crypt.decryptField(person.displayname), mobilenumber:person.mobilenumber, fid:person.fid, profileimage: Crypt.decryptField(person.profileimage)});
//             }
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: blockedUsers }); 
    }
    catch(e){
	    console.log('findrelationship error = ' + e );
        response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_GET_TREE, result: e });
    }
}