﻿var Config = new require("../config");
var db = new require('../helpers/db');
var uuid = require('uuid');
var xmpp = require('node-xmpp');
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var Crypt = require("../helpers/crypt");
var _ = require('underscore');
var relationshipController = require('../controllers/relationshipController');
var chatController = require('../controllers/chatController');
var CronJob = require('cron').CronJob;

var Chat = exports;

var MessageType = {
    SINGLE_CHAT: 0,
    GROUP_CHAT: 1,
    POST_MESSAGE: 2,
    NOTIFICATION: 3,
    REMINDER: 4,
    ACKNOWLEDGEMENT: 5,
    GROUPEXTRAMESSAGE: 6
};
var options = {
    type: 'client',
    jid: Config.FIREBASE_SENDER_ID + '@gcm.googleapis.com',
    password: Config.FIREBASE_SERVER_KEY,
    port: 5235,
    host: 'gcm.googleapis.com',
    legacySSL: true,
    preferredSaslMechanism: 'PLAIN'
};

var client;
Chat.isDraining = false;
Chat.messageQueue = [];

Chat.listen = function() {
    client = new xmpp.Client(options);
    client.connection.socket.setTimeout(0);
    client.connection.socket.setKeepAlive(true, 10000);

    client.on('online',
      function () {
          if(Chat.isDraining) {
              Chat.isDraining = false;
              var i = Chat.messageQueue.length;
              while (i--) {
                  client.send(Chat.messageQueue[i]);
              }
              Chat.messageQueue = [];
          }
          console.log("fcm online");
      }
    );
    client.on('error', function(e){
        console.log("Connection failed in XMPP connection.");
        console.error(e.toString());
    });

    client.on('close', function () {
        if (Chat.isDraining) {
            client.connect();
        } else {
            //self.events.emit('disconnected', self.connectionType);
            console.log("Disconnected by CCS server.");
            client.connect();
        }
    });

    client.on('stanza',
      function (stanza) {
          if (stanza.is('message') &&
              // Best to ignore an error
              stanza.attrs.type !== 'error') {

              //Message format as per here: https://developer.android.com/google/gcm/ccs.html#upstream
              var messageData = JSON.parse(stanza.getChildText("gcm"));
              console.log("fcm message = " + JSON.stringify(messageData));

              if (messageData && messageData.message_type != "ack" && messageData.message_type != "nack" && messageData.message_type != "receipt") {

                  var ackMsg = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
                      "to": messageData.from,
                      "message_id": messageData.message_id,
                      "message_type": "ack"
                  }));
                  //send back the ack.
                  client.send(ackMsg);
                  //console.log("Sent ack");

                  //Now do something useful here with the message
                  //e.g. awesomefunction(messageData);
                  //but let's just log it.
                  try {
                      if (messageData.data.type == MessageType.SINGLE_CHAT)
                          handleSingleChat(messageData.data);
                      else if (messageData.data.type == MessageType.GROUP_CHAT)
                          handleGroupChat(messageData.data);
                  }
                  catch(e) {console.log("Error : " + e);}


              } else if(messageData && messageData.message_type == "nack") {
                  console.log("Not acknowledged message. " + JSON.stringify(messageData));
                  // Check for device unregistered then remove from database
                  var messageDataTemp = {message_id : messageData.message_id};
                  sendAcknowledgement(messageDataTemp, "failed")                  
                  if(messageData.error == "DEVICE_UNREGISTERED") {
                      console.log("Clearing device token : " + messageData.from);
                      db.execute("MATCH(p:Person{devicetoken:{dt}}) SET p.devicetoken = ''", {dt:messageData.from});
                  }
              }
              else if(messageData && messageData.message_type == "ack") {
                  console.log("Acknowledgement received for " + messageData.message_id);
                  var messageDataTemp = {message_id : messageData.message_id};
                  sendAcknowledgement(messageDataTemp, "sent")                  

                  //Need to do something more here for a nack.
                  //console.log("message was an ack or nack...discarding");
                  //console.log(messageData);
              }
              else if(messageData && messageData.message_type == "control") {
                  console.log("Control message received for " + messageData.control_type);
                  if(messageData.control_type.toUpperCase() == "CONNECTION_DRAINING")
                      Chat.isDraining = true;
                  //Need to establish new connection if control type is connection draining
              }
              else if(messageData && messageData.message_type == "receipt") {
                  console.log("Receipt message received for " + messageData.message_id);
                  var messageDataTemp = {message_id : messageData.data.original_message_id};
                  sendAcknowledgement(messageDataTemp, "delivered")                  
                  //Need to establish new connection if control type is connection draining
              }

          } else {
              console.log("fcm error");
              console.log(stanza)
          }

      }
    );
}

function send(packet) {
    console.log("chatController send = "+ Chat.isDraining + " and " +JSON.stringify(packet));
    if(Chat.isDraining)
        Chat.messageQueue.push(packet);
    else
        client.send(packet);
}

async function handleGroupChat(incomingMessage) {
    /*
    Incoming packet structure for group chat
    var data = {
        type:1,
        key:<secret_key>,
        msg: {
            content:"",
            images:[<url1>, <url2>],
            videos:[<url1>, <url2>],
        },
        to:<gid>,
    }
    */
    console.log("Group chat with data packet : " + JSON.stringify(incomingMessage));
    var outgoingMessage = incomingMessage;
    var result = await db.execute("MATCH (source:Person {key:{key}})-[rs:IS_MEMBER_OF]->(destination:Group{gid:{gid}}) RETURN destination, source", {key:incomingMessage.key, gid:incomingMessage.to});
    
    result.records.forEach( async function (record) {
        var group = record.get(0).properties;
        var sourceperson = record.get(1).properties;

        for(var index =0; index < group.members.length; index++) {
		    console.log("handleGroupChat : " + group.members[index]);
            try {
                if(group.members[index] == sourceperson.fid)
                    continue;

                var memberResult = await db.execute("MATCH (person:Person{fid:{fid}}) RETURN person", { fid:group.members[index]});

                var member = memberResult.records[0]._fields[0].properties;

                if( member.devicetoken == null || member.devicetoken == '' || member.devicetoken == undefined)
                    continue;
				var message_id = incomingMessage.messageId;
    	        outgoingMessage.message_id = message_id;
            	outgoingMessage.to = member.devicetoken;
				var msgJson = incomingMessage.msg;            
				if(incomingMessage.mode=="fcm"){
					msgJson = msgJson.replace(/\\/g, "");
					console.log("chatcontroller msgJson1 = " + msgJson);
					msgJson = JSON.parse(msgJson);			
				}
				console.log("chatcontroller msgJson2 = " + msgJson);
				outgoingMessage.content_available = true;

				var msgContentType = msgJson.extraType;
				msgContentType = msgContentType.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	    			return letter.toUpperCase();
				});

				if(msgContentType == 'Text')
					msgContentType = msgJson.content;
                if(msgJson.extraType == 'groupextramessage'){
                	msgContentType = msgJson.extraString;
                }
                    var badgeCount = 0;
				    var unreadArray = member.unreadArray;
				    console.log('unreadArray = '+JSON.stringify(unreadArray));
				    if(unreadArray != undefined && unreadArray != null){
				       	 badgeCount = unreadArray.length;
        				 if (unreadArray.indexOf(msgJson.gid) == -1) {
			   	    	        setUnreadCount(unreadArray, msgJson.gid, group.members[index]);
				        	    badgeCount = badgeCount + 1;
			    	    } 
				    } else{
			    		 setUnreadCount(unreadArray, msgJson.gid, group.members[index]);
			    	     badgeCount = badgeCount + 1;        
				    }
				    badgeCount = badgeCount +"";                

                if(member.platform == undefined || member.platform == '' || member.platform == null || member.platform == 'android'){
                	outgoingMessage.data = {type:MessageType.GROUP_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, groupName: group.name, message_id: message_id};
                }else{
                	console.log("handleGroupChat : " + group.members[index] + ' and member.platform = ' + member.platform + ' and msgJson.extraType = ' + msgJson.extraType );

                	if(msgJson.extraType == 'groupextramessage'){
                		outgoingMessage.notification = {type:MessageType.GROUPEXTRAMESSAGE, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, groupName: group.name, message_id:message_id};
                	} else {
                    	outgoingMessage.notification = {type:MessageType.GROUP_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, groupName: group.name, message_id:message_id};
                	}
                	// outgoingMessage.data = {type:MessageType.GROUP_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, groupName: group.name};
                }
            	//outgoingMessage.notification = {title:Crypt.decryptField(sourceperson.displayname), text:outgoingMessage.data.msg.content};
            
            	console.log("chatcontroller handleGroupChat = " + JSON.stringify(outgoingMessage));

				saveMessage(msgJson, group.members[index], incomingMessage.type, message_id);

                var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
                send(packet)
                console.log("Sent group message to : " + member.displayname + " device token : "+ member.devicetoken);
            }
            catch(e) {
                console.log("Error : " + e);
                console.log("Failed to send group message to : " + group.members[index]);
            }
        }
    });
}

async function handleSingleChat(incomingMessage) {
    console.log("Recevied single chat with packet " + JSON.stringify(incomingMessage));
    try {
        var outgoingMessage = {};
    
        var result = await db.execute("MATCH (source:Person {key:{key}}) , (destination:Person{fid:{fid}}) RETURN destination, source", {key:incomingMessage.key, status:'approved', fid:incomingMessage.to});

        result.records.forEach(async function (record) {
            console.log("Found the user...");
            var destinationperson = record.get(0).properties;
            var sourceperson = record.get(1).properties;

			var message_id = incomingMessage.messageId;
            outgoingMessage.message_id = message_id;
            outgoingMessage.to = destinationperson.devicetoken;

			var msgJson = incomingMessage.msg;
            
			if(incomingMessage.mode=="fcm"){
				msgJson = msgJson.replace(/\\/g, "");
				console.log("chatcontroller msgJson1 = " + msgJson);
				msgJson = JSON.parse(msgJson);			
			}
			await saveMessage(msgJson, incomingMessage.to, incomingMessage.type, message_id);

            if( destinationperson.devicetoken == null || destinationperson.devicetoken == '' || destinationperson.devicetoken == undefined){
                var messageDataTemp = {message_id : message_id};
                sendAcknowledgement(messageDataTemp, "failed")                  
                return;
            }

			console.log("chatcontroller msgJson2 = " + msgJson);
			outgoingMessage.content_available = true;
			var msgContentType = msgJson.extraType;

			msgContentType = msgContentType.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	    		return letter.toUpperCase();
			});

			console.log("chatcontroller msgContentType = " + msgContentType);
			if(msgContentType == 'Text')
				msgContentType = msgJson.content;
			console.log("chatcontroller msgContentType = " + msgContentType);

                    var badgeCount = 0;
				    var unreadArray = destinationperson.unreadArray;
				    console.log('unreadArray = '+JSON.stringify(unreadArray));
				    if(unreadArray != undefined && unreadArray != null){
				       	 badgeCount = unreadArray.length;
        				 if (unreadArray.indexOf(msgJson.fid) == -1) {
			   	    	        setUnreadCount(unreadArray, msgJson.fid, incomingMessage.to);
				        	    badgeCount = badgeCount + 1;
			    	    } 
				    } else{
			    		 setUnreadCount(unreadArray, msgJson.fid, incomingMessage.to);
			    	     badgeCount = badgeCount + 1;        
				    }
				    badgeCount = badgeCount + "";
            if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
                outgoingMessage.data = {type:MessageType.SINGLE_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, message_id: message_id};
            } else{
                outgoingMessage.notification = {type:MessageType.SINGLE_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount, message_id: message_id};
                // outgoingMessage.data = {type:MessageType.SINGLE_CHAT, msg:msgJson, text:msgContentType, title:Crypt.decryptField(sourceperson.displayname), badge: badgeCount};
            }
            outgoingMessage.delivery_receipt_requested = true;
            //outgoingMessage.notification = {title:Crypt.decryptField(sourceperson.displayname), text:outgoingMessage.data.msg.content};
            
            console.log("chatcontroller handleSingleChat1 = " + JSON.stringify(outgoingMessage));

            var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
            send(packet)
        });
    }
    catch(e) {console.log("Single chat error : " + e);}

    /*var message = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
        "to": messageData.from,
        "message_id": messageData.message_id,
        "message_type": "ack"
    }));*/
}

async function saveMessage(msgJson, toFid, type, mid) {
	try{
        console.log("saveMessage msgJson= " + JSON.stringify(msgJson));
		if(type == 0){
				await db.execute("CREATE(m:Message {mid:{mid}, content:{content}, images:{images}, audios:{audios}, videos:{videos}, extraType:{extraType}, extraString:{extraString}, time:{time}, fid:{fid}})", {mid:mid, content:msgJson.content, audios:msgJson.audios, videos: msgJson.videos, images: msgJson.images, extraType: msgJson.extraType, extraString: msgJson.extraString, time:new Date().getTime(), fid:msgJson.fid});
			}
		else{
				await db.execute("CREATE(m:Message {mid:{mid}, content:{content}, images:{images}, audios:{audios}, videos:{videos}, extraType:{extraType}, extraString:{extraString}, time:{time}, fid:{fid}, gid:{gid}})", {mid:mid, content:msgJson.content, audios:msgJson.audios, videos: msgJson.videos, images: msgJson.images, extraType: msgJson.extraType, extraString: msgJson.extraString, time:new Date().getTime(), fid:msgJson.fid, gid: msgJson.gid});
			}
		await db.execute("Match (a:Person{fid:{fid}}),(b:Message{mid:{mid}}) create (a)-[r:IS_SENT{time:{time}}]->(b)",{fid:msgJson.fid, time:new Date().getTime(), mid:mid});
		await db.execute("Match (a:Person{fid:{fid}}),(b:Message{mid:{mid}}) create (b)-[r:SENT_TO{time:{time}}]->(a)",{fid:toFid, time:new Date().getTime(), mid:mid});
	}catch(e){
		console.log('saveMessage = '+e);
	}
}

async function setUnreadCount(unreadArray, fromFidorGid, toFid) {
	try{
        console.log("setUnreadCount fromFidorGid = " + fromFidorGid + " and " + toFid);
		var queryResult = await db.execute("Match (a:Person{fid:{toFid}}) return a.unreadArray",{toFid:toFid});
        console.log("setUnreadCount queryResult = " + JSON.stringify(queryResult));

		unreadArray = queryResult.records[0]._fields[0];
		    console.log(JSON.stringify(unreadArray));
		if (unreadArray == undefined || unreadArray == null)
		    unreadArray = [];
		    console.log(JSON.stringify(unreadArray));
        if (unreadArray.indexOf(fromFidorGid) == -1) {
            unreadArray.push(fromFidorGid);
        }
        console.log('toFid = ' + toFid + ' and unreadArray = ' + JSON.stringify(unreadArray));
		await db.execute("Match (a:Person{fid:{toFid}}) set a.unreadArray = {unreadArray}",{toFid:toFid, unreadArray:unreadArray});
	}catch(e){
		console.log('setUnreadCount = ' + e);
	}
}

Chat.clearBadgeCount = async function(request, response) {
    try {
        var data = request.body;
        var result = await db.execute("Match (p:Person{key:{secret_key}}) RETURN p", {secret_key:data.secret_key});
        if(!_.isEmpty(result.records)){
            var destinationperson = result.records[0]._fields[0].properties;
            var unreadArray = destinationperson.unreadArray;
            if(unreadArray != undefined && unreadArray != null){
                console.log('unreadArray = '+JSON.stringify(unreadArray));
                var index = unreadArray.indexOf(data.fidOrGid);
                if (index > -1) {
                    unreadArray = unreadArray.filter(function(item) {
                        return item !== data.fidOrGid
                    })
                    console.log('unreadArray = '+JSON.stringify(unreadArray));
		            await db.execute("Match (a:Person{key:{secret_key}}) set a.unreadArray = {unreadArray}",{secret_key:data.secret_key, unreadArray:unreadArray});
                }
            }
            if(data.typeChat == '0') {
    	        var messageData = {fid:data.fidOrGid, message_id: "all", openerFid: destinationperson.fid};
	            sendAcknowledgement(messageData, "read");
            }
        }
        if(data.fidOrGid != 'feed'){
            response.json({iserror:false, errorcode:0, result:{status:true}});
        }
    }
    catch(e) {
        console.log("Error in getting clearBadgeCount : " + e);
        response.json(new ERROR(ErrorCode.CLEAR_BADGE_COUNT));
    }
}

async function sendGroupMessageFromServer(incomingMessage) {
    /*
    Incoming packet structure for group chat
    var data = {
        type:1,
        key:<secret_key>,
        msg: {
            content:"",
            images:[<url1>, <url2>],
            videos:[<url1>, <url2>],
        },
        to:<gid>,
    }
    */
    console.log("sendGroupMessageFromServer : " + JSON.stringify(incomingMessage));
    var outgoingMessage = incomingMessage;
    var result = await db.execute("MATCH (source:Person {key:{key}}),(destination:Group{gid:{gid}}) RETURN destination, source", {key:incomingMessage.key, gid:incomingMessage.to});
    
    result.records.forEach( async function (record) {
        var group = record.get(0).properties;
        var sourceperson = record.get(1).properties;

        for(var index =0; index < group.members.length; index++) {
            
            try {
                if(group.members[index] == sourceperson.fid)
                    continue;

                var memberResult = await db.execute("MATCH (person:Person{fid:{fid}}) RETURN person", { fid:group.members[index]});

                var member = memberResult.records[0]._fields[0].properties;

                if( member.devicetoken == null || member.devicetoken == '' || member.devicetoken == undefined)
                    continue;
                outgoingMessage.to = member.devicetoken;
                outgoingMessage.message_id = uuid.v4();

                if(member.platform == undefined || member.platform == '' || member.platform == null || member.platform == 'android'){
                    outgoingMessage.data = {msg:incomingMessage.msg, type:MessageType.GROUP_CHAT, title:Crypt.decryptField(sourceperson.displayname), text:incomingMessage.msg.content};
                }else{
                    outgoingMessage.notification = {msg:incomingMessage.msg, type:MessageType.GROUP_CHAT, title:Crypt.decryptField(sourceperson.displayname), text:incomingMessage.msg.content};
                    outgoingMessage.data = {msg:incomingMessage.msg, type:MessageType.GROUP_CHAT, title:Crypt.decryptField(sourceperson.displayname), text:incomingMessage.msg.content};
                }
                //outgoingMessage.notification = {title:Crypt.decryptField(sourceperson.displayname), text:incomingMessage.msg.content};
                var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
                send(packet)
                console.log("Sent group message to : " + member.displayname + " device token : "+ member.devicetoken);
            }
            catch(e) {
                console.log("Error : " + e);
                console.log("Failed to send group message to : " + group.members[index]);
            }
        }
    });
}

Chat.sendMessage = async function(request, response) {
    try {
        var message = request.body;
        console.log("Message data : " + JSON.stringify(message))
        if(message.type == MessageType.SINGLE_CHAT) {
            handleSingleChat(message);
		    response.json({iserror:false, result:{to:message.to, messageId:message.messageId, status:true}});
        }
        else {
            handleGroupChat(message);
            response.json({iserror:false, result:{to:message.to, messageId:message.messageId, status:true}});
		}
    }
    catch(e){
        console.log("Error in sending chat message : " + e);
        response.json(new ERROR(ErrorCode.ERROR_IN_SENDING_MESSAGE));
    }
}


Chat.createGroup = async function(request, response) {
    //db.execute()
    try {
        var data = request.body;
        data.gid = uuid.v4();
        data.admins = [data.fid];
    
        var result = await db.execute("CREATE(group:Group {gid:{gid}, name:{name}, admins:{admins}, members:{members}, profileimage:{url}}) RETURN group", data);
        result = await db.execute("MATCH(person:Person{key:{secret_key}}), (group:Group{gid:{gid}}) WHERE NOT (person)-[:IS_MEMBER_OF]-(group) WITH person, group CREATE UNIQUE (person)-[:IS_MEMBER_OF]->(group)", data);
        if(data.members != undefined) {
            for(var index = 0; index < data.members.length; index++) {
                await db.execute("MATCH(person:Person{fid:{fid}}), (group:Group{gid:{gid}}) WHERE NOT (person)-[:IS_MEMBER_OF]->(group) WITH person, group CREATE UNIQUE (person)-[:IS_MEMBER_OF]->(group)", {fid:data.members[index], gid:data.gid});
            }
        }
        response.json({iserror:false, errorcode:0, result:{gid:data.gid}});
    }
    catch(e) {
        console.log("Error in create group : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_CREATE_GROUP));
    }

}

Chat.groups = async function(request, response) { 
    try {
        var data = request.body;
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group) RETURN group", data);
        var groupList = [];
        if(result.records.length != 0) {
        
        for(var index = 0; index < result.records.length; index++ ) {
        	var group = result.records[index]._fields[0].properties;
                
        var membersArray = [];
        for(var indexm =0; indexm < group.members.length; indexm++) {
            try {
                var memberResult = await db.execute("MATCH (person:Person{fid:{fid}}) RETURN person", { fid:group.members[indexm]});
                var person = memberResult.records[0]._fields[0].properties;
                person = Crypt.decryptPerson(person);

                personprofile={fid:person.fid, profileimage:person.profileimage, displayname:person.displayname};
				membersArray.push(personprofile);
            }
            catch(e) {
                console.log("Error : " + e);
                console.log("Failed to get group member: " + group.members[indexm]);
            }
        }
     
        groupList.push({gid:group.gid, name : group.name, admins:group.admins, members:membersArray, url:group.profileimage});
            }
        }
        response.json({iserror:false, errorcode:0, result:groupList});
        console.log("group list : " + JSON.stringify(groupList));
    }
    catch(e) {
        console.log("Error in getting group list : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.addMember = async function(request, response) { 
    try {
        var data = request.body;
        if(data.isadmin == "true") {
            await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group{gid:{gid}}) WHERE exists(group.admins) AND (person.fid in (group.admins)) AND NOT({fid} in (group.admins)) SET group.admins = group.admins + {fid}  RETURN person, group", data);
        }
        
        await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group{gid:{gid}}) WHERE exists(group.members) AND NOT({fid} in (group.members)) SET group.members = group.members + {fid}  RETURN person, group", data);
        
        await db.execute("MATCH(person:Person{fid:{fid}}),(group:Group{gid:{gid}}) CREATE UNIQUE (person)-[:IS_MEMBER_OF]->(group)", data);
        
        response.json({iserror:false, errorcode:0, result:{status:true}});
    }
    catch(e) {
        console.log("Error in adding member to group : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.removeMember = async function(request, response) { 
    try {
        var data = request.body;
        
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group{gid:{gid}}) SET group.admins = FILTER(x in group.admins WHERE x <> {fid}), group.members = FILTER(x in group.members WHERE x <> {fid}) WITH person MATCH(p:Person{fid:{fid}})-[r:IS_MEMBER_OF]->(g:Group{gid:{gid}}) DELETE r  RETURN person", data);
        if(data.type!=undefined) {
			console.log("data.type = " +data.type);
			var message={};
			message.to=data.gid;
			message.msg='{content:"",images:[],audios:[],videos:[],fid:"'+data.fid+'",gid:"'+data.gid+'",extraType:"groupextramessage",extraString:"'+data.fid+' left"}';
			console.log("message.msg = " +message.msg);
			message.key=data.secret_key;
            sendGroupMessageFromServer(message);
		}
        response.json({iserror:false, errorcode:0, result:{status:result.records.length != 0}});
    }
    catch(e) {
        console.log("Error in removing member from group : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.revokeAdminPermission = async function(request, response) { 
    try {
        var data = request.body;
        
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group{gid:{gid}}) WHERE exists(group.admins) AND (person.fid in (group.admins)) SET group.admins = FILTER(x in group.admins WHERE x <> {fid}) RETURN person", data);
               
        response.json({iserror:false, errorcode:0, result:{status:result.records.length != 0}});
    }
    catch(e) {
        console.log("Error in removing member from group : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.sendPostMessage = function(destinationperson, message) {

				    var badgeCount = 0;
				    var unreadArray = destinationperson.unreadArray;
				    console.log('unreadArray = '+JSON.stringify(unreadArray));
				    if(unreadArray != undefined && unreadArray != null){
				       	 badgeCount = unreadArray.length;
        				 if (unreadArray.indexOf('feed') == -1) {
			   	    	        setUnreadCount(unreadArray, 'feed', destinationperson.fid);
				        	    badgeCount = badgeCount + 1;
			    	    } 
				    } else{
			    		 setUnreadCount(unreadArray, 'feed', destinationperson.fid);
			    	     badgeCount = badgeCount + 1;
				    }
				    badgeCount = badgeCount +"";

    var to = destinationperson.devicetoken;
    if(to == null || to == "" || to == undefined)
        return ;

    var outgoingMessage = {
        type : 2,
        message_id: uuid.v4(),
        to : to,
        content_available : true
    };
    if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
        outgoingMessage.data = {type:MessageType.POST_MESSAGE, msg: message, text: "You have new post(s)", title : "officeG", badge: badgeCount};
    } else {
        outgoingMessage.notification = {type:MessageType.POST_MESSAGE, msg: message, text: "You have new post(s)", title : "officeG", badge: badgeCount};
        outgoingMessage.data = {type:MessageType.POST_MESSAGE, msg: message, text: "You have new post(s)", title : "officeG", badge: badgeCount};
    }

    console.log("Sending message with id : " + JSON.stringify(outgoingMessage));
    var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
    send(packet);
}

Chat.sendNotificationMessage = function(destinationperson, message, text) {
    var to = destinationperson.devicetoken;
    if(to == null || to == "" || to == undefined)
        return ;
    console.log("to = "+to+" and message = "+message);
    
    var badgeCount = 0;
    var unreadArray = destinationperson.unreadArray;
    console.log('unreadArray = '+JSON.stringify(unreadArray));
    if(unreadArray != undefined && unreadArray != null){
    	if(message.type == 1||message.type == 3||message.type == 5||message.type == 6){
	       	 badgeCount = unreadArray.length;
        	if (unreadArray.indexOf('relationship') == -1) {
    	        setUnreadCount(unreadArray, 'relationship', destinationperson.fid);
        	    badgeCount = badgeCount + 1;
        	}
        } 
    }else{
    	 setUnreadCount(unreadArray, 'relationship', destinationperson.fid);
         badgeCount = badgeCount + 1;        
    }
	    badgeCount = badgeCount +"";

    var outgoingMessage = {
        message_id: uuid.v4(),
        to : to,
		content_available: true,
        /*notification : {
            title : "officeG",
            text : text
        }*/
    };
        if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
            outgoingMessage.data = {type:MessageType.NOTIFICATION, msg: message, text: text, title : "officeG", badge: badgeCount};
        } else {
            outgoingMessage.notification = {type:MessageType.NOTIFICATION, msg: message, text: text, title : "officeG", badge: badgeCount};
            outgoingMessage.data = {type:MessageType.NOTIFICATION, msg: message, text: text, title : "officeG", badge: badgeCount};
        }

    console.log("Sending message with id : " + outgoingMessage.message_id);
    var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
    send(packet);

}

Chat.changeGroupName = async function(request, response) {
    try {
        var data = request.body;
        
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group{gid:{gid}}) SET group.name = {name}, group.profileimage = {url} RETURN person", data);
               
        response.json({iserror:false, errorcode:0, result:{status:result.records.length != 0}});
    }
    catch(e) {
        console.log("Error in changing group details : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.backup = async function(request, response) {
    try {
        var data = request.body;
        data.time = new Date().getTime();

        var result = await db.execute("MATCH(person:Person{key:{secret_key}}) CREATE(backup:Backup{time:{time}, url:{url}}) CREATE (person)-[:BACKUP]->(backup) RETURN backup", data);
               
        response.json({iserror:false, errorcode:0, result:{status:result.records.length != 0}});
    }
    catch(e) {
        console.log("Error in storing backup information : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.backupList = async function(request, response) {
    try {
        var data = request.body;

        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[:BACKUP]->(backup) RETURN backup", data);
        var backupList = [];

        for(var index = 0; index < result.records.length; index++ ) {
            var record = result.records[index];
            var backup = record._fields[0].properties;
            backupList.push({time:backup.time, url:backup.url });
        }

        response.json({iserror:false, errorcode:0, result:backupList});               
    }
    catch(e) {
        console.log("Error in getting chat backup list : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}


Chat.deleteGroup = async function(request, response) {
    try {
        var data = request.body;
        
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[:IS_MEMBER_OF]->(group:Group{gid:{gid}}) WHERE exists(group.admins) AND (person.fid in (group.admins)) DETACH DELETE group RETURN person", data);
               
        response.json({iserror:false, errorcode:0, result:{status:result.records.length != 0}});
    }
    catch(e) {
        console.log("Error in removing member from group : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.getUnreadMessages = async function(request, response) {
    try {
        var data = request.body;
        console.log("getUnreadMessages request : " + JSON.stringify(data));
        var result = await db.execute("MATCH(m:Message)-[SENT_TO]->(p:Person{key:{secret_key}}) WHERE m.time > {start_time} RETURN m", {start_time:parseFloat(data.start_time), secret_key:data.secret_key});
        var unreadMessageListByKey = {}, unreadMessageTotalList={};
        if(result.records.length != 0) {

        for(var index = 0; index < result.records.length; index++ ) {
        	var message = result.records[index]._fields[0].properties;
        	//unreadMessageList.push({mid:message.mid, content:message.content, images:message.images, audios:message.audios, videos:message.videos, extraType:message.extraType, extraString:message.extraString, time:message.time});

        	if(message["gid"] != undefined){
        		if(unreadMessageTotalList[message.gid]==undefined){

        			unreadMessageListByKey = {};

        			unreadMessageListByKey["fid"] = "";
        			unreadMessageListByKey["gid"] = message.gid;
        			unreadMessageListByKey["type"] = MessageType.GROUP_CHAT+"";

        			var unreadMessageArray = [];
        			var groupextramessageArray = [];
					if(message.extraType == "groupextramessage") {
	        			groupextramessageArray.push(message);					
					} else {
	        			unreadMessageArray.push(message);										
					}
        			unreadMessageListByKey["messages"] = unreadMessageArray;
        			unreadMessageListByKey["groupextramessages"] = groupextramessageArray;
        			unreadMessageTotalList[message.gid] = unreadMessageListByKey;
        		}
        		else{
        			if(message.extraType == "groupextramessage") {
		        		unreadMessageTotalList[message.gid]["groupextramessages"].push(message);
					} else {
		        		unreadMessageTotalList[message.gid]["messages"].push(message);
					}
        		}
        	}else{
        		if(unreadMessageTotalList[message.fid]==undefined){

        			unreadMessageListByKey = {};

        			unreadMessageListByKey["fid"] = message.fid;
        			unreadMessageListByKey["gid"] = "";
        			unreadMessageListByKey["type"] = MessageType.SINGLE_CHAT+"";

        			var unreadMessageArray = [];
        			unreadMessageArray.push(message);

        			unreadMessageListByKey["messages"] = unreadMessageArray;
        			unreadMessageTotalList[message.fid] = unreadMessageListByKey;

        		}
        		else{
	        		unreadMessageTotalList[message.fid]["messages"].push(message);
        		}
        	}
            }
        }
        console.log("inside : " + JSON.stringify(unreadMessageTotalList));
        response.json({iserror:false, errorcode:0, result:unreadMessageTotalList});
    }
    catch(e) {
        console.log("Error in getting getUnreadMessages : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

Chat.getFavouriteListAndGroups = async function(request, response) { 
    try {
		var favouriteListAndGroups = {};



    try {
        var key=request.body.secret_key;
        var favouriteList=[];
        var directRelationResult = await db.execute("MATCH (a:Person{key: {key}})-[rs:IS_RELATED_TO]->(b) WHERE rs.status = 'approved' AND NOT(a)-[:BLOCKED]-(b) RETURN b, rs", {key:key});

        if (!_.isEmpty(directRelationResult.records)){
            try {
            	console.log('directRelationResult = ' + JSON.stringify(directRelationResult));
                directRelationResult.records.forEach(function (record) {
                    var favourite=Crypt.decryptPerson(record.get(0).properties);
                    var relationship = record.get(1).properties;
                    favouriteList.push({fid:favourite.fid, firstname:favourite.firstname, lastname:favourite.lastname, displayname:Crypt.decryptField(relationship.displayname), profileimage:favourite.profileimage, uid:relationship.uid, mobilenumber:favourite.mobilenumber});
                });
           
            }
            catch (E) { }
        }   

        var result = await db.execute("MATCH (a:Person{key: {key}})-[rs:IS_FAVOURITE_TO]->(b) WHERE rs.status = 'approved' AND NOT (a)-[:BLOCKED]-(b) RETURN b, rs", {key:key});
        if (!_.isEmpty(result.records)){
            try {
                result.records.forEach(function (record) {
                    var favourite=Crypt.decryptPerson(record.get(0).properties);
                    var relationship = record.get(1).properties;
                    favouriteList.push({fid:favourite.fid, firstname:favourite.firstname, lastname:favourite.lastname, displayname:favourite.displayname, profileimage:favourite.profileimage, uid:relationship.uid,mobilenumber:favourite.mobilenumber});
                });
            }
            catch (E) { }
        }
        		favouriteListAndGroups.favouriteList = favouriteList;
    }
    catch(e) {
        console.log("Failed to retrieve fav list " + e);
    }




    try {
        var data = request.body;
        var result = await db.execute("MATCH(person:Person{key:{secret_key}})-[IS_MEMBER_OF]->(group:Group) RETURN group", data);
        var groupList = [];
        if(result.records.length != 0) {
        
        for(var index = 0; index < result.records.length; index++ ) {
        	var group = result.records[index]._fields[0].properties;
                
        var membersArray = [];
        for(var indexm =0; indexm < group.members.length; indexm++) {
            try {
                var memberResult = await db.execute("MATCH (person:Person{fid:{fid}}) RETURN person", { fid:group.members[indexm]});
                var person = memberResult.records[0]._fields[0].properties;
                person = Crypt.decryptPerson(person);

                personprofile={fid:person.fid, profileimage:person.profileimage, displayname:person.displayname};
				membersArray.push(personprofile);
            }
            catch(e) {
                console.log("Error : " + e);
                console.log("Failed to get group member: " + group.members[indexm]);
            }
        }
     
        groupList.push({gid:group.gid, name : group.name, admins:group.admins, members:membersArray, url:group.profileimage});
            }
        }
		favouriteListAndGroups.groups = groupList;
    }
    catch(e) {
        console.log("Error in getting group list : " + e);
    }

        response.json({iserror:false, errorcode:0, result:favouriteListAndGroups});
    }
    catch(e) {
        console.log("Error in getFavouriteListAndGroups : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

Chat.addReminder = async function(request, response) {
    try {
        var data = request.body;
        data.rid = uuid.v4();

        var result = await db.execute("CREATE(reminder:Reminder {rid:{rid}, time:{time}, message_id:{message_id}}) RETURN reminder", data);
        result = await db.execute("MATCH(person:Person{key:{secret_key}}), (reminder:Reminder{rid:{rid}}) CREATE (person)-[:CREATES]->(reminder) return person", data);
        var person = result.records[0]._fields[0].properties;

        if(data.remind_to != undefined) {
            for(var index = 0; index < data.remind_to.length; index++) {
                await db.execute("MATCH(person:Person{fid:{fid}}), (reminder:Reminder{rid:{rid}}) WHERE NOT (reminder)-[:REMIND_TO]->(person) WITH person, reminder CREATE UNIQUE (reminder)-[:REMIND_TO]->(person)", {fid:data.remind_to[index], rid:data.rid});
            }
        }
        
		createCron(data, person.fid);
        response.json({iserror:false, errorcode:0, result:{rid:data.rid}});
    }
    catch(e) {
        console.log("Error in adding reminder : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_ADD_REMINDER));
    }
}

async function createCron(data, reminding_fid) {
	var time = data.time;
    console.log("createCron " + JSON.stringify(data));
  	var schedule = require('node-schedule');
	var date = new Date(time);
	console.log('time = '+date);
	var j = schedule.scheduleJob(date, function(){
	  console.log('job is running');
        if(data.remind_to != undefined) {
            for(var index = 0; index < data.remind_to.length; index++) {
				sendReminderPN(data, data.remind_to[index], reminding_fid);
            }
        }
	});
}

async function sendReminderPN(data, fid, reminding_fid) {
    console.log("sendReminderPN fid " + fid);
    try {
    
    var memberResult = await db.execute("MATCH (person:Person{fid:{fid}}) RETURN person", { fid:fid});
	var destinationperson = memberResult.records[0]._fields[0].properties;

	var badgeCount = 0;
	var unreadArray = destinationperson.unreadArray;
	console.log('unreadArray = '+JSON.stringify(unreadArray));
	if(unreadArray != undefined && unreadArray != null){
		badgeCount = unreadArray.length;
	    if (unreadArray.indexOf('reminder') == -1) {
			setUnreadCount(unreadArray, 'reminder', destinationperson.fid);
			badgeCount = badgeCount + 1;
		} 
	} else{
		setUnreadCount(unreadArray, 'reminder', destinationperson.fid);
		badgeCount = badgeCount + 1;
	}
	badgeCount = badgeCount +"";

    var to = destinationperson.devicetoken;
    if(to == null || to == "" || to == undefined)
        return ;
	var message_id = uuid.v4();
    var outgoingMessage = {
        type : 4,
        message_id: message_id,
        to : to,
        content_available : true
    };
    if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
        outgoingMessage.data = {type:MessageType.REMINDER, reminder_mid:data.message_id, text: "You have reminder message(s)", title : "officeG", badge: badgeCount, message_id: message_id, reminding_fid:reminding_fid};
    } else {
        outgoingMessage.notification = {type:MessageType.REMINDER, reminder_mid:data.message_id, text: "You have reminder message(s)", title : "officeG", badge: badgeCount, message_id: message_id, reminding_fid:reminding_fid};
        // outgoingMessage.data = {type:MessageType.REMINDER, reminder_mid:data.message_id, text: "You have reminder message(s)", title : "officeG", badge: badgeCount, message_id: message_id};
    }

    console.log("Sending message with id : " + JSON.stringify(outgoingMessage));
    var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
    send(packet);
    }
    catch(e) {console.log("Single chat error : " + e);}
}

async function sendAcknowledgement(messageData, mode) {
    console.log("sendAcknowledgement For " + mode + " = " + JSON.stringify(messageData) + " and mode = " + mode + " and messageData.fid = " + messageData.fid);
    try {
        var outgoingMessage = {};
        var result;
        if(mode == "read") {
            result = await db.execute("MATCH (source:Person {fid:{fid}}) RETURN source", {fid:messageData.fid});
	    } else {
	        result = await db.execute("MATCH (source:Person)-[IS_SENT]->(message:Message {mid:{mid}}) RETURN source", {mid:messageData.message_id});
		}
        if (!_.isEmpty(result.records)) {
            destinationperson = result.records[0].get(0).properties;
            
            if( destinationperson.devicetoken == null || destinationperson.devicetoken == '' || destinationperson.devicetoken == undefined)
                return;
            outgoingMessage.to = destinationperson.devicetoken;
            outgoingMessage.message_id = uuid.v4();
			outgoingMessage.content_available = true;

	        if(mode == "read") {
            	if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
            	    outgoingMessage.data = {type:MessageType.ACKNOWLEDGEMENT, message_id:messageData.message_id, openerFid:messageData.openerFid, ackmode:mode};
        	    }else{
    	            outgoingMessage.notification = {type:MessageType.ACKNOWLEDGEMENT, message_id:messageData.message_id, openerFid:messageData.openerFid, ackmode:mode};
	            }
            } else {
            	if(destinationperson.platform == undefined || destinationperson.platform == '' || destinationperson.platform == null || destinationperson.platform == 'android'){
            	    outgoingMessage.data = {type:MessageType.ACKNOWLEDGEMENT, message_id:messageData.message_id, ackmode:mode};
        	    }else{
    	            outgoingMessage.notification = {type:MessageType.ACKNOWLEDGEMENT, message_id:messageData.message_id, ackmode:mode};
	            }
            }
            console.log("sendAcknowledgement For " + mode + " = " + JSON.stringify(outgoingMessage));
            var packet = new xmpp.Element('message').c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(outgoingMessage));
            send(packet)
		}
    }
    catch(e) {console.log("Single chat error : " + e);}
}