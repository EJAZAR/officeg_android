﻿var Config = new require("../config");
var crypto = require('crypto');
var uuid = require('uuid');
var _ = require('underscore');
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var db = new require('../helpers/db');
var MessageService = new require('../helpers/message');
var chatManager = require('../controllers/chatController');
var Crypt = require("../helpers/crypt");

var post = exports;
post.createPost = async function(request,response) {
    try {
        var postData=request.body;
        var key=postData.secret_key;
        var content=postData.content;
        var imageurls=postData.imageurls;
        var videourls=postData.videourls;
        var postid=uuid.v4();
        await db.execute("Create(post:Post{postid:{postid},content:{content},images:{images},videos:{videos},time:{time}}) return post", {postid:postid,content:content,images:imageurls,videos:videourls,time:new Date().getTime()});

        var result=await db.execute("Match (a:Person{key:{key}}),(b:Post{postid:{postid}}) create (a)-[r:IS_POSTED{time:{time}}]->(b) return r, a, b",{key:key,time:new Date().getTime(), postid:postid});
        var senderFid = result.records[0]._fields[1].properties.fid;
        var doi = result.records[0]._fields[1].properties.doi;
        if(doi == "" || doi == null || doi == undefined)
            doi = 3;

        // If the doi of interest is one then the current user will not selected as part of depth query so need to assign post explicitly
        if(parseInt(doi) == 1) {
            var res = await db.execute("MATCH (p:Post{postid:{postid}}), (a:Person{key:{key}}) create unique (p)-[rp:POSTED_TO]->(a) return a", {key:key, postid:postid});
            if(!_.isEmpty(res.records)) {
                res.records.forEach(function(result) {
                    var destPerson=result.get(0).properties;
                    console.log("doi = " + doi + " and destPerson = " + destPerson.fid);
                    if(senderFid != destPerson.fid){
                        chatManager.sendPostMessage(destPerson, {postid: postid , content:content,images:imageurls,videos:videourls, time:new Date().getTime(), likes:0, comments:[] , displayname:Crypt.decryptField(destPerson.displayname), profileimage:Crypt.decryptField(destPerson.profileimage), gender:Crypt.decryptField(destPerson.gender), fid:destPerson.fid, isfav:true, likingpersons:[], isfirst: true});
                    }
                });
            }
        }

        var res = await db.execute("MATCH (p:Post{postid:{postid}}),(a:Person{key:{key}})-[rels:IS_RELATED_TO*1.."+ doi + "]->(b:Person{status:'activated'}) WHERE ALL (rel in rels WHERE rel.status='approved') AND NOT (a)<-[:BLOCKED]-(b) AND NOT (a)<-[:IS_FAVOURITE_TO]-(b)  OPTIONAL MATCH (a)-[rf:IS_RELATED_TO]->(b) WHERE NOT (a)<-[:BLOCKED]-(b) AND NOT (a)-[:IS_FAVOURITE_TO]->(b) create unique (p)-[rp:POSTED_TO]->(b) return distinct a,  b, p, case WHEN (rf IS NOT NULL AND rf.status = 'approved') THEN true else false end as isfav, LENGTH(rels) as doi,  case WHEN (rf IS NOT NULL AND rf.status = 'approved') THEN true else false end as isfirst",{key:key,postid:postid,time:new Date().getTime()});
        var send_fids = [];
        if(!_.isEmpty(res.records)) {
            res.records.forEach(function(result) {
            	console.log("ios result : "+JSON.stringify(result));

                var srcPerson=result.get(0).properties;
                var post=result.get(2).properties;
                var destPerson=result.get(1).properties;
                var doi = parseInt(db.int(result.get(4)).toString());
                var dp_doi = destPerson.doi;
                if(dp_doi == "" || dp_doi == undefined || dp_doi == null)
                    dp_doi = 3;
                else
                    dp_doi = parseInt(dp_doi);

                console.log("FG id : " + destPerson.fid + "=> Doi to person : " + doi + ", doi of dest : " + dp_doi);
                if(doi <= dp_doi ) {
                    if(send_fids.indexOf(destPerson.fid) == -1) {
                        send_fids.push(destPerson.fid);
                        console.log("Sending message to : "+ destPerson.key + ", " + destPerson.displayname);
                        if(senderFid != destPerson.fid){
                        	if(destPerson.key != undefined && destPerson.key != '' && destPerson.key != null)
	                            chatManager.sendPostMessage(destPerson, {postid: post.postid , content:post.content,images:post.images,videos:post.videos, time:post.time, likes:0, comments:[] , displayname:Crypt.decryptField(srcPerson.displayname), profileimage:Crypt.decryptField(srcPerson.profileimage), gender:Crypt.decryptField(srcPerson.gender), fid:srcPerson.fid, isfav:result.get(3), likebyme:false, likingpersons:[], isfirst:result.get(5)});
                        }
                    }
                }
                else {
                        	if(destPerson.key != undefined && destPerson.key != '' && destPerson.key != null)
			                    db.execute("MATCH (p:Post{postid:{postid}})-[r:POSTED_TO]->(a:Person{key:{key}}) delete r",{key:destPerson.key,postid:postid});
                }
            });
        }

        var records = await db.execute("MATCH (p:Post{postid:{postid}}),(a:Person{key:{key}})-[rel:IS_FAVOURITE_TO]->(b:Person{status:'activated'})  WHERE rel.status = 'approved' AND NOT(a)<-[:BLOCKED]-(b) create unique (p)-[r:POSTED_TO]->(b) return a,b,p",{key:key,postid:postid});

        if(records.records.length != 0) {
            console.log(JSON.stringify(records));
            try{
                //var resFav = await db.execute("MATCH (p:Post{postid:{postid}}),(a:Person{key:{key}})-[rel:IS_FAVOURITE_TO]->(b:Person{status:'activated'}) WHERE rel.status='approved') return a,b,p",{key:key,postid:postid});

                records.records.forEach(function(result) {
                    var srcPerson=result.get(0).properties;
                    var post=result.get(2).properties;
                    var destPerson=result.get(1).properties;
                    //db.execute("MATCH(post:Post{postid:{postid}}), (p:Person{fid:{fid}}), (src:Person{key:{key}}) WHERE NOT (src)-[:BLOCKED]-(p) create unique (post)-[r:POSTED_TO]->(p)", {fid:destPerson.fid, postid:postid});
                    console.log("Sending message to fav : "+ destPerson.key + ", " + destPerson.displayname);
                    if(senderFid != destPerson.fid){
                        	if(destPerson.key != undefined && destPerson.key != '' && destPerson.key != null)
		                        chatManager.sendPostMessage(destPerson, {postid: post.postid , content:post.content,images:post.images,videos:post.videos, time:post.time, likes:0, comments:[] , displayname:Crypt.decryptField(srcPerson.displayname), profileimage:Crypt.decryptField(srcPerson.profileimage), gender:Crypt.decryptField(srcPerson.gender), fid:srcPerson.fid, isfav:true, likebyme:false, likingpersons:[]});
                    }
                }); 
            }
            catch(e){
                console.log("Failed to posting to favourite : " + e);
            }
        }

        console.log("Posted to all users.");
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{staus:true} }); 
    }
    catch(e){
        console.log("Failed in creating post : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_CREATE_POST));
    }
}

post.getPostList = async function(request,response){
    try {
        console.log("getPostList");
        var key=request.query.secret_key;

        request.body.secret_key = key;
        request.body.fidOrGid = "feed";
        await chatManager.clearBadgeCount(request, response);

        var postList=[];
        var d = new Date();
        d.setDate(d.getDate()-3);
        var start_time = d.getTime();
        console.log("getPostList "+start_time);

        var result=await db.execute("Match (a:Person{key:{key}})<-[rel:POSTED_TO]-(b:Post)-[ps:IS_POSTED]-(src:Person) OPTIONAL MATCH (a)-[r:IS_FAVOURITE_TO]->(src) OPTIONAL MATCH (a)-[re:IS_RELATED_TO]-(src) WHERE b.time >= {start_time} return  distinct  b, src, case WHEN (r IS NOT NULL AND r.status = 'approved') THEN true else false end as isfav, case WHEN (re IS NOT NULL AND re.status = 'approved') THEN true else false end as isfirst ORDER BY b.time desc",{key:key, start_time:start_time});
        if(!_.isEmpty(result.records)){
		
            try{
                for(var index = 0; index < result.records.length; index++ ) {
                    var record = result.records[index];
                    var post=record._fields[0].properties;
                    var comments = [], likingpersons = [];
                    var likeByMe=await db.execute("Match (a:Person{key:{key}})-[rel:LIKED]->(b:Post{postid:{postid}}) return distinct a",{key:key, postid:post.postid});
                    var likeResult=await db.execute("Match (a:Person)-[rel:LIKED]->(b:Post{postid:{postid}}) return distinct a,rel",{key:key, postid:post.postid});

                    var commentResult=await db.execute("Match (a:Person)-[ric:IS_COMMENTED]->(c:Comment)-[rct:COMMENTED_TO]->(b:Post{postid:{postid}}) return distinct a,c",{ postid:post.postid});

                
                    for(var cindex = 0; cindex < commentResult.records.length; cindex++) {
                        try {
                            var record = commentResult.records[cindex];
                            var person = record._fields[0].properties;
                            var comment = record._fields[1].properties;
                            comment.replycount=await replyCount(comment.cid);
                            comments.push({ fid:person.fid, displayname: Crypt.decryptField(person.displayname), time:comment.time, comment:comment, profileimage: Crypt.decryptField(person.profileimage)});
                        }
                        catch(e) {console.log("Error in comments " + e);}
                    }
                    
					for(var lindex = 0; lindex < likeResult.records.length; lindex++) {
                        try {
                            var record = likeResult.records[lindex];
                            var person = record._fields[0].properties;
                            var like = record._fields[1].properties;
                            likingpersons.push({ fid:person.fid, displayname: Crypt.decryptField(person.displayname), time:like.time, profileimage: Crypt.decryptField(person.profileimage)});
                        }
                        catch(e) {console.log("Error in comments " + e);}
                    }
                    
//                    console.log("comments "+JSON.stringify(comments));
                    var sourcePerson = result.records[index]._fields[1].properties;
                    //console.log(JSON.stringify(sourcePerson));
                    
                    postList.push({postid: post.postid , content:post.content,images:post.images,videos:post.videos, time:post.time, likebyme:likeByMe.records.length != 0, likes:likeResult.records.length, comments:comments , displayname:Crypt.decryptField(sourcePerson.displayname), profileimage:Crypt.decryptField(sourcePerson.profileimage), gender:Crypt.decryptField(sourcePerson.gender), fid:sourcePerson.fid, isfav: result.records[index]._fields[2], isfirst: result.records[index]._fields[3], likingpersons:likingpersons});
                }
                postList.sort(function(a,b) { return b.time - a.time;});
            }
            catch(e){console.log("Error : " + e);}
        }

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: postList });
    }
    catch(e) {
        console.log("Failed in get post list : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_POSTS));
    }
}

post.addLike = async function(request,response){
    try {
        var key=request.body.secret_key;
        var postid = request.body.postid;
        var result = await db.execute("MATCH(source:Person{key:{key}}), (p:Post{postid:{postid}})  WHERE NOT (source)-[:LIKED]-(p) WITH source, p CREATE UNIQUE (source)-[r:LIKED{time:{time}}]->(p) RETURN source", {key:key, postid: postid, time:new Date().getTime()});

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status: !_.isEmpty(result.records)} });
    }
    catch(e) {
        console.log("Failed in adding like to post : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_ADD_LIKE));
    }
}

post.removeLike = async function(request,response){
    try {
        var key=request.body.secret_key;
        var postid = request.body.postid;
        var result = await db.execute("MATCH(source:Person{key:{key}})-[r:LIKED]->(p:Post{postid:{postid}}) detach delete r return r", {key:key, postid: postid});
        if(result.records.length == 0){
        	response.json(new ERROR(ErrorCode.FAILED_TO_REMOVE_LIKE));
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status: !_.isEmpty(result.records)} });
    }
    catch(e) {
        console.log("Failed in removing added like to post : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_REMOVE_LIKE));
    }
}

post.addComment = async function(request,response){
    try {
        var postData = request.body;
        var key=postData.secret_key;
        var postid=postData.postid;
        var content=postData.content;
        var imageurls=postData.imageurls;
        var videourls=postData.videourls;
        var commentid=postData.cid;
        var cid=uuid.v4();
        await db.execute("Create(c:Comment{cid:{cid},content:{content},images:{images},videos:{videos},time:{time}}) return c", {cid:cid,content:content,images:imageurls,videos:videourls,time:new Date().getTime()});

        if(commentid==undefined||commentid==null||commentid.trim()==""){
	        var result=await db.execute("Match(p:Person{key:{key}}),(c:Comment{cid:{cid}}) create (p)-[r:IS_COMMENTED]->(c) return r, p, c",{key:key,time:new Date().getTime(), cid:cid});
    	    result=await db.execute("Match(c:Comment{cid:{cid}}),(p:Post{postid:{postid}}) create (c)-[r:COMMENTED_TO]->(p) return r, c, p",{key:key,time:new Date().getTime(), postid:postid, cid:cid});
        }else{
	        var result=await db.execute("Match(p:Person{key:{key}}),(c:Comment{cid:{cid}}) create (p)-[r:IS_REPLIED]->(c) return r, p, c",{key:key,time:new Date().getTime(), cid:cid});
    	    result=await db.execute("Match(c:Comment{cid:{cid}}),(p:Comment{cid:{commentid}}) create (c)-[r:REPLIED_TO]->(p) return r, c, p",{key:key,time:new Date().getTime(), commentid:commentid, cid:cid});
        }
        console.log("comment added");
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{cid:cid} });
    }
    catch(e) {
        console.log("Failed in adding comment to post : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_ADD_COMMENT));
    }
}

post.listOfLiked = async function(request,response){
    try {
        var postid=request.query.postid;
        console.log("listOfLiked : postid = "+postid);
		var postList=[];
            try{
                    var likeResult=await db.execute("Match (a:Person)-[rel:LIKED]->(b:Post{postid:{postid}}) return a ORDER BY rel.time",{postid:postid});
                    for(var lindex = 0; lindex < likeResult.records.length; lindex++) {
                        try {
                        	var person=likeResult.records[lindex]._fields[0].properties;
							postList.push({fid:person.fid, name:Crypt.decryptField(person.displayname),profileimage:Crypt.decryptField(person.profileimage)});
                        }
                        catch(e) {console.log("Error in lists " + e);}
                    }
                    console.log(JSON.stringify(postList));
            }
            catch(e){
            	console.log("Error : " + e);
            }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: postList });
    }
    catch(e) {
        console.log("Failed in get post list : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_POSTS));
    }
}

async function replyCount(cid) {
//    console.log("replyCount : "+cid);
    var replyResult=await db.execute("Match (c:Comment)-[rrt:REPLIED_TO]->(b:Comment{cid:{cid}}) return c",{cid:cid});
//    console.log("replyCount : "+replyResult.records.length);
//    console.log("replyCount : "+JSON.stringify(replyResult));
    return replyResult.records.length;
}

post.getReplies = async function(request,response){
    try {
        console.log("getReplies ");
        var cid=request.query.cid;

        var repliedResult=await db.execute("Match (a:Person)-[ric:IS_REPLIED]->(b:Comment)-[rct:REPLIED_TO]->(c:Comment{cid:{cid}}) return distinct a,b",{ cid:cid});
        var comments = [];

        for(var cindex = 0; cindex < repliedResult.records.length; cindex++) {
            try {
                var record = repliedResult.records[cindex];
                var person = record._fields[0].properties;
                var comment = record._fields[1].properties;
                comment.replycount=await replyCount(comment.cid);;
                comments.push({ fid:person.fid, displayname: Crypt.decryptField(person.displayname), time:comment.time, comment:comment, profileimage:Crypt.decryptField(person.profileimage)});
            }
            catch(e) {console.log("Error in comments " + e);}
        }
        console.log("comments "+JSON.stringify(comments));
        comments.sort(function(a,b) { return b.time - a.time;});

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: comments });
    }
    catch(e) {
        console.log("Failed in get post list : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_POSTS));
    }
}