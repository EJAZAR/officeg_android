﻿var Config = new require("../config");
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var uuid = require('uuid');
var db = new require('../helpers/db');
var MessageService = new require('../helpers/message');
var chatManager = require('../controllers/chatController');
var utils = require("../helpers/utils");
var Crypt = require("../helpers/crypt");

var nearby = exports;

nearby.updatelocation = async function(request, response) {
    try {
        var input = request.body;
        input.lat = parseFloat(input.lat);
        input.lon = parseFloat(input.lon);
        input.lastseen = new Date().getTime();
        await db.execute("MATCH (a:Person{key:{secret_key}}) SET a.latitude = {lat}, a.longitude = {lon}, a.lastseen = {lastseen} WITH a OPTIONAL MATCH(a:Person{key:{secret_key}})<-[r:RTREE_REFERENCE]-(b) WHERE r IS NULL CALL spatial.addNode('geom', a) YIELD node RETURN node", input);
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
    }
    catch(error) {
        response.json(new ERROR(ErrorCode.FAILED_TO_UPDATE_PROFILE));
    }
}

nearby.nearbyUsers = async function(request, response) {
    try {
        var input = request.body;
        var maxDistance = 10; // km
        input.lat = parseFloat(input.lat);
        input.lon = parseFloat(input.lon);
        input.maxdis = maxDistance;
        var people = [];
        if(input.doi == "" || input.doi == null || input.doi == undefined)
            input.doi = 3;
        var relations = await db.execute("CALL spatial.withinDistance('geom', {latitude: {lat}, longitude: {lon}}, {maxdis}) YIELD node AS d WITH d MATCH (d)-[r:IS_RELATED_TO*1.." + input.doi +"]->(a:Person{key:{secret_key}}) WHERE  ALL (rel in r WHERE rel.status='approved')  RETURN distinct d, LENGTH(r) LIMIT 250", input);

        for(var index = 0; index < relations.records.length; index++ ) {
            var person = Crypt.decryptPerson(relations.records[index]._fields[0].properties);

            var position = people.find(function(element){ return element.fid == person.fid;});
            if(position != undefined) {
                if(!position.isfav) {
                    position.isfav = db.int(relations.records[index]._fields[1]) == 1 ? true : false
                }
            }
            else {
                people.push({fid:person.fid,firstname:person.firstname,lastname:person.lastname,displayname:person.displayname, profileimage:person.profileimage, gender:person.gender, isfav: db.int(relations.records[index]._fields[1]) == 1 ? true : false, lat:person.latitude, lon:person.longitude, time:person.lastseen  });
            }
           
        }

        var favourites = await db.execute("CALL spatial.withinDistance('geom', {latitude: {lat}, longitude: {lon}}, {maxdis}) YIELD node AS d WITH d MATCH (d)-[r:IS_FAVOURITE_TO]->(a:Person{key:{secret_key}})  WHERE r.status='approved'  RETURN distinct d LIMIT 250", input);

        for(var index = 0; index < favourites.records.length; index++ ) {
            var person = Crypt.decryptPerson(favourites.records[index]._fields[0].properties);
            var position = people.find(function(element){ return element.fid == person.fid;});
            if(position != undefined) {
                position.isfav = true;
            }
            else {
                people.push({fid:person.fid,firstname:person.firstname,lastname:person.lastname,displayname:person.displayname, profileimage:person.profileimage, gender:person.gender, isfav:true, lat:person.latitude, lon:person.longitude, time:person.lastseen });
            }
        }

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: people });
    }
    catch(error) {
        console.log("Failed to get near by users " + error);
        response.json(new ERROR(ErrorCode.FAILED_TO_GET_GROUP_LIST));
    }
}

async function createUser(user) {
    user.data = Crypt.encryptPerson(user.data);
    console.log("createUser 1 : " + JSON.stringify(user));
    if(user.data.mobilenumber == undefined)
    	user.data.mobilenumber = "";
    var result = await db.execute("CREATE(person:Person {fid:{fid}, countrycode:{cc}, created_date:{ts}, updated_date:{ts}, mobilenumber:{number}, status: {status}, displayname:{displayname}, firstname:{firstname}, lastname:{lastname}, dob:{dob}, isalive:{isalive}, email:{email}, tile:{tile}, team:{team}, location:{location}, isadmin:{isadmin}}) RETURN person",
    {
        status:user.data.status,
        ts: new Date().getTime(),
        fid: user.fid,
        officenotification:true,
        number: user.data.mobilenumber,
        cc: user.data.countrycode,
        displayname:user.data.displayname,
        firstname:user.data.firstname,
        lastname:user.data.lastname,
        dob:user.data.dob,
        isalive:true,
        email:user.data.email,
        tile:user.data.tile,
        team:user.data.team,
        location:user.data.location,
        dob:user.data.dob,
        isadmin:user.data.isadmin
    });
    console.log("createUser 2 : " + JSON.stringify(result));
    return result;
}

async function updateUser(user) {
    user.data = Crypt.encryptPerson(user.data);
    console.log("updateUser 1 : " + JSON.stringify(user));
    if(user.data.mobilenumber == undefined)
    	user.data.mobilenumber = "";
    var result = await db.execute("MERGE(person:Person{fid:{fid}}) ON MATCH SET person.countrycode={cc}, person.updated_date={ts}, person.mobilenumber={number}, person.status= {status}, person.displayname={displayname}, person.firstname={firstname}, person.lastname={lastname}, person.dob={dob}, person.isalive={isalive}, person.email={email}, person.tile={tile}, person.team={team}, person.location={location}, person.isadmin={isadmin} RETURN person",
    {
        status:user.data.status,
        ts: new Date().getTime(),
        fid: user.fid,
        officenotification:true,
        number: user.data.mobilenumber,
        cc: user.data.countrycode,
        displayname:user.data.displayname,
        firstname:user.data.firstname,
        lastname:user.data.lastname,
        dob:user.data.dob,
        isalive:true,
        email:user.data.email,
        tile:user.data.tile,
        team:user.data.team,
        location:user.data.location,
        dob:user.data.dob,
        isadmin:user.data.isadmin
    });
    console.log("updateUser 2 : " + JSON.stringify(result));
    return result;
}

nearby.deleteTestData = async function(request, response) {
    try{
        var xls = require('excel');
    	console.log('deleteTestData');
			var xlsx = require('node-xlsx');

			var familyExcel1 = xlsx.parse('officeg.xlsx');
			var familyExcel = familyExcel1[0].data;

			console.log('deleteTestData familyExcel = '+JSON.stringify(familyExcel));

            var relation = require('../controllers/relationshipController');
    		console.log('deleteTestData length = ' +familyExcel.length);

            for(var index = 1; index < familyExcel.length; index++) {
            	var fid = familyExcel[index][1]+"";
            	console.log('deleteTestData fid = ' +fid);
                if(familyExcel[index][1]==undefined||familyExcel[index][1]==''||familyExcel[index][1]==null){
                    break;
                }
                await db.execute("MATCH(src:Person{fid:{srcfid}}) DETACH DELETE src", {srcfid:fid});
            }
            response.write("All existing records are deleted.<br/>");
            //response.end();
            return;

    }
    catch(E) {
        console.log("deleteTestData exception = " + E);
        response.write(E);
        response.end();
    }
    //response.write("Failed");
    //response.end();
}

nearby.uploadTestData = async function(request, response) {
    try{
    	console.log('uploadTestData');
        // await nearby.deleteTestData(request, response);
        var fidArray = [];
        var xls = require('excel');

			var Excel = require('exceljs');
			var workbook = new Excel.Workbook();

			var xlsx = require('node-xlsx');

			var familyExcel1 = xlsx.parse('officeg.xlsx');
			var familyExcel = familyExcel1[0].data;

			console.log('uploadTestData familyExcel = '+JSON.stringify(familyExcel));

            var relation = require('../controllers/relationshipController');

    		console.log('uploadTestData length = ' +familyExcel.length);

            for(var index = 1; index < familyExcel.length; index++) {
                if((familyExcel[index][0]==undefined||familyExcel[index][0]==''||familyExcel[index][0]==null) && (familyExcel[index][2]==undefined||familyExcel[index][2]==''||familyExcel[index][2]==null) ){
	            	break;
                }
                var mobilenum = '';
				if(familyExcel[index][5] != undefined && familyExcel[index][5] != null && familyExcel[index][5] != ''){
					mobilenum = familyExcel[index][5] + '';
				}
				var isadmin = false;
				if(familyExcel[index][14] != undefined && familyExcel[index][14] != null && familyExcel[index][14] != '' && familyExcel[index][14] == 'yes'){
                	isadmin = true;
                }

                var user = {
                    data: {
                        mobilenumber:mobilenum,
                        countrycode:"91",
                        displayname:familyExcel[index][2],
                        firstname:familyExcel[index][3],
                        lastname:familyExcel[index][4],
                        dob:"",
	                    status:'activated',
	                    email:familyExcel[index][6],
	                    tile:familyExcel[index][7],
	                    team:familyExcel[index][8],
	                    location:familyExcel[index][9],
	                    dob:familyExcel[index][10],
	                    isadmin:isadmin
                    }
                };
                console.log("uploadTestData user : " + JSON.stringify(user));
                try {
                    if(familyExcel[index][1]==undefined||familyExcel[index][1]==''||familyExcel[index][1]==null){
                    	var fid = utils.createFid() + "";
		                console.log("uploadTestData fid " + familyExcel[index][1] + " and " + fid);
						user.fid = fid;
						await createUser(user);
                    } else {
                    	var fid = familyExcel[index][1] + "";
		                console.log("uploadTestData fid " + familyExcel[index][1] + " and " + fid);
						user.fid = fid;
						await updateUser(user);
                    }
	                familyExcel[index][1] = fid;
	                fidArray.push(fid);
                }
                catch(E){
                    console.log(E);
                    response.write(E);
                    response.end();
                    break;
                }
            }

workbook.xlsx.readFile('officeg.xlsx')
    .then(function() {
        var worksheet = workbook.getWorksheet(familyExcel1[0].name);

		for (var m = 0; m < fidArray.length; m++) {
			console.log(m+ " and " +fidArray[m]);
	        var row = worksheet.getRow(m+2);
	        row.getCell(2).value = fidArray[m];
        	row.commit();
		}

        return workbook.xlsx.writeFile('officeg.xlsx');
    });

            try {
                for(var index = 1; index < familyExcel.length; index++) {
                    console.log("uploadTestData relation fid = "+familyExcel[index][1]);
                    if(familyExcel[index][1]==undefined||familyExcel[index][1]==''||familyExcel[index][1]==null){
                        break;
                    }
                    for(var rindex = 11; rindex <= 13; rindex++) {
                        if(familyExcel[index][rindex] == undefined)
                            continue;
                    	// console.log("familyExcel[index][rindex] = "+familyExcel[index][rindex]);
                    	var relArray = familyExcel[index][rindex] + '';
                        var rel = relArray.split(',');
                        var reltype = familyExcel[0][rindex]+"";
                        reltype = reltype.toUpperCase()+"";

                        for(var relindex = 0; relindex < rel.length; relindex++) {
                            if(rel[relindex]==undefined||rel[relindex]==null||rel[relindex]=='')
                                continue;
                            console.log("uploadTestData relation 5 = "+familyExcel[index][1]+" and "+ familyExcel[db.int(rel[relindex])][1]+" and "+ reltype);
                            await db.execute("MATCH (src:Person{fid:{srcfid}}), (des:Person{fid:{desfid}}) OPTIONAL MATCH (src)-[r:IS_RELATED_TO]->(des) DELETE r WITH src,des MERGE (src)-[:IS_RELATED_TO{reltype:{reltype}, status:'approved', displayname:des.displayname}]->(des)", {srcfid:familyExcel[index][1]+"", desfid:familyExcel[db.int(rel[relindex])][1]+"", reltype:reltype+""});
                            console.log("Created relation for " + reltype + " of fid : " + familyExcel[index][1] + " and fid : " + familyExcel[db.int(rel[relindex])][1]);
                        }
                    }
                }
            }
            catch(E) {
                    console.log(E);
                    response.write(E);
                    response.end();
            }

            response.write("New records are inserted into database.");
            response.end();
//         });
    }
    catch(E){
                    console.log(E);
                    response.write(E);
                    response.end();
    }
}