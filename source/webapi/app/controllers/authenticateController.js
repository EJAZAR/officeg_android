﻿var Config = new require("../config");
var https = require('https');
var uuid = require('uuid');
var _ = require('underscore');
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var db = new require('../helpers/db');
var MessageService = new require('../helpers/message');
var utils = require("../helpers/utils");
var Crypt = require("../helpers/crypt");

const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(Config.DB, neo4j.auth.basic(Config.DB_USER, Config.DB_PASS));
const session = driver.session();

var ErrorCode = {
    SUCCESS: 0,
    NUMBER_ALREADY_EXISTS: 1,
    FAILED_TO_CREATE_USER: 2,
    FAILED_TO_SEND_SMS: 3,
    NUMBER_DOES_NOT_EXISTS: 4,
    NUMBER_ALREADY_ACTIVATED: 5,
    PASSCODE_EXPIRED: 6,
    INVALID_PASSCODE: 7
};

var Person = exports;

Person.fireBaseRegistration = async function(request, response) {
    try {
        var mobilenumber = request.query.mobilenumber;
        var countrycode = request.query.countrycode;
        var devicetoken = request.query.devicetoken;
        var platform = request.query.platform;
        var fid = request.query.fid;
		console.log('platform = ' + platform + ' mode = ' + request.query.mode);
        if(platform == undefined || platform == '')
            platform = 'ios';
        var key = uuid.v4();
	    var result;
        console.log( mobilenumber + ' and ' + fid + ' and ' +  key+ ' and ' +  devicetoken+ ' and ' +  countrycode);
        if(request.query.mode != undefined && request.query.mode == 'fid'){
	    	result = await db.execute("MATCH(person:Person) WHERE person.fid = {fid} RETURN person", { fid: fid });	    
	    } else {
	    	result = await db.execute("MATCH(person:Person) WHERE person.mobilenumber = {number} RETURN person", { number: mobilenumber });
	    }
        console.log('result = ' +  JSON.stringify(result));
        var isnew = true, person1;
        if (!_.isEmpty(result.records)) {
            person1 = result.records[0].get(0).properties;
            if (person1.status == "activated") {
                isnew = false;
            } 
        } else {
            if(request.query.mode != undefined && request.query.mode == 'userexist'){
            	response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {userexist:false} });
            	return;
            }
        }
        console.log('isnew = ' +  isnew);

        if(request.query.mode != undefined && request.query.mode == 'fid'){
        	result = await db.execute("MATCH(person:Person{fid: {fid}}) SET person.status = 'activated', person.key={key}, person.countrycode = {cc}, person.devicetoken={devicetoken}, person.isnew=false, person.updated_date={ud}, person.isowner=true, person.platform={platform}, person.mobilenumber={mobilenumber} RETURN person", { mobilenumber: mobilenumber, fid: fid, key : key, devicetoken:devicetoken, cc:countrycode,
            cd: new Date().getTime(),
            ud: new Date().getTime(),
            platform: platform });
            if (_.isEmpty(result.records)) {
        		console.log("FGID does not exist");
		        response.json(new ERROR(ErrorCode.FAILED_TO_RETRIEVE_PROFILE));
		        return;
            }
		} else {
        	result = await db.execute("MERGE(person:Person{mobilenumber: {number}}) ON MATCH SET person.status = 'activated', person.key={key}, person.countrycode = {cc}, person.devicetoken={devicetoken}, person.isnew=false, person.updated_date={ud}, person.isowner=true, person.platform={platform} ON CREATE SET person.status = 'activated', person.key={key}, person.countrycode = {cc}, person.devicetoken={devicetoken}, person.isnew=true, person.created_date={cd}, person.fid={fid}, person.isowner=true, person.platform={platform}, person.familynotification='true' RETURN person", { number: mobilenumber, key : key, devicetoken:devicetoken, cc:countrycode,fid: utils.createFid(),
            cd: new Date().getTime(),
            ud: new Date().getTime(),
            platform: platform });
		}
        try {
            var person = Crypt.decryptPerson(result.records[0]._fields[0].properties);
            var waitingnotifications = await Person.getConnectionRequests(key);
            console.log("fireBaseRegistration person.isnew = " + person.isnew);

            if(!person.isnew) {
                console.log("fireBaseRegistration Sending existing details..."+JSON.stringify(person));
                console.log("fireBaseRegistration isadmin = " + person1.isadmin);
                var resultJson = { secret_key:key,
                    fid:person.fid,
                    firstname:person.firstname,
                    lastname:person.lastname,
                    profileimage:person.profileimage,
                    doi:person.doi,
                    gender:person.gender,
                    dob:person.dob,
                    displayname:person.displayname,
                    isnew:isnew,
                    platform:person.platform,
                    familynotification:person.familynotification,
                    waitingrequests:waitingnotifications.length,
                    isadmin:person1.isadmin
                };
                console.log("fireBaseRegistration resultJson = " + resultJson);

                response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: resultJson});
            }
            else
                response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{ secret_key:key, fid:person.fid, isnew:true,
                    waitingrequests:waitingnotifications.length, familynotification:'true' } });
        }
        catch(e) {console.log(e);}
    }
    catch(e) {
        console.log("Failed to register user..." + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_CREATE_USER));
    }
}

Person.register = async function (request, response) {
    try {
        console.log("Checking the user presence for : " + request.query.mobilenumber);
        // Get the input values
        var mobilenumber = request.query.mobilenumber;
        var countrycode = request.query.countrycode;
        var devicetoken = request.query.devicetoken;

        // Check the mobile number already exists
        var result = await db.execute("MATCH(person:Person) WHERE person.mobilenumber = {number} RETURN person", { number: mobilenumber });
        var person = null;
        var isActivated = false;
        var isnew = true;
        if (!_.isEmpty(result.records)) {
            var person = result.records[0].get(0).properties;
            if (person.status == "activated") {
                isnew = false;
                console.log("Already number is registered.");
                //response.json(new ERROR(ErrorCode.NUMBER_ALREADY_EXISTS));
                //response.end();
                //return;
            }
        }

        var otp = utils.createOTP(); // generate OTP

        var reqref = utils.createRefCode(); // generate reference number
        console.log("RefCode : " + reqref + " and OTP : " + otp);



        await db.execute("MERGE(person:Person{mobilenumber: {number}}) ON MATCH SET person.status = 'not activated', person.otp = {otp}, person.timestamp = {ts}, person.mobilenumber = {number}, person.countrycode = {cc}, person.isowner = true, person.refcode={code}, person.devicetoken={token} ON CREATE SET person = {status: 'not activated', otp:{otp}, timestamp:{ts}, mobilenumber:{number},countrycode:{cc}, fid:{fid}, isowner:true, ownerfid:'', refcode:{code}, created_date:{cd}, updated_date:{ud}, devicetoken:{token}}  RETURN person",
            {
                number: mobilenumber,
                code: reqref,
                otp: otp,
                ts: new Date().getTime(),
                cc: countrycode,
                fid: utils.createFid(),
                cd: new Date().getTime(),
                ud: new Date().getTime(),
                token:devicetoken
            }

        );

        MessageService.sendSMS(countrycode , mobilenumber, 'Your one time password is ' + otp + " for request #" + reqref + " to activate officeG application.");

        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: { refcode: reqref, number: mobilenumber, otp:otp, isnew:isnew } });

    }
    catch (err) {
        console.log("Failed to register user..." + err);
        response.json(new ERROR(ErrorCode.FAILED_TO_CREATE_USER));
    }

}

Person.getConnectionRequests = async function(key) {
    var result = await db.execute("MATCH (a)-[rs:SENT]-(b)-[rt:TO]-(c:Person {key: {key}}) WHERE b.active = 1 and b.type = 1 RETURN a, b,c", {key:key});
    var notifications = [];
    console.log("Connection notifications of " + result.records.length + "for key : " +key);
    for(var index=0; index< result.records.length; index++) {
        var record = result.records[index];
        //result.records.forEach(function (record) {
        var sourcePerson = record._fields[0].properties;
        var notification = record._fields[1].properties;
        var destinPerson = record._fields[2].properties;

        notifications.push({type: notification.type.low, message:notification.message, senderid:sourcePerson.fid, datetime:notification.created_date, mode: notification.mode, profileimage:Crypt.decryptField(sourcePerson.profileimage), id:db.int(record._fields[1].identity).toString() });
    }

    return notifications;
}

Person.verify = function (request, response) {
    var refcode = request.query.refcode;
    var otp = request.query.otp;
    var number = request.query.mobilenumber;
    var isnew = request.query.isnew;
	console.log(JSON.stringify(request.query));
    session.run("MATCH(person:Person) WHERE person.mobilenumber = {number} RETURN person", { number: number }).then(function (result) {
        var person = null;

        if (!_.isEmpty(result.records)) {
            try {
            	var mode = request.query.mode;
            	if(mode != '' && mode != undefined && mode != null) {
            		if(mode == 'verifyAccountAlreadyExistOrNot'){
                    	response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{ isexist : true }});
            		}
            	}
            
                result.records.forEach(function (record) {
                    //console.log(JSON.stringify(record));
                    var person = Crypt.decryptPerson(record.get(0).properties);
                    {
                        var timestamp = new Date(person.timestamp);
                        var diff = new Date() - timestamp;
                        if (diff > 1000 * 60 * 5) {
                            response.json(new ERROR(ErrorCode.PASSCODE_EXPIRED));
                            return;
                        }
                        if (person.otp != otp) {
                            response.json(new ERROR(ErrorCode.INVALID_PASSCODE));
                            return;
                        }
                        var key = uuid.v4();

                        session.run("MERGE(person:Person{mobilenumber: {number}}) ON MATCH SET person.status = 'activated', person.key={key} RETURN person ", { number: number, key : key }).then(async function (result) {
                            try {
                                var waitingnotifications = await Person.getConnectionRequests(key);
                                console.log ("isnew : " + isnew);
                                if(isnew == "false") {
                                    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{ secret_key:key,
                                        fid:person.fid,
                                        firstname:person.firstname,
                                        lastname:person.lastname,
                                        profileimage:person.profileimage,
                                        doi:person.doi,
                                        gender:person.gender,
                                        dob:person.dob,
                                        displayname:person.displayname,
                                        waitingrequests:waitingnotifications.length,
                                        isadmin:person.admin
                                    } });
                                    console.log("verify Sending existing details...");
                                }
                                else
                                    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{ secret_key:key, fid:person.fid, waitingrequests:waitingnotifications.length} });
                            }
                            catch(e) {console.log(e);}
                        }).catch(function () { });
                    }

                });


            }
            catch (err) {
                console.log("Erro : " + err);
                response.json(err);
                return;
            }
        }
        else {
            var mode = request.query.mode;
            if(mode != '' && mode != undefined && mode != null) {
            	if(mode == 'verifyAccountAlreadyExistOrNot'){
                    response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result:{isexist : false}});
            	}
            } else {
		            response.json(new ERROR(ErrorCode.NUMBER_DOES_NOT_EXISTS));
		    }
        }

    }).catch(function (error) {
        console.log("Failed" + error);
    });
    //response.json({refcode:refcode, otp : otp, "test":"test"});
}

Person.authenticate = function(request, response) {

}

Person.createProfile = async function (request, response) {
    var person = request.body;

    var key = "key";
    person.value = person.secret_key;
    if(request.query.isowner == "false") {
        person.value = request.query.fid;
        key = "fid";
    }

    if(person.isalive == undefined)
        person.isalive = true;

        var reltype = person.relationship;
        console.log(JSON.stringify(person));
		if(reltype != undefined && reltype != null && reltype != ''){
	    	var oppositeReltype = utils.getOppositeRelation(reltype, person.gender)
        }
        Crypt.encryptPerson(person);

    if(request.query.isconnected == "true") {
        console.log("Replace displayname for " + request.query.lfid + " - " + request.query.fid + " and display name : " + person.displayname + " reltype = " + reltype + " oppositeReltype = " + oppositeReltype);
		if(reltype != undefined && reltype != null && reltype != ''){
        	var res = await db.execute("MATCH(b:Person{fid:{fid}})<-[R:IS_RELATED_TO]-(a:Person{fid:{key}}) SET R.displayname={displayname}, R.reltype={reltype}", {fid:request.query.fid, key:request.query.lfid, displayname:person.displayname, reltype:reltype});
	        await db.execute("MATCH(b:Person{fid:{fid}})-[r:IS_RELATED_TO]->(a:Person{fid:{key}}) SET r.reltype={oppositeReltype}", {fid:request.query.fid, key:request.query.lfid, oppositeReltype:oppositeReltype});
        }else{
        	var res = await db.execute("MATCH(b:Person{fid:{fid}})<-[R:IS_RELATED_TO]-(a:Person{fid:{key}}) SET R.displayname={displayname}", {fid:request.query.fid, key:request.query.lfid, displayname:person.displayname});
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
        return;
    }

    //console.log("Person details :" + JSON.stringify(person));
    //console.log("isowner : " + request.query.isowner + " fid : " + request.query.fid + ". Key = " + key + " value = " + person.value);
    console.log("update details = display name : " + person.displayname + " reltype = " + reltype + " oppositeReltype = " + oppositeReltype);

            	var mobilenumber = request.body.mobilenumber;
				if(mobilenumber != undefined && mobilenumber != null && mobilenumber != '' && request.query.mode == "mobilenumber"){
					var temp = await db.execute("MATCH(a:Person{mobilenumber:{mobilenumber}}) return a", {mobilenumber:mobilenumber});
			        if(!_.isEmpty(temp.records)) {
			        	console.log('createProfile = mobile number already exist');
				        response.json(new ERROR(ErrorCode.DUPLICATE_CONNECTION_REQUEST));
				        return;
			        }
				}

    session.run("MERGE(person:Person{"+ key + ": {value}}) ON MATCH SET person.displayname={displayname}, person.firstname={firstname}, person.lastname={lastname}, person.dob={dob}, person.doi={doi}, person.pushnotification={pushnotification}, person.geolocationaccess={geolocationaccess}, person.tagging={tagging}, person.profileimage={profileimage}, person.gender={gender}, person.mobilenumber={mobilenumber}, person.isalive={isalive}, person.deceaseddate={deceaseddate} RETURN person ", person).then( async function (result) {
        if(!_.isEmpty(result.records)) {
            var user = result.records[0]._fields[0].properties;
            if(user.isowner == "false" || !user.isowner) {
        		console.log('inside '+JSON.stringify(user)+ ' and ' + request.body.mobilenumber);
				if(reltype != undefined && reltype != null && reltype != ''){
        	        var res = await db.execute("MATCH(b:Person{fid:{fid}})<-[R:IS_RELATED_TO]-(a:Person{fid:{ownerfid}}) SET R.displayname={displayname}, R.reltype={reltype}", {fid:user.fid, ownerfid:user.ownerfid, displayname:person.displayname, reltype:reltype});
		        	await db.execute("MATCH(b:Person{fid:{fid}})-[R:IS_RELATED_TO]->(a:Person{fid:{ownerfid}}) SET R.reltype={oppositeReltype}", {fid:user.fid, ownerfid:user.ownerfid, oppositeReltype:oppositeReltype});
            	}else{
            	    var res = await db.execute("MATCH(b:Person{fid:{fid}})<-[R:IS_RELATED_TO]-(a:Person{fid:{ownerfid}}) SET R.displayname={displayname}", {fid:user.fid, ownerfid:user.ownerfid, displayname:person.displayname});
            	}
				if(mobilenumber != undefined && mobilenumber != null && mobilenumber != '' && request.query.mode == "mobilenumber"){
	            	MessageService.sendSMS(request.body.countrycode, mobilenumber, "Welcome to officeG! Please download (http://bit.ly/2z9RSTU) and login with " + mobilenumber);
	            }
            }     
        }
        person.dob = Crypt.decryptField(person.dob);
        if(person.dob != undefined && person.dob != "" && person.dob != null) {
            var eventid = person.dob.substring(0, 5).replace('/', '_');
            try {
                await db.execute("MERGE (event:Event{eventid:{eventid}}) ON CREATE SET event.eventid = {eventid}",{eventid:eventid});
                await db.execute("MATCH (event:Event{eventid:{eventid}}) , (person:Person{"+ key + ":{value}})  CREATE UNIQUE (person)-[r:EVENT_ON{type:{type}}]->(event)", {value:person.value, eventid:eventid, type:"birthday"});
            }catch(error) {
                console.log(error);
            }
        }
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
        
    }).catch(function (error) {
        console.log("Failed in profile update : " + error)
        response.json(new ERROR(ErrorCode.FAILED_TO_UPDATE_PROFILE));
    });
}

Person.getProfile = function (request, response) {

    session.run("MATCH(person:Person) WHERE person.mobilenumber = {number} RETURN person", { number: request.query.mobilenumber }).then(function (result) {
        var person = null;

        if (!_.isEmpty(result.records)) {
            try {
                result.records.forEach(function (record) {
                }
                );
            }
            catch (E) { }
        }
        else
            response.json()
    });

}

Person.registerdevice = async function (request, response) {
    try {
        var result = await db.execute("MERGE(person:Person{key: {secret_key}}) ON MATCH SET person.devicetoken = {devicetoken} RETURN person", {
            secret_key: request.query.secret_key,
            devicetoken:request.query.devicetoken
        });
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
    }
    catch(e) {
        //console.log("Error in registerdevice : " +e);
        response.json(new ERROR(ErrorCode.FAILED_TO_REGISTER_DEVICE));
    }
}

Person.changenumber = async function(request, response) {
    
    try {
        request.body.otp = utils.createOTP(); // generate OTP
        request.body.reqref = utils.createRefCode(); // generate reference number
        request.body.ts = new Date().getTime();
        console.log(request.body);
        var result = await db.execute("MATCH(a:Person) WHERE a.key={secret_key} AND a.mobilenumber={mobilenumber} SET a.refcode={reqref}, a.otp={otp}, a.timestamp={ts} RETURN a", request.body);
        if(_.isEmpty(result.records)) {
            console.log("User does not match.");
            response.json(new ERROR(ErrorCode.INCORRECT_OLD_NUMBER));
            return ;
        }
       // MessageService.sendSMS(request.body.countrycode , request.body.mobilenumber, 'Your one time password is ' + request.body.otp + " for request #" + request.body.reqref + " to change officeG mobile number.")
        response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true, refcode: request.body.reqref, otp:request.body.otp} });
    }
    catch(e) {
        console.log("Error in changing the number : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_CHANGE_NUMBER));
    }
}

Person.verifynewnumber = async function(request, response) {    
    try {
        request.body.ts = new Date().getTime();
        console.log(request.body);
        var result = await db.execute("MATCH(a:Person) WHERE a.key={secret_key} SET a.mobilenumber = {mobilenumber}, a.countrycode={countrycode} RETURN a", request.body);

        //MessageService.sendSMS(request.query.countrycode + request.query.relmobilenumber, 'Your one time password is ' + request.query.otp + " for request #" + request.query.reqref + " to change officeG mobile number.")
        if(_.isEmpty(result))
            response.json({ iserror: true, errorcode: ErrorCode.INVALID_PASSCODE, result: {status:false} });
        else
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
    }
    catch(e) {
        console.log("Error in verify new number : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_CHANGE_NUMBER));
	}
}

Person.familynotification = async function(request, response) {
    try {
    	console.log('familynotification = ' + request.body.secret_key);
        var result = await db.execute("MATCH(a:Person{key: {secret_key}}) SET a.familynotification = {familynotification} RETURN a", request.body);
    	console.log('familynotification = ' + JSON.stringify(result));

        if(_.isEmpty(result))
            response.json({ iserror: true, errorcode: ErrorCode.INVALID_PASSCODE, result: {status:false} });
        else
            response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
    }
    catch(e) {
        console.log("Error in verify new number : " + e);
        response.json(new ERROR(ErrorCode.FAILED_TO_UPDATE_PROFILE));
	}
}

Person.logThis = async function (request, response) {
    var person = request.body;
    var logpage = person.logpage;
    var logcontent = person.logcontent;
    console.log('logThis = ' + logpage + ' and ' + logcontent);
	response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: {status:true} });
}