﻿
function Error(code) {
     return { iserror: true, errorcode: code, message: this.message[code] };
};

Error.prototype.message = {
    0: "Success",
    1: "Number already registered",
    2: "Failed to create user",
    3: "Unable to send SMS",
    4: "Number does not exists",
    5: "Number already activated",
    6: "Passcode expired",
    7: "Passcode is not valid",
    8: "Failed to update the profile",
    9: "Failed to connect with the profile",
    10: "Request is already approved",
    11: "Failed to register the device token",
    12: "Failed to change the number",
    13: "Failed to retrieve the relationship",
    14: "Failed to approve the request",
    15: "Failed to retrieve the favourite list",
    16: "Failed to disconnect the relation",
    17: "Failed to retrieve the profile",
    18: "Failed to retrieve notifications",
    19: "Failed to reject the relationship",
    20: "Failed to add comment to the post",
    21: "Failed to add like to the post",
    22: "Failed to retrieve the post list",
    23: "Failed to create the post",
    24: "Failed to create group",
    25: "Failed to get group list",
    26: "Incorrect previous mobilenumber provided",
    27: "Failed to get tree",
    28: "Failed to remove like from a the post",
    29: "Failed to merge",
    30: "Merge request has been already sent",
    31: "Failed to clear badge count",
	32: "Duplicate connection request",
	33: "Error in sending message",
	34: "Error in adding reminder"
};

module.exports = Error;

