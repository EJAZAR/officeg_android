﻿var Config = new require("../config");
var ERROR = require('../helpers/error');
var ErrorCode = new require('../helpers/errorcode');
var uuid = require('uuid');
var db = new require('../helpers/db');
var Crypt = new require('../helpers/crypt');
var MessageService = new require('../helpers/message');
var chatManager = require('../controllers/chatController');
var utils = require("../helpers/utils");
var schedule = require('node-schedule');
var _ = require('underscore');


var Scheduler = exports;

Scheduler.start = async function () {
    console.log("Scheduling started..." + new Date());
    Scheduler.job = schedule.scheduleJob('0 0 * * *', async function () {
        try {
            console.log("Schedule call in");
            var date = new Date();
            var id = ((date.getDate() < 10) ? "0" : "") + date.getDate() + "_" + (((date.getMonth()+1) < 10) ? "0" : "") + (date.getMonth() + 1);
            console.log("Schedule start for id " + id);
            var events = await db.execute("MATCH (event:Event{eventid:{eventid}})<-[r:EVENT_ON]-(person:Person) RETURN person,r ", { eventid: id });
            if(!_.isEmpty(events.records)) {
                events.records.forEach(async function(record) {
                    try {
                        var person = record.get(0).properties;
                        var eventtype = record.get(1).properties.type
                
                        if (person.doi == "" || person.doi == null || person.doi == undefined)
                            person.doi = 3;
                        var postid=uuid.v4();
                        var message = '';
            
                        if(eventtype == "birthday")
                            message = "Today is " + Crypt.decryptField(person.displayname) + "'s birthday.";
                        else if(eventtype == "deathday")
                            message = "Today is " + Crypt.decryptField(person.displayname) + "'s death anniversary.";
                        console.log("Message : " + message);
                        console.log("Key : " + JSON.stringify(person));
                        await db.execute("Create(post:Post{postid:{postid},content:{content},images:{images},videos:{videos},time:{time}}) return post", {postid:postid,content:message,images:[],videos:[],time:new Date().getTime()});

                        await db.execute("Match (a:Person{fid:{fid}}),(b:Post{postid:{postid}}) create (a)-[r:IS_POSTED{time:{time}}]->(b) return r, a, b",{fid:'ffffff',time:new Date().getTime(), postid:postid});

                        var res = await db.execute("MATCH (p:Post{postid:{postid}}),(a:Person{fid:{fid}})-[rels:IS_RELATED_TO*1.."+ person.doi + "]->(b:Person{status:'activated'}) WHERE ALL (rel in rels WHERE rel.status='approved') AND NOT (a)<-[:BLOCKED]-(b) AND b.fid <> a.fid   OPTIONAL MATCH (a)-[rf:IS_RELATED_TO]->(b) WHERE NOT (a)<-[:BLOCKED]-(b) AND NOT (a)-[:IS_FAVOURITE_TO]->(b) create unique (p)-[rp:POSTED_TO]->(b) return distinct a,  b, p, case WHEN (rf IS NOT NULL AND rf.status = 'approved') THEN true else false end as isfav",{fid:person.fid,postid:postid,time:new Date().getTime()});
                        if(!_.isEmpty(res.records)) {
                            res.records.forEach(function(result) {
                                var srcPerson=Crypt.decryptPerson(result.get(0).properties);
                                var post=result.get(2).properties;
                                var destPerson=Crypt.decryptPerson(result.get(1).properties);
                                console.log("Sending message to : "+ destPerson.key + ", " + destPerson.displayname);
                                chatManager.sendPostMessage(destPerson.devicetoken, {postid: post.postid , content:post.content,images:post.images,videos:post.videos, time:post.time, likes:0, comments:[] , displayname:"System", profileimage:srcPerson.profileimage, gender:srcPerson.gender, fid:srcPerson.fid, isfav:result.get(3), likebyme:false});
                            });
                        }


                        var records = await db.execute("MATCH (p:Post{postid:{postid}}),(a:Person{fid:{fid}})-[rel:IS_FAVOURITE_TO]->(b:Person{status:'activated'})  WHERE rel.status = 'approved' AND NOT(a)<-[:BLOCKED]-(b) create unique (p)-[r:POSTED_TO]->(b) return a,b,p",{fid:person.fid,postid:postid});

                        if(records.records.length != 0) {
                            try{
                                records.records.forEach(function(result) {
                                    var srcPerson=Crypt.decryptPerson(result.get(0).properties);
                                    var post=result.get(2).properties;
                                    var destPerson=Crypt.decryptField(result.get(1).properties);
                        
                                    console.log("Sending message to fav : "+ destPerson.fid + ", " + destPerson.displayname);
                                    chatManager.sendPostMessage(destPerson.devicetoken, {postid: post.postid , content:message,images:post.images,videos:post.videos, time:post.time, likes:0, comments:[] , displayname:"System", profileimage:srcPerson.profileimage, gender:srcPerson.gender, fid:srcPerson.fid, isfav:true, likebyme:false});
                                }); 
                            }
                            catch(e){
                                console.log("Failed to posting to favourite : " + e);
                            }
                        }
                    }
                    catch(error) {
                        console.log("Error in sending events : " + error);
                    }
                });
            }
        }
        catch(error) {
            console.log("Error in sending events : " + error);
        }
    });
}