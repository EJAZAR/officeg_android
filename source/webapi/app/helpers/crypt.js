﻿const crypto = require('crypto');
const Config = new require("../config");

const ENCRYPTION_KEY = new Buffer(Config.ENCRYPTION_KEY, 'hex'); // Must be 256 bytes (32 characters)
const IV_LENGTH = 16; // For AES, this is always 16

var Cryptography = module.exports;

Cryptography.encrypt = function(text) {
    let iv = crypto.randomBytes(IV_LENGTH);
    //console.log("Key : " + ENCRYPTION_KEY);
    let cipher = crypto.createCipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

Cryptography.decrypt = function (text) {
    try {
        let textParts = text.split(':');
        let iv = new Buffer(textParts.shift(), 'hex');
        let encryptedText = new Buffer(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }
    catch (e) {
        console.log("Failed to decrypt : " + text);
        return "";
    }
}

Cryptography.encryptField = function(field) {
    if (field == undefined || field == "" || field == null)
        return "";
    return Cryptography.encrypt(field);
}

Cryptography.decryptField = function(field) {
    if (field == undefined || field == "" || field == null)
        return "";
    return Cryptography.decrypt(field);
}

Cryptography.encryptPerson = function (person) {
    person.firstname = Cryptography.encryptField(person.firstname);
    person.lastname = Cryptography.encryptField(person.lastname);
    person.displayname = Cryptography.encryptField(person.displayname);
    person.dob = Cryptography.encryptField(person.dob);
    person.deceaseddate = Cryptography.encryptField(person.deceaseddate);

    //person.nativeplace = Cryptography.encryptField(person.nativeplace);
    person.bloodgroup = Cryptography.encryptField(person.bloodgroup);
    person.blood = Cryptography.encryptField(person.blood);
    person.gender = Cryptography.encryptField(person.gender);
    //person.mobilenumber = Cryptography.encryptField(person.mobilenumber);
    person.profileimage = Cryptography.encryptField(person.profileimage);
    person.profileimageurl = Cryptography.encryptField(person.profileimageurl);
    
    return person;
}

Cryptography.decryptPerson = function (person) {
    person.firstname = Cryptography.decryptField(person.firstname);
    person.lastname = Cryptography.decryptField(person.lastname);
    person.displayname = Cryptography.decryptField(person.displayname);
    person.dob = Cryptography.decryptField(person.dob);
    person.deceaseddate = Cryptography.decryptField(person.deceaseddate);

    //person.nativeplace = Cryptography.decryptField(person.nativeplace);
    person.bloodgroup = Cryptography.decryptField(person.bloodgroup);
    person.blood = Cryptography.decryptField(person.blood);
    person.gender = Cryptography.decryptField(person.gender);
    //person.mobilenumber = Cryptography.decryptField(person.mobilenumber);
    person.profileimage = Cryptography.decryptField(person.profileimage);
    person.profileimageurl = Cryptography.decryptField(person.profileimageurl);

    return person;
}