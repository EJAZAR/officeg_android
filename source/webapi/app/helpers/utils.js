﻿var crypto = require('crypto');

function randomValueHex(len) {
    return crypto.randomBytes(Math.ceil(len / 2))
        .toString('hex')
        .slice(0, len);
}

function getRandom(length) {
    if (length == undefined)
        length = 6; // Keep 6 as default
    var base = Math.pow(10, length);
    do {

        //console.log("Base " + base);
        var random = "" + parseInt(Math.random() * base) % base;
        if (random.length == length)
            return random;

    } while (true);

}

exports.createFid = function () {
    return randomValueHex(6);
}

exports.createOTP = function () {
    return getRandom(6);
}

exports.createRefCode = function () {
    return getRandom(7);
}

exports.getOppositeRelation = function (relationtype, gender) {
    gender = gender.toUpperCase();
    relationtype = relationtype.toUpperCase();
    console.log(relationtype+" and "+gender);
    if (relationtype == "MOTHER" || relationtype == "FATHER") {
        if (gender == "MALE")
            return "SON";
        else if (gender == "FEMALE")
            return "DAUGHTER";
    }
    else if (relationtype == "SON" || relationtype == "DAUGHTER") {
        if (gender == "MALE")
            return "FATHER";
        else if (gender == "FEMALE")
            return "MOTHER";
    }
    else if (relationtype == "BROTHER" || relationtype == "SISTER") {
        if (gender == "MALE")
            return "BROTHER";
        else if (gender == "FEMALE")
            return "SISTER";
    }
    else if (relationtype == "WIFE" || relationtype == "HUSBAND") {
        if (gender == "MALE")
            return "HUSBAND";
        else if (gender == "FEMALE")
            return "WIFE";
    }
    // Extended relations
    else if (relationtype == "NIECE" || relationtype == "NEPHEW") {
        if (gender == "MALE")
            return "UNCLE";
        else if (gender == "FEMALE")
            return "AUNT";
    }
    else if (relationtype == "UNCLE" || relationtype == "AUNT") {
        if (gender == "MALE")
            return "NEPHEW";
        else if (gender == "FEMALE")
            return "NIECE";
    }
    else if (relationtype == "GRANDFATHER" || relationtype == "GRANDMOTHER") {
        if (gender == "MALE")
            return "GRANDSON";
        else if (gender == "FEMALE")
            return "GRANDDAUGHTER";
    }
    else if (relationtype == "GRANDSON" || relationtype == "GRANDDAUGHTER") {
        if (gender == "MALE")
            return "GRANDFATHER";
        else if (gender == "FEMALE")
            return "GRANDMOTHER";
    }
    else if (relationtype == "COUSIN") {
        return "COUSIN";
    }
    else if (relationtype == "RELATION") {
        return "RELATION";
    }
    else if (relationtype == "REP") {
        return "BEHALF";
    }
    return "UNKNOWN";
}