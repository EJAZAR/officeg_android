var https = require('https');
var http = require('http');
var Config = new require("../config");
var admin = require("firebase-admin");
var serviceAccount = require("../../familyg-firebase-adminsdk.json");


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: Config.FIREBASE_DB_URL
});
function b2bsms(mobilenumber, message) {
    try {
		
        console.log("Sending SMS from b2b " + mobilenumber);
        var options = {
            host: 'sms.b2bsms.co.in',
            path: '/API/WebSMS/Http/v1.0a/index.php?username=TECHJK&password=5454&sender=FAMLYG&to=' + mobilenumber + '&message=' + encodeURIComponent(message) + "&reqid=1&format=json&route_id='70'",
            port: 80,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': '0'
            }
        };

        var req = http.request(options);
        //req.write(data);
        req.end();

        var responseData = '';
        req.on('response', function (res) {
            res.on('data', function (chunk) {
                responseData += chunk;

            });

            res.on('end', function () {
                //var nexmoResponse = JSON.parse(responseData);
                console.log("RESPONSE DATA: " +responseData);
                return true;
                /*{
                    console.log("Reference code sent to client " + reqref);
                     //response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: { refcode: reqref, number: mobilenumber } });
                 }
                 else
                     //response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_SEND_SMS, message: "Unable to send SMS to " + mobilenumber });
                 */
            });
        });
    }
    catch (error) {
        console.log("Error in sending SMS : " + error);
    }
}


exports.sendSMS = function (countrycode, mobilenumber, message) {
	//nexmo("971562737954","test message from FamilyG");
    if (countrycode == "91" || countrycode == "+91"){
		
        b2bsms(countrycode + mobilenumber, message);
	        console.log("Message credentials : " + message);

    }else{
        nexmo(countrycode + mobilenumber, message);
		        console.log("Message credentials : " + countrycode);
	}

}

function nexmo(mobilenumber, message) {
    console.log("Sending SMS using nexmo to " + mobilenumber);
	    console.log("Sending SMS to " + message);

    
    try {
        var data = JSON.stringify({
            api_key: Config.NEXMO_KEY,
            api_secret: Config.NEXMO_SECRET,
            to: mobilenumber,
            from: Config.NEXMO_FROM,
            text: message
        });

        var options = {
            host: 'rest.nexmo.com',
            path: '/sms/json',
            port: 443,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };

        var req = https.request(options);
        req.write(data);
        req.end();

        var responseData = '';
        req.on('response', function (res) {
            res.on('data', function (chunk) {
                responseData += chunk;

            });

            res.on('end', function () {
                var nexmoResponse = JSON.parse(responseData);
                console.log(responseData);
                return (nexmoResponse['messages'][0]['status'] === "0");
                /*{
                    console.log("Reference code sent to client " + reqref);
                     //response.json({ iserror: false, errorcode: ErrorCode.SUCCESS, result: { refcode: reqref, number: mobilenumber } });
                 }
                 else
                     //response.json({ iserror: true, errorcode: ErrorCode.FAILED_TO_SEND_SMS, message: "Unable to send SMS to " + mobilenumber });
                 */
            });
        });
    }
    catch (error) {
        console.log("Error in sending SMS : " + error);
    }
}

exports.sendPushNotification = function (title, subject, data, token) {
    var payload = {
        notification: {
            title: title,
            body: subject
        },
       data: data
    };
//Add a comment to this line

    admin.messaging().sendToDevice(token, payload);
}