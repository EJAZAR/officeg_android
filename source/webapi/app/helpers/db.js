﻿var Config = new require("../config");
const dbneo4j = require('neo4j-driver').v1;

const dbdriver = dbneo4j.driver(Config.DB, dbneo4j.auth.basic(Config.DB_USER, Config.DB_PASS));
const dbsession = dbdriver.session();

async function Execute(query, params) {
    
    var result  = await dbsession.run(query, params);
    
    return result;
}

exports.execute = Execute;
exports.int = dbneo4j.int;
    /*function (query, params) {
    console.log("In db");
    dbsession.run(query, params).then(function (result) {
        dbsession.close();
        
        return result;
    }).catch(function (error) {
        dbsession.close();
        throw error
    });
}*/