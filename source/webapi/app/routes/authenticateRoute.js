﻿'use strict'

var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })
var path = require('path');
var crypt = require('../helpers/crypt');

module.exports = function (app) {
    var authenticate = require('../controllers/authenticateController');
    var fgdataauthenticate = require('../controllers/fgdataauthenticateController');
    var relationship = require('../controllers/relationshipController');
    var fgdatarelationship = require('../controllers/fgdatarelationshipController');
    var post = require('../controllers/postController');
    var chat = require('../controllers/chatController');
    var nearby = require('../controllers/nearbyController');
    app.route('/uploadtestdata').get(nearby.uploadTestData);
    app.route('/deletetestdata').get(nearby.deleteTestData);
    app.route('/register')
        .get(authenticate.register);
    app.route('/firebaseregister')
        .get(authenticate.fireBaseRegistration);
    app.route('/fgdatafirebaseregister')
        .get(fgdataauthenticate.fgdatafireBaseRegistration);        
    app.route('/verify')
        .get(authenticate.verify);
    app.route('/logThis')
        .post(authenticate.logThis);
    app.route('/profile')
        .post(authenticate.createProfile);
    app.route('/fgdataprofile')
        .post(fgdataauthenticate.fgdatacreateProfile);
    app.route('/fgdataupdateprofile')
        .post(fgdataauthenticate.fgdataupdateprofile);
    app.route('/profile')
        .get(authenticate.getProfile);
    app.route('/fgdatarelationship/fgdatacreatefamily')
        .post(fgdatarelationship.fgdatacreatefamily);
    app.route('/fgdatarelationship/fgdataconnectfamily')
        .post(fgdatarelationship.fgdataconnectfamily);
    app.route('/fgdatarelationship/getfamiles')
        .get(fgdatarelationship.getfamiles);
    app.route('/relationship/connect')
        .post(relationship.connect);
    app.route('/relationship/disconnect')
        .post(relationship.disconnect);
    app.route('/fgdatarelationship/fgdatadisconnect')
        .post(fgdatarelationship.fgdatadisconnect);
    /*app.route('/relationship/connectWithFID')
        .get(relationship.connectWithFID);
    app.route('/relationship/createWithoutPhone')
        .post(relationship.createWithoutPhone);*/
    app.route('/relationship/getrelation')
        .get(relationship.getrelation);
    app.route('/fgdatarelationship/getfirstdegreerelations')
        .get(fgdatarelationship.getfirstdegreerelations);
    app.route('/getnotifications')
        .get(relationship.getnotifications);
    app.route('/registerdevice')
        .get(authenticate.registerdevice);
    app.route('/relationship/approve')
       .get(relationship.approve);
    app.route('/relationship/reject')
       .get(relationship.reject);
    app.route('/relationship/viewProfile')
      .get(relationship.viewProfile);
    app.route('/relationship/getFavouriteList')
      .get(relationship.getFavouriteList);
    app.route('/relationship/addFavourite')
      .get(relationship.addFavourite);
    app.route('/relationship/removeFavourite')
      .get(relationship.removeFavourite);
    app.route('/post/createPost')
     .post(post.createPost);
    app.route('/post/getPostList')
    .get(post.getPostList);
	app.route('/post/listOfLiked')
	.get(post.listOfLiked);
	app.route('/post/getReplies')
    .get(post.getReplies);
    app.post('/upload', upload.single("file"), async function(req,res) {
        // console.log("files:" + req.files);
        // res.send(req.files);
        try {
            console.log('file path:' + req.file.path);
            var OriginalName = req.file.originalname;
	        console.log("OriginalName:" + OriginalName);
	        console.log(JSON.stringify(req.file))
            var extension = OriginalName.split('.')[1];
            var FilePath = req.file.path;
            console.log("path:" + FilePath);
            var FileName = req.file.name;
            var fs = require('fs');
            var fileStream = fs.createReadStream(FilePath);
        
            var Config = new require("../config");
            var AWS = require('aws-sdk');
            // Load credentials and set region from JSON file
            AWS.config.loadFromPath(Config.AWS_CONFIG_PATH);

            // Create S3 service object
            var s3 = new AWS.S3({ apiVersion: '2006-03-01', httpOptions :{timeout :1000 * 60 * 10} });

            // call S3 to retrieve upload file to specified bucket
            var uploadParams = { Bucket: Config.AWS_S3_BUCKET_NAME, Key: '', Body: '' };
            uploadParams.Body = fileStream;
            //console.log(uploadParams.Body);

            var path = require('path');
            var uuid = require('uuid');
            uploadParams.Key = uuid.v4() + "." + extension ;
        
            console.log(" uploadParams.Key:" +  uploadParams.Key);

            // call S3 to retrieve upload file to specified bucket
            s3.upload(uploadParams, function(err, data) {
                try {
                    console.log("Error status : " + err);
                    console.log("Upload Success", data.Location);
                    res.json({iserror:false, errorcode:0, result: {url : data.Location}});
                }
                catch(e){
                    res.json({iserror:true, errorcode:50, result: null});
                }

            });
        }
        catch(error) {
            console.log("upload error :" + error);
            res.json({iserror:true, errorcode:50, result: null});
        }
            
    });

    app.route('/addlikes')
       .post(post.addLike); 
    app.route('/removelikes')
       .post(post.removeLike);
    app.route('/addcomments')
       .post(post.addComment);
    app.route('/changenumber')
        .post(authenticate.changenumber);
    app.route('/verifynewnumber')
        .post(authenticate.verifynewnumber);
    app.route('/familynotification')
        .post(authenticate.familynotification);
    app.route('/chat/creategroup')
        .post(chat.createGroup);
    app.route('/chat/deletegroup')
        .post(chat.deleteGroup);
    app.route('/chat/group/changename')
        .post(chat.changeGroupName);
    app.route('/chat/groups')
        .post(chat.groups);
    app.route('/chat/group/addmember')
        .post(chat.addMember);
    app.route('/chat/group/removemember')
        .post(chat.removeMember);
    app.route('/chat/group/revokeadminpermission')
        .post(chat.revokeAdminPermission);
    app.route('/chat/backup')
        .post(chat.backup);
    app.route('/chat/send')
        .post(chat.sendMessage);
    app.route('/chat/backuplist')
        .post(chat.backupList);
    app.route('/chat/getUnreadMessages')
        .post(chat.getUnreadMessages);
    app.route('/chat/clearBadgeCount')
        .post(chat.clearBadgeCount);        
    app.route('/chat/getFavouriteListAndGroups')
        .post(chat.getFavouriteListAndGroups);        
    app.route('/chat/addReminder')
        .post(chat.addReminder);        
    app.route('/relationship/blockuser')
        .post(relationship.blockUser);
    app.route('/relationship/unblockuser')
        .post(relationship.unblockUser);
    app.route('/relationship/getblockedusers')
        .post(relationship.getBlockedUsers);
    app.route('/relationship/merge')
        .post(relationship.merge);
    app.route('/relationship/ancestry')
        .get(relationship.ancestry);
    app.route('/relationship/findrelationship')
        .get(relationship.findrelationship);
    app.route('/sendtestmessage')
        .get(relationship.sendTestMessage);

    app.route('/location/update')
        .post(nearby.updatelocation);
    app.route('/location/nearby')
        .post(nearby.nearbyUsers);

    app.route('/pedigree/users')
        .get(relationship.getPedigreeUsers);
    app.get('/report/pedigree', function(req,res) {
        res.sendFile(path.join(__dirname + "/../reports/pedigree.html"));
    });

    app.route('/encrypt').get(function(request, response){
        response.json({enc:crypt.encrypt(request.query.text)});
    });
    app.route('/decrypt').get(function(request, response){
        response.json({enc:crypt.decrypt(request.query.text)});
    });
    app.route('/notifications/delete').get(relationship.deleteNotification);
    app.route('/report').get(relationship.getPedigreeReport);
    app.route('/pedigreetree').get(relationship.getPedigreeTree);
    app.route('/testsms').get(relationship.sendTestSMS);
    app.route('/tree').get(relationship.tree);
    app.route('/treeancestry').get(relationship.treeancestry);
    app.route('/fgdatatreeancestry').get(fgdatarelationship.fgdatatreeancestry);
}