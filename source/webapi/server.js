var https = require('https');
var fs = require('fs');


var options = {
    ca: fs.readFileSync('./ssl/intermediate.crt'),
    key: fs.readFileSync('./ssl/api.familyg.org.key'),
    cert: fs.readFileSync('./ssl/api.familyg.org.ssl.crt'),
};



var log = console.log;

console.log = function () {
    log.apply(console, [new Date().toISOString() + " : " + arguments[0]]);
};

require('monitor').start();
var express = require('express'),
  app = express(),
 port = process.env.PORT || 5925;
//   port = process.env.PORT || 443;

//var AuthenticateModel = require('./app/models/authenticateModel');
var bodyParser = require('body-parser');
var authorizeManager = require('./app/helpers/authorize');

app.use(function(request, response, next) {
    var key = request.header("host");
    //90console.log(key);
    //response.json({ iserror: true, errorcode: 5000, message: "Access denied" });
    next();
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var chat = require('./app/controllers/chatController');
var routes = require('./app/routes/authenticateRoute');
var scheduler = require('./app/helpers/scheduler');

routes(app);
chat.listen();
scheduler.start();


//  https.createServer(options, app).listen(port, function () {
//      console.log('FamilyG web api server started on: ' + port);
//  });

app.listen(port).on('error', function (err) {
  console.log("Error in port " + err);
});