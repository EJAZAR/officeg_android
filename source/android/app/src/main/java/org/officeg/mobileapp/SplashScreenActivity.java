package org.officeg.mobileapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.widget.Toast;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashScreenActivity extends AppCompatActivity {
    UserModel user;
    ProfileModel profile;
    private static final int WES_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        DataManager dm = DataManager.getInstance(getApplicationContext());
        user = UserModel.getInstance();

        profile = ProfileModel.getInstance();
        user.load(getApplicationContext());
        Log.i("FamilyG", "Secret key : " + user.secret_key);
        if (dm == null)
            Log.i("FamilyG", " Datamanager is null");
        else
            dm.createTableForChatHistory();
        checkPermission();
        updateVectorCompat();
    }

    private void twoMinsThread() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);
                    Log.i("FamilyG", "User key : " + user.secret_key);
                    afterSplashScreen();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };
        thread.start();
    }

    private void afterSplashScreen() {
        Utility.createAppPackages(this);
        if (user.secret_key == null || user.secret_key.equals("")) {
            Intent i = new Intent(SplashScreenActivity.this, PhoneAuthenticationActivity.class);
//            Intent i = new Intent(SplashScreenActivity.this, RegistrationActivity.class);
            startActivity(i);
            finish();
        } else {
            Log.i("FamilyG", "Is profile created? " + String.valueOf(user.isprofilecreated()));
            if (!user.isprofilecreated()) {
                Intent i = new Intent(SplashScreenActivity.this, PersonalProfileActivity.class);
                i.putExtra("type", "signup");
                startActivity(i);
                finish();
            } else {
                //Go to login activity
                Intent i = new Intent(SplashScreenActivity.this, BottomBarActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    private void updateVectorCompat() {
        Log.d("updateVectorCompat", "updateVectorCompat");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void checkPermission() {
        int result_wes = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
//        int result_ra = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        if (result_wes == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getApplicationContext(), "onRequestPermissionsResult checkPermission = true", Toast.LENGTH_SHORT).show();
            twoMinsThread();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, WES_REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        Toast.makeText(getApplicationContext(), "onRequestPermissionsResult requestCode = " + requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case WES_REQUEST_CODE:
//                Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults.length = " + grantResults.length, Toast.LENGTH_SHORT).show();
                if (grantResults.length > 0) {
//                    Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults[0] = " + grantResults[0], Toast.LENGTH_SHORT).show();
                    boolean wesPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (wesPermissionGranted) {
                        afterSplashScreen();
                    } else {
                        Toast.makeText(getApplicationContext(), "You need to grant permissions", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}