package org.officeg.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class LikeAdapter extends BaseAdapter {

    List<LikeModel> listvalues;
    LayoutInflater inflater;
    Context context;

    UserModel user = null;

    public LikeAdapter(Context context, List<LikeModel> listvalues) {
        this.listvalues = listvalues;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position)

    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LikeAdapter.MyViewHolder viewholder;
        try {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.commentlist_list_item, parent, false);
                viewholder = new LikeAdapter.MyViewHolder(convertView);
                convertView.setTag(viewholder);
            } else {
                viewholder = (LikeAdapter.MyViewHolder) convertView.getTag();
            }
            LikeModel likeModel = listvalues.get(position);
            viewholder.comment_display_name.setText(likeModel.getName());

            CircleImageView circleImageView = viewholder.circleImageView;
            if (likeModel.getProfileImage() != null && !likeModel.getProfileImage().trim().isEmpty()) {
                Utility.showProfilePicture(likeModel.getProfileImage(), context, circleImageView, likeModel.getName(), true);
            } else {
                circleImageView.setImageResource(R.drawable.ic_person);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //user = UserModel.getInstance();
        //user.fid = notification.senderid;
        //user.secret_key = notification.secretkey;

        return convertView;
    }

    private class MyViewHolder {
        TextView comment_display_name, comment_activedays, comment;
        CircleImageView circleImageView;

        public MyViewHolder(View item) {
            comment_display_name = (TextView) item.findViewById(R.id.comment_display_name);
            circleImageView = (CircleImageView) item.findViewById(R.id.comment_profile);
        }
    }
}
