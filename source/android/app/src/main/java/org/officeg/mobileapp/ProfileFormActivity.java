package org.officeg.mobileapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.profile.RelationshipType;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFormActivity extends AppCompatActivity {
    static final int DATE_DIALOG_ID = 1;
    static final int DATE_DIALOG_ID2 = 2;
    private static final int REQUEST_PICK_CONTACT = 1000;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 300;
    UserModel user = null;
    EditText edtxtphoneNumber, edtxtFirstName, edtxtFID, edtxtdate, edtxtdeathdate, edtxtdisplayname, edtxtlastname;
    Spinner doi;
    String phoneNumber, firstName, displayname, selectdoi, type, gender;
    Button btnSendInvite;
    String phoneNo = null, res;
    String name = null;
    CircleImageView imageView;
    ImageButton btnPreviousPage, pickImage;
    ProfileModel profile = null;
    Switch livingSwitch;
    TextInputLayout deathdate;
    TextInputLayout mobilenumber;
    TextInputLayout dob;
    TextInputLayout firstnametxtinput;
    TextInputLayout lastnametxtinput;
    TextInputLayout fidlayout;
    RelativeLayout rl_relationship;
    byte[] byteArray;
    RadioGroup genderradioGroup;
    ProgressDialog progDailog;
    RelativeLayout relativeLayoutgender, relativeLayoutprofileImage;
    Calendar calendar;
    int cur = 0;
    Toolbar toolbar;
    RadioButton male, female;
    String relType;
    TextView marque;
    TextInputLayout til;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Uri imageUri;
    private int year, month, day;
    boolean isValid;
    Phonenumber.PhoneNumber number;
    PhoneNumberUtil util = PhoneNumberUtil.getInstance();
    Spinner relationshipSpinner;
    private String TAG = ProfileFormActivity.class.getSimpleName();
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            if (cur == DATE_DIALOG_ID) {
                // set selected date into textview
//                edtxtdate.setText("Date1 : " + new StringBuilder().append(month + 1)
//                        .append("-").append(day).append("-").append(year)
//                        .append(" "));

                edtxtdate.setText(new StringBuilder().append(day).append("/")
                        .append(month + 1).append("/").append(year));
            } else {
//                edtxtdeathdate.setText("Date2 : " + new StringBuilder().append(month + 1)
//                        .append("-").append(day).append("-").append(year)
//                        .append(" "));

                edtxtdeathdate.setText(new StringBuilder().append(day).append("/")
                        .append(month + 1).append("/").append(year));
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_form);
//        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if (!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        user = UserModel.getInstance();
        profile = new ProfileModel();
        profile.setIsalive(true);

        toolbar = findViewById(R.id.toolbartop);
        toolbar.setTitleTextColor(Color.WHITE);
        marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        displayrelationship = (TextView) findViewById(R.id.txtview_relation);
//
//        displayrelationship.setText(String.valueOf(Utility.selectedRelationType));

//        source = (TextView) findViewById(R.id.txtview_source);

        imageView = findViewById(R.id.profile_image_id);
        relativeLayoutprofileImage = findViewById(R.id.profile_layout);
        pickImage = findViewById(R.id.imageButton_picture);

        deathdate = findViewById(R.id.input_layout_deathdate);
        livingSwitch = findViewById(R.id.living_switch);

        edtxtdate = findViewById(R.id.date_id);
        edtxtdeathdate = findViewById(R.id.death_date_id);
        genderradioGroup = findViewById(R.id.radioGroup_gender);
        mobilenumber = findViewById(R.id.input_layout_mobnumber);

        relativeLayoutgender = findViewById(R.id.relativeLayout_gender);
        firstnametxtinput = findViewById(R.id.input_layout_firstname);
        lastnametxtinput = findViewById(R.id.input_layout_lastname);
        male = findViewById(R.id.male_radio_btn);
        female = findViewById(R.id.female_radio_btn);
        relType = String.valueOf(Utility.selectedRelationType);

        doi = findViewById(R.id.degree_of_interest_id);

//        txtViewoption = (TextView) findViewById(R.id.txtview_connectwith);
//        edtxtdate.addTextChangedListener(dobtextwatcher);
//        edtxtdeathdate.addTextChangedListener(deathdatetextwatcher);
        dob = findViewById(R.id.input_layout_dob);


        Intent i = getIntent();
        type = i.getStringExtra("type");

        edtxtphoneNumber = findViewById(R.id.phone_number_id);
        edtxtFirstName = findViewById(R.id.firstname_id);
        edtxtFID = findViewById(R.id.fid_id);
//        relationshipSpinner = (Spinner) findViewById(R.id.spinner_relationship_id);
//        radioGroup = (RadioGroup) findViewById(R.id.radioGroup_id);
        btnSendInvite = findViewById(R.id.btn_send_invite);

        phoneNumber = edtxtphoneNumber.getText().toString();
        firstName = edtxtFirstName.getText().toString();

        edtxtdisplayname = findViewById(R.id.display_name_id);
        edtxtlastname = findViewById(R.id.lastname_id);
        til = findViewById(R.id.input_layout_dispname);
        fidlayout = findViewById(R.id.input_layout_fid);
        relationshipSpinner = findViewById(R.id.spinner_relationship);
        rl_relationship = findViewById(R.id.rl_relationship);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
//        showDate(year, month+1, day);

        if (type != null && type.equals("1")) {
            pickContact();
            mobilenumber.setVisibility(View.VISIBLE);
            fidlayout.setVisibility(View.GONE);
//            txtViewoption.setText("Connect with ");
            marque.setText("Connect a relation " + String.valueOf(Utility.selectedRelationType) + " for " + String.valueOf(Utility.selectedSource));
            marque.setSelected(true);
            profile.setType("1");
            profile.fid = user.fid;
            profile.isowner = true;
            if (String.valueOf(Utility.selectedRelationType).equalsIgnoreCase(getString(R.string.relation))) {
                rl_relationship.setVisibility(View.VISIBLE);
            }
        } else if (type != null && type.equals("2")) {
            createManually();
            til.setVisibility(View.GONE);
            firstnametxtinput.setVisibility(View.GONE);
            lastnametxtinput.setVisibility(View.GONE);
            btnSendInvite.setText("Connect");
            marque.setText("Connect a relation " + String.valueOf(Utility.selectedRelationType) + " for " + String.valueOf(Utility.selectedSource));
            marque.setSelected(true);
            profile.setType("2");
            profile.fid = user.fid;
            profile.isowner = true;
        } else if (type != null && type.equals("4")) {
            createManually();
            doi.setVisibility(View.VISIBLE);
            dob.setVisibility(View.VISIBLE);
            relativeLayoutgender.setVisibility(View.VISIBLE);
            livingSwitch.setVisibility(View.VISIBLE);
            fidlayout.setVisibility(View.GONE);
//            txtViewoption.setText("Create with ");
            marque.setText("Create a relation " + String.valueOf(Utility.selectedRelationType) + " for " + String.valueOf(Utility.selectedSource));
            marque.setSelected(true);
            btnSendInvite.setText("Create Relation");
            profile.setType("4");
            if (Utility.selectedFid == null)
                Utility.selectedFid = UserModel.getInstance().fid;
            profile.fid = Utility.selectedFid;
            pickImage.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            relativeLayoutprofileImage.setVisibility(View.VISIBLE);
            profile.isowner = false;
            // profile.ownerfid = user.fid;
            if (String.valueOf(Utility.selectedRelationType).equalsIgnoreCase(getString(R.string.relation))) {
                rl_relationship.setVisibility(View.VISIBLE);
            }
        }

        createManually();
        profileImage();

//        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                this, R.layout.spinner_item, getResources().getStringArray(R.array.relationship_array)) {
//
//            @Override
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View view = super.getDropDownView(position, convertView, parent);
//                TextView tv = (TextView) view;
//                return view;
//            }
//        };
//        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//        relationshipSpinner.setAdapter(spinnerArrayAdapter);
//        relationshipSpinner.setSelection(Utility.selectedRelationType.ordinal());
//
//        relationshipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Utility.logFA(mFirebaseAnalytics, "profileform screen", "relationship spinner", "spinner");
//                String selectedItemText = (String) parent.getItemAtPosition(position);
//                // If user change the default selection
//                // First item is disable and it is used for hint
//                Log.i("FamilyG","sele: "+selectedItemText);
//                Utility.selectedRelationType = RelationshipType.valueOf(relationshipSpinner.getSelectedItem().toString().toUpperCase());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//
//            }
//        });

        edtxtdisplayname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtxtdisplayname.setCursorVisible(true);
                til.setError(null);

            }
        });

        edtxtFID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtxtFID.setCursorVisible(true);
                fidlayout.setError(null);

            }
        });

        setCurrentDateOnView();
        addListenerOnButton();

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, getResources().getStringArray(R.array.relationship_array)) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        relationshipSpinner.setAdapter(spinnerArrayAdapter);
        relationshipSpinner.setSelection(Utility.selectedRelationType.ordinal());

        relationshipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utility.logFA(mFirebaseAnalytics, "profileform screen", "relationship spinner", "spinner");
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                String relationship = relationshipSpinner.getSelectedItem().toString().toUpperCase();
                relationship = relationship.replaceAll("\\s+", "");
                Utility.selectedRelationType = RelationshipType.valueOf(relationship);
                Log.i("FamilyG", "selected: " + selectedItemText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setCurrentDateOnView() {

//        tvDisplayDate = (TextView) findViewById(R.id.tvDate);
//        tvDisplayDate2 = (TextView) findViewById(R.id.tvDate2);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
//        edtxtdate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(month + 1).append("-").append(day).append("-")
//                .append(year).append(" "));

        edtxtdeathdate.setText(edtxtdate.getText().toString());
    }

    public void addListenerOnButton() {
        edtxtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        edtxtdeathdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID2);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case DATE_DIALOG_ID:
                System.out.println("onCreateDialog  : " + id);
                cur = DATE_DIALOG_ID;
                // set date picker as current date
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileFormActivity.this, R.style.MyTheme, datePickerListener, year, month, day);

                //to set the dialog to display in center of page
                Window window = datePickerDialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                return datePickerDialog;

            case DATE_DIALOG_ID2:
                cur = DATE_DIALOG_ID2;
                System.out.println("onCreateDialog2  : " + id);
                // set date picker as current date
                DatePickerDialog datePickerDialog1 = new DatePickerDialog(ProfileFormActivity.this, R.style.MyTheme, datePickerListener, year, month, day);

                //to set the dialog to display in center of page
                Window window1 = datePickerDialog1.getWindow();
                window1.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                window1.setGravity(Gravity.CENTER);
                return datePickerDialog1;

        }
        return null;
    }

    public void profileImage() {

        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "profileform screen", "pickimage", "button");
                selectImage();
            }
        });

    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFormActivity.this);
        builder.setTitle("Add Profile Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    try {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = Utility.createImageFile(ProfileFormActivity.this);
                        imageUri = Utility.getUriFromFile(getApplicationContext(), f);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            Bitmap scaled = null;
            final Dialog dialog = new Dialog(ProfileFormActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
            dialog.setContentView(R.layout.profilepic_confimation);
            Button dialogBlockButtonOk = dialog.findViewById(R.id.dialogbuttonok);
            Button dialogBlockButtonCancel = dialog.findViewById(R.id.dialogbuttoncancel);
            ImageView image = dialog.findViewById(R.id.confirm_image);
            if (requestCode == 1) {
                try {
                    bitmap = Utility.decodeSampledBitmapFromResource(imageUri, 200, 200, ProfileFormActivity.this);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    image.setImageBitmap(scaled);
                    final Bitmap finalScaled = scaled;
                    dialogBlockButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imageView.setImageBitmap(finalScaled);
                            dialog.dismiss();
                        }
                    });

                    dialogBlockButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    byteArray = stream.toByteArray();
                    user.profileimage = byteArray;
                    UserModel.getInstance().save();
                    profile.profileimage = byteArray;
                    Log.i("FamilyG", "Image size : " + byteArray.length);
                    /*String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(imageStream);
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Log.i("FamilyG", "bitmap is " + scaled);
                    image.setImageBitmap(scaled);
                    final Bitmap finalScaled = scaled;
                    dialogBlockButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imageView.setImageBitmap(finalScaled);
                            dialog.dismiss();
                        }
                    });
                    dialogBlockButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    byteArray = stream.toByteArray();
                    profile.profileimage = byteArray;
                    // imageView.setImageBitmap(selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PICK_CONTACT) {
                contactPicked(data);
            }
        }
        if (resultCode == RESULT_CANCELED && requestCode == REQUEST_PICK_CONTACT) {
            finish();
        }
    }


    public void pickContact() {
        Utility.logFA(mFirebaseAnalytics, "profileform screen", "pickcontact", "contact");

//        checkPermission();
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, REQUEST_PICK_CONTACT);
    }

    private void contactPicked(Intent data) {
        Cursor cursor;
        try {

            Uri uri = data.getData();
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex).replaceAll("[()\\s-]+", "");

            TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            String usersCountryISOCode = manager.getSimCountryIso().toUpperCase();

            Log.i("FamilyG", "Country code : " + usersCountryISOCode);
            Log.i("FamilyG", "Original number : " + phoneNo);

            Phonenumber.PhoneNumber phone = PhoneNumberUtil.getInstance().parseAndKeepRawInput(phoneNo, usersCountryISOCode.toUpperCase());

            Log.i("FamilyG", "Extracted number : " + phone.getCountryCode());

            name = cursor.getString(nameIndex);
            edtxtFirstName.setText(name);
            edtxtphoneNumber.setText(PhoneNumberUtil.getInstance().format(phone, PhoneNumberUtil.PhoneNumberFormat.E164));
            ((EditText) findViewById(R.id.display_name_id)).setText(name);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void createManually() {
//        btnPreviousPage = (ImageButton) findViewById(R.id.prevoiuspage_btn);
//
//        btnPreviousPage.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent i = new Intent(ProfileFormActivity.this, AddMemberActivity.class);
//                i .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(i);
//                finish();
//            }
//        });
//

        livingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Utility.logFA(mFirebaseAnalytics, "profileform screen", "livingswitch", "switch");

                // do something, the isChecked will be true if the switch is in the On position
                if (isChecked) {
                    deathdate.setVisibility(View.GONE);
                    profile.setIsalive(true);
                    dob.setVisibility(View.VISIBLE);
                } else {
                    deathdate.setVisibility(View.VISIBLE);
                    profile.setIsalive(false);
                    dob.setVisibility(View.GONE);
                }
            }
        });

//        edtxtdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setDate(v);
//            }
//        });


//        relationshipSpinner.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                try {
//                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//                } catch (Exception e) {
//                    // TODO: handle exception
//                }
//                return false;
//            }
//        });


        btnSendInvite = findViewById(R.id.btn_send_invite);
        btnSendInvite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                edtxtFID.setCursorVisible(false);
                edtxtdisplayname.setCursorVisible(false);
                Utility.logFA(mFirebaseAnalytics, "profileform screen", "sendinvite", "button");

                progDailog = new ProgressDialog(ProfileFormActivity.this);
                progDailog.setTitle("Please Wait");
                progDailog.setMessage("Loading...");
                progDailog.setIndeterminate(false);
                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDailog.setCancelable(true);
                progDailog.show();

                String namepattern = (getString(R.string.name_pattern));

                profile.secret_key = user.secret_key;
                displayname = ((EditText) findViewById(R.id.display_name_id)).getText().toString();


                if (edtxtFID.getText().toString().equals("")) {
                    fidlayout.setError("Please provide FGID");
                } else {
                    fidlayout.setError("");
                }

                //connect using fid will display user details by using their fid....
                if ((type.equals("2")) && (!edtxtFID.getText().toString().equals(""))) {
                    String fid = edtxtFID.getText().toString();
                    if (fid.equals(UserModel.getInstance().fid)) {
                        Toast.makeText(getApplicationContext(), getString(R.string.cannotconnectwithown), Toast.LENGTH_LONG).show();
                        progDailog.dismiss();
                        return;
                    }

                    ProfileModel profileModel;
                    profileModel = new ProfileModel();
                    profileModel.fid = fid;

                    profileModel.secret_key = UserModel.getInstance().secret_key;

                    APIManager viewProfile = new APIManager("relationship/viewProfile", profileModel, new APIListener() {
                        @Override
                        public void onSuccess(JSONObject object) {
                            til.setVisibility(View.VISIBLE);
                            firstnametxtinput.setVisibility(View.VISIBLE);
                            lastnametxtinput.setVisibility(View.VISIBLE);
                            try {
                                displayname = object.getString("displayname");
                                profile.displayname = displayname.trim();
                                Log.i("FamilyG", "display name is " + displayname);
                                edtxtdisplayname.setText(displayname);
                                edtxtFirstName.setText(object.getString("firstname"));
                                edtxtlastname.setText(object.getString("lastname"));
                                btnSendInvite.setText("Send invite");
                                if (String.valueOf(Utility.selectedRelationType).equalsIgnoreCase(getString(R.string.relation))) {
                                    rl_relationship.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(JSONArray array) {

                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            fidlayout.setError(errorMessage);
                        }
                    });
                    viewProfile.execute();
                }
                //connect using fid will display user details by using their fid....//
                if (displayname.equals("") || displayname.trim().length() == 0) {
                    edtxtdisplayname.requestFocus();

                    if (!type.equals("2")) {
                        til.setError("Please provide display name");
                    }
                    progDailog.hide();

                } else if ((!(displayname.matches(namepattern))) || (displayname.trim().length() == 0)) {
                    Log.i("FamilyG", "pattern mismatched");
                    edtxtdisplayname.requestFocus();
                    til.setError("Please provide a valid name");
                    progDailog.hide();
                } else {

                    profile.displayname = displayname.trim();
                    profile.status = getString(R.string.pending);
                    Log.i("FamilyG", "display name after editing " + displayname);
                    profile.mobilenumber = phoneNumber;
                    profile.fid = edtxtFID.getText().toString();
                    if (Utility.selectedFid == null || Utility.selectedFid.trim().isEmpty()) {
                        profile.ownerfid = UserModel.getInstance().fid;
                    } else {
                        profile.ownerfid = Utility.selectedFid;
                    }
                    profile.secret_key = user.secret_key;
                    profile.firstname = edtxtFirstName.getText().toString();
                    profile.lastname = edtxtlastname.getText().toString();
                    profile.dob = edtxtdate.getText().toString();
                    profile.deceaseddate = edtxtdeathdate.getText().toString();
                    profile.doi = selectdoi = ((Spinner) findViewById(R.id.degree_of_interest_id)).getSelectedItem().toString().replaceAll("[Selct DgrofIns:()\\s-]+", "");

                    String relType = String.valueOf(Utility.selectedRelationType).toUpperCase();
                    if (relType.equals("MOTHER") || relType.equals("DAUGHTER") || relType.equals("SISTER") ||
                            relType.equals("WIFE") || relType.equals("NIECE") || relType.equals("GRANDMOTHER") || relType.equals("AUNT") || relType.equals("GRANDDAUGHTER")) {
                        male.setChecked(false);
                        female.setChecked(true);
                    } else {
                        male.setChecked(true);
                        female.setChecked(false);
                    }

                    int selectedId = (genderradioGroup.getCheckedRadioButtonId());
                    RadioButton radioButton = findViewById(selectedId);
                    profile.gender = radioButton.getText().toString();
                    profile.setIsalive(((Switch) findViewById(R.id.living_switch)).isChecked());
                    Log.i("FamilyG", "PPPP: " + profile.isalive);

                    if (type.equals("4")) {
                        profile.type = "3";
                        profile.fid = "";
                        profile.countrycode = "";
                    } else {
                        profile.type = type;
                    }

                    Log.i("FamilyG", "Selected Gender: " + profile.gender + "Selected DOI: " + profile.doi + "Alive: " + profile.isalive + "Type: " + profile.type);
                    try {
                        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        String usersCountryISOCode = manager.getSimCountryIso();
                        Phonenumber.PhoneNumber number = null;
                        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
                        if (phoneNo != null) {
                            number = util.parseAndKeepRawInput(edtxtphoneNumber.getText().toString(), usersCountryISOCode.toUpperCase());
                            profile.setMobilenumber("" + number.getNationalNumber());
                        } else {
                            profile.mobilenumber = phoneNumber;
                        }
                        if (profile.mobilenumber.equals(UserModel.getInstance().mobilenumber)) {
                            Toast.makeText(getApplicationContext(), getString(R.string.cannotconnectwithown), Toast.LENGTH_LONG).show();
                            progDailog.dismiss();
                            return;
                        }

                        profile.setDisplayname(profile.displayname);
                        profile.setRelationship(String.valueOf(Utility.selectedRelationType));
                        assert number != null;

                        for (String r : util.getSupportedRegions()) {
                            try {
                                // check if it's a possible number
                                isValid = util.isPossibleNumber(edtxtphoneNumber.getText().toString(), r);
                                if (isValid) {
                                    number = util.parse(edtxtphoneNumber.getText().toString(), r);

                                    // check if it's a valid number for the given region
                                    isValid = util.isValidNumberForRegion(number, r);
                                    if (isValid)

                                        Log.i("countryCode", number.getCountryCode() + ", " + number.getNationalNumber());
                                }
                            } catch (NumberParseException e) {
                                e.printStackTrace();
                            }
                        }

                        profile.setCountrycode("" + number.getCountryCode());
                        Log.i("FamilyG", "Relationship : " + profile.getRelationship());
                    } catch (Exception e) {
                        Log.i("FamilyG", "Error : " + e.getMessage());
                    }

                    if (profile.profileimage == null) {
                        createProfile();
                    } else {
                        uploadProfileImage();
                    }
                }

                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        });

    }

    public void uploadProfileImage() {
        APIManager uploadapi = new APIManager("upload", profile.profileimage, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {

                    profile.profileimageurl = result.getString("url");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                createProfile();

            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                progDailog.dismiss();

            }
        }, APIManager.HTTP_METHOD.FILE);
        uploadapi.execute();
    }

    public void createProfile() {

        String userprofile = profile.toJSON();
        final byte[] profileModel = userprofile.getBytes(Charset.forName("UTF-8"));
        Log.i("FamilyG", "profileformimage" + userprofile);
        APIManager api = new APIManager("relationship/connect", profileModel, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {
                    Toast.makeText(getApplicationContext(), getString(R.string.connection_request_sent), Toast.LENGTH_LONG).show();
                    profile.fid = result.getString("fid");
                    Log.i("FamilyG fid", profile.fid);
                    profile.setRelationship(String.valueOf(Utility.selectedRelationType));
                    if (!profile.isowner) {
                        profile.setStatus(getString(R.string.approved));
                    }
                    UserModel.getInstance().profileMemberModel.AddMember(Utility.selectedFid, profile);
//                        Log.i("selected Fid: ", Utility.selectedFid);
//                        Log.i("selected relation: ", String.valueOf(Utility.selectedRelationType));
//                                Toast.makeText(getApplicationContext(), "invitation has been sent", Toast.LENGTH_LONG).show();

                    if (!profile.isowner && type != null && type.equals("4")) { // Check if the profile is owned by logged in user
                        if (UserModel.getInstance().ownedProfileList.indexOf(profile) == -1) {
                            UserModel.getInstance().ownedProfileList.add(profile);
//                            UserModel.getInstance().loadMembers(profile.fid);
                        }
                    }


                    Intent intent_activity = new Intent(ProfileFormActivity.this, AddMemberActivity.class);
                    intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_activity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progDailog.dismiss();
            }

            @Override
            public void onSuccess(JSONArray array) {
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                progDailog.dismiss();

            }
        }, APIManager.HTTP_METHOD.POST);

        api.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ProfileFormActivity.this, AddMemberActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("FamilyG", "Relationship: " + relType);
        if (relType.equals("MOTHER") || relType.equals("DAUGHTER") || relType.equals("SISTER") ||
                relType.equals("WIFE") || relType.equals("NIECE") || relType.equals("GRANDMOTHER") || relType.equals("AUNT") || relType.equals("GRANDDAUGHTER")) {
            male.setChecked(false);
            male.setEnabled(false);
            female.setChecked(true);
        } else if (relType.equals("COUSIN")) {
            male.setChecked(true);
            female.setChecked(false);
        } else if (relType.equalsIgnoreCase(getString(R.string.relation))) {
            male.setEnabled(true);
            female.setEnabled(true);
            male.setChecked(true);
            female.setChecked(false);
        } else {
            male.setChecked(true);
            female.setChecked(false);
            female.setEnabled(false);
        }
    }
}