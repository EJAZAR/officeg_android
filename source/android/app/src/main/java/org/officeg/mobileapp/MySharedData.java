package org.officeg.mobileapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.officeg.model.UserModel;

public class MySharedData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shared_data);
        UserModel userModel = UserModel.getInstance();
        EditText edtxtdisplayname = (EditText) findViewById(R.id.display_name_id);
        edtxtdisplayname.setText(userModel.getDisplayname());

        RadioButton male = (RadioButton) findViewById(R.id.male_radio_btn);
        RadioButton female = (RadioButton) findViewById(R.id.female_radio_btn);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        TextView marque = (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.my_shared_data);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_share_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        if (userModel.gender != null && (userModel.gender).equals("Male")) {
            male.setChecked(true);
            female.setChecked(false);
        } else if (userModel.gender != null && (userModel.gender).equals("Female")) {
            female.setChecked(true);
            male.setChecked(false);
        }

        if (userModel.firstname != null && !userModel.firstname.trim().isEmpty()) {
            RelativeLayout relFirstname = (RelativeLayout) findViewById(R.id.relFirstname);
            relFirstname.setVisibility(View.VISIBLE);
            EditText firstname_id = (EditText) findViewById(R.id.firstname_id);
            firstname_id.setText(userModel.firstname);
        }

        if (userModel.lastname != null && !userModel.lastname.trim().isEmpty()) {
            RelativeLayout relLastname = (RelativeLayout) findViewById(R.id.relLastname);
            relLastname.setVisibility(View.VISIBLE);
            EditText lastname_id = (EditText) findViewById(R.id.lastname_id);
            lastname_id.setText(userModel.lastname);
        }

        if (userModel.mobilenumber != null && !userModel.mobilenumber.trim().isEmpty()) {
            RelativeLayout mobilenumber_layout = (RelativeLayout) findViewById(R.id.mobilenumber_layout);
            mobilenumber_layout.setVisibility(View.VISIBLE);
            EditText mobilenumber_id = (EditText) findViewById(R.id.mobilenumber_id);
            mobilenumber_id.setText(userModel.mobilenumber);
        }

        if (userModel.dob != null && !userModel.dob.trim().isEmpty()) {
            RelativeLayout relDob = (RelativeLayout) findViewById(R.id.relDob);
            relDob.setVisibility(View.VISIBLE);
            EditText date_id = (EditText) findViewById(R.id.date_id);
            date_id.setText(userModel.dob);
        }

        Spinner doiSpinner = (Spinner) findViewById(R.id.degree_of_interest_id);

        if (userModel.doi != null && !userModel.doi.trim().isEmpty()) {
            try {
                int doiPosition = Integer.parseInt(userModel.doi);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.doi, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                doiSpinner.setAdapter(adapter);
                if (doiPosition == 0) {
                    doiSpinner.setSelection(3);
                } else if (doiPosition > 0 && doiPosition < 8) {
                    doiSpinner.setSelection(doiPosition);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();

            }

        }

    }
}
