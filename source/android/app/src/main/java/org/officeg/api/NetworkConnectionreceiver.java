package org.officeg.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkConnectionreceiver extends BroadcastReceiver {
    public static boolean isConnection = false;

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        Log.d("doInBackground", "isConnected = " + isConnected);
        return isConnected;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("doInBackground", "onReceive before= " + isConnection);
        //isConnection = isConnected(context);
        method_IsConnected(context);
    }

    private void method_IsConnected(Context context) {
        isConnection = isConnected(context);
    }
}