package org.officeg.chat;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;
import org.officeg.mobileapp.R;
import org.officeg.model.ChatModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.util.LinkedList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class ChatAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private LinkedList<ChatModel> chatModels;
    private Activity superActivity;

    public ChatAdapter(Activity activity) {
        superActivity = activity;
        chatModels = new LinkedList<ChatModel>();
        inflater = (LayoutInflater) activity
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chatModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clearAll() {
        chatModels = new LinkedList<ChatModel>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        try {
            ChatModel chatModel = chatModels.get(position);
            vi = inflater.inflate(R.layout.chat_favourite_list_adaptor, null);

            if (chatModel.getType().equalsIgnoreCase(superActivity.getString(R.string.favourite))) {
                TextView msg = vi.findViewById(R.id.chat_bubble_textview);
                msg.setText(chatModel.getProfileModel().getDisplayname());
                Integer unReadCount = chatModel.getUnreadCount();
                if (unReadCount != null && unReadCount != 0) {
                    TextView unreadCountTextView = vi.findViewById(R.id.chat_fav_list_unreadcount);
                    unreadCountTextView.setVisibility(View.VISIBLE);
                    unreadCountTextView.setText(String.valueOf(unReadCount));
                }
                ProfileModel profileModel = chatModel.getProfileModel();
                CircleImageView circleImageView = vi.findViewById(R.id.chat_fav_list_profile);
                if (profileModel.getProfileimageurl() != null && !profileModel.getProfileimageurl().trim().isEmpty()) {
                    Utility.showProfilePicture(profileModel.getProfileimageurl(), superActivity, circleImageView, profileModel.getDisplayname(), true);
                } else {
                    circleImageView.setImageResource(R.drawable.ic_person);
                }
                String chatMessage = chatModel.getMsg();
                if (chatMessage != null) {
                    Log.d("chatMessage", chatMessage);
                    JSONObject msgJsonObject = new JSONObject(chatMessage);

                    LinearLayout ll_lastchatcontent = vi.findViewById(R.id.ll_lastchatcontent);
                    if (msgJsonObject.has("reminder_mid")) {
                        ImageView imageImageView = new ImageView(superActivity);
                        imageImageView.setImageResource(R.drawable.ic_alarm);
                        ll_lastchatcontent.addView(imageImageView);

                        TextView imageTextView = new TextView(superActivity);
                        imageTextView.setText(" Reminder");
                        ll_lastchatcontent.addView(imageTextView);
                    } else {
                        String extraType = msgJsonObject.getString("extraType");
                        if (extraType.equalsIgnoreCase("text")) {
                            String content = msgJsonObject.getString("content");
                            content = content.length() > 30 ? content.substring(0, 30) + "..." : content;

                            TextView contentTextView = new TextView(superActivity);
                            contentTextView.setText(content);
                            contentTextView.setSingleLine();
                            ll_lastchatcontent.addView(contentTextView);
                        } else if (extraType.equalsIgnoreCase("image")) {
                            ImageView imageImageView = new ImageView(superActivity);
                            imageImageView.setImageResource(R.drawable.ic_camera_black_small);
                            ll_lastchatcontent.addView(imageImageView);

                            TextView imageTextView = new TextView(superActivity);
                            imageTextView.setText(" Image");
                            ll_lastchatcontent.addView(imageTextView);
                        } else if (extraType.equalsIgnoreCase("video")) {
                            ImageView videoImageView = new ImageView(superActivity);
                            videoImageView.setImageResource(R.drawable.ic_videocam_black_small);
                            ll_lastchatcontent.addView(videoImageView);

                            TextView videoTextView = new TextView(superActivity);
                            videoTextView.setText(" Video");
                            ll_lastchatcontent.addView(videoTextView);
                        } else if (extraType.equalsIgnoreCase("audio")) {
                            ImageView audioImageView = new ImageView(superActivity);
                            audioImageView.setImageResource(R.drawable.ic_mic_black_small);
                            ll_lastchatcontent.addView(audioImageView);

                            TextView audioTextView = new TextView(superActivity);
                            audioTextView.setText(" Audio");
                            ll_lastchatcontent.addView(audioTextView);
                        }
                    }
                }
            } else if (chatModel.getType().equalsIgnoreCase(superActivity.getString(R.string.group))) {
                TextView msg = vi.findViewById(R.id.chat_bubble_textview);
                msg.setText(chatModel.getGroupModel().getName());
                Integer unReadCount = chatModel.getUnreadCount();
                if (unReadCount != null && unReadCount != 0) {
                    TextView unreadCountTextView = vi.findViewById(R.id.chat_fav_list_unreadcount);
                    unreadCountTextView.setVisibility(View.VISIBLE);
                    unreadCountTextView.setText(String.valueOf(unReadCount));
                }

                CircleImageView circleImageView = vi.findViewById(R.id.chat_fav_list_profile);
                String groupIcon = chatModel.getGroupModel().getUrl();
                if (groupIcon != null && !groupIcon.isEmpty()) {
                    Utility.showProfilePicture(groupIcon, superActivity, circleImageView, chatModel.getGroupModel().getName(), true);
                } else {
                    circleImageView.setImageResource(R.drawable.ic_group);
                }
                String chatMessage = chatModel.getMsg();
                if (chatMessage != null) {
                    Log.d("chatMessage", chatMessage);
                    JSONObject msgJsonObject = new JSONObject(chatMessage);

                    TextView displayNameTextView = new TextView(superActivity);
                    displayNameTextView.setSingleLine();
                    displayNameTextView.setEllipsize(TextUtils.TruncateAt.END);

                    LinearLayout ll_lastchatcontent = vi.findViewById(R.id.ll_lastchatcontent);
                    ll_lastchatcontent.addView(displayNameTextView);

                    if (msgJsonObject.has("reminder_mid")) {
                        ImageView imageImageView = new ImageView(superActivity);
                        imageImageView.setImageResource(R.drawable.ic_alarm);
                        ll_lastchatcontent.addView(imageImageView);

                        TextView imageTextView = new TextView(superActivity);
                        imageTextView.setText(" Reminder");
                        ll_lastchatcontent.addView(imageTextView);
                    } else {
                        String displayName;
                        String fid = msgJsonObject.getString("fid");

                        if (fid.equalsIgnoreCase(UserModel.getInstance().fid)) {
                            displayName = "Me";
                        } else {
                            displayName = Utility.profileModelMap.get(fid).getDisplayname();
                        }
                        displayName = displayName.length() > 10 ? displayName.substring(0, 10) + "..." : displayName;

                        String extraType = msgJsonObject.getString("extraType");
                        if (!extraType.equals(superActivity.getString(R.string.groupextramessage))) {
                            displayNameTextView.setText(displayName + " : ");
                        } else {
                            displayNameTextView.setText(msgJsonObject.getString("extraString"));
                        }
                        if (extraType.equalsIgnoreCase("text")) {
                            String content = msgJsonObject.getString("content");
                            content = content.length() > 17 ? content.substring(0, 17) + "..." : content;
                            TextView contentTextView = new TextView(superActivity);
                            contentTextView.setText(content);
                            contentTextView.setSingleLine();
                            ll_lastchatcontent.addView(contentTextView);
                        } else if (extraType.equalsIgnoreCase("image")) {
                            ImageView imageImageView = new ImageView(superActivity);
                            imageImageView.setImageResource(R.drawable.ic_camera_black_small);
                            ll_lastchatcontent.addView(imageImageView);

                            TextView imageTextView = new TextView(superActivity);
                            imageTextView.setText(" Image");
                            ll_lastchatcontent.addView(imageTextView);
                        } else if (extraType.equalsIgnoreCase("video")) {
                            ImageView videoImageView = new ImageView(superActivity);
                            videoImageView.setImageResource(R.drawable.ic_videocam_black_small);
                            ll_lastchatcontent.addView(videoImageView);

                            TextView videoTextView = new TextView(superActivity);
                            videoTextView.setText("Video");
                            ll_lastchatcontent.addView(videoTextView);
                        } else if (extraType.equalsIgnoreCase("audio")) {
                            ImageView audioImageView = new ImageView(superActivity);
                            audioImageView.setImageResource(R.drawable.ic_mic_black_small);
                            ll_lastchatcontent.addView(audioImageView);

                            TextView audioTextView = new TextView(superActivity);
                            audioTextView.setText("Audio");
                            ll_lastchatcontent.addView(audioTextView);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

    public void add(ChatModel chatModel) {
        chatModels.add(chatModel);
    }
}
