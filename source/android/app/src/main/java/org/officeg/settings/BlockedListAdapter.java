package org.officeg.settings;


import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;
import org.officeg.notification.MessageListener;
import org.officeg.notification.MessageModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BlockedListAdapter extends BaseAdapter implements MessageListener {

    ArrayList listvalues = new ArrayList();
    LayoutInflater inflater;
    Context context;
    UserModel user = null;
    byte[] bytelistcomment, bytelistlike, byteblock;
    private FirebaseAnalytics mFirebaseAnalytics;

    public BlockedListAdapter(Context context, ArrayList listvalues) {
        this.listvalues = listvalues;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public BlockedListModel getItem(int position) {

        return (BlockedListModel) listvalues.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("FamilyG", "creating postview: ");
        BlockedListModel blockedListModel = (BlockedListModel) listvalues.get(position);
        BlockedListAdapter.MyViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.blockedlist, parent, false);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            viewholder = new BlockedListAdapter.MyViewHolder(convertView, blockedListModel);
            viewholder.addListener(this);

            convertView.setTag(viewholder);
        } else {
            viewholder = (BlockedListAdapter.MyViewHolder) convertView.getTag();
        }

        //viewholder.setFeedModel(blockedListModel);
        return convertView;
    }

    @Override
    public void removeMessage(MessageModel messageModel) {

    }

    @Override
    public void removeBlock(Object messageModel) {
        listvalues.remove(messageModel);
        notifyDataSetChanged();
    }

    private class MyViewHolder {

        TextView displayname, mobilenumber;
        CircleImageView profileimage;
        Button unblock;
        BlockedListModel itemModel;

        private List<MessageListener> listeners = new ArrayList<MessageListener>();

        public MyViewHolder(View item, final BlockedListModel itemModel) {
            this.itemModel = itemModel;
            displayname = item.findViewById(R.id.blockdisplayname);
            mobilenumber = item.findViewById(R.id.blockmobile);
            profileimage = item.findViewById(R.id.block_image_id);
            unblock = item.findViewById(R.id.unblock);

            displayname.setText(itemModel.getDisplayname());
            mobilenumber.setText(itemModel.getMobilenumber());
            if (itemModel.getProfileimage() != null && !itemModel.getProfileimage().trim().isEmpty()) {
                Utility.showProfilePicture(itemModel.getProfileimage(), context, profileimage, itemModel.getDisplayname(), true);
            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }


            unblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.logFA(mFirebaseAnalytics, "blockedlist screen", "unblock", "button");
                    final Dialog dialog = new Dialog(view.getRootView().getContext());
                    dialog.setContentView(R.layout.unblock_alert);
                    TextView blockmessage = dialog.findViewById(R.id.unblocktext);
                    // blockmessage.setText("Do you want to Unblock this user?");
                    Button dialogBlockButtonyes = dialog.findViewById(R.id.dialogButtonYes);
                    Button dialogBlockButtonno = dialog.findViewById(R.id.dialogButtonNo);
                    dialogBlockButtonyes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject object = new JSONObject();
                            try {
                                object.put("secret_key", UserModel.getInstance().secret_key);
                                object.put("fid", itemModel.getFid());
                                Log.i("jobjblock", String.valueOf(object));
                                byteblock = object.toString().getBytes(Charset.forName("UTF-8"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            APIManager api = new APIManager("relationship/unblockuser", byteblock, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject result) {
                                    raiseRemoveEvent();

                                }

                                @Override
                                public void onSuccess(JSONArray object) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();

                                }
                            }, APIManager.HTTP_METHOD.POST);

                            api.execute();
                            dialog.dismiss();

                        }
                    });

                    dialogBlockButtonno.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });


                    dialog.show();
                }
            });

        }

        public void addListener(MessageListener toAdd) {
            listeners.add(toAdd);
        }

   /*     public void setFeedModel(final BlockedListModel itemModel) {
            this.itemModel = itemModel;
            displayname.setText(itemModel.getDisplayname());
            mobilenumber.setText(itemModel.getMobilenumber());
            if (itemModel.getProfileimage() != null && !itemModel.getProfileimage().trim().isEmpty()) {
                ImageLoader imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(itemModel.getProfileimage(), profileimage);

            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }


            unblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.unblock_alert);
                    TextView blockmessage = (TextView) dialog.findViewById(R.id.unblocktext);
                    Button dialogBlockButton = (Button) dialog.findViewById(R.id.dialogButtonUnBlock);
                    dialogBlockButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject object = new JSONObject();
                            try {
                                object.put("secret_key", UserModel.getInstance().secret_key);
                                object.put("fid", itemModel.getFid());
                                Log.i("jobjblock", String.valueOf(object));
                                byteblock = object.toString().getBytes();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            APIManager api = new APIManager("relationship/unblockuser", byteblock, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject result) {


                                }

                                @Override
                                public void onSuccess(JSONArray object) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {

                                }
                            }, APIManager.HTTP_METHOD.POST);

                            api.execute();
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            });

        }*/

        void raiseRemoveEvent() {
            //  Log.i("FamilyG",itemModel.getSenderid());
            // Notify everybody that may be interested.
            for (MessageListener hl : listeners)
                hl.removeBlock(itemModel);
        }
    }
}

