package org.officeg.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class CommentList extends BaseAdapter {

    List<CommentModel> listvalues;
    LayoutInflater inflater;
    Context context;

    UserModel user = null;

    public CommentList(Context context, List<CommentModel> listvalues) {
        this.listvalues = listvalues;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position)

    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        CommentList.MyViewHolder viewholder;
        try {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.commentlist_list_item, parent, false);
                viewholder = new CommentList.MyViewHolder(convertView);
                convertView.setTag(viewholder);
            } else {
                viewholder = (CommentList.MyViewHolder) convertView.getTag();
            }
            CommentModel commentDetail = listvalues.get(position);
            viewholder.comment_display_name.setText(commentDetail.getName());
            viewholder.comment_activedays.setText(Utility.ElapsedTime(commentDetail.getTimeCommented()));
            JSONObject commentObject = new JSONObject(commentDetail.getComment());
            viewholder.comment.setText(commentObject.getString("content"));

            CircleImageView circleImageView = viewholder.circleImageView;
            if (commentDetail.getProfileImage() != null && !commentDetail.getProfileImage().trim().isEmpty()) {
                Utility.showProfilePicture(commentDetail.getProfileImage(), context, circleImageView, commentDetail.getName(), true);
            } else {
                circleImageView.setImageResource(R.drawable.ic_person);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //user = UserModel.getInstance();
        //user.fid = notification.senderid;
        //user.secret_key = notification.secretkey;

        return convertView;
    }

    private class MyViewHolder {
        TextView comment_display_name, comment_activedays, comment;
        CircleImageView circleImageView;

        public MyViewHolder(View item) {
            comment_display_name = (TextView) item.findViewById(R.id.comment_display_name);
            comment_activedays = (TextView) item.findViewById(R.id.comment_active_days);
            comment = (TextView) item.findViewById(R.id.comment_content);
            comment.setVisibility(View.VISIBLE);
            circleImageView = (CircleImageView) item.findViewById(R.id.comment_profile);
        }
    }
}
