package org.officeg.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.officeg.mobileapp.R;

import java.util.Collections;
import java.util.List;

public class LikesFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View currentView = inflater.inflate(R.layout.activity_comments_total, container, false);

        ListView likeListView = (ListView) currentView.findViewById(R.id.commentlist);

        Bundle extras = this.getArguments();

        FeedItemModel feedItem = (FeedItemModel) extras.get("feedItemModel");
        List<LikeModel> likeList = feedItem.getLikeList();

        if (likeList.size() > 0) {
            Collections.sort(likeList);
            Collections.reverse(likeList);
            LikeAdapter likeAdapter = new LikeAdapter(getContext(), likeList);
            likeListView.setAdapter(likeAdapter);
            ((BaseAdapter) likeListView.getAdapter()).notifyDataSetChanged();
        } else {
            TextView noCommentsTV = (TextView) currentView.findViewById(R.id.empty_notification);
            noCommentsTV.setVisibility(View.VISIBLE);
        }

        LinearLayout comment_edittext = (LinearLayout) currentView.findViewById(R.id.comment_edittext);
        comment_edittext.setVisibility(View.GONE);

        return currentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
