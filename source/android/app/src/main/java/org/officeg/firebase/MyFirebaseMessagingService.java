package org.officeg.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.database.DataManager;
import org.officeg.mobileapp.BottomBarActivity;
import org.officeg.mobileapp.ChatMessageActivity;
import org.officeg.mobileapp.NotificationActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.GroupModel;
import org.officeg.model.UserModel;
import org.officeg.notification.MessageModel;
import org.officeg.util.Utility;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final int POST_ID = 1;
    private static String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private static int notificationCount = 0;

    @Override
    public void onMessageSent(String s) {
        Log.d(TAG, "onMessageSent: " + s);
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        Log.d(TAG, "onSendError: " + s + " and " + e.getLocalizedMessage());
        super.onSendError(s, e);
    }

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "onDeletedMessages");
        super.onDeletedMessages();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        Bundle extras = new Bundle();
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            extras.putString(entry.getKey(), entry.getValue());
        }
        extras.putLong("datetime", remoteMessage.getSentTime());
        Log.d(TAG, "googlesenttime = " + new Date(remoteMessage.getSentTime()).toString());
        String type = extras.getString("type");

        // Post notification of received message.
        try {
            if (type.equalsIgnoreCase("2")) {
                if (BottomBarActivity.isActive) {
                    Log.i(TAG, "Received post.active " + extras.toString());
                    // OK
                    Intent broadcastintent = new Intent("POST_RECEIVED");
                    broadcastintent.putExtra("post", extras);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastintent);
                } else {
                    Log.i(TAG, "Received post.inactive " + extras.toString());
                    extras.putString("screen", "post");
                    sendNotification(extras);
                }
            } else if (type.equalsIgnoreCase("3")) {
                Log.i("FamilyG", "Bottombar active status" + BottomBarActivity.isActive);
                if (NotificationActivity.isActive) {
                    Log.i(TAG, "Received message notificationactivity.active " + extras.toString());
                    // OK
                    Intent broadcastintent = new Intent("MESSAGE_RECEIVED");
                    broadcastintent.putExtra("messageforrequest", extras);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastintent);
                }
                if (!BottomBarActivity.isActive && !NotificationActivity.isActive) {
                    Log.i(TAG, "Received notification.inactive " + extras.toString());
                    extras.putString("screen", "relationship");
                    sendNotification(extras);
                }
                UserModel.getInstance().loadMembers();
            } else if (type.equalsIgnoreCase("0") || type.equalsIgnoreCase("1")) {
                Log.i("FamilyG", "chat message received = " + extras.toString());

                String msgstr = extras.getString("msg");
                String fidOrGid;

                JSONObject msgStrJson = new JSONObject(msgstr);
                if (msgStrJson.has("gid")) {
                    fidOrGid = msgStrJson.getString("gid");
                } else {
                    fidOrGid = msgStrJson.getString("fid");
                }

                extras.putString("typeChat", type);
                extras.putString("fidOrGid", fidOrGid);
                String extraType = msgStrJson.has("extraType") ? msgStrJson.getString("extraType") : "";
                insertChatIntoDB(extras, extraType);
                if (!extraType.equals(getString(R.string.groupextramessage))) {
                    DataManager dataManager = DataManager.getInstance(getApplicationContext());
                    Integer unreadMessageCount = dataManager.getUnreadMessageCount(fidOrGid);
                    extras.putInt("unreadcount", unreadMessageCount);
                }
                extras.putBoolean("isNotificationClicked", true);
                sendNotification(extras);
                Intent chatPageIntent = new Intent("CHATMESSAGE_RECEIVED");
                chatPageIntent.putExtra("chatNotification", extras);
                LocalBroadcastManager.getInstance(this).sendBroadcast(chatPageIntent);

            } else if (type.equalsIgnoreCase("4")) {
                Log.d(TAG, "reminder received = " + extras.toString());

                String reminder_mid = extras.getString("reminder_mid");
                String reminding_fid = extras.getString("reminding_fid");
                DataManager dataManager = DataManager.getInstance(getApplicationContext());
                ChatMessageModel chatMessageModel = dataManager.getMessage(reminder_mid);
                extras.putString("msg", "{\"reminder_mid\":\"" + reminder_mid + "\", \"reminding_fid\":\"" + reminding_fid + "\"}");
                extras.putString("fidOrGid", chatMessageModel.getFid());
                JSONObject msgStrJson = new JSONObject(chatMessageModel.getTotalContent());
                if (msgStrJson.has("gid")) {
                    extras.putString("typeChat", "1");
                } else {
                    extras.putString("typeChat", "0");
                }
                Integer unreadMessageCount = dataManager.getUnreadMessageCount(chatMessageModel.getFid());
                extras.putInt("unreadcount", unreadMessageCount);
                extras.putBoolean("isNotificationClicked", true);

                JSONObject totalContent = new JSONObject(chatMessageModel.getTotalContent());
                insertChatIntoDB(extras, totalContent.getString("extraType"));
                sendNotification(extras);
                Intent chatPageIntent = new Intent("CHATMESSAGE_RECEIVED");
                chatPageIntent.putExtra("chatNotification", extras);
                LocalBroadcastManager.getInstance(this).sendBroadcast(chatPageIntent);
            } else if (type.equalsIgnoreCase("5")) {
                String message_id = extras.getString("message_id");
                String ackmode = extras.getString("ackmode");

                DataManager dataManager = DataManager.getInstance(getApplicationContext());
                ContentValues row = new ContentValues();
                if (ackmode.equalsIgnoreCase("sent")) {
                    row.put("acknowledgementStatus", 1);
                    dataManager.updateTable("chathistory", row, "messageId", message_id);
                } else if (ackmode.equalsIgnoreCase("delivered")) {
                    row.put("acknowledgementStatus", 2);
                    dataManager.updateTable("chathistory", row, "messageId", message_id);
                } else if (ackmode.equalsIgnoreCase("failed")) {
                    row.put("acknowledgementStatus", 4);
                    dataManager.updateTable("chathistory", row, "messageId", message_id);
                } else {
                    row.put("acknowledgementStatus", 3);
                    dataManager.updateTable("chathistory", row, "fid", extras.getString("openerFid"));
                }
                Intent chatPageIntent = new Intent("CHATMESSAGE_RECEIVED");
                chatPageIntent.putExtra("chatNotification", extras);
                LocalBroadcastManager.getInstance(this).sendBroadcast(chatPageIntent);

            } else {
                sendNotification(extras);
            }
            if (BottomBarActivity.isActive) {
                Intent cfr_notification_badge = new Intent("CFR_NOTIFICATION_BADGE");
                cfr_notification_badge.putExtras(extras);
                LocalBroadcastManager.getInstance(this).sendBroadcast(cfr_notification_badge);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertChatIntoDB(Bundle extras, String extraType) {
        try {
            String msgstr = extras.getString("msg");
            Long longval = extras.getLong("datetime");

            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            ContentValues row = new ContentValues();
            row.put("fid", extras.getString("fidOrGid"));
            row.put("msg", msgstr);
            if (extras.getString("type").equalsIgnoreCase("4")) {
                String reminding_fid = extras.getString("reminding_fid");
                if (UserModel.getInstance().fid.equalsIgnoreCase(reminding_fid)) {
                    row.put("mode", "to");
                } else {
                    row.put("mode", "from");
                }
            } else {
                row.put("mode", "from");
            }
            if (!extraType.equals(getString(R.string.groupextramessage))) {
                row.put("readstatus", "1");
            } else {
                row.put("readstatus", "0");
            }
            if (extras.containsKey("message_id"))
                row.put("messageId", extras.getString("message_id"));
            Date date = new Date(longval);
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dt.setTimeZone(TimeZone.getTimeZone("GMT"));
            row.put("datetime", dt.format(date));
            dataManager.insertTable("chathistory", row);
//            dataManager.getChatHistory(fidOrGid);
            updateGroupDetailsIfModifiedNotification(extras);
        } catch (Exception e) {
            Log.d("insertChatIntoDB", e.getMessage());
        }
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Bundle extras) {
        String type = extras.getString("type");

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String pushNotificationContent = "";
        String title;
        if (type.equalsIgnoreCase("0")) {
            title = extras.getString("fidOrGid");
            String displayName = extras.getString("title");
            pushNotificationContent = "You have chat message(s) from " + displayName;
        } else if (type.equalsIgnoreCase("1")) {
            title = extras.getString("fidOrGid");
            String displayName = extras.getString("groupName");
            pushNotificationContent = "You have chat message(s) in " + displayName;
        } else if (type.equalsIgnoreCase("2")) {
            title = "feed";
            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            notificationCount = dataManager.getUnreadFeedMessagesCount();

            if (notificationCount == 0) {
                pushNotificationContent = "You have " + ++notificationCount + " feed message";
            } else {
                pushNotificationContent = "You have " + ++notificationCount + " feed messages";
            }
        } else if (type.equalsIgnoreCase("3")) {
            title = "relationship";
            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            notificationCount = dataManager.getUnreadRelationMessagesCount();

            if (notificationCount == 0) {
                pushNotificationContent = "You have " + ++notificationCount + " relationship message";
            } else {
                pushNotificationContent = "You have " + ++notificationCount + " relationship messages";
            }
        } else if (type.equalsIgnoreCase("4")) {
            title = getString(R.string.reminder);
            pushNotificationContent = "Reminder for you";
        } else {
            title = "other";
            pushNotificationContent = "You have push notifications";
        }


        Intent pushNotifiIntent;

        if (type.equalsIgnoreCase("0") || type.equalsIgnoreCase("1")) {
            pushNotifiIntent = new Intent(this, ChatMessageActivity.class);
        } else if (type.equalsIgnoreCase("2")) {
            pushNotifiIntent = new Intent(this, BottomBarActivity.class);
        } else if (type.equalsIgnoreCase("3")) {
            pushNotifiIntent = new Intent(this, BottomBarActivity.class);
        } else if (type.equalsIgnoreCase("4")) {
            pushNotifiIntent = new Intent(this, ChatMessageActivity.class);
        } else {
            pushNotifiIntent = new Intent(this, BottomBarActivity.class);
        }
        pushNotifiIntent.putExtras(extras);

        pushNotifiIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                pushNotifiIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_app)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(pushNotificationContent))
                        .setContentText(pushNotificationContent).setSound(alarmSound);

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(title, POST_ID, mBuilder.build());

        insertBadgeIntoDB(title, pushNotificationContent, extras, POST_ID, type, notificationCount);
        // showLastFewNotifications(title);
    }

    private void showLastFewNotifications(String title) {
        DataManager dataManager = DataManager.getInstance(getApplicationContext());
        List<MessageModel> messageModels = dataManager.getUnreadMessages();
        for (MessageModel messageModel : messageModels) {
            if (!title.equals(messageModel.getSenderid())) {
                Intent pushNotifiIntent;
                Bundle extras = new Bundle();

                String type = messageModel.getType();
                if (type.equalsIgnoreCase("0") || type.equalsIgnoreCase("1")) {
                    pushNotifiIntent = new Intent(this, ChatMessageActivity.class);
                    extras.putString("typeChat", messageModel.getType());
                    extras.putString("fidOrGid", messageModel.getId());
                } else if (type.equalsIgnoreCase("2")) {
                    pushNotifiIntent = new Intent(this, BottomBarActivity.class);
                    extras.putString("screen", "post");
                } else if (type.equalsIgnoreCase("3")) {
                    pushNotifiIntent = new Intent(this, BottomBarActivity.class);
                    extras.putString("screen", "relationship");
                    pushNotifiIntent.putExtras(extras);
                } else if (type.equalsIgnoreCase("4")) {
                    pushNotifiIntent = new Intent(this, ChatMessageActivity.class);
                } else {
                    pushNotifiIntent = new Intent(this, BottomBarActivity.class);
                }
                pushNotifiIntent.putExtras(extras);
                pushNotifiIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        pushNotifiIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_app)
                                .setContentTitle(getString(R.string.app_name))
                                .setStyle(new NotificationCompat.BigTextStyle()
                                        .bigText(messageModel.getNotificationmessage()))
                                .setContentText(messageModel.getNotificationmessage());

                mBuilder.setContentIntent(contentIntent);
                mBuilder.setAutoCancel(true);
                mBuilder.setWhen(messageModel.getNotificationTime().getTime());
                NotificationManager mNotificationManager = (NotificationManager)
                        this.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(messageModel.getSenderid(), messageModel.getPostid(), mBuilder.build());
            }
        }
    }

    private void insertBadgeIntoDB(String fidOrGid, String pushNotificationContent, Bundle extras, int POST_ID, String type, int count) {
        try {
            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            ContentValues row = new ContentValues();
            row.put("fidOrGid", fidOrGid);
            row.put("content", pushNotificationContent);
            row.put("postid", POST_ID);
            row.put("type", type);
            row.put("count", count);
            Long longval = extras.getLong("datetime");

            Date date = new Date(longval);
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dt.setTimeZone(TimeZone.getTimeZone("GMT"));
            row.put("datetime", dt.format(date));
            dataManager.isUnreadMessageExist(fidOrGid);
            dataManager.insertTable("unreadnotification", row);

        } catch (Exception e) {
            Log.e(TAG, "insertBadgeIntoDB = " + e.getMessage());
        }
    }

    private void updateGroupDetailsIfModifiedNotification(Bundle extras) {
        try {
            String msgstr = extras.getString("msg");
            JSONObject chatContent = new JSONObject(msgstr);
            final String extraType = chatContent.has("extraType") ? chatContent.getString("extraType") : "";
            if (extraType.equals(getString(R.string.groupextramessage))) {
                Log.d(TAG, "updateGroupDetailsIfModifiedNotification = groupdetailsmodified");
                UserModel userModel = UserModel.getInstance();
                byte[] userModelBA = userModel.toJSON().getBytes(Charset.forName("UTF-8"));
                APIManager groupsApi = new APIManager("chat/groups", userModelBA, new APIListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        Log.d(TAG, "inside onsuccess" + result.toString());
                    }

                    @Override
                    public void onSuccess(JSONArray array) {
                        try {
                            Log.d(TAG, "inside onsuccess" + array.toString());
                            Utility.groupModelMap = new HashMap<>();
                            for (int k = 0; k < array.length(); k++) {
                                JSONObject singleGroup = array.getJSONObject(k);
                                GroupModel groupModel1 = new GroupModel();
                                groupModel1 = groupModel1.toObject(singleGroup.toString());
                                Utility.groupModelMap.put(groupModel1.getGid(), groupModel1);
                            }
                            DataManager dataManager = DataManager.getInstance(getApplicationContext());
                            dataManager.insertChatFavouritesAndGroups(null, Utility.groupModelMap);
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(String errorMessage, String errorCode) {
                        Log.e(TAG, errorMessage);
                    }

                }, APIManager.HTTP_METHOD.POST);
                groupsApi.execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}