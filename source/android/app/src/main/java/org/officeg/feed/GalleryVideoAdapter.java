package org.officeg.feed;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import org.officeg.chat.DownloadFileTask;
import org.officeg.mobileapp.R;
import org.officeg.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryVideoAdapter extends BaseAdapter {
    public List<String> mVideoIds = new ArrayList<>();
    private Context mContext;
    private String TAG = GalleryVideoAdapter.class.getSimpleName();

    public GalleryVideoAdapter(Context context) {
        mContext = context;
    }

    public int getCount() {
        return mVideoIds.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int index, View view, ViewGroup viewGroup) {
        // TODO Auto-generated method stub

        String fileUrl = mVideoIds.get(index);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View videoView = inflater.inflate(R.layout.feed_video, null);

        try {
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, mContext);
            ImageView imageView = (ImageView) videoView.findViewById(R.id.chat_bubble_imageview);

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d("VideoPost = ", "if" + fileCheck.getAbsolutePath());
                DownloadFileTask downloadFileTask = new DownloadFileTask(mContext, videoView, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, mContext.getResources().getString(R.string.video));
            } else {
                Log.d("VideoPost = ", "else" + fileCheck.getAbsolutePath());
                final Uri uri = Utility.getUriFromFile(mContext, fileCheck);

                //String mp4filename = fileName.replace(".png", ".mp4");
                File afterRename = new File(receivedDirectory, fileName);
                //Utility.copy(superActivity, uri, afterRename);
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);
                if (bitmap == null) {
                    Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                }
                imageView.setImageBitmap(bitmap);
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        videoView.setLayoutParams(new Gallery.LayoutParams(200, 200));


        return videoView;
    }
}

