package org.officeg.mobileapp;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.feed.ImageLoader;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.Charset;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {
    TextView backup, changenumber, help, faq, privacyPolicy, displayname, mobilenumber, blockedList, fid, percentage, appinfo, mySharedData, bookmarks;
    ImageButton btnEdit;
    UserModel user = null;
    CircleImageView imageView;
    SeekBar seekbar;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String TAG = SettingsFragment.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.settings);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        checkInternetConnection();

        // Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getActivity());
       /* if(!networkCheck)
            Toast.makeText(getActivity(), APIManager.errorMessage, Toast.LENGTH_LONG).show();*/

        user = UserModel.getInstance();
        displayname = findViewById(R.id.user_profile_name_id);
        mobilenumber = findViewById(R.id.user_profile_phnumber_id);
        fid = findViewById(R.id.user_profile_fid_id);
        seekbar = findViewById(R.id.seekbar);
        percentage = findViewById(R.id.percent);
        mySharedData = findViewById(R.id.id_myshareddata);
        bookmarks = findViewById(R.id.id_bookmarks);
        try {
            seekbar.setProgress(Integer.parseInt(user.profilecompleteness));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        percentage.setText(user.profilecompleteness + "%");

        Log.i("FamilyG", "profile completeness " + user.profilecompleteness);

        seekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        Log.i("FamilyG", "Mobile number in settings page: " + user.mobilenumber);
        displayname.setText(user.displayname);
        mobilenumber.setText(user.mobilenumber);
        fid.setText("OCID: " + user.fid);
        user.save();
        imageView = findViewById(R.id.profile_image_id);
//        if (user.profileimage != null) {
//            Bitmap profileimagebitmap = BitmapFactory.decodeByteArray(user.profileimage, 0, user.profileimage.length);
//            int nh = (int) (profileimagebitmap.getHeight() * (512.0 / profileimagebitmap.getWidth()) );
//            Bitmap scaled = Bitmap.createScaledBitmap(profileimagebitmap, 512, nh, true);
//            imageView.setImageBitmap(scaled);
//        }

        if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {

            ImageLoader imageLoader = imageLoader = new ImageLoader(getApplicationContext());
            imageLoader.DisplayImage(user.profileimageurl, imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*if (user.profileimageurl == null && user.profileimageurl.trim().isEmpty()) {

                }*/

                    Intent i = new Intent(getApplicationContext(), ProfilePicFullscreenActivity.class);
                    i.putExtra("bitmap", user.profileimageurl);
                    startActivity(i);

                }
            });

        } else {
            imageView.setImageResource(R.drawable.default_profile_image);
        }


        settings();

    }

    private void checkInternetConnection() {
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if (!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

    }

    public void settings() {
        btnEdit = findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "edit", "button");
                Intent i = new Intent(getApplicationContext(), PersonalProfileActivity.class);
                i.putExtra("type", "EditFromSettings");
                startActivity(i);
            }
        });

        backup = findViewById(R.id.backup_id);
        backup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "backup", "textview");
                Intent i = new Intent(getApplicationContext(), BackupActivity.class);
                startActivity(i);
            }
        });

        changenumber = findViewById(R.id.changenumber_id);
        changenumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "changenumber", "textview");
                Intent i = new Intent(getApplicationContext(), ChangeNumberActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        faq = findViewById(R.id.FAQ_id);
        faq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "faq", "textview");
                    Intent faqBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/faq.html"));
                    startActivity(faqBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        help = findViewById(R.id.help_id);
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "help", "textview");
                    Intent helpBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/help.html"));
                    startActivity(helpBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        privacyPolicy = findViewById(R.id.privacy_id);
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "privacy policy", "textview");
                    Intent privacyPolicyBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/Privacypolicy.html"));
                    startActivity(privacyPolicyBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        blockedList = findViewById(R.id.blocked_id);
        blockedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "blockedlist", "textview");
                Intent i = new Intent(getApplicationContext(), BlockedListactivity.class);
                startActivity(i);
            }
        });

        appinfo = findViewById(R.id.appInfo_id);
        appinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "appinfo", "textview");
                Intent i = new Intent(getApplicationContext(), AppInfoActivity.class);
                startActivity(i);
            }
        });

        mySharedData = findViewById(R.id.id_myshareddata);
        mySharedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "myshareddata", "textview");
                Intent i = new Intent(getApplicationContext(), MySharedData.class);
                startActivity(i);
            }
        });
        bookmarks = findViewById(R.id.id_bookmarks);
        bookmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "bookmarks", "textview");
                Intent i = new Intent(getApplicationContext(), BookmarksActivity.class);
                startActivity(i);
            }
        });
        boolean familynotification = false;
        if (user.familynotification != null && user.familynotification.equalsIgnoreCase("true")) {
            familynotification = true;
        }
        final ToggleButton toggle_notification = findViewById(R.id.toggle_notification);
        toggle_notification.setChecked(familynotification);
        toggle_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean familynotification = toggle_notification.isChecked();
                updateConnectionNotificationStatus(familynotification);
            }
        });
    }

    private void updateConnectionNotificationStatus(boolean familynotification) {
        try {
            String familynotificationStr = "false";
            if (familynotification) {
                familynotificationStr = "true";
            }
            user.familynotification = familynotificationStr;
            String userprofile = user.toJSON();
            byte[] userbytearray = userprofile.getBytes(Charset.forName("UTF-8"));
            APIManager api = new APIManager("familynotification", userbytearray, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    user.save();
                    Toast.makeText(getApplicationContext(), "Updated successfully", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(JSONArray array) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, "updateConnectionNotificationStatus = " + errorMessage);
                }
            }, APIManager.HTTP_METHOD.POST);
            api.execute();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}