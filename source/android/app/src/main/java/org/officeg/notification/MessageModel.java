package org.officeg.notification;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.util.Utility;

import java.io.Serializable;
import java.util.Date;

public class MessageModel implements Serializable {

    private Long time;
    private String notificationmessage, senderid;
    private String profileimage;

    private String relation;

    public void setType(String type) {
        this.type = type;
    }

    private String type, id, relationpath, anotherfid, mergefid;

    private int postid;
    private Date notificationTime;

    public MessageModel() {

    }

    public Date getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(Date notificationTime) {
        this.notificationTime = notificationTime;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public MessageModel(JSONObject notification) {

        try {
            if (notification.has("message")) {
                notificationmessage = notification.getString("message");
                Log.i("FamilyG", "Message " + notificationmessage);

            }
            if (notification.has("datetime")) {

                time = notification.getLong("datetime");
            }
            if (notification.has("senderid")) {

                senderid = notification.getString("senderid");
            }
            if (notification.has("profileimage")) {

                profileimage = notification.getString("profileimage");
            }
            if (notification.has("type")) {

                type = notification.getString("type");
            }
            if (notification.has("id")) {

                id = notification.getString("id");

                Log.i("FamilyG", "id " + id);
            }
            if (notification.has("relationpath")) {
                relationpath = notification.getString("relationpath");
            }
            if (notification.has("mergefid")) {
                mergefid = notification.getString("mergefid");
            }
            if (notification.has("anotherfid")) {
                anotherfid = notification.getString("anotherfid");
            }

            if (notification.has("relation")) {
                relation = notification.getString("relation");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //   Log.i("check1", notificationmessage);
    }

    public long getDateInMillis() {
        return time;
    }

    public String getTime() {
        return Utility.ElapsedTime(time);
    }

    public String getNotificationmessage() {
        return notificationmessage;
    }

    public String getSenderid() {
        return senderid;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getRelationpath() {
        return relationpath;
    }

    public String getAnotherfid() {
        return anotherfid;
    }

    public String getMergefid() {
        return mergefid;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public void setNotificationmessage(String notificationmessage) {
        this.notificationmessage = notificationmessage;
    }

    public void setSenderid(String senderid) {
        this.senderid = senderid;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

/*
    @Override
    public int compareTo(MessageModel messageModel) {
        return this.getId().compareTo(messageModel.getId());
    }

    public static Comparator<MessageModel> MessageId = new Comparator<MessageModel>() {

        public int compare(MessageModel model1, MessageModel model2) {
            return Integer.parseInt(model2.getId()) - Integer.parseInt(model1.getId());
        }
    };*/
}