package org.officeg.feed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ImagesAndVideosPagerAdapter extends FragmentPagerAdapter {

    private String title[] = {"Images", "Videos"};
    private Bundle extras;

    public ImagesAndVideosPagerAdapter(FragmentManager manager, Bundle extras) {
        super(manager);
        this.extras = extras;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            ImagesFragment likesFragment = new ImagesFragment();
            likesFragment.setArguments(extras);
            return likesFragment;
        } else {
            VideosFragment commentsFragment = new VideosFragment();
            commentsFragment.setArguments(extras);
            return commentsFragment;
        }
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}