package org.officeg.chat;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by techiejackuser on 15/09/17.
 */

public class ChatCommon {
    private String TAG = ChatCommon.class.getSimpleName();

    public void setCommonContent(final ChatMessageModel chatMessageModel, View vi, final Context context, final ListView listView, final ArrayList<ChatMessageModel> chatMessageModels, final ChatMessageAdapter chatMessageAdapter) {
        try {
            Date datetime = null;
            if (chatMessageModel.getDatetime() != null) {
                datetime = chatMessageModel.getDatetime();
            } else {
                datetime = new Date(Utility.getNetworkTime(context));
            }

            if (chatMessageModel.getTypeFromOrTo().equalsIgnoreCase("to")) {
                if (!chatMessageModel.getMessageContentType().equalsIgnoreCase("text")) {
                    ProgressBar progressbar = vi.findViewById(R.id.progressbar);
                    if (chatMessageModel.isIsprogress()) {
                        progressbar.setVisibility(View.VISIBLE);
                    } else {
                        progressbar.setVisibility(View.GONE);
                    }
                }
                if (chatMessageModel.getAcknowledgementStatus() != null) {
                    if (chatMessageModel.getChatType0or1().equals("0")) {
                        if (chatMessageModel.getAcknowledgementStatus().equals(0)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(1)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(2)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(3)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(4)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (chatMessageModel.getAcknowledgementStatus().equals(1)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(0)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.VISIBLE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        } else if (chatMessageModel.getAcknowledgementStatus().equals(4)) {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.VISIBLE);
                        } else {
                            ImageView chat_check = vi.findViewById(R.id.chat_check_upload);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_circle_green);
                            chat_check.setVisibility(View.GONE);

                            chat_check = vi.findViewById(R.id.chat_check_error);
                            chat_check.setVisibility(View.GONE);
                        }
                    }
                }
            }

            LinearLayout bubble_layout = vi.findViewById(R.id.bubble_layout);
            LinearLayout bubble_layout_parent = vi.findViewById(R.id.bubble_layout_parent);

            if (chatMessageModel.getExtraString() != null && chatMessageModel.getExtraString().equals(context.getString(R.string.reminder))) {
                LinearLayout reminder_layout = vi.findViewById(R.id.reminder_layout);
                reminder_layout.setVisibility(View.VISIBLE);

                reminder_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        for (int k = 0; k < chatMessageModels.size(); k++) {
                            ChatMessageModel iterationChatMessageModel = chatMessageModels.get(k);
                            if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(chatMessageModel.getReminderMid())) {
                                final int l = k;
                                listView.setSelection(l);
                                chatMessageModels.get(l).setSelected(true);
                                Utility.notifyDataSetChangedWithoutScroll(listView, chatMessageAdapter);
                                listView.smoothScrollToPositionFromTop(l, 0, 1000);
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        chatMessageModels.get(l).setSelected(false);
                                        Utility.notifyDataSetChangedWithoutScroll(listView, chatMessageAdapter);
                                    }
                                }, 2000);
                                break;
                            }
                        }
                    }
                });
            }

            if (chatMessageModel.isreply() && chatMessageModel.getSourceMessageForReply() != null) {
                LinearLayout reply_layout = vi.findViewById(R.id.reply_layout);
                reply_layout.setVisibility(View.VISIBLE);

                if (chatMessageModel.getTypeFromOrTo().equalsIgnoreCase("from")) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    Resources r = context.getResources();
                    int px = (int) TypedValue.applyDimension(
                            TypedValue.COMPLEX_UNIT_DIP,
                            3,
                            r.getDisplayMetrics()
                    );
                    params.setMargins(0, px, 0, 0);
                    reply_layout.setLayoutParams(params);
                }
                reply_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatMessageModel originalChatMessageModel = new ChatMessageModel();
                        originalChatMessageModel = originalChatMessageModel.toObject(chatMessageModel.getSourceMessageForReply());
                        for (int k = 0; k < chatMessageModels.size(); k++) {
                            ChatMessageModel iterationChatMessageModel = chatMessageModels.get(k);
                            if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(originalChatMessageModel.getMessageId())) {
                                final int l = k;
                                listView.setSelection(l);
                                chatMessageModels.get(l).setSelected(true);
                                Utility.notifyDataSetChangedWithoutScroll(listView, chatMessageAdapter);
                                listView.smoothScrollToPositionFromTop(l, 0, 1000);
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        chatMessageModels.get(l).setSelected(false);
                                        Utility.notifyDataSetChangedWithoutScroll(listView, chatMessageAdapter);
                                    }
                                }, 2000);
                                break;
                            }
                        }
                    }
                });

                TextView reply_sender = vi.findViewById(R.id.reply_sender);
                ChatMessageModel sourceMessageForReplyChatMessageModel = new ChatMessageModel();
                sourceMessageForReplyChatMessageModel = sourceMessageForReplyChatMessageModel.toObject(chatMessageModel.getSourceMessageForReply());

                String senderName;
                ChatMessageModel totalContentChatMessageModel = new ChatMessageModel();
                totalContentChatMessageModel = totalContentChatMessageModel.toObject(sourceMessageForReplyChatMessageModel.getTotalContent());
                String fid = totalContentChatMessageModel.getFid();
                if (UserModel.getInstance().fid.equals(fid)) {
                    senderName = "You";
                } else if (Utility.profileModelMap.containsKey(fid)) {
                    ProfileModel tempprofileModel = Utility.profileModelMap.get(fid);
                    senderName = tempprofileModel.getDisplayname();
                } else {
                    senderName = "";
                }
                reply_sender.setText(senderName);

                LinearLayout reply_text_layout = vi.findViewById(R.id.reply_text_layout);
                if (sourceMessageForReplyChatMessageModel.getMessageContentType().equals(context.getString(R.string.text))) {
                    TextView replyTextView = new TextView(context);
                    replyTextView.setText(sourceMessageForReplyChatMessageModel.getText());
                    replyTextView.setEllipsize(TextUtils.TruncateAt.END);
                    replyTextView.setSingleLine(true);
                    reply_text_layout.addView(replyTextView);
                } else if (sourceMessageForReplyChatMessageModel.getMessageContentType().equals(context.getString(R.string.image))) {
                    ImageView replyImageView = new ImageView(context);
                    replyImageView.setImageResource(R.drawable.ic_camera_black_small);
                    reply_text_layout.addView(replyImageView);
                    TextView replyTextView = new TextView(context);
                    replyTextView.setText(" Image");
                    reply_text_layout.addView(replyTextView);

                    ImageView reply_small_image_holder = vi.findViewById(R.id.reply_small_image_holder);
                    reply_small_image_holder.setVisibility(View.VISIBLE);

                    String fileUrl = sourceMessageForReplyChatMessageModel.getImage();
                    String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

                    File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);

                    final File fileCheck = new File(receivedDirectory + File.separator + fileName);
                    if (!fileCheck.exists()) {
                        Log.d(TAG, "if " + fileCheck.getAbsolutePath());

                        DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, reply_small_image_holder, false);
                        downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.image));
                    } else {
                        Log.d(TAG, "else " + fileCheck.getAbsolutePath());
                        final Uri uri = Utility.getUriFromFile(context, fileCheck);
                        final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, context);
                        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                        reply_small_image_holder.setImageBitmap(bitmap);
                    }

                } else if (sourceMessageForReplyChatMessageModel.getMessageContentType().equals(context.getString(R.string.audio))) {
                    ImageView replyImageView = new ImageView(context);
                    replyImageView.setImageResource(R.drawable.ic_mic_black_small);
                    reply_text_layout.addView(replyImageView);
                    TextView replyTextView = new TextView(context);
                    replyTextView.setText(" Audio");
                    reply_text_layout.addView(replyTextView);

                } else if (sourceMessageForReplyChatMessageModel.getMessageContentType().equals(context.getString(R.string.video))) {
                    ImageView replyImageView = new ImageView(context);
                    replyImageView.setImageResource(R.drawable.ic_videocam_black_small);
                    reply_text_layout.addView(replyImageView);
                    TextView replyTextView = new TextView(context);
                    replyTextView.setText(" Video");
                    reply_text_layout.addView(replyTextView);


                    ImageView reply_small_image_holder = vi.findViewById(R.id.reply_small_image_holder);
                    reply_small_image_holder.setVisibility(View.VISIBLE);

                    String fileUrl = sourceMessageForReplyChatMessageModel.getVideo();
                    String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

                    File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, context);

                    final File fileCheck = new File(receivedDirectory + File.separator + fileName);

                    if (!fileCheck.exists()) {
                        Log.d("VideoPost = ", "if" + fileCheck.getAbsolutePath());
                        DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, reply_small_image_holder, false);
                        downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.video));
                    } else {
                        Log.d("VideoPost = ", "else" + fileCheck.getAbsolutePath());
                        final Uri uri = Utility.getUriFromFile(context, fileCheck);

                        //String mp4filename = fileName.replace(".png", ".mp4");
                        File afterRename = new File(receivedDirectory, fileName);
                        //Utility.copy(context, uri, afterRename);
                        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                                MediaStore.Images.Thumbnails.MINI_KIND);
                        if (bitmap == null) {
                            Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                        }
                        reply_small_image_holder.setImageBitmap(bitmap);
                    }


                }
            }

            if (chatMessageModel.getTypeFromOrTo().equalsIgnoreCase("to")) {
                bubble_layout_parent.setGravity(Gravity.RIGHT);
                if (chatMessageModel.getMessageContentType().equalsIgnoreCase("image") && chatMessageModel.getMessageContentType().equalsIgnoreCase("video")) {
                    bubble_layout_parent.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                if (chatMessageModel.getMessageContentType().equalsIgnoreCase("text")) {
                    bubble_layout.setBackgroundResource(R.drawable.bubble11);
                } else {
                    bubble_layout.setBackgroundResource(R.drawable.bubble1);
                }
            } else {
                bubble_layout_parent.setGravity(Gravity.LEFT);
                if (chatMessageModel.getMessageContentType().equalsIgnoreCase("text")) {
                    bubble_layout.setBackgroundResource(R.drawable.bubble21);
                } else {
                    bubble_layout.setBackgroundResource(R.drawable.bubble2);
                }
            }
            if (chatMessageModel.getTypeFromOrTo().equalsIgnoreCase("from")) {
                ProfileModel profileModel = (ProfileModel) chatMessageModel.getGroupOrProfileModel();
                if (chatMessageModel.getChatType0or1().equalsIgnoreCase("1")) {
                    TextView nameTextView = vi.findViewById(R.id.chat_bubble_name);
                    nameTextView.setVisibility(View.VISIBLE);
                    nameTextView.setText(profileModel.getDisplayname());
                }
            }

            if (chatMessageModel.isSelected()) {
                bubble_layout_parent.setBackgroundColor(context.getResources().getColor(R.color.black_trans80));
            }

            TextView timeTextView = vi.findViewById(R.id.chat_bubble_time);
            String dateFormat = "hh:mm a";
            SimpleDateFormat dt = new SimpleDateFormat(dateFormat);
            timeTextView.setText(dt.format(datetime));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
