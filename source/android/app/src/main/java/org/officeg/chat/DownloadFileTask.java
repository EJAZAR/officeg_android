package org.officeg.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.officeg.mobileapp.R;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFileTask extends AsyncTask<String, String, String> {
    ProgressDialog PD;
    View vi;
    Context context;
    ImageView imageView;
    boolean isProgress;

    public DownloadFileTask(Context context, View vi, ImageView imageView, boolean isProgress) {
        this.context = context;
        this.vi = vi;
        this.imageView = imageView;
        this.isProgress = isProgress;
    }

    @Override
    protected void onPreExecute() {
        if (isProgress) {
            PD = ProgressDialog.show(context, "Please Wait", "Please Wait ...", true);
        } else {
            ProgressBar progressbar = vi.findViewById(R.id.progressbar);
            progressbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        String fileURL = urls[0];
        String fileName = urls[1];
        String type = urls[2];
        downloadFile(fileURL, fileName, type);
//        downloadFile("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_30mb.mp4", "Sample.mp4");
        return "done";

    }

    protected void onPostExecute(Boolean result) {
        PD.dismiss();
    }

    private void downloadFile(String fileURL, final String fileName, final String fileType) {
        FileOutputStream f = null;
        try {
            String type = "";
            if (fileType.equalsIgnoreCase(context.getResources().getString(R.string.image))) {
                type = Environment.DIRECTORY_PICTURES;
            } else if (fileType.equalsIgnoreCase(context.getResources().getString(R.string.video))) {
                type = Environment.DIRECTORY_MOVIES;
            } else if (fileType.equalsIgnoreCase(context.getResources().getString(R.string.audio))) {
                type = Environment.DIRECTORY_MUSIC;
            }

            final File receivedDirectory = Utility.getReceivedDirectoryByType(type, context);
            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.connect();
            final File fileFinal = new File(receivedDirectory,
                    fileName);
            f = new FileOutputStream(fileFinal);
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            Log.d("fileFinal = ", fileFinal.getAbsolutePath());

            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d("runONUiThread", "");
                        final Uri uri = Utility.getUriFromFile(context, fileFinal);
                        if (fileType.equalsIgnoreCase(context.getResources().getString(R.string.image))) {
                            final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, context);
                            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            if (bitmap != null) {
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                                imageView.setImageBitmap(bitmap);
                            } else {
                                if (PD != null)
                                    PD.dismiss();
                            }
                        } else if (fileType.equalsIgnoreCase(context.getResources().getString(R.string.video))) {
//                            String mp4filename = fileName.replace(".png", ".mp4");
                            File afterRename = new File(receivedDirectory, fileName);
//                            Utility.copy(context, uri, afterRename);
                            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                                    MediaStore.Images.Thumbnails.MINI_KIND);
                            if (bitmap == null) {
                                Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                            }
                            imageView.setImageBitmap(bitmap);
                        }
                        if (isProgress) {
                            PD.dismiss();
                        } else {
                            stopProgressBar();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("DownloadFileTask", "downloadFile Error...." + e.toString());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("DownloadFileTask", "Error...." + e.toString());
            if (isProgress)
                PD.dismiss();
        } finally {
            try {
                if (f != null)
                    f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void stopProgressBar() {
        ProgressBar progressbar = vi.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
    }
}