package org.officeg.api;

import org.json.JSONArray;
import org.json.JSONObject;

public interface APIListener {
    void onSuccess(JSONObject object);

    void onSuccess(JSONArray array);

    void onFailure(String errorMessage, String errorCode);
}

