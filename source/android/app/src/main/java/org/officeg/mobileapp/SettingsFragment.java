package org.officeg.mobileapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.feed.ImageLoader;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.Charset;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsFragment extends Fragment {
    TextView backup, changenumber, help, faq, privacyPolicy, displayname, mobilenumber, blockedList, fid, percentage, appinfo, mySharedData;
    ImageButton btnEdit;
    UserModel user = null;
    CircleImageView imageView;
    SeekBar seekbar;
    private View currentView;
    private FirebaseAnalytics mFirebaseAnalytics;
    RelativeLayout appColor;
    private String TAG = SettingsFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.action_settings);
        currentView = inflater.inflate(R.layout.activity_settings, container, false);
        container.removeAllViews();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        checkInternetConnection();

        // Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getActivity());
       /* if(!networkCheck)
            Toast.makeText(getActivity(), APIManager.errorMessage, Toast.LENGTH_LONG).show();*/

        user = UserModel.getInstance();
        displayname = (TextView) currentView.findViewById(R.id.user_profile_name_id);
        mobilenumber = (TextView) currentView.findViewById(R.id.user_profile_phnumber_id);
        fid = (TextView) currentView.findViewById(R.id.user_profile_fid_id);
        seekbar = (SeekBar) currentView.findViewById(R.id.seekbar);
        percentage = (TextView) currentView.findViewById(R.id.percent);
        mySharedData = (TextView) currentView.findViewById(R.id.id_myshareddata);
        appColor = (RelativeLayout) currentView.findViewById(R.id.profile_layout);
        appColor.setBackgroundResource(R.color.appColor);
        try {
            seekbar.setProgress(Integer.parseInt(user.profilecompleteness));
        } catch (NumberFormatException e) {
            e.printStackTrace();

        }
        percentage.setText(user.profilecompleteness + "%");

        Log.i("FamilyG", "profile completeness " + user.profilecompleteness);

        seekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        Log.i("FamilyG", "Mobile number in settings page: " + user.mobilenumber);
        displayname.setText(user.displayname);
        mobilenumber.setText(user.mobilenumber);
        fid.setText("OCID: " + user.fid);
        user.save();
        imageView = (CircleImageView) currentView.findViewById(R.id.profile_image_id);
//        if (user.profileimage != null) {
//            Bitmap profileimagebitmap = BitmapFactory.decodeByteArray(user.profileimage, 0, user.profileimage.length);
//            int nh = (int) (profileimagebitmap.getHeight() * (512.0 / profileimagebitmap.getWidth()) );
//            Bitmap scaled = Bitmap.createScaledBitmap(profileimagebitmap, 512, nh, true);
//            imageView.setImageBitmap(scaled);
//        }

        if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {

            ImageLoader imageLoader = imageLoader = new ImageLoader(getActivity());
            imageLoader.DisplayImage(user.profileimageurl, imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*if (user.profileimageurl == null && user.profileimageurl.trim().isEmpty()) {

                }*/

                    Intent i = new Intent(getActivity(), ProfilePicFullscreenActivity.class);
                    i.putExtra("bitmap", user.profileimageurl);
                    startActivity(i);

                }
            });

        } else {
            imageView.setImageResource(R.drawable.default_profile_image);
        }


        settings();

        return currentView;
    }

    private void checkInternetConnection() {
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getActivity());
        if (!networkCheck)
            Toast.makeText(getActivity(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

    }

    public void settings() {
        btnEdit = (ImageButton) currentView.findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "edit", "button");
                Intent i = new Intent(getActivity(), PersonalProfileActivity.class);
                i.putExtra("type", "EditFromSettings");
                startActivity(i);
            }
        });

        backup = (TextView) currentView.findViewById(R.id.backup_id);
        backup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "backup", "textview");
                Intent i = new Intent(getActivity(), BackupActivity.class);
                startActivity(i);
            }
        });

        changenumber = (TextView) currentView.findViewById(R.id.changenumber_id);
        changenumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "changenumber", "textview");
                Intent i = new Intent(getActivity(), ChangeNumberActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        faq = (TextView) currentView.findViewById(R.id.FAQ_id);
        faq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "faq", "textview");
                    Intent faqBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/faq.html"));
                    startActivity(faqBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        help = (TextView) currentView.findViewById(R.id.help_id);
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "help", "textview");
                    Intent helpBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/help.html"));
                    startActivity(helpBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        privacyPolicy = (TextView) currentView.findViewById(R.id.privacy_id);
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "settings screen", "privacy policy", "textview");
                    Intent privacyPolicyBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.familyg.org/Privacypolicy.html"));
                    startActivity(privacyPolicyBrowserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        blockedList = (TextView) currentView.findViewById(R.id.blocked_id);
        blockedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "blockedlist", "textview");
                Intent i = new Intent(getActivity(), BlockedListactivity.class);
                startActivity(i);
            }
        });

        appinfo = (TextView) currentView.findViewById(R.id.appInfo_id);
        appinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "appinfo", "textview");
                Intent i = new Intent(getActivity(), AppInfoActivity.class);
                startActivity(i);
            }
        });

        mySharedData = (TextView) currentView.findViewById(R.id.id_myshareddata);
        mySharedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "settings screen", "myshareddata", "textview");
                Intent i = new Intent(getActivity(), MySharedData.class);
                startActivity(i);
            }
        });
        boolean familynotification = false;
        if (user.familynotification != null && user.familynotification.equalsIgnoreCase("true")) {
            familynotification = true;
        }
        final ToggleButton toggle_notification = (ToggleButton) currentView.findViewById(R.id.toggle_notification);
        toggle_notification.setChecked(familynotification);
        toggle_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean familynotification = toggle_notification.isChecked();
                updateConnectionNotificationStatus(familynotification);
            }
        });
    }

    private void updateConnectionNotificationStatus(boolean familynotification) {
        try {
            String familynotificationStr = "false";
            if (familynotification) {
                familynotificationStr = "true";
            }
            user.familynotification = familynotificationStr;
            String userprofile = user.toJSON();
            byte[] userbytearray = userprofile.getBytes(Charset.forName("UTF-8"));
            APIManager api = new APIManager("familynotification", userbytearray, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    user.save();
                    Toast.makeText(getContext(), "Updated successfully", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(JSONArray array) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, "updateConnectionNotificationStatus = " + errorMessage);
                }
            }, APIManager.HTTP_METHOD.POST);
            api.execute();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    BottomNavigationView bottomNavigation = (BottomNavigationView) getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_feed_id);
                    return true;
                }
                return false;
            }
        });
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}

