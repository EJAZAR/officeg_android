package org.officeg.mobileapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.feed.FeedItemModel;
import org.officeg.feed.ImageLoader;
import org.officeg.feedpost.PostScreenItems;
import org.officeg.feedpost.PostScreenItemsmodel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostScreenActivity extends AppCompatActivity {
    private static final int UPLOAD_IMAGE_REQUEST = 1881;
    private static final int CAPTURE_IMAGE_REQUEST = 1882;
    private static final int UPLOAD_VIDEO_REQUEST = 1883;
    private static final int RECORD_VIDEO_REQUEST = 1884;
    private static final int PERMISSION_REQUEST_CODE = 200;
    public boolean canceled;
    PostScreenItems postScreenItems;
    ListView listviewdynamic;
    Context context;
    Uri imageUri;
    EditText messagearea;
    Button post;
    ArrayList imageurllist = new ArrayList();
    ArrayList videourllist = new ArrayList();
    byte[] videolistbyte, byteArray, bytelist;
    Bitmap bitmap;
    ArrayList<FeedItemModel> imagelist = new ArrayList<FeedItemModel>();
    ArrayList videolist = new ArrayList();
    JSONObject objectjson;
    ProgressDialog progDailog;
    CircleImageView profileimage;
    APIManager api;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String TAG = PostScreenActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postscreen);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_post);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        if (checkPermission()) {
//            Log.i("permission", "Already granted");
//        } else {
//            requestPermission();
//        }

        messagearea = findViewById(R.id.messagearea);
        messagearea.setMovementMethod(new ScrollingMovementMethod());
        ImageButton chooseimage = findViewById(R.id.image_icon);
        ImageButton choosevideo = findViewById(R.id.video_icon);
        post = findViewById(R.id.btn_post);
        profileimage = findViewById(R.id.profile_image_postscreen);
        if (UserModel.getInstance().profileimageurl != null && !UserModel.getInstance().profileimageurl.trim().isEmpty()) {
            ImageLoader imageLoader = new ImageLoader(this);
            imageLoader.DisplayImage(UserModel.getInstance().profileimageurl, profileimage);
        } else {

            profileimage.setImageResource(R.drawable.default_profile_image);

        }
        context = this;

        postScreenItems = new PostScreenItems(PostScreenActivity.this);
        listviewdynamic = findViewById(R.id.list);
        listviewdynamic.setAdapter(postScreenItems);


        //imageurllist.add(imageurl);

       /* imageurllist.add("http://api.androidhive.info/feed/img/cosmos.jpg");
        imageurllist.add("http://api.androidhive.info/feed/img/time_best.jpg");
        imageurllist.add("http://api.androidhive.info/feed/img/cosmos.jpg");*/

        // videourllist.add("http://techslides.com/demos/sample-videos/small.mp4");


        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "post screen", "post", "button");
                canceled = false;
                Log.i("secodetime post", "" + canceled);

                if (messagearea.getText().toString().trim().isEmpty() && imagelist.isEmpty() && videolist.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Can't post empty message", Toast.LENGTH_LONG).show();
                } else {
                    progDailog = new ProgressDialog(PostScreenActivity.this);
                    progDailog.setTitle("Please Wait");
                    progDailog.setMessage("Loading...");
                    progDailog.setCancelable(false);
                    progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    // progDailog.setCanceledOnTouchOutside(false);
                    progDailog.show();

                   /* progDailog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            progDailog.setCancelable(true);
                            canceled = true;
                            Log.i("pdcallinf", "" + canceled);
                        }

                    });*/

//                    progDailog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                        public void onCancel(DialogInterface dialog) {
//                            canceled = true;
//                            Log.i("pdcallinf", "" + canceled);
//                            api.cancel(true);
//                            Log.i("FamilyG", "IsCancelled" + api.isCancelled());
//                            //finish();
//                        }
//                    });

                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }


                    getPostItems();

                }

                // post.setEnabled(false);


            }


        });

        chooseimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "post screen", "choose image", "button");
                final CharSequence[] options = {getString(R.string.takephoto), getString(R.string.choosefromgallery)};

                AlertDialog.Builder builder = new AlertDialog.Builder(PostScreenActivity.this);
                builder.setTitle("Add Photo!");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getString(R.string.takephoto))) {
                            try {
                                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File imageFile = Utility.createImageFile(PostScreenActivity.this);
                                imageUri = Utility.getUriFromFile(getApplicationContext(), imageFile);
                                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(takePhotoIntent, CAPTURE_IMAGE_REQUEST);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (options[item].equals(getString(R.string.choosefromgallery))) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), UPLOAD_IMAGE_REQUEST);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });
                builder.show();
            }
        });

        choosevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "post screen", "choose video", "button");
                final CharSequence[] options = {getString(R.string.takevideo), getString(R.string.choosefromgallery)};
                AlertDialog.Builder builder = new AlertDialog.Builder(PostScreenActivity.this);
                builder.setTitle("Add Video!");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getString(R.string.takevideo))) {
                            try {
                                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                                    startActivityForResult(takeVideoIntent, RECORD_VIDEO_REQUEST);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (options[item].equals(getString(R.string.choosefromgallery))) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("video/*");
                                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                startActivityForResult(Intent.createChooser(intent, "Select Video"), UPLOAD_VIDEO_REQUEST);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                builder.show();
            }
        });
// getting content from share
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            marque.setText(R.string.share);
            Bundle extras = getIntent().getExtras();
            extras.putBoolean("isShare", true);
            getIntent().putExtras(extras);
            Log.i("FamilyG", "type is " + type);
            if ("text/plain".equals(type)) {
                handleSendText(intent);
            } else if (type.startsWith("image/")) {
                handleSendFile(intent);
                Log.i("FamilyG", "type is image" + type);
            } else if (type.startsWith("video/")) {
                handleSendFileforVideo(intent);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == UPLOAD_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {

                if (data.getClipData() != null) {
//                    Toast.makeText(context, "clipdata not null", Toast.LENGTH_LONG).show();
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {
//                        Toast.makeText(context, "i value = " + i, Toast.LENGTH_LONG).show();

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);

                        String messageString = Utility.getRealPathFromUri(context, uri);
//                        Toast.makeText(context, "messageString = " + messageString, Toast.LENGTH_LONG).show();

                        String messageType = "image";

                        String compressedImagePath = Utility.compressImage(getApplicationContext(), uri);
//                        Toast.makeText(context, "compressedImagePath = " + compressedImagePath, Toast.LENGTH_LONG).show();

                        Log.d("PostScreenActivity", "compressedImagePath = " + compressedImagePath);
                        float fileSizeInMB = Utility.getFilesize(compressedImagePath);
                        if (fileSizeInMB <= 16) {
                            final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);

                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                            byteArray = stream.toByteArray();
                            FeedItemModel feedItemModel = new FeedItemModel();
                            feedItemModel.setImagebytearray(byteArray);
                            feedItemModel.setFileNameBeforeRename(compressedImagePath);

                            imagelist.add(feedItemModel);
                            Log.i("FamilyG", String.valueOf(imagelist.size()));

                            PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString,
                                    messageType, bitmap);
                            postScreenItems.add(postScreenItemsmodel);
                            postScreenItems.notifyDataSetChanged();

                        } else {
                            Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                        }
                    }
                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                } else {
                    try {
//                        Toast.makeText(context, "inside getdata()", Toast.LENGTH_LONG).show();
                        final Uri uploadUri = data.getData();
                        String uriPath = URLEncoder.encode(uploadUri.getPath(), "UTF-8");
                        String extension = MimeTypeMap.getFileExtensionFromUrl(uriPath);
                        ContentResolver cR = getApplicationContext().getContentResolver();
                        String type = cR.getType(uploadUri);

                        if (type == null)
                            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//                        Toast.makeText(context, "type = " + type, Toast.LENGTH_LONG).show();

                        if (type.contains(getString(R.string.image))) {
//                            Toast.makeText(context, "inside image", Toast.LENGTH_LONG).show();

                            String messageString = Utility.getRealPathFromUri(context, uploadUri);
//                            Toast.makeText(context, "messageString = " + messageString, Toast.LENGTH_LONG).show();

                            String messageType = "image";

                            String compressedImagePath = Utility.compressImage(getApplicationContext(), uploadUri);
//                            Toast.makeText(context, "compressedImagePath = " + compressedImagePath, Toast.LENGTH_LONG).show();

                            Log.d("PostScreenActivity", "compressedImagePath = " + compressedImagePath);
                            float fileSizeInMB = Utility.getFilesize(compressedImagePath);
                            if (fileSizeInMB <= 16) {
                                final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                                byteArray = stream.toByteArray();
                                FeedItemModel feedItemModel = new FeedItemModel();
                                feedItemModel.setImagebytearray(byteArray);
                                feedItemModel.setFileNameBeforeRename(compressedImagePath);

                                imagelist.add(feedItemModel);
                                Log.i("FamilyG", String.valueOf(imagelist.size()));

                                PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString,
                                        messageType, bitmap);
                                postScreenItems.add(postScreenItemsmodel);
                                postScreenItems.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {

                String messageString = imageUri.toString();
                String messageType = "image";
                /*bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                Log.i("FamilyG", String.valueOf(bitmap));*/

                //Image compression.....
                final String compressedImagePath = Utility.compressImage(getApplicationContext(), imageUri);
                Log.d("PostScreenActivity", "compressedImagePath = " + compressedImagePath);
                float fileSizeInMB = Utility.getFilesize(compressedImagePath);
                if (fileSizeInMB <= 16) {
                    //final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);
                    bitmap = Utility.decodeSampledBitmapFromResource(imageUri, 200, 200, PostScreenActivity.this);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                    byteArray = stream.toByteArray();
                    Log.i("FamilyG", String.valueOf(byteArray));

                    FeedItemModel feedItemModel = new FeedItemModel();
                    feedItemModel.setImagebytearray(byteArray);
                    feedItemModel.setFileNameBeforeRename(compressedImagePath);

                    imagelist.add(feedItemModel);
                    Log.i("FamilyG", String.valueOf(imagelist.size()));
                    Log.i("FamilyG", String.valueOf(imagelist));


                    PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString,
                            messageType, bitmap);
                    postScreenItems.add(postScreenItemsmodel);
                    postScreenItems.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                }
            }

            if (requestCode == UPLOAD_VIDEO_REQUEST && resultCode == RESULT_OK && data != null) {

                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);

                        String messageString = Utility.getRealPathFromUri(context, uri);

                        String messageType = "video";
                        Log.i("FamilyG", "uploadvideo from gallery" + String.valueOf(uri));

                        String selectedPath = getPathvideo(uri);
                        float fileSizeInMB = Utility.getFilesize(selectedPath);
                        if (fileSizeInMB <= 16) {
                            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath,
                                    MediaStore.Images.Thumbnails.MINI_KIND);
                            int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                            //String extension = MimeTypeMap.getFileExtensionFromUrl(selectedPath);
                            try {
                                FileInputStream fis = new FileInputStream(new File(selectedPath));
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                byte[] buf = new byte[1024];
                                for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                                    baos.write(buf, 0, readNum);
                                }

                                byteArray = baos.toByteArray();
                                Log.i("videobyte", String.valueOf(byteArray));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.i("videobyteoutsidetry", String.valueOf(byteArray));

                            videolist.add(byteArray);
                            PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString, messageType, scaled);
                            postScreenItems.add(postScreenItemsmodel);
                            postScreenItems.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                        }
                    }
                    Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                } else {
                    try {
//                        Toast.makeText(context, "inside getdata()", Toast.LENGTH_LONG).show();
                        final Uri uploadUri = data.getData();
                        String uriPath = URLEncoder.encode(uploadUri.getPath(), "UTF-8");
                        String extension = MimeTypeMap.getFileExtensionFromUrl(uriPath);
                        ContentResolver cR = getApplicationContext().getContentResolver();
                        String type = cR.getType(uploadUri);

                        if (type == null)
                            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//                        Toast.makeText(context, "type = " + type, Toast.LENGTH_LONG).show();

                        if (type.contains(getString(R.string.video))) {

//                            Toast.makeText(context, "inside video", Toast.LENGTH_LONG).show();

                            String messageString = Utility.getRealPathFromUri(context, uploadUri);
//                            Toast.makeText(context, "messageString = " + messageString, Toast.LENGTH_LONG).show();

                            String messageType = "video";
                            Log.i("FamilyG", "uploadvideo from gallery" + String.valueOf(uploadUri));

                            String selectedPath = getPathvideo(uploadUri);
                            float fileSizeInMB = Utility.getFilesize(selectedPath);
                            if (fileSizeInMB <= 16) {
                                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath,
                                        MediaStore.Images.Thumbnails.MINI_KIND);
                                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                                //String extension = MimeTypeMap.getFileExtensionFromUrl(selectedPath);
                                try {
                                    FileInputStream fis = new FileInputStream(new File(selectedPath));
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                    byte[] buf = new byte[1024];
                                    for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                                        baos.write(buf, 0, readNum);
                                    }

                                    byteArray = baos.toByteArray();
                                    Log.i("videobyte", String.valueOf(byteArray));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.i("videobyteoutsidetry", String.valueOf(byteArray));

                                videolist.add(byteArray);
                                PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString, messageType, scaled);
                                postScreenItems.add(postScreenItemsmodel);
                                postScreenItems.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

            }

            if (requestCode == RECORD_VIDEO_REQUEST && resultCode == Activity.RESULT_OK) {
                imageUri = data.getData();
                String messageString = null;
                String messageType = "video";
                Log.i("FamilyG", "recordvideo" + String.valueOf(imageUri));

                String selectedPath = getPath(imageUri);

                float fileSizeInMB = Utility.getFilesize(selectedPath);
                if (fileSizeInMB <= 16) {

                    Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath,
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    Log.i("FamilyG", "bitmap" + bitmap);
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Log.i("FamilyG", selectedPath);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    FileInputStream fis = new FileInputStream(new File(selectedPath));

                    byte[] buf = new byte[1024];
                    int n;
                    while (-1 != (n = fis.read(buf)))
                        baos.write(buf, 0, n);

                    byteArray = baos.toByteArray();
                    videolist.add(byteArray);

                    PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString,
                            messageType, scaled);
                    postScreenItems.add(postScreenItemsmodel);
                    postScreenItems.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();
                }
            }

        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void getPostItems() {

        final Thread thread = new Thread() {
            @Override
            public void run() {

                try {

                    for (int index = 0; index < imagelist.size(); index++) {
                        final FeedItemModel feedItemModel = imagelist.get(index);

                        api = new APIManager("upload", feedItemModel.getImagebytearray(), Utility.fileFormatMap.get("image"), new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {
                                String imageUrl;
                                try {
                                    //Renaming copy of image in local storage....
                                    Log.d("PostScreenActivity", "uploadFile image = " + result.getString("url"));
                                    imageUrl = result.getString("url");
                                    String fileNameAfterUpload = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);

                                    File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, getApplicationContext());
                                    File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                                    File beforeRename = new File(feedItemModel.getFileNameBeforeRename());
                                    beforeRename.renameTo(afterRename);
                                    Log.d("PostScreenActivity", "beforeRename = " + beforeRename);
                                    Log.d("PostScreenActivity", "afterRename = " + afterRename);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccess(JSONArray object) {

                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

                            }
                        }, APIManager.HTTP_METHOD.FILE);

                        try {
                            JSONObject result = api.execute().get();
                            imageurllist.add(result.getJSONObject("result").getString("url"));

                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                        Log.i("FamilyG", String.valueOf(imageurllist));
                    }
                    Log.i("FamilyG", String.valueOf(objectjson));

                    for (int index = 0; index < videolist.size(); index++) {
                        videolistbyte = (byte[]) videolist.get(index);


                        api = new APIManager("upload", videolistbyte, Utility.fileFormatMap.get("video"), new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {

                            }

                            @Override
                            public void onSuccess(JSONArray object) {

                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                progDailog.dismiss();
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

                            }
                        }, APIManager.HTTP_METHOD.FILE);

                        try {
                            JSONObject result = api.execute().get();
                            videourllist.add(result.getJSONObject("result").getString("url"));

                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                        Log.i("FamilyG", String.valueOf(videourllist));
                    }

                    JSONObject dataObj = new JSONObject();
                    try {
                        dataObj.put("secret_key", UserModel.getInstance().secret_key);
                        dataObj.put("content", messagearea.getText().toString());
                        JSONArray images = new JSONArray();
                        JSONArray videos = new JSONArray();
                        Log.i("FamilyG", String.valueOf(imageurllist.size()));
                        for (int index = 0; index < imageurllist.size(); index++) {
                            images.put(imageurllist.get(index));
                        }
                        for (int index = 0; index < videourllist.size(); index++) {
                            videos.put(videourllist.get(index));
                        }
                        dataObj.put("imageurls", images);
                        dataObj.put("videourls", videos);
                        Log.i("FamilyG", String.valueOf(dataObj));

                        try {
                            bytelist = dataObj.toString().getBytes(Charset.forName("UTF-8"));
                            Log.i("FamilyG", dataObj.toString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!canceled) {
                        APIManager api = new APIManager("post/createPost", bytelist, new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {
                           /* Fragment fragment = new FeedFragment();
                            FragmentManager fm = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fm.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentContainer, fragment);
                            fragmentTransaction.addToBackStack(String.valueOf(fragment));
                            fragmentTransaction.commit();*/
                                progDailog.dismiss();
                                onBackPressed();

                            }

                            @Override
                            public void onSuccess(JSONArray object) {

                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                progDailog.dismiss();
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

                            }
                        }, APIManager.HTTP_METHOD.POST);

                        api.execute().get();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        };
        thread.start();

    }

//    private boolean checkPermission() {
//        Toast.makeText(getApplicationContext(), "checkPermission", Toast.LENGTH_SHORT).show();
//        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
//        return result == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermission() {
//        Toast.makeText(getApplicationContext(), "requestPermission", Toast.LENGTH_SHORT).show();
//        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0) {
//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if (locationAccepted) {
//                    } else {
//                        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//                            showMessageOKCancel("You need to allow access to both the permissions",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
//                                                    PERMISSION_REQUEST_CODE);
//                                        }
//                                    });
//                            return;
//                        }
//                    }
//                }
//                break;
//        }
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(PostScreenActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    public String getPath(Uri uri) {
        // String[] projection = {MediaStore.MediaColumns.DATA};
        String filePath = null;
        Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DATA}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath == null ? uri.getPath() : filePath;
    }

    public String getPathvideo(Uri uri) {
        String filePath = null;
        filePath = generateFromKitkat(uri, context);

        if (filePath != null) {
            return filePath;
        }

        Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DATA}, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath == null ? uri.getPath() : filePath;
    }

    private String generateFromKitkat(Uri uri, Context context) {
        String filePath = null;
        if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);

            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Video.Media.DATA};
            String sel = MediaStore.Video.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().
                    query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);


            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            cursor.close();
        }
        return filePath;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        //  backPressed = true;
//        Log.i("backpressed", "" + canceled);

        //onBackPressed();
        Intent i = new Intent(PostScreenActivity.this, BottomBarActivity.class);
        finish();
        startActivity(i);
    }

    void handleSendText(Intent intent) {
        messagearea = findViewById(R.id.messagearea);
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Log.i("SHAREDTEXT", sharedText);
            messagearea.setText(sharedText, TextView.BufferType.EDITABLE);
        }
    }

    void handleSendFile(Intent intent) {
        try {
            Uri fileUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (fileUri != null) {
                String messageString = fileUri.toString();
                String messageType = "image";

                //Image compression.....
                final String compressedImagePath = Utility.compressImage(getApplicationContext(), fileUri);
                Log.d("PostScreenActivity", "compressedImagePath = " + compressedImagePath);
                float fileSizeInMB = Utility.getFilesize(compressedImagePath);
                if (fileSizeInMB <= 16) {
                    //final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);
                    bitmap = Utility.decodeSampledBitmapFromResource(fileUri, 200, 200, PostScreenActivity.this);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                    byteArray = stream.toByteArray();
                    Log.i("FamilyG", String.valueOf(byteArray));

                    FeedItemModel feedItemModel = new FeedItemModel();
                    feedItemModel.setImagebytearray(byteArray);
                    feedItemModel.setFileNameBeforeRename(compressedImagePath);

                    imagelist.add(feedItemModel);
                    Log.i("FamilyG", String.valueOf(imagelist.size()));
                    Log.i("FamilyG", String.valueOf(imagelist));
                    Log.i("FamilyG", "image for share" + bitmap);


                    PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(messageString,
                            messageType, bitmap);
                    Log.i("FamilyG", "postitems" + postScreenItemsmodel);

                    postScreenItems.add(postScreenItemsmodel);
                    postScreenItems.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();

                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }


    void handleSendFileforVideo(Intent intent) {
        Uri fileUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);

        if (fileUri != null) {

            Log.i("FamilyG", "uploadvideo: " + String.valueOf(fileUri));

            String selectedPath = getPathvideo(fileUri);
            Log.i("FamilyG", "Video path: " + selectedPath);

            float fileSizeInMB = Utility.getFilesize(selectedPath);
            if (fileSizeInMB <= 16) {
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath,
                        MediaStore.Images.Thumbnails.MINI_KIND);
                //   int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                // Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                //String extension = MimeTypeMap.getFileExtensionFromUrl(selectedPath);
                try {
                    FileInputStream fis = new FileInputStream(new File(selectedPath));
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    byte[] buf = new byte[1024];
                    for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                        baos.write(buf, 0, readNum);
                    }

                    byteArray = baos.toByteArray();
                    Log.i("videobyte", String.valueOf(byteArray));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("videobyteoutsidetry", String.valueOf(byteArray));

                videolist.add(byteArray);
                PostScreenItemsmodel postScreenItemsmodel = new PostScreenItemsmodel(null, "video", bitmap);
                postScreenItems.add(postScreenItemsmodel);
                postScreenItems.notifyDataSetChanged();
            } else {
                Toast.makeText(getApplicationContext(), "You can't upload more than 16MB", Toast.LENGTH_LONG).show();

            }
        }

    }
}
