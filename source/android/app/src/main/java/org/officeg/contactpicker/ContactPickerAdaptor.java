package org.officeg.contactpicker;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.util.Utility;

import java.util.LinkedList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class ContactPickerAdaptor extends BaseAdapter {

    private LayoutInflater inflater;
    private LinkedList<ProfileModel> phoneContacts, tempPhoneContacts;
    private Activity superActivity;

    public ContactPickerAdaptor(Activity activity) {
        phoneContacts = new LinkedList<ProfileModel>();
        superActivity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (phoneContacts == null)
            return 0;
        return phoneContacts.size();
    }

    @Override
    public Object getItem(int position) {
        return phoneContacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.contact_picker_adapter, null);

        final CheckBox checkBox = convertView.findViewById(R.id.checkbox_group_addmember);

        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ProfileModel profileModel = (ProfileModel) cb.getTag();
                profileModel.setSelected(cb.isChecked());
            }
        });
        ProfileModel profileModel = phoneContacts.get(position);
//        checkBox.setText(profileModel.getDisplayname());
        checkBox.setChecked(profileModel.isSelected());
        checkBox.setTag(profileModel);

        TextView name = convertView.findViewById(R.id.name_group_addmember);
        name.setText(profileModel.getDisplayname());

        CircleImageView circleImageView = convertView.findViewById(R.id.circle_group_addmember);
        if (profileModel.getProfileimageurl() != null && !profileModel.getProfileimageurl().trim().isEmpty()) {
            Utility.showProfilePicture(profileModel.getProfileimageurl(), superActivity, circleImageView, profileModel.getDisplayname(), true);
        } else {
            circleImageView.setImageResource(R.drawable.ic_person);
        }
        return convertView;
    }

    public void setPhoneContactList(LinkedList<ProfileModel> profileModelList) {
        this.phoneContacts = profileModelList;
        this.tempPhoneContacts = new LinkedList<ProfileModel>();
        this.tempPhoneContacts.addAll(phoneContacts);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        phoneContacts.clear();
        if (charText.length() == 0) {
            phoneContacts.addAll(tempPhoneContacts);
        } else {
            for (ProfileModel contact : tempPhoneContacts) {
                if (contact.getDisplayname().toLowerCase(Locale.getDefault()).contains(charText)) {
                    phoneContacts.add(contact);
                }
            }
            Log.d("Azar", phoneContacts.size() + "");
        }
        notifyDataSetChanged();
    }
}
