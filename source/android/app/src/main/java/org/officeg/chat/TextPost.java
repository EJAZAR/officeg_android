package org.officeg.chat;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;


/**
 * Created by techiejackuser on 09/06/17.
 */

public class TextPost {
    View vi;
    boolean isLongClick;

    public TextPost(View view) {
        vi = view;
    }

    public void postText(ChatMessageModel chatMessageModel, Context context) {
        try {
            TextView textview = vi.findViewById(R.id.chat_bubble_textview);
            if (chatMessageModel.getExtraString() != null && chatMessageModel.getExtraString().equals(context.getString(R.string.reminder))) {
                textview.setText("Reminder for : \"" + chatMessageModel.getText() + "\"");
            } else {
                textview.setText(chatMessageModel.getText());
            }

            if (chatMessageModel.getTypeFromOrTo().equals("from")) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(5, 3, 5, 0);
                textview.setLayoutParams(params);
            }
            int chatTextLength = chatMessageModel.getText().length();
            LinearLayout bubble_layout_orientation = vi.findViewById(R.id.bubble_layout_orientation);
            if (chatTextLength < 20) {
                bubble_layout_orientation.setOrientation(LinearLayout.HORIZONTAL);
                textview.setMinEms(1);
            }
            bubble_layout_orientation.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            textview.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isLongClick = true;
                    return false;
                }
            });

            textview.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP && isLongClick) {
                        isLongClick = false;
                        return true;
                    }
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        isLongClick = false;
                    }
                    return v.onTouchEvent(event);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
