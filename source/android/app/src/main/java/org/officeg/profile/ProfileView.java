package org.officeg.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.officeg.feed.ImageLoader;
import org.officeg.mobileapp.MergeActivity;
import org.officeg.mobileapp.PersonalProfileActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileView {

    LayoutInflater inflater;
    View view;
    RelativeLayout circleBorder;
    CircleImageView imageView;
    TextView txtName, txtNumber;
    String connected;

    public ProfileView(final ProfileModel profileModel, final Context context, ViewGroup parent) {
        inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.profile, parent, false);
        Log.i("FamilyG", "ProfileModel:" + profileModel.getDisplayname());
        txtName = (TextView) view.findViewById(R.id.contact_name_id);
        txtName.setText(profileModel.getDisplayname());
        txtNumber = (TextView) view.findViewById(R.id.contact_number_id);
        imageView = (CircleImageView) view.findViewById(R.id.imageView_contact);
        circleBorder = (RelativeLayout) view.findViewById(R.id.lay_rel_img);

//        if(profileModel.getMobilenumber()!= null && !profileModel.getMobilenumber().isEmpty())
//            txtNumber.setText(profileModel.getMobilenumber());
//        else
        if (profileModel.getFid() != null)
            txtNumber.setText("FGID: " + profileModel.getFid());

        if (profileModel.profileimageurl != null && !profileModel.profileimageurl.trim().isEmpty()) {
            ImageLoader imageLoader = imageLoader = new ImageLoader(context);
            imageLoader.DisplayImage(profileModel.profileimageurl, imageView);

        } else {
            imageView.setImageResource(R.drawable.default_profile_image);
        }

        if (profileModel.status != null && !profileModel.status.isEmpty() && profileModel.status.equals(context.getString(R.string.pending)))
            circleBorder.setBackgroundResource(R.drawable.red_circle);

        parent.addView(view);

        Log.i("FamilyG", "Owner FID is " + profileModel.getOwnerfid());
//        if(profileModel.getOwnerfid()!=null) {
//            if (profileModel.getOwnerfid().equals(UserModel.getInstance().getFid())) {

        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                editProfile(profileModel, context);
            }
        });

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public boolean onLongClick(View v) {

                PopupMenu menu = new PopupMenu(context, imageView, Gravity.TOP);
                menu.getMenu().add(0, 1, 1, "Edit");
                menu.getMenu().add(0, 2, 2, "Merge");
                //noinspection RestrictedApi

                MenuPopupHelper menuHelper = new MenuPopupHelper(context, (MenuBuilder) menu.getMenu(), imageView);
                //noinspection RestrictedApi

                menuHelper.setForceShowIcon(true);
                //noinspection RestrictedApi
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 1: {
                                editProfile(profileModel, context);
                                return true;
                            }
                            case 2: {

                                Intent i = new Intent(context, MergeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                Bundle extras = new Bundle();
                                extras.putString("fid", profileModel.getFid());
                                extras.putString("displayname", profileModel.getDisplayname());

                                i.putExtras(extras);
                                context.startActivity(i);

                                return true;
                            }
                        }
                        return false;
                    }
                });
                menuHelper.show();

                return false;
            }
        });
    }

    private void editProfile(ProfileModel profileModel, Context context) {

        Intent i = new Intent(context, PersonalProfileActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra("type","profileView");
//                i.putExtra("profile",profileModel);
        Log.i("FamilyG", "ProfileModel:" + profileModel.getDisplayname());
        Log.i("FamilyG", "ProfileModel:" + profileModel.getMobilenumber());
        Log.i("FamilyG", "ProfileModel ownerfid: " + profileModel.getOwnerfid());
        Log.i("FamilyG", "ProfileModel type: " + profileModel.getType());
        Log.i("FamilyG", "ProfileModel isalive: " + profileModel.getIsalive());
        Log.i("FamilyG", "ProfileModel isowner: " + profileModel.getIsowner());
        Log.i("FamilyG", "ProfileModel userfid: " + UserModel.getInstance().getFid());
        Log.i("FamilyG", "ProfileModel selected person fid: " + Utility.selectedFid);
        Log.i("FamilyG", "ProfileModel ownerfid " + profileModel.getOwnerfid());
        Log.i("FamilyG", "ProfileModel deceased date " + profileModel.getDeceaseddate());

        if (profileModel.getOwnerfid() == null)
            connected = String.valueOf(true);
        else if (profileModel.getOwnerfid().equals(Utility.selectedFid)) {
            connected = String.valueOf(false);
        }

        Bundle extras = new Bundle();
        extras.putString("type", "profileView");
        extras.putString("displayname", profileModel.getDisplayname());
        extras.putString("firstname", profileModel.getFirstname());
        Log.i("FamilyG", "ProfileModel firstname is:" + profileModel.getFirstname());
        Log.i("FamilyG", "ProfileModel surname is:" + profileModel.getLastname());
        extras.putString("lastname", profileModel.getLastname());
        extras.putString("profileimage", profileModel.getProfileimageurl());
        extras.putString("doi", profileModel.getDoi());
        extras.putString("dob", profileModel.getDob());
        extras.putString("gender", profileModel.getGender());
        extras.putString("fid", profileModel.getFid());
        extras.putString("mobilenumber", profileModel.getMobilenumber());
        extras.putString("ownerfid", profileModel.getOwnerfid());
        extras.putString("connected", connected);
        extras.putString("LFID", Utility.selectedFid);
        extras.putBoolean("isalive", profileModel.getIsalive());
        extras.putString("deceaseddate", profileModel.getDeceaseddate());
        extras.putString("relationship", profileModel.getRelationship());

        Log.i("FamilyG", "ProfileModel conn: " + connected);
        i.putExtras(extras);
        context.startActivity(i);

    }
//            else {
//                imageView.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        Log.i("FamilyG", "ProfileModel else ownerfid: " +profileModel.getOwnerfid());
//                        Toast.makeText(context, "You can't edit connected people profile", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        }else{
//            imageView.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
////                    Toast.makeText(context, "You can't edit connected people profile", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
    //  }
}
