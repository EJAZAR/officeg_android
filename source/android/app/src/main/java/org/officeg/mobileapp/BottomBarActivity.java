package org.officeg.mobileapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.firebase.SharedPrefManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.notification.MessageModel;
import org.officeg.util.Utility;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class BottomBarActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener, TreeFragment.OnFragmentInteractionListener, FeedFragment.OnFragmentInteractionListener,
        ChatFragment.OnFragmentInteractionListener, NearByFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener {
    private static final String TAG = BottomBarActivity.class.getSimpleName();
    public static boolean isActive = false;
    Toolbar toolbar;
    UserModel user = null;
    TextView textCartItemCount;
    BroadcastReceiver mMessageReceiver;
    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int activeId = -1;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_bar);
        user = UserModel.getInstance();
        user.load(getApplicationContext());
        progressDialog = new ProgressDialog(BottomBarActivity.this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        isActive = true;
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        registerLocalBroadCastRcr(); //Method to get notifications count

        bottomNavigation = findViewById(R.id.bottom_nav);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigation);

        fragmentManager = getSupportFragmentManager();
        fragment = new ChatFragment();
        activeId = R.id.ic_chat_id;
        showProgrssDialog();
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //      onBackPressed();
            }
        });

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_fragmentholder, fragment).commit();

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                return onItemSelected(item.getItemId());
            }
        });

        String token = SharedPrefManager.getInstance(BottomBarActivity.this).getItem("tagtoken");
        user.devicetoken = token;

        //Log.i("FamilyG", user.secret_key);
        Log.i("FamilyG", user.devicetoken);
        Log.i("FamilyG", user.secret_key + "  " + user.devicetoken);

        APIManager api = new APIManager("registerdevice", user, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                user.devicetoken = null;
            }

            @Override
            public void onSuccess(JSONArray object) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                //Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

            }
        });

        api.execute();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String fromscreen = extras.getString("screen");
            if (fromscreen.equalsIgnoreCase("post")) {
               /* Intent intent = new Intent(BottomBarActivity.this, NotificationActivity.class);
                startActivity(intent);*/
                BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
                bottomNavigation.setSelectedItemId(R.id.ic_feed_id);
                showProgrssDialog();
            } else if (fromscreen.equalsIgnoreCase("openchatpage")) {
                BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
                bottomNavigation.setSelectedItemId(R.id.ic_chat_id);
                showProgrssDialog();
            } else if (fromscreen.equalsIgnoreCase("relationship")) {
                Intent intent = new Intent(BottomBarActivity.this, NotificationActivity.class);
                startActivity(intent);
            } else if (fromscreen.equalsIgnoreCase("nearbypage")) {
                BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
                bottomNavigation.setSelectedItemId(R.id.ic_nearby_id);
            }

        }
        Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, true);
    }

    private void registerLocalBroadCastRcr() {
        mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle extras = intent.getExtras();
                if (extras != null) {
//                    if (intent.hasExtra("chatNotification")) {
//                        Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, false);
//                    } else {
//                        int count = b.getInt("notification");
//                        Log.i("FamilyG", "count from broadcast " + count);
//                        mCartItemCount = count;
//                        invalidateOptionsMenu();
//                    }

                    String type = extras.getString("type");
                    if (type.equalsIgnoreCase("2")) {

                        DataManager dataManager = DataManager.getInstance(getApplicationContext());
                        int unreadFeedCount = 0;
                        if (activeId != R.id.ic_feed_id) {
                            unreadFeedCount = dataManager.getUnreadFeedMessagesCount();
                        }
                        dataManager.isUnreadMessageExist("feed");

                        String pushNotificationContent;
                        if (unreadFeedCount == 0) {
                            pushNotificationContent = "You have " + ++unreadFeedCount + " feed message";
                        } else {
                            pushNotificationContent = "You have " + ++unreadFeedCount + " feed messages";
                        }

                        Utility.insertBadgeIntoDB("feed", pushNotificationContent, "1", type, unreadFeedCount, getApplicationContext());
                        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
                        Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), activeId == R.id.ic_chat_id, activeId == R.id.ic_feed_id);

                    } else if (type.equalsIgnoreCase("3")) {

                        DataManager dataManager = DataManager.getInstance(getApplicationContext());

                        int unreadRelationshipCount = dataManager.getUnreadRelationMessagesCount();

                        String pushNotificationContent;
                        if (unreadRelationshipCount == 0) {
                            pushNotificationContent = "You have " + ++unreadRelationshipCount + " relationship message";
                        } else {
                            pushNotificationContent = "You have " + ++unreadRelationshipCount + " relationship messages";
                        }

                        Utility.insertBadgeIntoDB("relationship", pushNotificationContent, "1", type, unreadRelationshipCount, getApplicationContext());
                        setupBadge();
                    } else if (type.equalsIgnoreCase("0") || type.equalsIgnoreCase("1")) {
                        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
                        Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), activeId == R.id.ic_chat_id, activeId == R.id.ic_feed_id);
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mMessageReceiver, new IntentFilter("CFR_NOTIFICATION_BADGE"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem item_notification = menu.findItem(R.id.action_notification_id);
        View actionView_notification = MenuItemCompat.getActionView(item_notification);
        actionView_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(item_notification);
            }
        });

        final MenuItem item_addmember = menu.findItem(R.id.action_addmember_id);
        View actionView_addmember = MenuItemCompat.getActionView(item_addmember);
        if (user.isadmin) {
            item_addmember.setVisible(true);
        } else {
            item_addmember.setVisible(false);
        }
        actionView_addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(item_addmember);
            }
        });

        final MenuItem item_settings = menu.findItem(R.id.action_settings_id);
        View actionView_settings = MenuItemCompat.getActionView(item_settings);
        actionView_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(item_settings);
            }
        });

        textCartItemCount = actionView_notification.findViewById(R.id.cart_badge);
        //textCartItemCount.setText("1");
        setupBadge();
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        switch (id) {
            case R.id.action_notification_id:
                Utility.logFA(mFirebaseAnalytics, "homepage", "notifiction icon", "button");
                Intent notification_intent = new Intent(BottomBarActivity.this, NotificationActivity.class);
                startActivity(notification_intent);
                break;
//            case R.id.action_addmember_id:
//                Utility.logFA(mFirebaseAnalytics, "homepage", "addmember icon", "button");
//                Intent intent_activity = new Intent(BottomBarActivity.this, AddMemberActivity.class);
//                startActivity(intent_activity);
//                break;
            case R.id.action_settings_id:
                Utility.logFA(mFirebaseAnalytics, "homepage", "settings icon", "button");
                Intent intent_activity_settings = new Intent(BottomBarActivity.this, SettingsActivity.class);
                startActivity(intent_activity_settings);
                break;
        }
//        if (id == R.id.action_pedigree_id) {
//            Utility.logFA(mFirebaseAnalytics, "homepage", "pedigree icon", "button");
//            Intent intent_activity = new Intent(BottomBarActivity.this, PedigreeViewActivity.class);
////            intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent_activity);
////            finish();
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {

        try {
            ProfileModel profileModel = new ProfileModel();
            profileModel.secret_key = UserModel.getInstance().secret_key;
            int waitingrequests = Integer.parseInt(UserModel.getInstance().waitingrequests == null ? "0" : UserModel.getInstance().waitingrequests);

            if (waitingrequests != 0) {
                profileModel.type = "1";
            }

            APIManager api = new APIManager("getnotifications", profileModel, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {

                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.i("check", String.valueOf(array));
                    try {
                        ArrayList<MessageModel> listvalues = new ArrayList<>();

                        for (int index = 0; index < array.length(); index++) {
                            JSONObject notificationObject = array.getJSONObject(index);
                            MessageModel messageModel = new MessageModel(notificationObject);
                            if (messageModel.getType().equals("1") || messageModel.getType().equals("3") || messageModel.getType().equals("5") || messageModel.getType().equals("6")) {
                                listvalues.add(messageModel);
                            }
                        }
                        int notificationCount = listvalues.size();
                        if (textCartItemCount != null) {
                            if (notificationCount == 0) {
                                textCartItemCount.setVisibility(View.GONE);
                            } else {
                                textCartItemCount.setText(String.valueOf(Math.min(notificationCount, 99)));
                                textCartItemCount.setVisibility(View.VISIBLE);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

                }
            });

            api.execute();

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private boolean onItemSelected(int id) {

        if (activeId == id)
            return true;
        activeId = id;
        switch (id) {
            case R.id.ic_tree_id:
                if (getCurrentFocus() != null) {
                    Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Utility.logFA(mFirebaseAnalytics, "homepage", "tree", "button");
                fragment = new TreeFragment();
                Log.i("FamilyG", "tree: " + id);
                Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, false);
                break;
            case R.id.ic_feed_id:
                Utility.logFA(mFirebaseAnalytics, "homepage", "feed", "button");
                fragment = new FeedFragment();
                showProgrssDialog();
                Log.i("FamilyG", "feed_id: " + id);
//                View actionView = MenuItemCompat.getActionView(item);
//                //View v = bottomNavigation.getChildAt(4);
//                textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);
//                textCartItemCount.setText("1");
                Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, true);
                break;
            case R.id.ic_chat_id:
                Utility.logFA(mFirebaseAnalytics, "homepage", "chat", "button");
                fragment = new ChatFragment();
                showProgrssDialog();
                Log.i("FamilyG", "chat_id: " + id);
                Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), true, false);
                break;
            case R.id.ic_nearby_id:
                Utility.logFA(mFirebaseAnalytics, "homepage", "nearby", "button");
                fragment = new NearByFragment();
                Log.i("FamilyG", "nearby_id: " + id);
                Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, false);
                break;
//            case R.id.ic_indulge:
//                Utility.logFA(mFirebaseAnalytics, "homepage", "indulge", "button");
//                fragment = new IndulgeFragment();
//                Log.i("FamilyG", "settings_id: " + id);
//                Utility.setChatAndFeedCount(bottomNavigation, getApplicationContext(), false, false);
//                break;
        }
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_fragmentholder, fragment).commit();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        //setActiveState(true);
        BottomBarActivity.isActive = true;
        setupBadge();
    }

    private void setFragement(Bundle extras) {
        String fromscreen = extras.getString("screen");
        if (fromscreen.equalsIgnoreCase("post")) {
               /* Intent intent = new Intent(BottomBarActivity.this, NotificationActivity.class);
                startActivity(intent);*/
            BottomNavigationView bottomNavigation = findViewById(R.id.bottom_nav);
            bottomNavigation.setSelectedItemId(R.id.ic_feed_id);
            showProgrssDialog();
        } else if (fromscreen.equalsIgnoreCase("relationship")) {
            Intent intent = new Intent(BottomBarActivity.this, NotificationActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // setActiveState(false);
        BottomBarActivity.isActive = false;

    }

    @Override
    protected void onStop() {
        super.onStop();
        BottomBarActivity.isActive = false;
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
        isActive = false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // getIntent() should always return the most recent
//        setIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            setFragement(extras);
        }
        int temp = activeId;
        activeId = -1;
        onItemSelected(temp);

        //  Refresh notificationCount
        registerLocalBroadCastRcr();
        invalidateOptionsMenu();
    }

    public void showProgrssDialog() {
        progressDialog.show();
    }

    public void hideProgrssDialog() {
        progressDialog.dismiss();
    }
}

class BottomNavigationViewHelper {

    @SuppressLint("RestrictedApi")
    static void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {

        } catch (IllegalAccessException e) {

        }
    }
}