package org.officeg.feed;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.officeg.chat.DownloadFileTask;
import org.officeg.mobileapp.R;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryImageAdapter extends BaseAdapter {
    public List<String> mImageIds = new ArrayList<>();
    private Context mContext;
    private String TAG = GalleryImageAdapter.class.getSimpleName();

    public GalleryImageAdapter(Context context) {
        mContext = context;
    }

    public int getCount() {
        return mImageIds.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    // Override this method according to your need
    public View getView(int index, View view, ViewGroup viewGroup) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View imageHolder = inflater.inflate(R.layout.feed_image, null);
        try {
            ImageView imageView = (ImageView) imageHolder.findViewById(R.id.chat_bubble_imageview);

            String fileUrl = mImageIds.get(index);
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, mContext);

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);

            if (!fileCheck.exists()) {
                Log.d(TAG, "if " + fileCheck.getAbsolutePath());

                DownloadFileTask downloadFileTask = new DownloadFileTask(mContext, imageHolder, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, mContext.getResources().getString(R.string.image));
            } else {

                Uri uri = Utility.getUriFromFile(mContext, fileCheck);
                Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, mContext);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    imageView.setImageBitmap(bitmap);
                } else {
                    Log.e(TAG, "bitmap null");
                }

            }
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(200, 200));

            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        return imageHolder;
    }
}

