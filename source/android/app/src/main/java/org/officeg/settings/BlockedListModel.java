package org.officeg.settings;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class BlockedListModel implements Serializable {

    String displayname, mobilenumber, profileimage, fid;

    public BlockedListModel(JSONObject post) {

        try {
            fid = post.getString("fid");
            displayname = post.getString("displayname");
            mobilenumber = post.getString("mobilenumber");
            if (post.getString("profileimage") != "") {
                profileimage = post.getString("profileimage");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getFid() {
        return fid;
    }

    public String getDisplayname() {
        return displayname;
    }


    public String getProfileimage() {
        return profileimage;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }


}

