package org.officeg.mobileapp;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.model.UserModel;
import org.officeg.settings.RestoreAdapter;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;

public class BackupActivity extends AppCompatActivity {
    Button btnBackup;
    ProgressDialog PD;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.backup_settings_title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        updateBackupHistory();
        btnBackup = findViewById(R.id.btn_backup);
        btnBackup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Utility.logFA(mFirebaseAnalytics, "backup screen", "backup", "button");

                    PD = ProgressDialog.show(BackupActivity.this, "Please Wait", "Please Wait ...", true);
                    String currentDBPath = "/data/data/" + getApplicationContext().getPackageName() + "/databases/FamilyGUserDatabase";
                    File currentDB = new File(currentDBPath);
                    Log.d("BackupActivity", "backup  = " + currentDBPath);
                    int size = (int) currentDB.length();
                    final byte[] byteArray = new byte[size];
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(currentDB));
                    buf.read(byteArray, 0, byteArray.length);
                    buf.close();
//                    exportDB(getApplicationContext());
                    APIManager uploadapi = new APIManager("upload", byteArray, new APIListener() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                String backupUrl = result.getString("url");
                                Log.d("BackupActivity", "backupUrl  = " + backupUrl);

                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("secret_key", UserModel.getInstance().getSecret_key());
                                jsonObject.put("url", backupUrl);
                                byte[] byteArray = jsonObject.toString().getBytes(Charset.forName("UTF-8"));

                                APIManager chatBackupApi = new APIManager("chat/backup", byteArray, new APIListener() {
                                    @Override
                                    public void onSuccess(JSONObject result) {
                                        try {
                                            Log.d("chatBackupApi", "chatBackupApi  = " + result.toString());
                                            updateBackupHistory();
                                            PD.dismiss();
                                            Toast.makeText(getApplicationContext(), "Backing up completed", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            Log.e("chatBackupApi", "chatBackupApi e = " + e.getMessage());
                                            PD.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onSuccess(JSONArray array) {
                                        PD.dismiss();
                                    }

                                    @Override
                                    public void onFailure(String errorMessage, String errorCode) {
                                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                                        PD.dismiss();
                                    }
                                }, APIManager.HTTP_METHOD.POST);
                                chatBackupApi.execute();

                            } catch (Exception e) {
                                Log.e("BackupActivity", "backup e = " + e.getMessage());
                                PD.dismiss();
                            }
                        }

                        @Override
                        public void onSuccess(JSONArray array) {
                            PD.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            PD.dismiss();
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                        }
                    }, APIManager.HTTP_METHOD.FILE);
                    uploadapi.execute();
                } catch (Exception e) {
                    Log.e("BackupActivity", "backup e = " + e.getMessage());
                }
            }
        });


//        btnRestore = (Button) findViewById(R.id.btn_restore);
//        btnRestore.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                try {
////                    "https://familyg.s3-us-west-2.amazonaws.com/3807cca2-62ff-41c9-b74a-38c565048dc4.png"
////                    restoreDataBase();
//                    importDB(getApplicationContext());
//                } catch (Exception e) {
//                    Log.e("BackupActivity", "backup e = " + e.getMessage());
//                }
//            }
//        });

    }

    private void updateBackupHistory() {
        try {
            final RestoreAdapter restoreAdapter = new RestoreAdapter(BackupActivity.this);
            ListView listView = findViewById(R.id.listview_restore);
            listView.setAdapter(restoreAdapter);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("secret_key", UserModel.getInstance().getSecret_key());
            byte[] byteArray = jsonObject.toString().getBytes(Charset.forName("UTF-8"));

            APIManager chatBackupListApi = new APIManager("chat/backuplist", byteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d("chatBackupListApi", "chatBackupListApi  = " + result.toString());
                }

                @Override
                public void onSuccess(JSONArray array) {
                    try {
                        Log.d("chatBackupListApi", "chatBackupListApi  = " + array.toString());
                        TextView nobackuphistory = findViewById(R.id.nobackuphistory);
                        if (array.length() == 0) {
                            nobackuphistory.setVisibility(View.VISIBLE);
                        } else {
                            nobackuphistory.setVisibility(View.GONE);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonObject = (JSONObject) array.get(i);
                                restoreAdapter.add(jsonObject.toString());
                                restoreAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        Log.e("chatBackupListApi", "chatBackupListApi" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e("chatBackupListApi", "chatBackupListApi e = " + errorCode + " and " + errorMessage);
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                }
            }, APIManager.HTTP_METHOD.POST);
            chatBackupListApi.execute();
        } catch (Exception e) {
            Log.e("BackupActivity", "BackupActivity" + e.getMessage());
        }
    }
}