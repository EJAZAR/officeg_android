package org.officeg.chat.group;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AddMembersActivity extends AppCompatActivity {
    Activity activity;
    ArrayList<ProfileModel> profileModels = new ArrayList<ProfileModel>();
    private String TAG = AddMembersActivity.class.getSimpleName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Next").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 1) {
            final List<String> fids = new ArrayList<String>();

            for (ProfileModel profileModel1 : profileModels) {
                if (profileModel1.isSelected())
                    fids.add(profileModel1.getFid());
            }
            if (fids.size() == 0) {
                Toast.makeText(getApplicationContext(), "Kindly select atleast one member", Toast.LENGTH_LONG).show();

            } else {
                fids.add(UserModel.getInstance().getFid());
                Intent createGroupIntent = new Intent(AddMembersActivity.this, CreateGroupActivity.class);
                createGroupIntent.putExtra("fids", fids.toString());
                startActivity(createGroupIntent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.members_listview);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        activity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        TextView marque= (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_add_member);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (!Utility.profileModelMap.isEmpty()) {
            Log.d(TAG, "Utility.profileModelMap exist");
            afterGettingFavouritesList();
        } else {
            final UserModel userModel = UserModel.getInstance();
            APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", userModel, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "inside onsuccess");
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "Utility.profileModelMap reload");
                    try {
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject object = array.getJSONObject(k);
                            ProfileModel profileModel = new ProfileModel();
                            profileModel = profileModel.toObject(object.toString());

                            if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                String profileModelKey = profileModel.getFid();
                                if (Utility.profileModelMap.containsKey(profileModelKey)) {
                                    Utility.profileModelMap.remove(profileModelKey);
                                }
                                Utility.profileModelMap.put(profileModelKey, profileModel);
                            }
                        }
                        afterGettingFavouritesList();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, errorMessage);
                }
            });
            getFavouriteListApi.execute();
        }
    }

    private void afterGettingFavouritesList() {
        try {
            CreateGroupAdapter createGroupAdapter = new CreateGroupAdapter(activity);
            ListView listView = (ListView) findViewById(R.id.list_creategroup);
            listView.setAdapter(createGroupAdapter);
            for (Map.Entry<String, ProfileModel> entry : Utility.profileModelMap.entrySet()) {
                profileModels.add(entry.getValue());
                createGroupAdapter.add(entry.getValue());
                createGroupAdapter.notifyDataSetChanged();
            }
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_group_addmember);
                    checkBox.performClick();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
