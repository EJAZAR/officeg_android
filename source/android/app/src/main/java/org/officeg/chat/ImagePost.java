package org.officeg.chat;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by techiejackuser on 09/06/17.
 */

public class ImagePost {
    View vi;
    Context context;
    private String TAG = ImagePost.class.getSimpleName();
    ChatMessageAdapter chatMessageAdapter;

    public ImagePost(View view, Context context, ChatMessageAdapter chatMessageAdapter) {
        vi = view;
        this.context = context;
        this.chatMessageAdapter = chatMessageAdapter;
    }

    public void postImage(final ChatMessageModel chatMessageModel) {
        try {

            String fileUrl = chatMessageModel.getImage();
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);
            ImageView imageView = (ImageView) vi.findViewById(R.id.chat_bubble_imageview);

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d(TAG, "if " + fileCheck.getAbsolutePath());

                DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.image));
            } else {
                Log.d(TAG, "else " + fileCheck.getAbsolutePath());
                final Uri uri = Utility.getUriFromFile(context, fileCheck);
                final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, context);
                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                imageView.setImageBitmap(bitmap);
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (fileCheck.exists()) {
                            Dialog imageDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                            ImageView imageview = new ImageView(context);
                            Bitmap myBitmap = BitmapFactory.decodeFile(fileCheck.getAbsolutePath());
                            imageview.setImageBitmap(myBitmap);
                            imageDialog.setContentView(imageview);
                            imageDialog.show();
//                            Intent imageViewIntent = new Intent(context, ZoomInZoomOut.class);
//                            imageViewIntent.putExtra("filePath", fileCheck.getAbsolutePath());
//                            context.startActivity(imageViewIntent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    View parent = (View) v.getParent();
                    parent.performLongClick();
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}