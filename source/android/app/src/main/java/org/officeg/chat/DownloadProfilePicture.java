package org.officeg.chat;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadProfilePicture extends AsyncTask<String, String, String> {
    ProgressDialog PD;
    ImageView imageView;
    Context context;
    File fileFinal;
    boolean showProfileWhenClick;
    private String TAG = DownloadProfilePicture.class.getSimpleName();

    public DownloadProfilePicture(Context context, File fileCheck, ImageView imageView, boolean showProfileWhenClick) {
        this.context = context;
        this.imageView = imageView;
        this.fileFinal = fileCheck;
        this.showProfileWhenClick = showProfileWhenClick;
    }

    @Override
    protected void onPreExecute() {
        PD = ProgressDialog.show(context, "Please Wait", "Please Wait ...", true);
    }

    @Override
    protected String doInBackground(String... urls) {
        String fileURL = urls[0];
        downloadFile(fileURL);
//        downloadFile("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_30mb.mp4", "Sample.mp4");
        return "done";
    }

    protected void onPostExecute(Boolean result) {
        PD.dismiss();
    }

    private void downloadFile(String fileURL) {
        FileOutputStream f = null;
        try {
            Log.d(TAG, "downloadFile");
            if (!fileFinal.exists()) {
                URL url = new URL(fileURL);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();

                f = new FileOutputStream(fileFinal);
                InputStream in = c.getInputStream();
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
            }
            Log.d(TAG, "fileFinal = " + fileFinal.getAbsolutePath());

            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "runONUiThread");
                        final Uri uri = Utility.getUriFromFile(context, fileFinal);
                        final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 50, 50, context);
                        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 3, stream);
                        imageView.setImageBitmap(bitmap);
                        if (showProfileWhenClick) {
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ImageView imageview = new ImageView(context);
                                    try {
                                        File imgFile = new File(fileFinal.getAbsolutePath());
                                        if (imgFile.exists()) {
                                            Dialog imageDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                                            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                                            imageview.setImageBitmap(myBitmap);

                                            imageDialog.setContentView(imageview);
                                            imageDialog.show();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                        PD.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            Log.e("DownloadProfilePicture", "Error...." + e.toString());
            PD.dismiss();
        } finally {
            try {
                if (f != null)
                    f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}