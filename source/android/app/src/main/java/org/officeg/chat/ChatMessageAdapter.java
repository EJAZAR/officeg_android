package org.officeg.chat;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.util.Utility;

import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class ChatMessageAdapter extends BaseAdapter {

    public ArrayList<ChatMessageModel> chatMessageModels;
    Context context;
    MediaPlayer mediaPlayer;
    private LayoutInflater inflater = null;
    ChatMessageAdapter chatMessageAdapter;

    public ChatMessageAdapter(Context context, MediaPlayer mediaPlayer) {
        this.context = context;
        this.chatMessageModels = new ArrayList<ChatMessageModel>();
        inflater = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        this.mediaPlayer = mediaPlayer;
        this.chatMessageAdapter = this;
    }

    @Override
    public int getCount() {
        return chatMessageModels.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessageModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View vi, ViewGroup parent) {
        try {
            ChatMessageModel chatMessageModel = chatMessageModels.get(position);
            if (chatMessageModel.getChatType0or1() == null) {
                vi = inflater.inflate(R.layout.chatactivity_header, null);
                Date date = chatMessageModel.getDatetime();
                TextView textView = vi.findViewById(R.id.list_header_title);
                textView.setText(Utility.getDayFromDate(date, context));
                return vi;
            } else if (chatMessageModel.getMessageContentType().equalsIgnoreCase(context.getString(R.string.groupextramessage))) {
                vi = inflater.inflate(R.layout.chatactivity_header, null);
                TextView textView = vi.findViewById(R.id.list_header_title);
                textView.setPadding(30, 15, 30, 15);
                textView.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.appContrast, null));
                textView.setText(chatMessageModel.getExtraString());
                return vi;
            } else if (chatMessageModel.getMessageContentType().equalsIgnoreCase("text")) {
                vi = inflater.inflate(R.layout.chatactivity_bubble_text, null);
                TextPost textPost = new TextPost(vi);
                textPost.postText(chatMessageModel, context);
            } else if (chatMessageModel.getMessageContentType().equalsIgnoreCase("image")) {
                vi = inflater.inflate(R.layout.chatactivity_bubble_image, null);
                ImagePost imagePost = new ImagePost(vi, context, chatMessageAdapter);
                imagePost.postImage(chatMessageModel);
            } else if (chatMessageModel.getMessageContentType().equalsIgnoreCase("video")) {
                vi = inflater.inflate(R.layout.chatactivity_bubble_video, null);
                VideoPost videoPost = new VideoPost(vi, context, chatMessageAdapter);
                videoPost.postVideo(chatMessageModel);
            } else if (chatMessageModel.getMessageContentType().equalsIgnoreCase(context.getString(R.string.audio))) {
                vi = inflater.inflate(R.layout.chatactivity_bubble_audio, null);
                AudioPost audioPost = new AudioPost(vi, context, mediaPlayer, chatMessageAdapter);
                audioPost.postAudio(chatMessageModel);
            }
            ListView listView = (ListView) parent;
            ChatCommon chatCommon = new ChatCommon();
            chatCommon.setCommonContent(chatMessageModel, vi, context, listView, chatMessageModels, chatMessageAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

    public void add(ChatMessageModel chatMessageModel) {
        chatMessageModels.add(chatMessageModel);
    }

    public void add(Integer index, ChatMessageModel chatMessageModel) {
        chatMessageModels.add(index, chatMessageModel);
    }

    public void remove(ChatMessageModel chatMessageModel) {
        chatMessageModels.remove(chatMessageModel);
    }

}