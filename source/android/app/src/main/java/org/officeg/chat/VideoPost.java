package org.officeg.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.officeg.mobileapp.R;
import org.officeg.mobileapp.VideoPlayerActivity;
import org.officeg.model.ChatMessageModel;
import org.officeg.util.Utility;

import java.io.File;


/**
 * Created by techiejackuser on 09/06/17.
 */

public class VideoPost {
    View vi;
    Context context;
    ChatMessageAdapter chatMessageAdapter;

    public VideoPost(View view, Context context, ChatMessageAdapter chatMessageAdapter) {
        vi = view;
        this.context = context;
        this.chatMessageAdapter = chatMessageAdapter;
    }

    public void postVideo(ChatMessageModel chatMessageModel) {

        try {
            String fileUrl = chatMessageModel.getVideo();
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, context);
            ImageView imageView = (ImageView) vi.findViewById(R.id.chat_bubble_imageview);

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d("VideoPost = ", "if" + fileCheck.getAbsolutePath());
                DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.video));
            } else {
                Log.d("VideoPost = ", "else" + fileCheck.getAbsolutePath());
                final Uri uri = Utility.getUriFromFile(context, fileCheck);

                //String mp4filename = fileName.replace(".png", ".mp4");
                File afterRename = new File(receivedDirectory, fileName);
                //Utility.copy(context, uri, afterRename);
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);
                if (bitmap == null) {
                    Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                }
                imageView.setImageBitmap(bitmap);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    Uri uri = Utility.getUriFromFile(context, fileCheck);
                    intent.putExtra("videoUri", uri.toString());
                    context.startActivity(intent);
                }
            });
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    View parent = (View) v.getParent();
                    parent.performLongClick();
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}