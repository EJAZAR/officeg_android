package org.officeg.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.database.DataManager;
import org.officeg.profile.ProfileMemberModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserModel implements Serializable {
    public String mobilenumber;
    public String countrycode;
    public String secret_key;
    public String displayname;
    public String firstname;
    public String lastname;
    public String dob;
    public String doi;
    public String pushnotification = "";
    public String geolocationaccess = "";
    public String tagging = "";
    public String number;
    public String refcode;
    public String otp;
    public String gender;
    public String relationship;
    public String relmobilenumber;
    public String fid;
    public String devicetoken;
    public String postid = "";
    public String relfirstname;
    public String relsurname;
    public byte[] profileimage;
    public String profileimageurl = "";
    public Double latitude;
    public Double longitude;
    public String word;
    public String profilecompleteness;
    public String waitingrequests;
    public String mode;

    public Boolean isnew;

    public boolean isadmin() {
        return isadmin;
    }

    public void setIsadmin(boolean isadmin) {
        this.isadmin = isadmin;
    }

    public ProfileMemberModel profileMemberModel = new ProfileMemberModel();

    public Boolean isprofilecreated = false;

    public Boolean isadmin = false;

    public List<ProfileModel> profileModelList = null;
    public List<ProfileModel> ownedProfileList = new ArrayList<ProfileModel>();

    private static UserModel user = new UserModel();

    public String platform;

    public String familynotification;

    private UserModel() {

    }

    public static UserModel getInstance() {
        return user;
    }

    private void loadMembers(final String userfid) throws ExecutionException, InterruptedException {

        ProfileModel model = new ProfileModel();
        model.setFid(userfid);
        model.secret_key = secret_key;

        APIManager getrelationapi = new APIManager("relationship/getrelation", model, new APIListener() {
            @Override
            public void onSuccess(JSONObject object) {

            }

            @Override
            public void onSuccess(JSONArray array) {

                try {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject person = (JSONObject) array.get(i);
                        ContentValues row = new ContentValues();
                        ProfileModel member = new ProfileModel();

                        if (person.has("fid")) {
                            row.put("FamilyGID", person.getString("fid"));
                            member.setFid(person.getString("fid"));
                        }

                        if (person.has("displayname")) {
                            row.put("DisplayName", person.getString("displayname"));
                            member.setDisplayname(person.getString("displayname"));
                        }

                        if (person.has("firstname")) {
                            row.put("FirstName", person.getString("firstname"));
                            member.setFirstname(person.getString("firstname"));
                        }

                        if (person.has("lastname")) {
                            row.put("LastName", person.getString("lastname"));
                            member.setLastname(person.getString("lastname"));
                        }

                        if (person.has("dob")) {
                            row.put("DOB", person.getString("dob"));
                            member.setDob(person.getString("dob"));
                        }

                        if (person.has("gender")) {
                            row.put("Gender", person.getString("gender"));
                            member.setGender(person.getString("gender"));
                        }

                        if (person.has("mobilenumber")) {
                            row.put("MobileNumber", person.getString("mobilenumber"));
                            member.setMobilenumber(person.getString("mobilenumber"));
                        }

                        if (person.has("deceaseddate")) {
                            row.put("Deathdate", person.getString("deceaseddate"));
                            member.setDeceaseddate(person.getString("deceaseddate"));
                        }

                        if (person.has("isowner")) {
                            row.put("IsOwner", person.getBoolean("isowner"));
                            member.setIsowner(person.getBoolean("isowner"));
                        }

                        if (person.has("isfav")) {
                            row.put("IsFavourite", person.getString("isfav"));
                            member.setIsfavourite(person.getString("isfav"));
                        }

                        if (person.has("doi")) {
                            row.put("doi", person.getString("doi"));
                            member.setDoi(person.getString("doi"));
                        }

                        if (person.has("profileimage")) {
                            row.put("ImageUrl", person.getString("profileimage"));
                            member.setProfileimageurl(person.getString("profileimage"));
                        }

                        if (person.has("ownerfid")) {
                            row.put("ownerfid", person.getString("ownerfid"));
                            member.setOwnerfid(person.getString("ownerfid"));
                        }

                        if (person.has("isalive")) {
                            row.put("isalive", person.getString("isalive"));
                            member.setIsalive(person.getBoolean("isalive"));
                        }

                        if (person.has("status")) {
                            row.put("FamilyGID", person.getString("status"));
                            member.setStatus(person.getString("status"));
                        }

                        if (person.has("platform")) {
                            row.put("platform", person.getString("platform"));
                            member.setPlatform(person.getString("platform"));
                        }

                        if (person.has("relationtype")) {
                            row.put("relationtype", person.getString("relationtype"));
                            member.setRelationship(person.getString("relationtype"));
                        }

                        profileMemberModel.AddMember(userfid, member);
                        if (profileModelList.indexOf(member) == -1) {

                            profileModelList.add(member);
                            //dm.insertTable("relationships", row);
                        }

                        if (!member.isowner && member.ownerfid!=null && member.ownerfid.equalsIgnoreCase(fid)) { // Check if the profile is owned by logged in user
                            if (ownedProfileList.indexOf(member) == -1) {
                                ownedProfileList.add(member);
                                loadMembers(member.fid);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.i("FamilyG", "error in inserting: " + e);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Log.e("UserModel", errorMessage);

            }
            // return the profile list
        });
        getrelationapi.execute().get();

    }

    public void loadMembers() {
        profileModelList = new ArrayList<ProfileModel>();
        ownedProfileList = new ArrayList<ProfileModel>();
        profileMemberModel.clear();
        ProfileModel model = new ProfileModel();
        model.fid = fid;
        model.displayname = "Myself";
        ownedProfileList.add(model);
        try {
            loadMembers(fid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void load(Context ctx) {
        List userList = new ArrayList();
        DataManager dm = DataManager.getInstance(ctx);
        Cursor cursor = dm.Execute("select * from userDetails");
        UserModel user = UserModel.getInstance();
        Class userClass = user.getClass();
        while (cursor.moveToNext()) {
            try {
                if (cursor.getType(1) == Cursor.FIELD_TYPE_BLOB) {
                    Log.d("UserModel1", cursor.getString(0) + " and " + cursor.getBlob(1));
                    userClass.getField(cursor.getString(0)).set(user, cursor.getBlob(1));
                } else if (Boolean.class.isAssignableFrom(userClass.getField(cursor.getString(0)).getType())) {
                    Log.d("UserModel1", cursor.getString(0) + " and " + (cursor.getInt(1) > 0));
                    userClass.getField(cursor.getString(0)).set(user, cursor.getInt(1) > 0);
                } else {
                    Log.d("UserModel1", cursor.getString(0) + " and " + cursor.getString(1));
                    userClass.getField(cursor.getString(0)).set(user, cursor.getString(1));
                }
            } catch (IllegalAccessException e) {
                Log.e("UserModel", "E = " + e.getMessage());
            } catch (NoSuchFieldException e) {
                Log.e("UserModel", "E = " + e.getMessage());
            } catch (Exception e) {
                Log.e("UserModel", "E = " + e.getMessage());
            }
            userList.add(user);
        }
        loadMembers();
        Log.i("userlist:", "" + userList);
    }


    public void save() {
        DataManager dm = DataManager.getInstance(null);
        dm.delete("userDetails");
        dm.ExecuteKeyValueNonQuery("userDetails", user, null);
        dm.close(); // Closing database connection
    }


    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobilenumber", getMobilenumber());
            jsonObject.put("countrycode", getCountrycode());
            jsonObject.put("secret_key", getSecret_key());
            jsonObject.put("displayname", getDisplayname());
            jsonObject.put("firstname", getFirstname());
            jsonObject.put("lastname", getLastname());
            jsonObject.put("lastname", getLastname());
            jsonObject.put("dob", getDob());
            jsonObject.put("doi", getDoi());
            jsonObject.put("pushnotification", getPushnotification());
            jsonObject.put("geolocationaccess", getGeolocationaccess());
            jsonObject.put("tagging", getTagging());
            jsonObject.put("profileimageurl", getProfileimageurl());
            jsonObject.put("gender", getGender());
            jsonObject.put("relationship", getRelationship());
            jsonObject.put("relmobilenumber", getRelmobilenumber());
            jsonObject.put("fid", getFid());
            jsonObject.put("devicetoken", getDevicetoken());
            jsonObject.put("postid", getPostid());
            jsonObject.put("otp", getOtp());
            jsonObject.put("lat", getLatitude());
            jsonObject.put("lon", getLongitude());
            jsonObject.put("platform", getPlatform());
            jsonObject.put("familynotification", getFamilynotification());
            jsonObject.put("isadmin", isadmin());

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public String getWaitingrequests() {
        return waitingrequests;
    }

    public void setWaitingrequests(String waitingrequests) {
        this.waitingrequests = waitingrequests;
    }

    public String getProfilecompleteness() {
        return profilecompleteness;
    }

    public void setProfilecompleteness(String profilecompleteness) {
        this.profilecompleteness = profilecompleteness;
    }

    public Boolean getIsnew() {
        return isnew;
    }

    public void setIsnew(Boolean isnew) {
        this.isnew = isnew;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public boolean isprofilecreated() {
        return isprofilecreated;
    }

    public void setIsprofilecreated(boolean isprofilecreated) {
        this.isprofilecreated = isprofilecreated;
    }

    public String getRelsurname() {
        return relsurname;
    }

    public void setRelsurname(String relsurname) {
        this.relsurname = relsurname;
    }

    public String getRelfirstname() {
        return relfirstname;
    }

    public void setRelfirstname(String relfirstname) {
        this.relfirstname = relfirstname;
    }

    public String getProfileimageurl() {
        return profileimageurl;
    }

    public void setProfileimageurl(String profileimageurl) {
        this.profileimageurl = profileimageurl;
    }


    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }


    public String getRelmobilenumber() {
        return relmobilenumber;
    }

    public void setRelmobilenumber(String relmobilenumber) {
        this.relmobilenumber = relmobilenumber;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getPushnotification() {
        return pushnotification;
    }

    public void setPushnotification(String pushnotification) {
        this.pushnotification = pushnotification;
    }

    public String getGeolocationaccess() {
        return geolocationaccess;
    }

    public void setGeolocationaccess(String geolocationaccess) {
        this.geolocationaccess = geolocationaccess;
    }

    public String getTagging() {
        return tagging;
    }

    public void setTagging(String tagging) {
        this.tagging = tagging;
    }

    public byte[] getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(byte[] profileimage) {
        this.profileimage = profileimage;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getFamilynotification() {
        return familynotification;
    }

    public void setFamilynotification(String familynotification) {
        this.familynotification = familynotification;
    }

}