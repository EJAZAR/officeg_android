package org.officeg.model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatMessageModel implements Serializable {

    private String messageContentType;
    private String chatType0or1;
    private Object groupOrProfileModel;
    private String totalContent;
    private String fid;
    private Date datetime;
    private String text;
    private String image;
    private String video;
    private String audio;
    private String gid;
    private String typeFromOrTo;
    private String extraString;
    private Integer unreadCount;
    private boolean isprogress;
    private String messageId;
    private boolean isSelected;
    private boolean isreply;
    private boolean isreminder;
    private String reminderMid;
    private Integer acknowledgementStatus;
    private String sourceMessageForReply;

    public Integer getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getTypeFromOrTo() {
        return typeFromOrTo;
    }

    public void setTypeFromOrTo(String typeFromOrTo) {
        this.typeFromOrTo = typeFromOrTo;
    }

    public String getMessageContentType() {
        return messageContentType;
    }

    public void setMessageContentType(String messageContentType) {
        this.messageContentType = messageContentType;
    }

    public Object getGroupOrProfileModel() {
        return groupOrProfileModel;
    }

    public void setGroupOrProfileModel(Object groupOrProfileModel) {
        this.groupOrProfileModel = groupOrProfileModel;
    }

    public String getChatType0or1() {
        return chatType0or1;
    }

    public void setChatType0or1(String chatType0or1) {
        this.chatType0or1 = chatType0or1;
    }

    public String toJSON(ChatMessageModel chatMessageModel) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("messageContentType", chatMessageModel.getMessageContentType());
            jsonObject.put("typeFromOrTo", chatMessageModel.getTypeFromOrTo());
            jsonObject.put("chatType0or1", chatMessageModel.getChatType0or1());
            jsonObject.put("groupOrProfileModel", chatMessageModel.getGroupOrProfileModel());
            jsonObject.put("totalContent", chatMessageModel.getTotalContent());
            jsonObject.put("fid", chatMessageModel.getFid());
            jsonObject.put("gid", chatMessageModel.getGid());
            if (jsonObject.has("datetime")) {
                String timeStamp = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(chatMessageModel.getDatetime());
                jsonObject.put("datetime", timeStamp);
            }
            jsonObject.put("text", chatMessageModel.getText());
            jsonObject.put("image", chatMessageModel.getImage());
            jsonObject.put("audio", chatMessageModel.getAudio());
            jsonObject.put("video", chatMessageModel.getVideo());
            jsonObject.put("messageId", chatMessageModel.getMessageId());
            jsonObject.put("isreply", chatMessageModel.isreply());
            jsonObject.put("extraString", chatMessageModel.getExtraString());
            jsonObject.put("unreadCount", chatMessageModel.getUnreadCount());
            jsonObject.put("isprogress", chatMessageModel.isIsprogress());
            jsonObject.put("isSelected", chatMessageModel.isSelected());
            jsonObject.put("sourceMessageForReply", chatMessageModel.getSourceMessageForReply());
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public ChatMessageModel toObject(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            setMessageContentType(jsonObject.has("messageContentType") ? jsonObject.getString("messageContentType") : null);
            setTypeFromOrTo(jsonObject.has("typeFromOrTo") ? jsonObject.getString("typeFromOrTo") : null);
            setChatType0or1(jsonObject.has("chatType0or1") ? jsonObject.getString("chatType0or1") : null);
            setGroupOrProfileModel(jsonObject.has("groupOrProfileModel") ? jsonObject.get("groupOrProfileModel") : null);
            setTotalContent(jsonObject.has("totalContent") ? jsonObject.getString("totalContent") : null);
            setFid(jsonObject.has("fid") ? jsonObject.getString("fid") : null);
            if (jsonObject.has("datetime")) {
                Date datetime = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(jsonObject.getString("datetime"));
                setDatetime(datetime);
            }
            setText(jsonObject.has("text") ? jsonObject.getString("text") : null);
            setImage(jsonObject.has("image") ? jsonObject.getString("image") : null);
            setAudio(jsonObject.has("audio") ? jsonObject.getString("audio") : null);
            setVideo(jsonObject.has("video") ? jsonObject.getString("video") : null);
            setMessageId(jsonObject.has("messageId") ? jsonObject.getString("messageId") : null);
            setIsreply(jsonObject.has("isreply") && jsonObject.getBoolean("isreply"));
            setGid(jsonObject.has("gid") ? jsonObject.getString("gid") : null);
            setExtraString(jsonObject.has("extraString") ? jsonObject.getString("extraString") : null);
            setUnreadCount(jsonObject.has("unreadCount") ? jsonObject.getInt("unreadCount") : 0);
            setIsprogress(jsonObject.has("isprogress") && jsonObject.getBoolean("isprogress"));
            setSelected(jsonObject.has("isSelected") && jsonObject.getBoolean("isSelected"));
            setSourceMessageForReply(jsonObject.has("sourceMessageForReply") ? jsonObject.getString("sourceMessageForReply") : null);
            return this;
        } catch (Exception e) {
            Log.d("ChatMessageModel", "toJson = " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public String getTotalContent() {
        return totalContent;
    }

    public void setTotalContent(String totalContent) {
        this.totalContent = totalContent;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = new Date(datetime.getTime());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getExtraString() {
        return extraString;
    }

    public void setExtraString(String extraString) {
        this.extraString = extraString;
    }

    public boolean isIsprogress() {
        return isprogress;
    }

    public void setIsprogress(boolean isprogress) {
        this.isprogress = isprogress;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isreply() {
        return isreply;
    }

    public void setIsreply(boolean isreply) {
        this.isreply = isreply;
    }

    public boolean isreminder() {
        return isreminder;
    }

    public void setIsreminder(boolean isreminder) {
        this.isreminder = isreminder;
    }

    public String getReminderMid() {
        return reminderMid;
    }

    public void setReminderMid(String reminderMid) {
        this.reminderMid = reminderMid;
    }

    public Integer getAcknowledgementStatus() {
        return acknowledgementStatus;
    }

    public void setAcknowledgementStatus(Integer acknowledgementStatus) {
        this.acknowledgementStatus = acknowledgementStatus;
    }

    public String getSourceMessageForReply() {
        return sourceMessageForReply;
    }

    public void setSourceMessageForReply(String sourceMessageForReply) {
        this.sourceMessageForReply = sourceMessageForReply;
    }
}