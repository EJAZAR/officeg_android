package org.officeg.nearby;

import org.officeg.util.Utility;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Donpp on 8/22/2017.
 */

public class NearbyModel implements Serializable {

    String displayname,latitude, longitude,relationship_type,profileimage,fid;
    Long time;
    boolean isfav;
    public NearbyModel(JSONObject nearbyobject) {

        try {
            displayname = nearbyobject.getString("displayname");
            latitude = nearbyobject.getString("lat");
            longitude = nearbyobject.getString("lon");
            if(!(nearbyobject.getString("profileimage").equals(""))) {
                profileimage = nearbyobject.getString("profileimage");
            }
            time=nearbyobject.getLong("time");
            fid=nearbyobject.getString("fid");
            isfav= nearbyobject.getBoolean("isfav");



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getDisplayname() {
        return displayname;
    }


    public Double getLatitude() {
        return Double.parseDouble(latitude);
    }

    public Double getLongitude() {
        return Double.parseDouble(longitude);
    }

    public String getProfileimage() {
        return profileimage;
    }

    public String getTime() {
        return Utility.ElapsedTime(time);
    }

    public String getFid() {
        return fid;
    }
}
