package org.officeg.settings;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.database.DataManager;
import org.officeg.mobileapp.R;
import org.officeg.mobileapp.TreeActivity;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.notification.MessageListener;
import org.officeg.notification.MessageModel;
import org.officeg.util.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BookmarksAdapter extends BaseAdapter implements MessageListener {

    ArrayList listvalues = new ArrayList();
    LayoutInflater inflater;
    Context context;
    UserModel user = null;
    byte[] bytelistcomment, bytelistlike, byteblock;
    private FirebaseAnalytics mFirebaseAnalytics;

    public BookmarksAdapter(Context context, ArrayList listvalues) {
        this.listvalues = listvalues;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public ProfileModel getItem(int position) {

        return (ProfileModel) listvalues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("FamilyG", "creating postview: ");
        ProfileModel profileModel = (ProfileModel) listvalues.get(position);
        BookmarksAdapter.MyViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.bookmarkslist, parent, false);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            viewholder = new BookmarksAdapter.MyViewHolder(convertView, profileModel);
            viewholder.addListener(this);

            convertView.setTag(viewholder);
        } else {
            viewholder = (BookmarksAdapter.MyViewHolder) convertView.getTag();
        }

        //viewholder.setFeedModel(blockedListModel);
        return convertView;
    }

    @Override
    public void removeMessage(MessageModel messageModel) {

    }

    @Override
    public void removeBlock(Object messageModel) {
        listvalues.remove(messageModel);
        notifyDataSetChanged();
    }

    private class MyViewHolder {

        TextView displayname;
        CircleImageView profileimage;
        Button removebookmark, viewbookmark;
        ProfileModel itemModel;

        private List<MessageListener> listeners = new ArrayList<MessageListener>();

        MyViewHolder(View item, final ProfileModel itemModel) {
            this.itemModel = itemModel;
            displayname = item.findViewById(R.id.bookmarkdisplayname);
            profileimage = item.findViewById(R.id.bookmark_image_id);
            removebookmark = item.findViewById(R.id.removebookmark);
            viewbookmark = item.findViewById(R.id.viewbookmark);

            displayname.setText(itemModel.getDisplayname());
            if (itemModel.getProfileimage() != null && !itemModel.getProfileimageurl().trim().isEmpty()) {
                Utility.showProfilePicture(itemModel.getProfileimageurl(), context, profileimage, itemModel.getDisplayname(), true);
            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }

            removebookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context)
                            .setTitle("Confirm")
                            .setMessage("Do you really want to remove?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    DataManager dataManager = DataManager.getInstance(context);
                                    dataManager.deleteTable("bookmarks", "fid", itemModel.getFid());
                                    raiseRemoveEvent();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
            });
            viewbookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chatMessageIntent = new Intent(context, TreeActivity.class);
                    chatMessageIntent.putExtra("profileModel", itemModel.toJSON());
                    context.startActivity(chatMessageIntent);
                }
            });
        }

        void addListener(MessageListener toAdd) {
            listeners.add(toAdd);
        }

   /*     public void setFeedModel(final BlockedListModel itemModel) {
            this.itemModel = itemModel;
            displayname.setText(itemModel.getDisplayname());
            mobilenumber.setText(itemModel.getMobilenumber());
            if (itemModel.getProfileimage() != null && !itemModel.getProfileimage().trim().isEmpty()) {
                ImageLoader imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(itemModel.getProfileimage(), profileimage);

            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }


            unblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.unblock_alert);
                    TextView blockmessage = (TextView) dialog.findViewById(R.id.unblocktext);
                    Button dialogBlockButton = (Button) dialog.findViewById(R.id.dialogButtonUnBlock);
                    dialogBlockButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject object = new JSONObject();
                            try {
                                object.put("secret_key", UserModel.getInstance().secret_key);
                                object.put("fid", itemModel.getFid());
                                Log.i("jobjblock", String.valueOf(object));
                                byteblock = object.toString().getBytes();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            APIManager api = new APIManager("relationship/unblockuser", byteblock, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject result) {


                                }

                                @Override
                                public void onSuccess(JSONArray object) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {

                                }
                            }, APIManager.HTTP_METHOD.POST);

                            api.execute();
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            });

        }*/

        public void raiseRemoveEvent() {
            //  Log.i("FamilyG",itemModel.getSenderid());
            // Notify everybody that may be interested.
            for (MessageListener hl : listeners)
                hl.removeBlock(itemModel);
        }
    }
}

