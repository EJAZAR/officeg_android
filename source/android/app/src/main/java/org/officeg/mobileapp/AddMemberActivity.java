package org.officeg.mobileapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.contactpicker.ContactPickerActivity;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.profile.ProfileView;
import org.officeg.profile.RelationshipType;
import org.officeg.util.Utility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddMemberActivity extends AppCompatActivity {

    final Context context = this;
    Spinner relationshipSpinner;
    String fid;
    TextView marque;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final String TAG = AddMemberActivity.class.getSimpleName();

    // View lookup cache
    private static class ProfileListView {
        TextView txtRelationshipType;
        android.support.v7.widget.AppCompatImageView btnAddMember;
        CircleImageView imageView;
        RelativeLayout relativeLayoutAddMember;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        relationshipSpinner = (Spinner) findViewById(R.id.spinner_source_id);

        final ArrayAdapter<ProfileModel> spinnerArrayAdapter = new ArrayAdapter<ProfileModel>(
                this, R.layout.spinner_item, UserModel.getInstance().ownedProfileList);
        relationshipSpinner.setAdapter(spinnerArrayAdapter);
//        relationshipSpinner.setSelection(0);

        relationshipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utility.logFA(mFirebaseAnalytics, "addmember screen", "relationshipspinner", "spinner");
                Log.d(TAG, "relationshipSpinner");
                ProfileModel selectedModel = (ProfileModel) parent.getItemAtPosition(position);
                if (!Utility.selectedFid.equalsIgnoreCase(selectedModel.getFid())) {
                    Utility.selectedFid = selectedModel.getFid();
                    // If user change the default selection
                    // First item is disable and it is used for hint
                    Utility.selectedSource = relationshipSpinner.getSelectedItem().toString();
                    updateMembers();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Utility.selectedFid = UserModel.getInstance().fid;
        Utility.selectedSource = relationshipSpinner.getSelectedItem().toString();
        updateMembers();
    }

    private void updateMembers() {
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        marque = (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_add_member);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMemberActivity.this, BottomBarActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        String gender = UserModel.getInstance().gender, spouseRelation, hiddenRelation;
        if (gender == null) {
            spouseRelation = getString(R.string.wife).toLowerCase();
            hiddenRelation = getString(R.string.husband).toLowerCase();
        } else {
            if (gender.equalsIgnoreCase(getString(R.string.male))) {
                spouseRelation = getString(R.string.wife).toLowerCase();
                hiddenRelation = getString(R.string.husband).toLowerCase();
            } else {
                spouseRelation = getString(R.string.husband).toLowerCase();
                hiddenRelation = getString(R.string.wife).toLowerCase();
            }
        }
//        LinearLayout ll_add_spouse = (LinearLayout) findViewById(getResources().getIdentifier("ll_add_" + spouseRelation, "id", getPackageName()));
//        ll_add_spouse.setVisibility(View.VISIBLE);

        LinearLayout ll_relations_firstdegree = (LinearLayout) findViewById(R.id.ll_relations_firstdegree);
        ll_relations_firstdegree.removeAllViews();

        LinearLayout ll_relations_seconddegree = (LinearLayout) findViewById(R.id.ll_relations_seconddegree);
        ll_relations_seconddegree.removeAllViews();

        // first degree relations
        for (int k = 0; k < 3; k++) {
            RelationshipType relationship = RelationshipType.values()[k];
            ArrayList<ProfileModel> profiles = UserModel.getInstance().profileMemberModel.getProfilesByRelationship(Utility.selectedFid, relationship);
            ll_relations_firstdegree = (LinearLayout) findViewById(R.id.ll_relations_firstdegree);
            addRelationshipByType(relationship, k, profiles, ll_relations_firstdegree, 5850, hiddenRelation);
        }

        // relations
//        RelationshipType relationship = RelationshipType.RELATION;
//        ArrayList<ProfileModel> profiles = UserModel.getInstance().profileMemberModel.getProfilesByRelationship(Utility.selectedFid, relationship);
//        ll_relations_seconddegree = (LinearLayout) findViewById(R.id.ll_relations_seconddegree);
//        addRelationshipByType(RelationshipType.RELATION, 0, profiles, ll_relations_seconddegree, 3960, hiddenRelation);

        // second degree relations
        int m = 0;
        for (int k = 8; k < RelationshipType.length() - 1; k++) {
            RelationshipType relationship = RelationshipType.values()[k];
            ArrayList<ProfileModel> profiles = UserModel.getInstance().profileMemberModel.getProfilesByRelationship(Utility.selectedFid, relationship);
            ll_relations_seconddegree = (LinearLayout) findViewById(R.id.ll_relations_seconddegree);
            if (profiles.size() > 0) {
                addRelationshipByType(relationship, m++, profiles, ll_relations_seconddegree, 3961, hiddenRelation);
            }
        }

        LinearLayout ll_invite_members = (LinearLayout) findViewById(R.id.ll_invite_members);
        ll_invite_members.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMemberActivity.this, ContactPickerActivity.class);
                startActivity(i);
            }
        });

        UserModel.getInstance().loadMembers();
    }

    private void addRelationshipByType(RelationshipType relationship, int k, ArrayList<ProfileModel> profiles, LinearLayout layoutAdapter, int uniqueId, String hiddenRelation) {
        LinearLayout singleLineLinearLayout;
        int currentId;
        if (k % 2 == 0) {
            Log.d(TAG, "k = " + k);

            currentId = uniqueId + k;
            singleLineLinearLayout = new LinearLayout(getApplicationContext());
            float scale = getApplicationContext().getResources().getDisplayMetrics().density;
            int dpValue = (int) (50 * scale + 0.5f);

            LinearLayout.LayoutParams singleLineLLparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpValue);
            singleLineLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            singleLineLinearLayout.setWeightSum(2.0f);
            singleLineLinearLayout.setId(currentId);
            singleLineLinearLayout.setLayoutParams(singleLineLLparams);

            layoutAdapter.addView(singleLineLinearLayout);

            View lineView = new View(getApplicationContext());
            scale = getApplicationContext().getResources().getDisplayMetrics().density;
            dpValue = (int) (1.5 * scale + 0.5f);
            lineView.setBackgroundColor(getResources().getColor(R.color.background_ed));
            lineView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpValue));
            layoutAdapter.addView(lineView);

        } else {
            currentId = uniqueId + (k - 1);
            singleLineLinearLayout = (LinearLayout) findViewById(currentId);
        }
        String relationtype = String.valueOf(relationship);
        if (singleLineLinearLayout != null && !relationtype.equalsIgnoreCase(hiddenRelation)) {
            addLeftorRightWidget(profiles, currentId, relationship, singleLineLinearLayout);
        }

    }

    private void addLeftorRightWidget(ArrayList<ProfileModel> profiles, int currentId, RelationshipType relationship, LinearLayout singleLineLinearLayout) {
        View add_member_relation = LayoutInflater.from(this).inflate(
                R.layout.add_member_relation, null);
        LinearLayout ll_add_father = (LinearLayout) add_member_relation.findViewById(R.id.ll_add_relation);
        if (ll_add_father.getParent() != null)
            ((ViewGroup) ll_add_father.getParent()).removeView(ll_add_father);

        singleLineLinearLayout.addView(ll_add_father);
        TextView tv_add_relation = (TextView) findViewById(R.id.tv_add_relation);
        tv_add_relation.setId(R.id.tv_add_relation + currentId);
        String relationtype = String.valueOf(relationship);
        relationtype = relationtype.substring(0, 1).toUpperCase() + relationtype.substring(1).toLowerCase();

        tv_add_relation.setText(relationtype);

        ImageView imageView_add = (ImageView) findViewById(R.id.iv_add_relation);
        imageView_add.setId(R.id.iv_add_relation + currentId);

        ImageView imageView_tick = (ImageView) findViewById(R.id.iv_tick_relation);
        imageView_tick.setId(R.id.iv_tick_relation + currentId);

        ImageView imageView_pending = (ImageView) findViewById(R.id.iv_pending_relation);
        imageView_pending.setId(R.id.iv_pending_relation + currentId);

        LinearLayout ll_add = (LinearLayout) findViewById(R.id.ll_add_relation);
        ll_add.setId(R.id.ll_add_relation + currentId);

        if (profiles.size() > 0) {
            boolean isNotPending = false;
            for (ProfileModel profileModel : profiles) {
                if (!profileModel.status.equals(getString(R.string.pending))) {
                    isNotPending = true;
                    break;
                }
            }
            if (isNotPending) {
                imageView_add.setVisibility(View.GONE);
                imageView_tick.setVisibility(View.VISIBLE);
                ll_add.setOnClickListener(new AddOnClickListener(relationship, true));
            } else {
                imageView_add.setVisibility(View.GONE);
                imageView_pending.setVisibility(View.VISIBLE);
                ll_add.setOnClickListener(new AddOnClickListener(relationship, true));
            }
        } else {
            ll_add.setOnClickListener(new AddOnClickListener(relationship, false));
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent(this, BottomBarActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();
    }

    class AddOnClickListener implements View.OnClickListener {
        RelationshipType relationshipType;
        boolean isRelationAvailable;

        AddOnClickListener(RelationshipType relationshipType, boolean isRelationAvailable) {
            this.relationshipType = relationshipType;
            this.isRelationAvailable = isRelationAvailable;
        }

        @Override
        public void onClick(View v) {
            Utility.selectedRelationType = relationshipType;
            if (isRelationAvailable) {
                Dialog relationDialog = new Dialog(context);
                View currentRelationView = getCurrentRelationView(relationshipType, relationDialog);
                relationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                relationDialog.setContentView(currentRelationView);
                relationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                relationDialog.show();
            } else {
                connectOption(relationshipType);
            }
        }
    }

    public View getCurrentRelationView(final RelationshipType relationshipType, final Dialog relationDialog) {

        ProfileModel profileModel = ProfileModel.getInstance();
        Bitmap imagebitmap = null;
        if (profileModel.profileimage != null) {
            imagebitmap = BitmapFactory.decodeByteArray(profileModel.profileimage, 0, profileModel.profileimage.length);
        }

        final ProfileListView viewHolder = new ProfileListView();
        View convertView = getLayoutInflater().inflate(R.layout.profile_member_view, null);
        viewHolder.txtRelationshipType = (TextView) convertView.findViewById(R.id.memebertype_id);
        viewHolder.btnAddMember = (android.support.v7.widget.AppCompatImageView) convertView.findViewById(R.id.btn_add_member);
        viewHolder.relativeLayoutAddMember = (RelativeLayout) convertView.findViewById(R.id.relativeLayout);
        viewHolder.imageView = (CircleImageView) convertView.findViewById(R.id.imageView_contact);

        convertView.setTag(viewHolder);

        LinearLayout profileList = (LinearLayout) convertView.findViewById(R.id.profileList);
        profileList.removeAllViews();

        ArrayList<ProfileModel> profiles = UserModel.getInstance().profileMemberModel.getProfilesByRelationship(Utility.selectedFid, relationshipType);
        for (int index = 0; index < profiles.size(); index++) {
            new ProfileView(profiles.get(index), context, profileList);
        }
        viewHolder.txtRelationshipType.setText(relationshipType.name());
        if (profileModel.profileimage != null)
            viewHolder.imageView.setImageBitmap(imagebitmap);
        viewHolder.btnAddMember.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                connectOption(relationshipType);
                relationDialog.dismiss();
            }
        });

        viewHolder.relativeLayoutAddMember.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                connectOption(relationshipType);
            }
        });
        return convertView;
    }

    public void connectOption(RelationshipType relationshipType) {
        View connectRelationView = getConnectRelationView();
        Dialog relationDialog = new Dialog(context);
        relationDialog.setContentView(connectRelationView);
        relationDialog.getWindow().setBackgroundDrawableResource(R.drawable.round_corner);
        relationDialog.setTitle("Add " + String.valueOf(relationshipType).toLowerCase() + " from");
        relationDialog.show();
    }

    public View getConnectRelationView() {
        View convertView = getLayoutInflater().inflate(R.layout.add_family_member_popup, null);
        LinearLayout ll_phone_contact_book = (LinearLayout) convertView.findViewById(R.id.ll_phone_contact_book);
        ll_phone_contact_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMemberActivity.this, ProfileFormActivity.class);
                i.putExtra("type", "1");
                startActivity(i);
            }
        });

        LinearLayout ll_using_fgid = (LinearLayout) convertView.findViewById(R.id.ll_using_fgid);
        ll_using_fgid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMemberActivity.this, ProfileFormActivity.class);
                i.putExtra("type", "2");
                startActivity(i);
            }
        });

        LinearLayout ll_without_phone = (LinearLayout) convertView.findViewById(R.id.ll_without_phone);
        ll_without_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMemberActivity.this, ProfileFormActivity.class);
                i.putExtra("type", "4");
                startActivity(i);
            }
        });

        return convertView;
    }
}