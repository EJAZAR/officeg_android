package org.officeg.mobileapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;

public class ChangeNumberActivity extends AppCompatActivity {
    final Context context = this;
    EditText oldNumber, newNumber,oldCountryCode,newCountryCode;
    Button btnNext;
    TextView error;
    UserModel user = null;
    ProfileModel profile = null;
    String oldMobileNumber, newMobileNumber, newCC, oldCC, res, mobileNumberPattern, type, editOption,editCountrycode,editPhonenumber;
    private FirebaseAnalytics mFirebaseAnalytics;
    String present_country,presentCountryCode;
    int phoneLength;
    String[] countries,codes;
    ProgressDialog progDailog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);
//        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if(!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        user = UserModel.getInstance();
        profile = ProfileModel.getInstance();
        Intent i = getIntent();
        editOption = i.getStringExtra("type");
        editPhonenumber = i.getStringExtra("enteredNewNumber");
        editCountrycode = i.getStringExtra("enteredNewCountryCode");
        Log.i("FamilyG","editCountrycode: " +editCountrycode);


        codes = this.getResources().getStringArray(R.array.country_codes);
        oldNumber = (EditText) findViewById(R.id.oldnumber_id);
        newNumber = (EditText) findViewById(R.id.newnumber_id);
        oldCountryCode = (EditText) findViewById(R.id.oldcountrycode_id);
        newCountryCode = (EditText) findViewById(R.id.newcountrycode_id);
        error = (TextView) findViewById(R.id.id_mobile_number_err_msg);
        oldNumber.requestFocus();

        if (editOption != null && editOption.equals("Edit")) {
            oldNumber.setText(user.mobilenumber);
            newNumber.setSelection(newNumber.getText().length());
            newCountryCode.setText(editCountrycode);
            newNumber.setText(editPhonenumber);
            oldMobileNumber = oldNumber.getText().toString();
            newMobileNumber = newNumber.getText().toString();
            Log.i("FamilyG: ", "oldNo from edit: " + oldMobileNumber);
            Log.i("FamilyG: ", "newNo from edit: " + newMobileNumber);

            changeNumber();
        } else
            changeNumber();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        TextView marque= (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.change_number_title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());
                Intent intent = new Intent(ChangeNumberActivity.this, BottomBarActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        oldNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldNumber.setCursorVisible(true);
                error.setError(null);
                error.setText(null);
            }
        });

        newNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldNumber.setCursorVisible(true);
                error.setError(null);
                error.setText(null);
            }
        });

        setDefaultCountryCode();
    }

    public void changeNumber() {
        final String pattern = getString(R.string.mobile_number_pattern);
        btnNext = (Button) findViewById(R.id.btn_next_id);
        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oldNumber.setCursorVisible(false);
                newNumber.setCursorVisible(false);
                Utility.logFA(mFirebaseAnalytics, "changenumber screen", "next", "button");
                progDailog = new ProgressDialog(ChangeNumberActivity.this);
                progDailog.setTitle("Please Wait");
                progDailog.setMessage("Loading...");
                progDailog.setIndeterminate(false);
                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDailog.setCancelable(true);
                progDailog.show();
                String newCode = newCountryCode.getText().toString().replaceAll("[+]","");

                String c[] = getResources().getStringArray(R.array.only_codes);

                try {
                    String number = newCode.replaceAll("\\s+", "");
                    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                    String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(number));
                    String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                    phoneLength = exampleNumber.length();
                    Log.i("FamilyG", "length inside " + phoneLength);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                user = UserModel.getInstance();
                Log.i("FamilyG: ", "on click next");

                mobileNumberPattern = (getString(R.string.mobile_number_pattern));
                oldNumber = (EditText) findViewById(R.id.oldnumber_id);
                newNumber = (EditText) findViewById(R.id.newnumber_id);

                oldMobileNumber = oldNumber.getText().toString();
                newMobileNumber = newNumber.getText().toString();

                Log.i("FamilyG: ", "oldNo onclick nxt: " + oldMobileNumber);
                Log.i("FamilyG: ", "newNo onclick nxt: " + newMobileNumber);

                newCC = ((TextView) findViewById(R.id.newcountrycode_id)).getText().toString();
                oldCC = ((TextView) findViewById(R.id.oldcountrycode_id)).getText().toString();

                if (oldMobileNumber.equals("") || newMobileNumber.equals("")) {
                    error.setError("");
                    error.setText(R.string.empty_mobile_number);
                    progDailog.dismiss();
                } else if (!(oldMobileNumber.matches(mobileNumberPattern)) && (newMobileNumber.matches(mobileNumberPattern))) {
                    error.setError("");
                    error.setText(R.string.invalid_mobile_number);
                    progDailog.dismiss();
                } else if (!(oldMobileNumber.equals(user.mobilenumber))) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.custom_alert);

                    // set the custom_alert dialog components - text, image and button
                    TextView text = (TextView) dialog.findViewById(R.id.text);
                    text.setText("The phone number you entered doesn't match with your accounts");

                    Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                    // if button is clicked, close the custom_alert dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    progDailog.dismiss();
                }else if (oldMobileNumber.equals(newMobileNumber)) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.custom_alert);

                    // set the custom_alert dialog components - text, image and button
                    TextView text = (TextView) dialog.findViewById(R.id.text);
                    text.setText("Your old and new phone numbers are same");

                    Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                    // if button is clicked, close the custom_alert dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
//                            Intent i = new Intent(ChangeNumberActivity.this,BottomBarActivity.class);
//                            startActivity(i);
                        }
                    });
                    dialog.show();
                    progDailog.dismiss();
                }
                else if(!(Arrays.asList(c).contains(newCode))){
                    Log.i("FamilyG","Code not present");
                    error.setError("");
                    error.setText("Entered country code doesnt exists");
                    progDailog.dismiss();
                }
                else if (!(newMobileNumber.replaceAll("[\\D]", "").matches(pattern))) {
                    error.setError("");
                    error.setText(R.string.invalid_mobile_number);
                    progDailog.dismiss();
                }
                else {
                    profile.countrycode = newCC;
                    profile.mobilenumber = oldMobileNumber;
                    profile.secret_key = user.getSecret_key();

                    Log.i("FamilyG ", "Profile MobileNo: " + profile.mobilenumber);
                    Log.i("FamilyG ", "Profile SK: " + profile.secret_key);
                    Log.i("FamilyG ", "Profile countrycode: " + profile.countrycode);


                    String profilemodel = profile.toJSON();
                    byte[] profilebytearray = profilemodel.getBytes(Charset.forName("UTF-8"));

                    APIManager api = new APIManager("changenumber", profilebytearray, new APIListener() {

                        @Override
                        public void onSuccess(JSONObject result) {

                            Log.i("FamilyG", user.mobilenumber);
                            try {
                                res = result.getString("refcode");
                                user.otp = result.getString("otp");
                                Log.i("Ref==", res);
                                Intent intent_activity = new Intent(ChangeNumberActivity.this, PhoneAuthenticationActivity.class);
                                Log.i("FamilyG: ", "Profile MobileNo onsuccess: " + profile.mobilenumber);
                                intent_activity.putExtra("NewPhoneNumber", newMobileNumber);
                                intent_activity.putExtra("OldPhoneNumber", profile.mobilenumber);
                                intent_activity.putExtra("CountryCode", profile.countrycode);
                                intent_activity.putExtra("oldCountryCode", oldCC);
                                intent_activity.putExtra("Secretkey", profile.secret_key);
                                intent_activity.putExtra("Referencecode", res);
                                intent_activity.putExtra("OTP", user.otp);
                                intent_activity.putExtra("type", "changenumber");
                                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent_activity);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            user.save();
                            progDailog.dismiss();

                        }

                        @Override
                        public void onSuccess(JSONArray array) {

                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            error.setError("");
                            error.setText(errorMessage);
                            progDailog.dismiss();
                        }
                    }, APIManager.HTTP_METHOD.POST);

                    api.execute();
                }

                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }

        });



    }

    public void setDefaultCountryCode(){
        String CountryID = "";
        String Country = "";
        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        Locale loc = new Locale("", CountryID);
        present_country = loc.getDisplayCountry();
        Log.i("FamilyG","Present countryname: " +present_country);
        Log.i("FamilyG","Present CountryID: " +CountryID);
        final String simCountry = manager.getSimCountryIso();
        Log.i("FamilyG","Present simCountry: " +simCountry);


        String networkCountry = manager.getNetworkCountryIso();
        String locale = networkCountry.toLowerCase(Locale.getDefault());
        Log.i("FamilyG","Present locale: " +locale);


       countries = this.getResources().getStringArray(R.array.country_arrays);
        for (int i = 0; i < countries.length; i++) {
            String g = countries[i];
            if (g.equals(present_country)) {
                Country = g;
                Log.i("FamilyG", "Country : " + Country);
                break;
            }
        }


        for (int i = 0; i < codes.length; i++) {
            String[] g = codes[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                presentCountryCode = g[0];
                Log.i("FamilyG", "CountryZip code : " + presentCountryCode);
                break;
            }
        }

        Intent i =getIntent();
        String oldCC = i.getStringExtra("enteredOldCountryCode");

        oldCountryCode.setText(oldCC);
        oldCountryCode.setText(presentCountryCode);
        if (editOption != null && editOption.equals("Edit")) {
            newNumber.setText(editPhonenumber);
        }
        else {

            newCountryCode.setText(presentCountryCode);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ChangeNumberActivity.this, BottomBarActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
