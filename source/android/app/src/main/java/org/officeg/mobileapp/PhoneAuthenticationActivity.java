package org.officeg.mobileapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.controls.FGEdit;
import org.officeg.firebase.SharedPrefManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PhoneAuthenticationActivity extends Activity implements AdapterView.OnItemSelectedListener, FGEdit.FGEditListener {

    final Context context = this;
    String phoneNumber, mobileNumberPattern, country, code, token, newcountryCode, newMobileNo;
    Button btnSendOtp;
    TextView error, country_code_view, resendOTPTextView;
    String[] codes_array, country_array;
    SearchableSpinner selected_country;
    int index_code;
    UserModel user = null;
    String mobileNumber, country_code;
    String CountryZipCode = "";
    int check = 0;
    RelativeLayout regPage, otpPage;
    String type;
    Button verify;
    TextView display_input_mobile_number;
    FGEdit otp_verifyCode1;
    Intent i;
    int phoneLength;
    double weight_displayname, weight_firstname, weight_surname, weight_gender, weight_doi, weight_dob, weight_profileimage;
    Boolean newNumber = false;
    int toatalweightageinint;
    boolean called = false;
    PhoneAuthProvider.ForceResendingToken mResendToken;
    ImageButton editBtn;
    ProgressDialog progDailog;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationCallbacks;
    private String TAG = "PhoneAuthenticActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_authentication);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mAuth = FirebaseAuth.getInstance();

        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if (!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Toast.makeText(PhoneAuthenticationActivity.this, "Now you are logged In" + firebaseAuth.getCurrentUser().getProviderId(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PhoneAuthenticationActivity.this, TutorialActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };


        user = UserModel.getInstance();
        display_input_mobile_number = findViewById(R.id.input_mobile_number);

        codes_array = getResources().getStringArray(R.array.country_codes);
        otp_verifyCode1 = findViewById(R.id.otp_edText1);
        resendOTPTextView = findViewById(R.id.resendcode_btn);
        editBtn = findViewById(R.id.btn_edit);
        error = findViewById(R.id.id_mobile_number_err_msg);


        country_array = getResources().getStringArray(R.array.country_arrays);

        selected_country = findViewById(R.id.spinner_country);
        selected_country.setTitle("Select Country");
        country = selected_country.getSelectedItem().toString();

        index_code = Arrays.asList(country_array).indexOf(country);

        code = Arrays.asList(codes_array).get(index_code);
        Log.i("newcountryCode", code);
        country_code_view = findViewById(R.id.ccode_view);


        String CountryID = "";
        String Country = "";


        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        Locale loc = new Locale("", CountryID);
        String displayName = loc.getDisplayCountry();
        Log.d("FamilyG", "User is from " + displayName);
        selected_country.setPrompt(displayName);


        String[] country_name = this.getResources().getStringArray(R.array.country_arrays);
        for (int i = 0; i < country_name.length; i++) {
            String g = country_name[i];
            if (g.equals(displayName)) {
                Country = g;
                Log.i("FamilyG", "Country : " + Country);
                selected_country.setSelection(i);
                break;
            }
        }

        //String[] rl=this.getResources().getStringArray(R.array.country_codes);
        for (int i = 0; i < codes_array.length; i++) {
            String[] g = codes_array[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                Log.i("FamilyG", "CountryZip code : " + CountryZipCode);
                break;
            }
        }
        country_code_view.setText("+" + CountryZipCode);

        selected_country.setOnItemSelectedListener(this);
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try {
            int number = Integer.parseInt(CountryZipCode.replaceAll("\\s+", ""));
            Log.i("FamilyG", "" + number);
            String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(number);
            String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
            phoneLength = exampleNumber.length();
            Log.i("FamilyG", "length " + phoneLength);
        } catch (Exception e) {
            e.printStackTrace();
        }


        regPage = findViewById(R.id.registration_page);
        otpPage = findViewById(R.id.otp_page);


        i = getIntent();
        type = i.getStringExtra("type");
        newcountryCode = i.getStringExtra("CountryCode");
        Log.i("FamilyG: ", "type==: " + type);
        if (type != null && type.equals("changenumber")) {
            regPage.setVisibility(View.GONE);
            otpPage.setVisibility(View.VISIBLE);
            otp_verifyCode1.setText("");
            newMobileNo = i.getStringExtra("NewPhoneNumber");
            Log.i("FamilyG", "New number: " + newMobileNo);
            display_input_mobile_number.setText("+" + newcountryCode + " " + newMobileNo);
            verifyAccountAlreadyExistOrNot();
//          fromChangeNumber();
        }

        findViewById(R.id.ph_numb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditText) findViewById(R.id.ph_numb)).setCursorVisible(true);

                error.setError(null);
                error.setText(null);

            }
        });

        otp_verifyCode1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_verifyCode1.setCursorVisible(true);
                error.setError(null);
                error.setText(null);
            }
        });

        sendOTP();
        ImageView editButton = findViewById(R.id.ic_edit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regPage.setVisibility(View.VISIBLE);
                otpPage.setVisibility(View.GONE);
                EditText ph_numb = findViewById(R.id.ph_numb);
                ph_numb.requestFocus();
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(ph_numb, InputMethodManager.SHOW_IMPLICIT);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    private void verifyAccountAlreadyExistOrNot() {
        try {
            String mobilenumber = ((EditText) findViewById(R.id.ph_numb)).getText().toString();
            ProfileModel userModel = new ProfileModel();
            userModel.mobilenumber = mobilenumber;
            userModel.mode = "verifyAccountAlreadyExistOrNot";
            APIManager api = new APIManager("verify", userModel, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        boolean isUserExist = result.getBoolean("isexist");
                        if (isUserExist) {
                            sendCode();
                        } else {
                            Toast.makeText(getApplicationContext(), "Your account doesn't exist. Kindly contact admin", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(JSONArray array) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), "onFailure verifyAccountAlreadyExistOrNot = " + errorMessage, Toast.LENGTH_LONG).show();
                }
            });
            api.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPaste() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        String text = clipboard.getText().toString();
        if (text.length() == 6 && Character.isDigit(text.charAt(0)) && Character.isDigit(text.charAt(1)) && Character.isDigit(text.charAt(2))
                && Character.isDigit(text.charAt(3)) && Character.isDigit(text.charAt(4)) && Character.isDigit(text.charAt(5))) {
            otp_verifyCode1.setText("" + text);
        }
        Log.i("FamilyG", "Clip board data : " + clipboard.getText().toString());
    }

    public void sendOTP() {
        verify = findViewById(R.id.btn_verify);
        btnSendOtp = findViewById(R.id.btn_otp);
        final int max = 15, min = 5;
        final String pattern = getString(R.string.mobile_number_pattern);


        btnSendOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((EditText) findViewById(R.id.ph_numb)).setCursorVisible(false);
                Utility.logFA(mFirebaseAnalytics, "Registration", "btnSendOtp", "button");

                token = SharedPrefManager.getInstance(PhoneAuthenticationActivity.this).getItem("tagtoken");

               /* if (token == null) {
                    Toast.makeText(PhoneAuthenticationActivity.this, "Token not generated", Toast.LENGTH_LONG).show();
                    return;
                }*/

                mobileNumberPattern = (getString(R.string.mobile_number_pattern));

                codes_array = getResources().getStringArray(R.array.country_codes);
                country_array = getResources().getStringArray(R.array.country_arrays);

                selected_country = findViewById(R.id.spinner_country);

                country = selected_country.getSelectedItem().toString();

                index_code = Arrays.asList(country_array).indexOf(country);

                code = Arrays.asList(codes_array).get(index_code);
                Log.i("newcountryCode", code);

                phoneNumber = ((EditText) findViewById(R.id.ph_numb)).getText().toString();

                country_code = "+" + CountryZipCode;
                mobileNumber = country_code + phoneNumber;
                Log.i("FamilyG", "CountryCode1:" + country_code);

                while ((phoneNumber.length() > 1) && (phoneNumber.charAt(0) == '0') && (code.equals("91")))
                    phoneNumber = phoneNumber.substring(1);
                Log.i("replace num", "" + phoneNumber.length());

                Log.i("country", country);
                if (phoneNumber.equals("")) {
                    error.setError("");
                    error.setText(R.string.empty_mobile_number);
                } else if (!(phoneNumber.replaceAll("[\\D]", "").matches(pattern))) {
                    Log.i("FamilyG", "Entered phone number length: " + phoneNumber.replaceAll("[\\D]", "").length());
                    error.setError("");
                    error.setText(R.string.invalid_mobile_number);

                } else {

                    Log.i("FamilyG", "cc:" + user.countrycode);
                    Log.i("FamilyG", "mob number: " + user.mobilenumber);

                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.editnumber_alert);


                    // set the custom_alert dialog components - text, image and button
                    TextView text = dialog.findViewById(R.id.number);
                    text.setText(country_code_view.getText().toString() + " " + phoneNumber.replaceAll("[\\D]", ""));

                    Button dialogButtonok = dialog.findViewById(R.id.dialogButtonOK);
                    Button dialogButtonedit = dialog.findViewById(R.id.dialogButtonEDIT);

                    // if button is clicked, close the custom_alert dialog
                    dialogButtonok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            verifyAccountAlreadyExistOrNot();
                        }
                    });

                    dialogButtonedit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            ((EditText) findViewById(R.id.ph_numb)).setSelection(((EditText) findViewById(R.id.ph_numb)).getText().length());
                        }
                    });
                    dialog.show();
                }
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    public void sendCode() {
        try {
            String phoneNumber = country_code_view.getText().toString() + ((EditText) findViewById(R.id.ph_numb)).getText().toString();

            i = getIntent();
            if (phoneNumber.equals("+910210186900") || phoneNumber.equals("+910210186901") || phoneNumber.equals("+910210186902") || phoneNumber.equals("+910210186903") || phoneNumber.equals("+910210186904") || phoneNumber.equals("+910210186905")) {
                Toast.makeText(PhoneAuthenticationActivity.this, "Gandhi family number", Toast.LENGTH_LONG).show();
//            Intent i = new Intent(PhoneAuthenticationActivity.this, TutorialActivity.class);
//            startActivity(i);
//            finish();
                user = UserModel.getInstance();
                user.mobilenumber = ((EditText) findViewById(R.id.ph_numb)).getText().toString();
                user.countrycode = country_code_view.getText().toString();
                user.devicetoken = token;
                user.save();
                firebaseRegisterApi();

            } else {

                type = i.getStringExtra("type");
                Log.i("FamilyG: ", "type==: " + type);
                if (type != null && type.equals("changenumber")) {
                    Log.i("FamilyG", "sendcode new mob numb: " + newMobileNo);
                    phoneNumber = display_input_mobile_number.getText().toString();
                    Log.i("FamilyG", "Change Numberrrrrr : " + phoneNumber);
                }

                setUpVerificatonCallbacks();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        this,               // Activity (for callback binding)
                        verificationCallbacks);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void resendCode() {
        Log.i("FamilyG", "resendCode");
        String phoneNumber = country_code_view.getText().toString().trim() + ((EditText) findViewById(R.id.ph_numb)).getText().toString();

        if (type != null && type.equals("changenumber")) {
            phoneNumber = display_input_mobile_number.getText().toString();
        }

        Toast.makeText(PhoneAuthenticationActivity.this, "OTP has been resent to " + phoneNumber, Toast.LENGTH_SHORT).show();

        setUpVerificatonCallbacks();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                verificationCallbacks,
                mResendToken);
    }

    public void editNumber() {

        Utility.logFA(mFirebaseAnalytics, "verifyotp", "edit", "button");
        i = getIntent();
        type = i.getStringExtra("type");
        Log.i("FamilyG: ", "change number type: " + type);
        newMobileNo = i.getStringExtra("NewPhoneNumber");
        newcountryCode = i.getStringExtra("CountryCode");
        if (type != null && type.equals("changenumber")) {
            Log.i("FamilyG: ", "for change number");
            Intent intent_activity = new Intent(PhoneAuthenticationActivity.this, ChangeNumberActivity.class);
            intent_activity.putExtra("type", "Edit");
//            Log.i("FamilyG","edit phnumb: " +newMobileNo);
//            Log.i("FamilyG","edit cc: "+newcountryCode);
            intent_activity.putExtra("enteredNewNumber", newMobileNo);
            intent_activity.putExtra("enteredNewCountryCode", newcountryCode);
            intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent_activity);
            finish();
        } else {
            Log.i("FamilyG: ", "Edit reg " + user.mobilenumber);
            regPage.setVisibility(View.VISIBLE);
            otpPage.setVisibility(View.GONE);
            ((EditText) findViewById(R.id.ph_numb)).setText(phoneNumber.replaceAll("[\\D]", ""));
            ((EditText) findViewById(R.id.ph_numb)).setSelection(((EditText) findViewById(R.id.ph_numb)).getText().length());
        }
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneAuthenticationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progDailog = new ProgressDialog(PhoneAuthenticationActivity.this);
                        progDailog.setTitle("Please Wait");
                        progDailog.setMessage("Loading...");
                        progDailog.setIndeterminate(false);
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(true);
                        progDailog.show();
                        if (task.isSuccessful()) {

                            i = getIntent();
                            type = i.getStringExtra("type");
                            if (type != null && type.equals("changenumber")) {
//                                Toast.makeText(PhoneAuthenticationActivity.this,"Change number", Toast.LENGTH_LONG).show();
                                verifyNewNumber();
                            } else {
                                user.mode = "userexist";
                                firebaseRegisterApi();
                            }

                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(PhoneAuthenticationActivity.this, "Exception = " + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                progDailog.dismiss();
                            } else {
                                Toast.makeText(PhoneAuthenticationActivity.this, "failed to sign in with credential" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                progDailog.dismiss();
                            }
                        }
                    }
                });
    }


    private void setUpVerificatonCallbacks() {

        verificationCallbacks =
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        Log.i("FamilyG", "onVerificationCompleted");
                        //Called if it is not needed to enter verification code
                        if (called) {
                            otp_verifyCode1.setText(phoneAuthCredential.getSmsCode());
                            otp_verifyCode1.setSelection(otp_verifyCode1.getText().length());
                            return;
                        }
                        signInWithCredential(phoneAuthCredential);


                        Log.i("FamilyG", "called: " + called);
                        newMobileNo = i.getStringExtra("NewPhoneNumber");
                        newcountryCode = i.getStringExtra("CountryCode");

                        user = UserModel.getInstance();

                        i = getIntent();
                        type = i.getStringExtra("type");
                        if (type != null && type.equals("changenumber")) {
                            user.mobilenumber = newMobileNo;
                            user.countrycode = newcountryCode;
                        } else {
                            user.mobilenumber = phoneNumber.replaceAll("[\\D]", "");
                            user.countrycode = country_code_view.getText().toString();
                            user.devicetoken = token;
                            user.save();
                        }
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        //incorrect phone number, verification code, emulator, etc.
                        Toast.makeText(PhoneAuthenticationActivity.this, "onVerificationFailed " + e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Error in verfication failed: " + e.getMessage());
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        //now the code has been sent, save the verificationId we may need it
                        super.onCodeSent(verificationId, forceResendingToken);
                        called = true;

                        mVerificationId = verificationId;
                        mResendToken = forceResendingToken;
                        Log.i("FamilyG", "In validation mVerificationId: " + mVerificationId);

                        i = getIntent();
                        type = i.getStringExtra("type");
                        if (!(type != null && type.equals("changenumber"))) {
                            regPage.setVisibility(View.GONE);
                            otpPage.setVisibility(View.VISIBLE);
                            otp_verifyCode1.setText("");
                            display_input_mobile_number.setText(country_code_view.getText().toString().trim() + " " + phoneNumber.replaceAll("[\\D]", ""));
                        }
                        Log.i("FamilyG", "onCodeSent");

                        verify.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                progDailog = new ProgressDialog(PhoneAuthenticationActivity.this);
                                progDailog.setTitle("Please Wait");
                                progDailog.setMessage("Loading...");
                                progDailog.setIndeterminate(false);
                                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDailog.setCancelable(true);
                                progDailog.show();
                                otp_verifyCode1.setCursorVisible(false);
                                Log.i("FamilyG", "phVerificationID: " + mVerificationId);
                                Log.i("FamilyG", "OTP: " + otp_verifyCode1.getText().toString());
                                if (otp_verifyCode1.getText().toString().trim().equals("")) {
                                    Toast.makeText(PhoneAuthenticationActivity.this, "OTP field should not be empty", Toast.LENGTH_SHORT).show();
                                    progDailog.dismiss();

                                } else {
                                    i = getIntent();
                                    type = i.getStringExtra("type");
                                    if (type != null && type.equals("changenumber")) {
                                        verifyNewNumber();

                                    } else {
                                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp_verifyCode1.getText().toString());
                                        signInWithCredential(credential);
                                        user = UserModel.getInstance();
                                        user.mobilenumber = phoneNumber.replaceAll("[\\D]", "");
                                        user.countrycode = country_code_view.getText().toString();
                                        user.devicetoken = token;
                                        user.save();
                                    }
                                }

                                try {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }

                            }
                        });

                        resendOTPTextView.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                otp_verifyCode1.setText("");
                                resendCode();

                                try {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }
                            }


                        });

                        editBtn.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Log.i("FamilyG", "onclick edit button");
                                editNumber();

                                try {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }
                            }


                        });
                    }

                    //                    }
                    @Override
                    public void onCodeAutoRetrievalTimeOut(String verificationId) {
                        //called after timeout if onVerificationCompleted has not been called
                        super.onCodeAutoRetrievalTimeOut(verificationId);
                    }

                };
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent,
                               View view, int pos, long id) {
        if (++check > 1) {
            country_code_view = findViewById(R.id.ccode_view);
            String countrySelected = parent.getItemAtPosition(pos).toString();
            Log.i("selcountry", country);
            country = selected_country.getSelectedItem().toString();
            index_code = Arrays.asList(country_array).indexOf(countrySelected);
            code = codes_array[index_code];
            Log.i("FamilyG", "code inside onItemclick" + code);
            String[] str = code.split(",");
            country_code_view.setText("+" + str[0]);
            try {
                String number = str[0].replaceAll("\\s+", "");
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(number));
                String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                phoneLength = exampleNumber.length();
                Log.i("FamilyG", "length inside " + phoneLength);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.logFA(mFirebaseAnalytics, "Registration", "CountrySelection", "Spinner");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void verifyNewNumber() {
        progDailog = new ProgressDialog(PhoneAuthenticationActivity.this);
        progDailog.setTitle("Please Wait");
        progDailog.setMessage("Loading...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(true);
        progDailog.show();

        i = getIntent();

        ProfileModel profile = ProfileModel.getInstance();
        newMobileNo = i.getStringExtra("NewPhoneNumber");
        newcountryCode = i.getStringExtra("CountryCode");

        String secretkey = i.getStringExtra("Secretkey");


        Log.i("FamilyG: ", "new..: " + newMobileNo);
        user = UserModel.getInstance();


        user.countrycode = newcountryCode;
        user.secret_key = secretkey;
        user.mobilenumber = newMobileNo;
//        Log.i("FamilyG", "mob:" + user.mobilenumber + "cc:" + user.countrycode + "sk:" + user.secret_key + "relmob:" + user.relmobilenumber + "newotp:" + user.otp);
        user.save();
        String userprofile = user.toJSON();
        Log.i("FamilyG", "change number userprofile: " + userprofile);
        byte[] userbytearray = userprofile.getBytes(Charset.forName("UTF-8"));
        APIManager api = new APIManager("verifynewnumber", userbytearray, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {

                user.mobilenumber = newMobileNo;
                Log.i("FamilyG: ", "New mobile number: " + user.mobilenumber);
                user.save();
                Toast.makeText(getApplicationContext(), "Number Changed", Toast.LENGTH_LONG).show();
                Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());
                Intent intent_activity = new Intent(PhoneAuthenticationActivity.this, BottomBarActivity.class);
                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_activity);
                finish();
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                error.setError("");
                error.setText(errorMessage);
                progDailog.dismiss();

            }
        }, APIManager.HTTP_METHOD.POST);
        api.execute();
    }

    public void firebaseRegisterApi() {
        progDailog = new ProgressDialog(PhoneAuthenticationActivity.this);
        progDailog.setTitle("Please Wait");
        progDailog.setMessage("Loading...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(true);
        progDailog.show();
        user.platform = getString(R.string.android);
        Log.i("FamilyG", "firebaseRegisterApi calling..");
        APIManager api = new APIManager("firebaseregister", user, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                Log.i("FamilyG", "FireBaseRegister API called");
                try {
                    if (user.mode != null && user.mode.equals("userexist") && result.has("userexist")) {
                        user.mode = "";
                        checkForFgid();
                    } else {
                        newNumber = result.getBoolean("isnew");
                        if (result.getBoolean("isnew")) {
                            Log.i("FamilyG", "Firebase api call isnew executing");
                            user.secret_key = result.getString("secret_key");
                            user.waitingrequests = result.getString("waitingrequests");
                            user.mobilenumber = phoneNumber.replaceAll("[\\D]", "");
                            Log.i("FamilyG", "onsuccess verify: " + user.mobilenumber);
                            user.fid = result.getString("fid");
                            Log.i("FamilyG", "fid in firebaseregister api: " + user.fid);
                            Log.i("FamilyG", "waitingreq in firebaseregister api: " + user.waitingrequests);
                            user.setFamilynotification(result.has("familynotification") ? result.getString("familynotification") : null);
                            user.save();
                            Intent i = new Intent(PhoneAuthenticationActivity.this, TutorialActivity.class);
                            Toast.makeText(PhoneAuthenticationActivity.this, "Verification completed ", Toast.LENGTH_SHORT).show();
                            startActivity(i);
                            finish();
                        } else {
                            if ((!result.has("displayname")) || (result.getString("displayname").equals(""))) {
                                Log.i("FamilyG", "Firebase api call displayname is empty");
                                user.secret_key = result.getString("secret_key");
                                user.mobilenumber = phoneNumber.replaceAll("[\\D]", "");
                                Log.i("FamilyG", "onsuccess verify: " + user.mobilenumber);
                                user.fid = result.getString("fid");
                                user.setIsadmin(result.has("isadmin") && result.getBoolean("isadmin"));
                                user.waitingrequests = result.getString("waitingrequests");
                                user.setFamilynotification(result.has("familynotification") ? result.getString("familynotification") : null);
                                user.save();
                                Intent i = new Intent(PhoneAuthenticationActivity.this, TutorialActivity.class);
                                Toast.makeText(PhoneAuthenticationActivity.this, "Verification completed ", Toast.LENGTH_SHORT).show();
                                startActivity(i);
                                finish();
                            } else {
                                Log.i("FamilyG", "Firebase api call preload executing");
                                user.setDisplayname(result.getString("displayname"));
                                user.setFirstname(result.getString("firstname"));
                                user.setLastname(result.has("lastname") ? result.getString("lastname") : null);
                                user.setGender(result.getString("gender"));
                                user.setDoi("3");
                                user.setIsadmin(result.has("isadmin") && result.getBoolean("isadmin"));
                                user.setDob(result.getString("dob"));
                                user.setProfileimageurl(result.getString("profileimage"));
                                user.setFid(result.getString("fid"));
                                user.setSecret_key(result.getString("secret_key"));
                                user.setIsprofilecreated(true);
                                user.setFamilynotification(result.has("familynotification") ? result.getString("familynotification") : null);
                                if (!result.getString("displayname").equals("")) {
                                    weight_displayname = 11.12;
                                }
                                if (!result.getString("firstname").equals("")) {
                                    weight_firstname = 11.12;
                                }
                                if (!result.getString("lastname").equals("")) {
                                    weight_surname = 11.12;
                                }
                                if (!result.getString("gender").equals("")) {
                                    weight_gender = 11.12;
                                }
                                if (!result.getString("dob").equals("")) {
                                    weight_dob = 11.12;
                                }
                                if (!result.getString("profileimage").equals("")) {
                                    weight_profileimage = 11.12;
                                }
                                double toatalweightage = weight_profileimage + weight_displayname + weight_gender + weight_firstname + weight_surname + weight_doi + weight_dob;
                                toatalweightageinint = (int) toatalweightage;
                                user.setProfilecompleteness(String.valueOf(toatalweightageinint));
                                user.save();
                                Intent intent = new Intent(PhoneAuthenticationActivity.this, BottomBarActivity.class);
                                Toast.makeText(PhoneAuthenticationActivity.this, "Verification completed ", Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                progDailog.dismiss();
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                error.setText(errorMessage);
                error.setError("");
            }
        });
        api.execute();

    }

    private void checkForFgid() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Do you already have fid?");

        final EditText input = new EditText(this);
        input.setHint("Fid sample : a1b2c3");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(50, LinearLayout.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(lp);
        builder.setView(input);

        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fid = input.getText().toString();
                if (!fid.trim().isEmpty()) {
                    user.fid = fid;
                    user.mode = "fid";
                    firebaseRegisterApi();
                } else {
                    Toast.makeText(getApplicationContext(), "Kindly enter valid fgid", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                firebaseRegisterApi();
            }
        });

        builder.show();
    }

    @Override
    public void onBackPressed() {
        if (otpPage.getVisibility() == View.VISIBLE) {
            if (type != null && type.equals("changenumber")) {
                String oldCountryCode = i.getStringExtra("oldCountryCode");
                Log.i("FamilyG: ", "for change number");
                Intent intent_activity = new Intent(PhoneAuthenticationActivity.this, ChangeNumberActivity.class);
                intent_activity.putExtra("type", "Edit");
//            Log.i("FamilyG","edit phnumb: " +newMobileNo);
//            Log.i("FamilyG","edit cc: "+newcountryCode);
                intent_activity.putExtra("enteredOldCountryCode", oldCountryCode);
                intent_activity.putExtra("enteredNewNumber", newMobileNo);
                intent_activity.putExtra("enteredNewCountryCode", newcountryCode);
                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_activity);
                finish();
            } else {
                regPage.setVisibility(View.VISIBLE);
                otpPage.setVisibility(View.GONE);
                ((EditText) findViewById(R.id.ph_numb)).setText(phoneNumber.replaceAll("[\\D]", ""));
                ((EditText) findViewById(R.id.ph_numb)).setSelection(((EditText) findViewById(R.id.ph_numb)).getText().length());
            }
            // Its visible
        } else {
            // Either gone or invisible
            finish();
        }
    }
}
