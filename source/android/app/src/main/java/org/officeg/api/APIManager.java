package org.officeg.api;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;

import static org.officeg.api.NetworkConnectionreceiver.isConnection;


public class APIManager extends AsyncTask<Void, Void, JSONObject> {

    public static String errorMessage, errorCode;
    Object param;
    String command;
    APIListener callback;
    JSONObject response;
    boolean isError = true;
    //String method;
    byte[] json_bytes;
    String boundary = "*****";
    String crlf = "\r\n";
    String attachmentName = "file";
    String attachmentFileName = "bitmap.png";

    //    public static String link = "https://api.familyg.org/";
    //    public static String link = "http://13.127.114.227:5925/";
    //    public static String link = "http://35.154.171.111:5926/";
    //    public static String link = "http://192.168.43.187:8080/";
    public static String link = "http://13.233.183.80:5925/";

    private HTTP_METHOD method = HTTP_METHOD.GET;
    private JSONObject result;
    private String TAG = APIManager.class.getSimpleName();

    public APIManager(String command, Object param, APIListener callback) {
        this.command = command;
        this.param = param;
        this.callback = callback;
    }


    public APIManager(String command, byte[] json_bytes, APIListener callback, HTTP_METHOD method) {
        this.command = command;
        this.json_bytes = json_bytes;
        this.callback = callback;
        this.method = method;
    }

    public APIManager(String command, byte[] json_bytes, String fileName, APIListener callback, HTTP_METHOD method) {
        this.command = command;
        this.json_bytes = json_bytes;
        this.callback = callback;
        this.method = method;
        this.attachmentFileName = fileName;
    }

    public JSONObject getResult() {
        return this.result;
    }

    protected void onPreExecute() {
        Log.d(TAG, "onPreExecute");
    }

    protected JSONObject doInBackground(Void... urls) {
        try {
            //network error...
            if (!isConnection) {
                errorMessage = "Unable to connect to server. Please ensure WiFi or Mobile data is enabled.";
                errorCode = "2";
                Log.d(TAG, "doInBackground " + errorMessage);
            }
            //network error ..
            else {
                if (response == null || isError) {  //server failure
                    errorMessage = "Something went wrong. Please retry again.";
                    errorCode = "1";
                    // Log.d(TAG, "doInBackground " + errorMessage);
                }
            }
            //to cancel the  post while uploading...
            if (isCancelled()) {
                Log.i(TAG, "" + isCancelled());
                return (null);
            }
            //to cancel the  post while uploading...

            URL url;

            HttpURLConnection urlConnection = null;
            if (method == HTTP_METHOD.FILE) {
                Log.i(TAG, "working");
                try {
                    url = new URL(link + command);
                    Log.i(TAG, "" + url);
                    try {
                        urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setReadTimeout(600000);
                        urlConnection.setConnectTimeout(600000);

                        urlConnection.setRequestMethod("POST");
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        urlConnection.setDoOutput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Connection", "Keep-Alive");
                        //urlConnection.setRequestProperty("Content_Length", String.valueOf(json_bytes.length));
                        urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);
                        DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());

                        os.writeBytes("--" + this.boundary + this.crlf);
                        os.writeBytes("Content-Disposition: form-data; name=\"" +
                                this.attachmentName + "\";filename=\"" +
                                this.attachmentFileName + "\"" + this.crlf);
                        os.writeBytes(this.crlf);
                        os.write(json_bytes);
                        os.writeBytes(this.crlf);
                        os.writeBytes("--" + this.boundary + "--" + this.crlf);
                        //Log.i("iffff", json_bytes.toString());

                        os.flush();
                        os.close();
                        try {

                            int responsecode = urlConnection.getResponseCode();

                            if (responsecode == HttpsURLConnection.HTTP_OK) {

                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), Charset.forName("UTF-8")));

                                StringBuilder stringBuilder = new StringBuilder();
                                String line;
                                while ((line = bufferedReader.readLine()) != null) {
                                    stringBuilder.append(line).append("\n");
                                }
                                bufferedReader.close();

                                return processResponse(stringBuilder.toString());
                            }

                        } finally {
                            urlConnection.disconnect();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    Log.e(TAG, "ERROR " + e.getMessage(), e);
                }

            } else if (method == HTTP_METHOD.GET) {
                Class paramClass = param.getClass();
                Field[] fields = paramClass.getFields();
                StringBuilder sb = new StringBuilder();

                for (int index = 0; index < fields.length; index++) {
                    String fieldName = fields[index].getName();
                    try {
                        Object value = fields[index].get(param);
                        if (value == null)
                            continue;
                        sb.append(fieldName);
                        sb.append("=");
                        sb.append(URLEncoder.encode(value.toString(), "UTF-8"));
                        if (index != fields.length - 1) {
                            sb.append("&");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    url = new URL(link + command + "?" + sb.toString());
                    Log.i(TAG, "url = " + url);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), Charset.forName("UTF-8")));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferedReader.close();
                        return processResponse(stringBuilder.toString());
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "ERROR " + e.getMessage(), e);
                    // mException = e;
                }
            } else if (method == HTTP_METHOD.POST) {
                // urlConnection.setReadTimeout(600000);
                // urlConnection.setConnectTimeout(600000);
                try {
                    url = new URL(link + command);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    Log.i(TAG, "" + url);
                    urlConnection.setRequestMethod("POST");

                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    urlConnection.setDoOutput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Connection", "Keep-Alive");
                    urlConnection.setRequestProperty("Content_Length", String.valueOf(json_bytes.length));
                    // urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);
                    DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
                    os.write(json_bytes);
                    os.flush();
                    os.close();

                    int responsecode = urlConnection.getResponseCode();
                    if (responsecode == HttpsURLConnection.HTTP_OK) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), Charset.forName("UTF-8")));

                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferedReader.close();

                        return processResponse(stringBuilder.toString());
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                } finally {
                    assert urlConnection != null;
                    urlConnection.disconnect();
                }
            }

        /*if(PostScreenActivity.canceled){
            cancel(true);
            Log.i(TAG,""+canceled);

        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject processResponse(String res) {
        Log.i(TAG, "Response from server " + res);
        this.response = null;
        if (res != null) {
            try {
                response = new JSONObject(res);
                isError = response.getBoolean("iserror");
                if (isError) {
                    errorMessage = response.getString("message");
                    errorCode = response.getString("errorcode");
                }
            } catch (JSONException e) {
                Log.i(TAG, e.getMessage());
            }
        }
        return this.response;
    }

    protected void onPostExecute(JSONObject res) {
        Log.i(TAG, "response");
        try {
            if (callback != null) {
                if (res == null || isError) {
                    if (errorMessage == null) {
                        errorMessage = "Unable to connect to server. Please ensure WiFi or Mobile data is enabled.";
                        errorCode = "2";
                    }
                    callback.onFailure(errorMessage, errorCode);
                } else {

                    if (response.get("result") instanceof JSONArray) {
                        callback.onSuccess(response.getJSONArray("result"));
                    } else if (response instanceof JSONObject) {
                        callback.onSuccess(response.getJSONObject("result"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum HTTP_METHOD {
        GET,
        POST,
        FILE
    }
}