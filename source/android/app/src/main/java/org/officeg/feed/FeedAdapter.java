package org.officeg.feed;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.chat.DownloadFileTask;
import org.officeg.firebase.SharedPrefManager;
import org.officeg.mobileapp.CommentsAndLikesActivity;
import org.officeg.mobileapp.DetailedFeedActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class FeedAdapter extends BaseAdapter {

    private ArrayList<FeedItemModel> listvalues;
    private Context context;
    private UserModel user;
    private byte[] bytelistcomment;
    private byte[] byteblock;
    private ProfileModel profileModel = null;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean isLongClick = false;
    private String TAG = FeedAdapter.class.getSimpleName();

    public FeedAdapter(Context context, ArrayList<FeedItemModel> listvalues) {
        this.listvalues = listvalues;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public Object getItem(int position) {
        return listvalues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listvalues.hashCode();
    }

    public void add(FeedItemModel feedItemModel) {
        listvalues.add(0, feedItemModel);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Log.i("FamilyG", "creating postview: ");
        View vi = view;
        final MyViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

            vi = inflater.inflate(R.layout.feedlist, null);
            holder = new MyViewHolder();
            holder.displayname = vi.findViewById(R.id.name_id);
            holder.time = vi.findViewById(R.id.active_days);
            holder.profileimage = vi.findViewById(R.id.chat_bubble_imageview);
            holder.userstatus = vi.findViewById(R.id.webView1);
            holder.like = vi.findViewById(R.id.btn_like);
            holder.commentimage = vi.findViewById(R.id.commentimage);
            holder.contentView = vi.findViewById(R.id.viewgrouplinear);
            holder.comment = vi.findViewById(R.id.btn_comment);
            holder.addtofav = vi.findViewById(R.id.addtofav);
            holder.removefromfav = vi.findViewById(R.id.removefromfav);
            holder.block = vi.findViewById(R.id.block);
            holder.contentLinear = vi.findViewById(R.id.content_layout);
            holder.likeimagefill = vi.findViewById(R.id.likeimagefill);
            holder.likeimagenotfill = vi.findViewById(R.id.likeimagenotfill);
            holder.profileimage.setTag(position);
            vi.setTag(holder);

        } else {
            holder = (MyViewHolder) vi.getTag();
        }
        FeedItemModel feedmodel = listvalues.get(position);
        setFeedModel(feedmodel, holder, vi);
        return vi;
    }

    public void setFeedModel(final FeedItemModel itemModel, final MyViewHolder holder, View vi) {
        try {

            Log.i("FamilyG ", "login user fid " + UserModel.getInstance().getFid());
            Log.i("FamilyG ", "post user fid " + itemModel.getFid());
            if (itemModel.getFid().equals(UserModel.getInstance().getFid())) {

                holder.block.setVisibility(View.GONE);
                holder.addtofav.setVisibility(View.GONE);
                holder.removefromfav.setVisibility(View.GONE);

            } else {
               /* block.setVisibility(View.VISIBLE);
                addtofav.setVisibility(View.VISIBLE);
                removefromfav.setVisibility(View.GONE);*/
                holder.block.setVisibility(View.VISIBLE);
                Log.i("FamilyG ", "isfav " + itemModel.isfav);
                if (itemModel.isfirst) {
                    holder.addtofav.setVisibility(View.GONE);
                    holder.removefromfav.setVisibility(View.GONE);
                } else if (itemModel.isfav) {
                    holder.addtofav.setVisibility(View.GONE);
                    holder.removefromfav.setVisibility(View.VISIBLE);
                    Log.i("FamilyG ", "fav " + itemModel.isfav);
                } else {
                    holder.addtofav.setVisibility(View.VISIBLE);
                    holder.removefromfav.setVisibility(View.GONE);
                    Log.i("FamilyG ", "non fav " + itemModel.isfav);
                }
            }

            if (itemModel.getFid().equals("ffffff")) {
                holder.block.setVisibility(View.GONE);
                holder.addtofav.setVisibility(View.GONE);
                holder.removefromfav.setVisibility(View.GONE);
                holder.like.setVisibility(View.GONE);
                holder.comment.setVisibility(View.GONE);
                holder.commentimage.setVisibility(View.GONE);
                holder.displayname.setVisibility(View.GONE);
                holder.profileimage.setVisibility(View.GONE);
                holder.time.setVisibility(View.GONE);
            }
//            else {
//                if (itemModel.getFid().equals(UserModel.getInstance().getFid())) {
//
//                    holder.block.setVisibility(View.GONE);
//                    holder.addtofav.setVisibility(View.GONE);
//                    holder.removefromfav.setVisibility(View.GONE);
//
//                } else {
//               /* block.setVisibility(View.VISIBLE);
//                addtofav.setVisibility(View.VISIBLE);
//                removefromfav.setVisibility(View.GONE);*/
//                    holder.block.setVisibility(View.VISIBLE);
//                    Log.i("FamilyG ", "isfav " + itemModel.isfave);
//                    if (itemModel.isfave) {
//                        holder.addtofav.setVisibility(View.GONE);
//                        holder.removefromfav.setVisibility(View.VISIBLE);
//                        Log.i("FamilyG ", "fav " + itemModel.isfave);
//                    } else {
//                        holder.addtofav.setVisibility(View.VISIBLE);
//                        holder.removefromfav.setVisibility(View.GONE);
//                        Log.i("FamilyG ", "non fav " + itemModel.isfave);
//
//                    }
//                }
//                holder.like.setVisibility(View.VISIBLE);
//                holder.comment.setVisibility(View.VISIBLE);
//                holder.commentimage.setVisibility(View.VISIBLE);
//                holder.displayname.setVisibility(View.VISIBLE);
//                holder.profileimage.setVisibility(View.VISIBLE);
//                holder.time.setVisibility(View.VISIBLE);
//            }

//        this.itemModel = itemModel;
            //userstatus.loadData("<html><body style=text-align:justify;color:black>" + itemModel.getContent() + "</body></html>", "text/html", "utf-8");
            if (itemModel.getContent().equals("")) {
                holder.userstatus.setVisibility(View.GONE);
                holder.contentLinear.setVisibility(View.GONE);
                Log.i("emptycontent", "empty");

            } else {
                holder.contentLinear.setVisibility(View.VISIBLE);
                holder.userstatus.setVisibility(View.VISIBLE);
                holder.userstatus.setText(itemModel.getContent());
                Log.i("contenttt", itemModel.getContent());

            }

            holder.like.setText(String.valueOf(itemModel.getTotallikes()));
            holder.comment.setText(String.valueOf(itemModel.getTotalComments()));
            holder.displayname.setText(itemModel.getDisplayname());

            holder.time.setText(itemModel.getTime());
            holder.contentView.removeAllViews();
//            contentView1.removeAllViews();
            int sizeOfFeed = itemModel.getImagelist().size() + itemModel.getVideolist().size();
            int addedFiles = 0;
            for (int index = 0; index < itemModel.getImagelist().size(); index++) {
                if (++addedFiles == 3) {
                    break;
                }
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View imageHolder = inflater.inflate(R.layout.feed_image, null);

                try {
                    final String fileUrl = itemModel.getImagelist().get(index);
                    String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

                    File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);
                    ImageView imageView = imageHolder.findViewById(R.id.chat_bubble_imageview);

                    final File fileCheck = new File(receivedDirectory + File.separator + fileName);
                    if (!fileCheck.exists()) {
                        Log.d(TAG, "if " + fileCheck.getAbsolutePath());
                        DownloadFileTask downloadFileTask = new DownloadFileTask(context, imageHolder, imageView, false);
                        downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.image));
                    } else {
                        try {
                            Log.d(TAG, "else " + fileCheck.getAbsolutePath());
                            Uri uri = Utility.getUriFromFile(context, fileCheck);
                            Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, context);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            if (bitmap != null) {
                                Log.d(TAG, "else bitmap not null");
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                                imageView.setImageBitmap(bitmap);
                            } else {
                                int fileDownloadIntValue = 0;
                                String fileDownloadValue = SharedPrefManager.getInstance(context).getItem(fileName);
                                Log.d(TAG, "else else " + fileDownloadValue);
                                if (fileDownloadValue != null) {
                                    fileDownloadIntValue = Integer.parseInt(fileDownloadValue);
                                    fileDownloadIntValue++;
                                }
                                if (fileDownloadIntValue < 2) {
                                    Log.d(TAG, "else else downloading again");
                                    SharedPrefManager.getInstance(context).setItem(fileName, String.valueOf(fileDownloadIntValue));
                                    DownloadFileTask downloadFileTask = new DownloadFileTask(context, imageHolder, imageView, false);
                                    downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.image));
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "bitmap exception " + e.getMessage());
                            e.printStackTrace();
                        }
                    }

                    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                    int width = metrics.widthPixels, height;
                    if (sizeOfFeed > 1) {
                        width = width / 2;
                    }
                    height = width;

                    RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(width, height);
                    imageView.setLayoutParams(buttonLayoutParams);

                    if (addedFiles == 2 && sizeOfFeed > 2) {
                        imageView.setImageAlpha(100);

                        RelativeLayout feedRelativeLayout = new RelativeLayout(context);
                        RelativeLayout.LayoutParams linearParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        feedRelativeLayout.setLayoutParams(linearParams);
                        feedRelativeLayout.addView(imageHolder);
                        TextView countTextView = new TextView(context);

                        RelativeLayout.LayoutParams playLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        playLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                        countTextView.setLayoutParams(playLayoutParams);

                        countTextView.setTextColor(context.getResources().getColor(R.color.colorWhite));
                        int plusCount = sizeOfFeed - 1;
                        String extraText = "";
                        if (itemModel.getVideolist().size() == 0) {
                            plusCount = plusCount - 1;
                        }
                        extraText = "+" + plusCount;

                        countTextView.setText(extraText);
                        countTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 34);
                        feedRelativeLayout.addView(countTextView);

                        holder.contentView.addView(feedRelativeLayout);
                    } else {
                        holder.contentView.addView(imageHolder);
                    }
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Intent totalcommentintent = new Intent(context, DetailedFeedActivity.class);
                                totalcommentintent.putExtra("itemModel", itemModel);
                                totalcommentintent.putExtra("tab", 0);
                                context.startActivity(totalcommentintent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            for (int index = 0; index < itemModel.getVideolist().size(); index++) {
                if (++addedFiles == 3) {
                    break;
                }
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View videoView = inflater.inflate(R.layout.feed_video, null);

                try {
                    final String fileUrl = itemModel.getVideolist().get(index);
                    String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

                    File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, context);
                    ImageView imageView = videoView.findViewById(R.id.chat_bubble_imageview);

                    final File fileCheck = new File(receivedDirectory + File.separator + fileName);
                    if (!fileCheck.exists()) {
                        Log.d(TAG, "if " + fileCheck.getAbsolutePath());
                        DownloadFileTask downloadFileTask = new DownloadFileTask(context, videoView, imageView, false);
                        downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.video));
                    } else {
                        Log.d(TAG, "else " + fileCheck.getAbsolutePath());
                        Uri uri = Utility.getUriFromFile(context, fileCheck);
                        File afterRename = new File(receivedDirectory, fileName);
                        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                                MediaStore.Images.Thumbnails.MINI_KIND);
                        if (bitmap == null) {
                            Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                        }
                        imageView.setImageBitmap(bitmap);
                    }

                    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                    int width = metrics.widthPixels, height;
                    if (sizeOfFeed > 1) {
                        width = width / 2;
                    }
                    height = width;

                    RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(width, height);
                    imageView.setLayoutParams(buttonLayoutParams);

                    if (addedFiles == 2 && sizeOfFeed > 2) {
                        imageView.setImageAlpha(100);

                        RelativeLayout feedRelativeLayout = new RelativeLayout(context);
                        RelativeLayout.LayoutParams linearParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        feedRelativeLayout.setLayoutParams(linearParams);
                        feedRelativeLayout.addView(videoView);
                        TextView countTextView = new TextView(context);

                        RelativeLayout.LayoutParams playLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        playLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                        countTextView.setLayoutParams(playLayoutParams);

                        countTextView.setTextColor(context.getResources().getColor(R.color.colorWhite));
                        int plusCount = sizeOfFeed - 1;
                        String extraText = "";
                        if (itemModel.getImagelist().size() == 0) {
                            plusCount = plusCount - 1;
                        }
                        extraText = "+" + plusCount;
                        countTextView.setText(extraText);
                        countTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 34);
                        feedRelativeLayout.addView(countTextView);

                        holder.contentView.addView(feedRelativeLayout);
                    } else {
                        holder.contentView.addView(videoView);
                    }

                    videoView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                Intent totalcommentintent = new Intent(context, DetailedFeedActivity.class);
                                totalcommentintent.putExtra("itemModel", itemModel);
                                totalcommentintent.putExtra("tab", 1);
                                context.startActivity(totalcommentintent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (itemModel.getProfileimage() != null && !itemModel.getProfileimage().trim().isEmpty()) {
//            ImageLoader imageLoader = new ImageLoader(context);
//            imageLoader.DisplayImage(itemModel.getProfileimage(), holder.profileimage);

                final String fileUrl = itemModel.getProfileimage();
                String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

                File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);
                final File fileCheck = new File(receivedDirectoryByType + File.separator + fileName);
                if (!fileCheck.exists()) {
                    Log.d(TAG, "if " + fileCheck.getAbsolutePath());
                    DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, holder.profileimage, false);
                    downloadFileTask.execute(fileUrl, fileName, context.getResources().getString(R.string.image));
                } else {
                    Log.d(TAG, "else " + fileCheck.getAbsolutePath());
                    Uri uri = Utility.getUriFromFile(context, fileCheck);
                    Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, context);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (bitmap != null) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                        holder.profileimage.setImageBitmap(bitmap);
                    }
                }
            } else {
                holder.profileimage.setImageResource(R.drawable.default_profile_image);
            }

            holder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utility.logFA(mFirebaseAnalytics, "feed screen", "totalcomments", "button");
                    Intent totalcommentintent = new Intent(context, CommentsAndLikesActivity.class);
                    totalcommentintent.putExtra("feedItemModel", itemModel);
                    totalcommentintent.putExtra("tab", 1);
                    context.startActivity(totalcommentintent);
                }
            });

            holder.commentimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utility.logFA(mFirebaseAnalytics, "feed screen", "totalcomments", "button");
                    Intent totalcommentintent = new Intent(context, CommentsAndLikesActivity.class);
                    totalcommentintent.putExtra("feedItemModel", itemModel);
                    totalcommentintent.putExtra("tab", 1);
                    context.startActivity(totalcommentintent);
                }
            });

            holder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemModel.getLikeList().size() != 0) {
                        Utility.logFA(mFirebaseAnalytics, "feed screen", "totallikes", "button");
                        Intent totalcommentintent = new Intent(context, CommentsAndLikesActivity.class);
                        totalcommentintent.putExtra("feedItemModel", itemModel);
                        totalcommentintent.putExtra("tab", 0);
                        context.startActivity(totalcommentintent);
                    } else {
                        Toast.makeText(context, "Oops..\nThere's no likes, yet.", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.logFA(mFirebaseAnalytics, "feed screen", "block", "button");

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.block_alert);
                    TextView blockmessage = dialog.findViewById(R.id.blocktext);
                    Button dialogBlockButtonyes = dialog.findViewById(R.id.dialogButtonYes);
                    Button dialogBlockButtonno = dialog.findViewById(R.id.dialogButtonNo);

                    dialogBlockButtonyes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject object = new JSONObject();
                            try {
                                object.put("secret_key", UserModel.getInstance().secret_key);
                                object.put("fid", itemModel.getFid());
                                Log.i("jobjblock", String.valueOf(object));
                                byteblock = object.toString().getBytes(Charset.forName("UTF-8"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            APIManager api = new APIManager("relationship/blockuser", byteblock, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject result) {


                                }

                                @Override
                                public void onSuccess(JSONArray object) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();

                                }
                            }, APIManager.HTTP_METHOD.POST);

                            api.execute();
                            dialog.dismiss();

                        }
                    });

                    dialogBlockButtonno.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                }
            });
            user = UserModel.getInstance();

            holder.addtofav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder favourite_confirmationConfirmationDialog = new AlertDialog.Builder(context);
                    favourite_confirmationConfirmationDialog.setTitle(context.getString(R.string.favourite_confirmation));
                    favourite_confirmationConfirmationDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.dismiss();

                            Utility.logFA(mFirebaseAnalytics, "feed screen", "addtofav", "button");
                            ProfileModel profileModel = new ProfileModel();
                            profileModel.secret_key = user.getSecret_key();
                            profileModel.fid = itemModel.getFid();
                            profileModel.relationpath = "";
                            Log.i("addtofav", profileModel.secret_key + " " + profileModel.fid);
                            APIManager addfavouriteapi = new APIManager("relationship/addFavourite", profileModel, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject object) {
                                    Toast.makeText(context, context.getString(R.string.request_has_been_sent_favourite), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onSuccess(JSONArray array) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();

                                }
                            });
                            addfavouriteapi.execute();

                        }
                    });

                    favourite_confirmationConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.cancel();
                        }
                    });
                    favourite_confirmationConfirmationDialog.show();

                }
            });

            holder.removefromfav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder favourite_confirmationConfirmationDialog = new AlertDialog.Builder(context);
                    favourite_confirmationConfirmationDialog.setTitle(context.getString(R.string.favourite_remove_confirmation));
                    favourite_confirmationConfirmationDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.dismiss();

                            Utility.logFA(mFirebaseAnalytics, "feed screen", "removefromfav", "button");
                            profileModel = new ProfileModel();
                            profileModel.secret_key = user.getSecret_key();
                            profileModel.fid = itemModel.getFid();
                            Log.i("addtofav", profileModel.secret_key + " " + profileModel.fid);
                            APIManager removefavouriteapi = new APIManager("relationship/removeFavourite", profileModel, new APIListener() {

                                @Override
                                public void onSuccess(JSONObject object) {
                                    itemModel.isfav = !itemModel.isfav;
                                    holder.addtofav.setVisibility(View.VISIBLE);
                                    holder.removefromfav.setVisibility(View.GONE);
                                    Toast.makeText(context, context.getString(R.string.removed_from_favourite), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onSuccess(JSONArray array) {

                                }

                                @Override
                                public void onFailure(String errorMessage, String errorCode) {
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();

                                }
                            });
                            removefavouriteapi.execute();
                        }
                    });

                    favourite_confirmationConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.cancel();
                        }
                    });
                    favourite_confirmationConfirmationDialog.show();

                }
            });
            setLikeImageBasedOnStatus(holder, itemModel);
            holder.userstatus.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {    //Long press to share text
                    isLongClick = true;

                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.share_alert);
                    final TextView share_text = dialog.findViewById(R.id.share);
                    share_text.setText("Share");
                    share_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            String contentType = "text";
                            sharePostContent(contentType, null, itemModel);
                            Log.i("Clicked", itemModel.getContent());

                        }
                    });

                    dialog.show();
                    return true;
                }

            });

            holder.userstatus.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP && isLongClick) {
                        isLongClick = false;
                        return true;
                    }
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        isLongClick = false;
                    }
                    return v.onTouchEvent(event);
                }
            });

            holder.likeimagenotfill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.likeimagefill.setVisibility(View.VISIBLE);
                    holder.likeimagenotfill.setVisibility(View.GONE);
                    likeApi(true, holder, itemModel);
                }
            });

            holder.likeimagefill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.likeimagenotfill.setVisibility(View.VISIBLE);
                    holder.likeimagefill.setVisibility(View.GONE);
                    likeApi(false, holder, itemModel);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    private void likeApi(final boolean likeStatus, final MyViewHolder holder, final FeedItemModel itemModel) {
        invertLikeStatus(holder, itemModel);
        Utility.logFA(mFirebaseAnalytics, "feed screen", "like", "button");
        JSONObject object = new JSONObject();
        try {
            object.put("secret_key", UserModel.getInstance().secret_key);
            object.put("postid", itemModel.getPostid());

            byte[] bytelistlike = object.toString().getBytes(Charset.forName("UTF-8"));
            Log.i("byte", object.toString());
            String apiLike = likeStatus ? "addlikes" : "removelikes";
            APIManager api = new APIManager(apiLike, bytelistlike, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    long time = System.currentTimeMillis();
                    if (likeStatus) {
                        itemModel.addLike(UserModel.getInstance().displayname, time, UserModel.getInstance().fid, UserModel.getInstance().profileimageurl);
                    } else {
                        itemModel.removeLike(UserModel.getInstance().fid);
                    }
                }

                @Override
                public void onSuccess(JSONArray object) {
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                    invertLikeStatus(holder, itemModel);
                    setLikeImageBasedOnStatus(holder, itemModel);
                }
            }, APIManager.HTTP_METHOD.POST);
            api.execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setLikeImageBasedOnStatus(MyViewHolder holder, FeedItemModel itemModel) {
        if (itemModel.isLikedByMe) {
            holder.likeimagenotfill.setVisibility(View.GONE);
            holder.likeimagefill.setVisibility(View.VISIBLE);
        } else {
            holder.likeimagefill.setVisibility(View.GONE);
            holder.likeimagenotfill.setVisibility(View.VISIBLE);
        }
    }

    private void invertLikeStatus(MyViewHolder holder, FeedItemModel itemModel) {
        itemModel.isLikedByMe = !itemModel.isLikedByMe;
        if (itemModel.isLikedByMe) {
            holder.like.setText(String.valueOf(itemModel.incrementLikeCount()));
        } else {
            holder.like.setText(String.valueOf(itemModel.decrementLikeCount()));
        }
    }

    private void sharePostContent(String contentType, String fileUrl, FeedItemModel itemModel) {     //share image and video
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);


        if (contentType.equalsIgnoreCase("text")) {
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, itemModel.getContent());
        } else if (contentType.equalsIgnoreCase("image")) {
            File receivedDirectory = null;
            //fileUrl = chatMessageModel.getImage();
            sendIntent.setType("image/*");
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
            receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);
            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            File tempShareFile = Utility.createTempShareFile(fileCheck, receivedDirectory, contentType, context);
            sendIntent.putExtra(Intent.EXTRA_STREAM, Utility.getUriFromFile(context, tempShareFile));
        } else if (contentType.equalsIgnoreCase("video")) {
            File moviesDirectory = null;

            File receivedDirectory = null;
            //fileUrl = chatMessageModel.getImage();
            sendIntent.setType("video/*");
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            if (android.os.Environment.getExternalStorageState().equals(
                    android.os.Environment.MEDIA_MOUNTED))
                moviesDirectory = new File(
                        android.os.Environment.getExternalStorageDirectory(),
                        "FamilyG/TempVideos");

            else
                moviesDirectory = context.getCacheDir();
            if (!moviesDirectory.exists())
                moviesDirectory.mkdirs();

            File fileCheck = new File(moviesDirectory
                    + File.separator + "Received" + File.separator + fileName);
            File tempShareFile = Utility.createTempShareFile(fileCheck, moviesDirectory, contentType, context);
            sendIntent.putExtra(Intent.EXTRA_STREAM, Utility.getUriFromFileForShareVideo(context, tempShareFile));

        }
        context.startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    private class MyViewHolder {
        TextView displayname, time;
        CircleImageView profileimage;
        TextView userstatus;
        TextView like, comment;
        ImageButton addtofav, block, removefromfav, commentimage, likeimagefill, likeimagenotfill;
        LinearLayout contentView;
        EditText commentedit;
        LinearLayout contentLinear;
    }
}