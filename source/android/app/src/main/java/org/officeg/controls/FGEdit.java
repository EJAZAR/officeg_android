package org.officeg.controls;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;

/**
 * Created by Guru on 8/21/2017.
 */

public class FGEdit extends android.support.v7.widget.AppCompatEditText {

    public interface FGEditListener {
        void onPaste();
    }

    ArrayList<FGEditListener> listeners;

    public FGEdit(Context context)
    {
        super(context);
        listeners = new ArrayList<>();
    }

    public FGEdit(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        listeners = new ArrayList<>();
    }

    public FGEdit(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        listeners = new ArrayList<>();
    }

    public void addListener(FGEditListener listener) {
        try {
            listeners.add(listener);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        boolean consumed = super.onTextContextMenuItem(id);
        switch (id){
            case android.R.id.cut:
                onTextCut();
                break;
            case android.R.id.paste:
                onTextPaste();
                break;
            case android.R.id.copy:
                onTextCopy();
        }
        return consumed;
    }

    public void onTextCut(){
    }

    public void onTextCopy(){
    }

    /**
     * adding listener for Paste for example
     */
    public void onTextPaste(){
        for (FGEditListener listener : listeners) {
            listener.onPaste();
        }
    }
}
