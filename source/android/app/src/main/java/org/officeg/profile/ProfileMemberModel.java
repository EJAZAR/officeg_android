package org.officeg.profile;

import android.util.Log;

import org.officeg.model.ProfileModel;
import org.officeg.model.RelationshipModel;
import org.officeg.model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProfileMemberModel {

    public Map<String, ProfileModel> members = new HashMap<String, ProfileModel>();
    public Map<String, ArrayList<RelationshipModel>> memberMapping = new HashMap<String, ArrayList<RelationshipModel>>();
    private static final String TAG = ProfileMemberModel.class.getSimpleName();

    public void AddMember(String parentfid, ProfileModel profileModel) {
        // When you are adding member for logged in user then use parentfid as logged in user fid
        // When you are adding member for newly created member say grandfather to father then we need use father fid. Since first the fatherfid will not be created until we send the data to server,
        // so you create unique fid with word "temp" infront of it. For example "temp" + uniquenumber", when sending this class to server server check whether fid
//        starts with "temp" if so then it will create new otherwise it will update

        // Check whether profileModel.fid is empty, if empty then create a temporary fid as like explained above
        if (profileModel.fid == null || profileModel.fid.equals(""))
            profileModel.fid = "temp" + UUID.randomUUID().toString();
        Log.i("FamilyG", profileModel.fid);
        RelationshipType relationshipType = RelationshipType.valueOf(profileModel.relationship);
        Log.i("FamilyG", relationshipType.name());
        members.put(profileModel.fid, profileModel);
        ArrayList<RelationshipModel> list = memberMapping.get(parentfid);
        if (list == null) {
            list = new ArrayList<RelationshipModel>();
            memberMapping.put(parentfid, list);
        }
        // Check if fid is already part of the list and then only add, otherwise there will be duplicate
        list.add(new RelationshipModel(profileModel.fid, relationshipType));
        Log.i("profilemember fid: ", profileModel.fid);
    }

    public void RemoveMember(String removeFid) {
        try {
            if (memberMapping.containsKey(removeFid)) {
                memberMapping.remove(removeFid);
            }
            ArrayList<RelationshipModel> relationshipModels = memberMapping.get(UserModel.getInstance().fid);
            for (RelationshipModel relationshipModel : relationshipModels) {
                if (relationshipModel.getFid().equalsIgnoreCase(removeFid)) {
                    relationshipModels.remove(relationshipModel);
                    break;
                }
            }
            memberMapping.put(UserModel.getInstance().fid, relationshipModels);
            Log.d(TAG, removeFid);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void clear() {
        members.clear();
        memberMapping.clear();
    }

    public void UpdateMember(String parentfid, ProfileModel profileModel) {
        // it will be same as AddMember method only thing is find the member is already exists in "members " list if so then update the object rather adding new
    }

    public ArrayList<ProfileModel> getProfilesByRelationship(String fid, RelationshipType relationshipType) {
        ArrayList<ProfileModel> memberList = new ArrayList<ProfileModel>();

        //Log.i("FamilyG","Retriving profiles for Relationship type :"+relationshipType.name());
        ArrayList<RelationshipModel> relationList = memberMapping.get(fid);

        if (relationList != null) {
            for (int index = 0; index < relationList.size(); index++) {
                RelationshipModel relationshipModel = relationList.get(index);
                //Log.i("FamilyG","Relationship type :"+relationshipModel.getRelationshipType().name());
                if (relationshipModel.getRelationshipType() == relationshipType)
                    memberList.add(members.get(relationshipModel.getFid()));
            }
        }
        return memberList;
    }
}
