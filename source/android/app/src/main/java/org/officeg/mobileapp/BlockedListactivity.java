package org.officeg.mobileapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.model.UserModel;
import org.officeg.settings.BlockedListAdapter;
import org.officeg.settings.BlockedListModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

public class BlockedListactivity extends AppCompatActivity {
    ListView blockedList;
    UserModel user = null;
    ArrayList<BlockedListModel> blockedlistvalues = new ArrayList<>();
    BlockedListAdapter blockedListadapter;
    byte[] blockListbyte;
    private FirebaseAnalytics mFirebaseAnalytics;
    LinearLayout emptyBlockList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_listactivity);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_blockedlist);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        blockedList = findViewById(R.id.blockedlist);
        getBlockedList();
    }

    private void getBlockedList() {

        user = UserModel.getInstance();
        JSONObject object = new JSONObject();
        try {
            object.put("secret_key", UserModel.getInstance().secret_key);
            Log.i("jobjblock", String.valueOf(object));
            blockListbyte = object.toString().getBytes(Charset.forName("UTF-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIManager api = new APIManager("relationship/getblockedusers", blockListbyte, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.i("check", String.valueOf(array));
                try {
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject postobject = array.getJSONObject(index);
                        BlockedListModel blockitemsmodel = new BlockedListModel(postobject);
                        blockedlistvalues.add(blockitemsmodel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (blockedlistvalues != null && !blockedlistvalues.isEmpty()) {
                    blockedListadapter = new BlockedListAdapter(getWindow().getContext(), blockedlistvalues);
                    blockedList.setAdapter(blockedListadapter);
                    ((BaseAdapter) blockedList.getAdapter()).notifyDataSetChanged();
                } else {
                    emptyBlockList = findViewById(R.id.empty_blocklist);
                    emptyBlockList.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

            }
        }, APIManager.HTTP_METHOD.POST);

        api.execute();

    }
}
