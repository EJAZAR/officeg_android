package org.officeg.feed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;

public class CommentsFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_tab, container, false);
        final View currentView = inflater.inflate(R.layout.activity_comments_total, container, false);

        Bundle extras = this.getArguments();

        final FeedItemModel feedItemModel = (FeedItemModel) extras.get("feedItemModel");
        List<CommentModel> comments = feedItemModel.getCommentList();
        final CommentList adapter = new CommentList(getContext(), comments);
        if (comments.size() > 0) {
            Collections.sort(comments);
//            Collections.reverse(comments);
            ListView commentlistView = (ListView) currentView.findViewById(R.id.commentlist);
            commentlistView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            TextView noCommentsTV = (TextView) currentView.findViewById(R.id.empty_notification);
            noCommentsTV.setVisibility(View.VISIBLE);
        }
        final LinearLayout comment_edittext = (LinearLayout) currentView.findViewById(R.id.comment_edittext);
        comment_edittext.setVisibility(View.VISIBLE);
        final EditText commentedit = (EditText) currentView.findViewById(R.id.chatactivityeditText);
        ImageView sendButton = (ImageView) currentView.findViewById(R.id.chatsendmessage);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contentChat = commentedit.getText().toString().trim();
                if (contentChat.equals("")) {
                    Toast.makeText(getContext(), "Kindly enter comment to send", Toast.LENGTH_SHORT).show();
                } else {
                    NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getContext());
                    if (!NetworkConnectionreceiver.isConnection) {
                        Toast.makeText(getContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    byte[] bytelistcomment = new byte[0];
                    JSONObject object = new JSONObject();
                    try {
                        object.put("secret_key", UserModel.getInstance().secret_key);
                        object.put("postid", feedItemModel.getPostid());
                        object.put("content", commentedit.getText().toString());
                        object.put("imageurls", new JSONArray());
                        object.put("videourls", new JSONArray());

                        Log.i("jobjcomment", String.valueOf(object));
                        bytelistcomment = object.toString().getBytes(Charset.forName("UTF-8"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // dialog.dismiss();
                    APIManager api = new APIManager("addcomments", bytelistcomment, new APIListener() {

                        @Override
                        public void onSuccess(JSONObject result) {
                            long time = System.currentTimeMillis();
                            Log.i("longtime", String.valueOf(time));
                            JSONArray jsonImagesArray = new JSONArray();
                            JSONArray jsonVideosArray = new JSONArray();

                            JSONObject commentObject = new JSONObject();
                            try {
                                commentObject.put("content", commentedit.getText().toString());
                                commentObject.put("imageurls", jsonImagesArray);
                                commentObject.put("videourls", jsonVideosArray);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            CommentModel commentModel = new CommentModel(UserModel.getInstance().displayname, time, commentObject.toString(), UserModel.getInstance().profileimageurl);
                            feedItemModel.addComment(commentModel);
                            if (feedItemModel.commentList.size() == 1) {
                                TextView noCommentsTV = (TextView) currentView.findViewById(R.id.empty_notification);
                                noCommentsTV.setVisibility(View.GONE);

                                ListView commentlistView = (ListView) currentView.findViewById(R.id.commentlist);
                                commentlistView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } else {
                                adapter.notifyDataSetChanged();
                            }

                            commentedit.setText("");
                        }

                        @Override
                        public void onSuccess(JSONArray object) {
                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();

                        }
                    }, APIManager.HTTP_METHOD.POST);
                    api.execute();
                }

            }
        });

        return currentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
