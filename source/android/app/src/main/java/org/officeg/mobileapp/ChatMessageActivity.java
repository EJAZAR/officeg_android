package org.officeg.mobileapp;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.chat.ChatMessageAdapter;
import org.officeg.chat.group.CreateGroupAdapter;
import org.officeg.chat.group.ForwardActivity;
import org.officeg.chat.group.ViewGroupMembersActivity;
import org.officeg.database.DataManager;
import org.officeg.firebase.SharedPrefManager;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.RECORD_AUDIO;

public class ChatMessageActivity extends AppCompatActivity {
    private final int UPLOAD_FILE_REQUEST = 1881;
    private final int CAPTURE_IMAGE_REQUEST = 1882;
    private final int RECORD_VIDEO_REQUEST = 1883;
    private final int REQUEST_RECORD_AUDIO = 1885;
    public MediaPlayer mediaPlayer = new MediaPlayer();
    FirebaseAnalytics mFirebaseAnalytics;
    GroupModel groupModel;
    ProfileModel profileModel;
    Uri imageUri;
    boolean isNotificationFrom = false;
    TextView marque;
    private ChatMessageAdapter chatMessageAdapter;
    private String typeChat, type;
    private String fidOrGid;
    private Integer unreadCount;
    private ListView listview_chatmessage;
    private String TAG = ChatMessageActivity.class.getSimpleName();
    private static final int RA_REQUEST_CODE = 100;
    private boolean isActivityActive = true;
    BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle extras = intent.getBundleExtra("chatNotification");
                String msgstr = extras.getString("msg");
                final Long dateLongval = extras.getLong("datetime");
                //Reminder
                if (extras.getString("type").equals("4")) {
                    JSONObject chatContent = new JSONObject(msgstr);
                    DataManager dataManager = DataManager.getInstance(context);
                    ChatMessageModel chatMessageModel = dataManager.getMessage(extras.getString("reminder_mid"));
                    chatContent = new JSONObject(chatMessageModel.getTotalContent());
                    String reminding_fid = extras.getString("reminding_fid");

                    if (profileModel != null && (profileModel.getFid().equalsIgnoreCase(chatContent.getString("fid")) || UserModel.getInstance().fid.equalsIgnoreCase(chatContent.getString("fid"))) && !chatContent.has("gid")) {
                        postChatContent(chatContent, dateLongval, "0", true, extras, reminding_fid);
                    } else if (groupModel != null && groupModel.getGid().equalsIgnoreCase(chatContent.getString("gid"))) {
                        postChatContent(chatContent, dateLongval, "1", true, extras, reminding_fid);
                    }
                } else if (extras.getString("type").equals("5")) {
                    String ackmode = extras.getString("ackmode");
                    String message_id = extras.getString("message_id");
                    if (typeChat.equals("0")) {
                        if (ackmode.equalsIgnoreCase("sent")) {
                            for (int k = 0; k < chatMessageAdapter.chatMessageModels.size(); k++) {
                                ChatMessageModel iterationChatMessageModel = chatMessageAdapter.chatMessageModels.get(k);
                                if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(message_id)) {
                                    if (iterationChatMessageModel.getAcknowledgementStatus() != null && iterationChatMessageModel.getAcknowledgementStatus() < 1) {
                                        Log.d(TAG, "emergency = " + message_id);
                                        iterationChatMessageModel.setAcknowledgementStatus(1);
                                    }
                                    Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                                    break;
                                }
                            }
                        } else if (ackmode.equalsIgnoreCase("delivered")) {
                            for (int k = 0; k < chatMessageAdapter.chatMessageModels.size(); k++) {
                                ChatMessageModel iterationChatMessageModel = chatMessageAdapter.chatMessageModels.get(k);
                                if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(message_id)) {
                                    if (iterationChatMessageModel.getAcknowledgementStatus() != null && iterationChatMessageModel.getAcknowledgementStatus() < 2)
                                        iterationChatMessageModel.setAcknowledgementStatus(2);
                                    Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                                    break;
                                }
                            }
                        } else if (ackmode.equalsIgnoreCase("failed")) {
                            for (int k = 0; k < chatMessageAdapter.chatMessageModels.size(); k++) {
                                ChatMessageModel iterationChatMessageModel = chatMessageAdapter.chatMessageModels.get(k);
                                if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(message_id)) {
                                    if (iterationChatMessageModel.getAcknowledgementStatus() != null && iterationChatMessageModel.getAcknowledgementStatus() == 0)
                                        iterationChatMessageModel.setAcknowledgementStatus(4);
                                    Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                                    break;
                                }
                            }
                        } else {
                            for (int k = 0; k < chatMessageAdapter.chatMessageModels.size(); k++) {
                                ChatMessageModel iterationChatMessageModel = chatMessageAdapter.chatMessageModels.get(k);
                                if (iterationChatMessageModel.getAcknowledgementStatus() != null && !iterationChatMessageModel.isreminder() && iterationChatMessageModel.getAcknowledgementStatus() > 0 && iterationChatMessageModel.getAcknowledgementStatus() < 3)
                                    iterationChatMessageModel.setAcknowledgementStatus(3);
                            }
                            Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                        }
                    } else {
                        if (ackmode.equalsIgnoreCase("sent")) {
                            for (int k = 0; k < chatMessageAdapter.chatMessageModels.size(); k++) {
                                ChatMessageModel iterationChatMessageModel = chatMessageAdapter.chatMessageModels.get(k);
                                if (iterationChatMessageModel.getMessageId() != null && iterationChatMessageModel.getMessageId().equals(message_id)) {
                                    if (iterationChatMessageModel.getAcknowledgementStatus() != null && iterationChatMessageModel.getAcknowledgementStatus() < 1) {
                                        Log.d(TAG, "emergency = " + message_id);
                                        iterationChatMessageModel.setAcknowledgementStatus(1);
                                    }
                                    Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    JSONObject chatContent = new JSONObject(msgstr);
                    if (isActivityActive) {
                        clearBadgeCount();
                    }
                    if (profileModel != null && profileModel.getFid().equalsIgnoreCase(chatContent.getString("fid")) && !chatContent.has("gid")) {
                        postChatContent(chatContent, dateLongval, "0", false, extras, null);
                    } else if (groupModel != null && groupModel.getGid().equalsIgnoreCase(chatContent.getString("gid"))) {
                        postChatContent(chatContent, dateLongval, "1", false, extras, null);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "BroadcastReceiver = " + e.getMessage());
            }
        }
    };
    private ImageView chatsendmessageButton;
    private ImageView recordAudioButton;
    private MediaRecorder mRecorder;
    private String mAudioFileName = null;
    private boolean isRecordAudioButtonPressed = false;
    public static ChatMessageActivity chatMessageActivity;
    private ChatMessageModel selectedChatMessageModel;
    private boolean isUploadInProgress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);
        chatMessageActivity = this;

        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                Log.d(TAG, extras.containsKey("typeChat") + " and " + extras.containsKey("fidOrGid"));

                if (extras.containsKey("isNotificationClicked") && extras.getBoolean("isNotificationClicked")) {
                    UserModel user = UserModel.getInstance();
                    user.load(getApplicationContext());
                }
                if (!extras.containsKey("fidOrGid") || !extras.containsKey("typeChat")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.unableopenmessage), Toast.LENGTH_SHORT).show();
                    Intent chatPage = new Intent(getApplicationContext(), BottomBarActivity.class);
                    chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    chatPage.putExtra("screen", "openchatpage");
                    startActivity(chatPage);
                } else {
                    fidOrGid = extras.getString("fidOrGid");
                    typeChat = extras.getString("typeChat");
                    type = extras.getString("type");
                    unreadCount = extras.getInt("unreadcount");

                    Log.d(TAG, extras.getString("typeChat") + " and " + extras.getString("fidOrGid"));

                    if (typeChat.equalsIgnoreCase("0")) {
                        if (extras.containsKey("profileModel")) {
                            Log.d(TAG, "type0 if");
                            String profileModelString = extras.getString("profileModel");
                            profileModel = new ProfileModel();
                            profileModel = profileModel.toObject(profileModelString);
                            chatView(typeChat, fidOrGid);
                        } else {
                            Log.d(TAG, "type0 else");
                            if (Utility.profileModelMap.containsKey(fidOrGid)) {
                                profileModel = Utility.profileModelMap.get(fidOrGid);
                                chatView(typeChat, fidOrGid);
                            } else {
                                getProfileFromFid(typeChat, fidOrGid);
                            }
                        }
                    } else if (typeChat.equalsIgnoreCase("1")) {
                        if (extras.containsKey("groupModel")) {
                            Log.d(TAG, "type1 if");
                            String groupModelString = extras.getString("groupModel");
                            groupModel = new GroupModel();
                            groupModel = groupModel.toObject(groupModelString);
                            chatView(typeChat, fidOrGid);
                        } else {
                            Log.d(TAG, "type1 else");
                            if (Utility.groupModelMap.containsKey(fidOrGid)) {
                                groupModel = Utility.groupModelMap.get(fidOrGid);
                                afterGettingGroups(typeChat, fidOrGid);
                            } else {
                                getGroupFromGid(typeChat, fidOrGid);
                            }
                        }
                    }
                    clearBadgeCount();
                }
            } else {
                Log.d(TAG, "else");
                Toast.makeText(getApplicationContext(), "Redirecting to home page", Toast.LENGTH_LONG).show();
                Intent i = new Intent(ChatMessageActivity.this, BottomBarActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .registerReceiver(mMessageReceiver, new IntentFilter("CHATMESSAGE_RECEIVED"));
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } catch (Exception e) {
            Log.e(TAG, "onCreateView = " + e.getMessage());
        }
    }

    private void clearBadgeCount() {
        try {
            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            dataManager.deleteTable("unreadnotification", "fidOrGid", fidOrGid);
            JSONObject object = new JSONObject();
            object.put("secret_key", UserModel.getInstance().secret_key);
            object.put("fidOrGid", fidOrGid);
            object.put("typeChat", typeChat);
            byte[] clearBadgeCountJson = object.toString().getBytes(Charset.forName("UTF-8"));

            APIManager api = new APIManager("chat/clearBadgeCount", clearBadgeCountJson, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "clearBadgeCount success");
                }

                @Override
                public void onSuccess(JSONArray object) {
                    Log.e(TAG, "clearBadgeCount error");
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, "clearBadgeCount error");
                }
            }, APIManager.HTTP_METHOD.POST);

            api.execute();

        } catch (Exception e) {
            Log.e(TAG, "clearBadgeCount = " + e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (typeChat.equalsIgnoreCase("0")) {
            getMenuInflater().inflate(R.menu.chat_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            Utility.logFA(mFirebaseAnalytics, "chat screen", "block", "menubutton");
            final Dialog dialog = new Dialog(this.getWindow().getContext());
            dialog.setContentView(R.layout.block_alert);
            dialog.setTitle(getString(R.string.block));
            Button dialogBlockButtonyes = dialog.findViewById(R.id.dialogButtonYes);
            Button dialogBlockButtonno = dialog.findViewById(R.id.dialogButtonNo);

            dialogBlockButtonyes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("secret_key", UserModel.getInstance().secret_key);
                        object.put("fid", profileModel.getFid());
                        byte[] byteblock = object.toString().getBytes(Charset.forName("UTF-8"));

                        APIManager api = new APIManager("relationship/blockuser", byteblock, new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {
                                Toast.makeText(getApplicationContext(), "User Blocked", Toast.LENGTH_SHORT).show();
                                Intent chatPage = new Intent(getApplicationContext(), BottomBarActivity.class);
                                chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                chatPage.putExtra("screen", "openchatpage");
                                startActivity(chatPage);
                                dialog.dismiss();
                            }

                            @Override
                            public void onSuccess(JSONArray object) {
                                Toast.makeText(getApplicationContext(), "User Blocked", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                Toast.makeText(getApplicationContext(), "Failed to block user", Toast.LENGTH_SHORT).show();
                            }
                        }, APIManager.HTTP_METHOD.POST);

                        api.execute();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            dialogBlockButtonno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getGroupFromGid(final String typeChat, final String fidOrGid) {
        isNotificationFrom = true;
        UserModel userModel = UserModel.getInstance();
        userModel.load(getApplicationContext());
        Log.d(TAG, " userModel.toJSON() = " + userModel.toJSON());
        if (Utility.groupModelMap.containsKey(fidOrGid)) {
            groupModel = Utility.groupModelMap.get(fidOrGid);
            Log.d(TAG, "Utility.groupModelMap contains " + fidOrGid);
            afterGettingGroups(typeChat, fidOrGid);
        } else {
            byte[] userModelBA = userModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager createGroupApi = new APIManager("chat/groups", userModelBA, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "inside onsuccess" + result.toString());
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "Utility.groupModelMap reload");
                    try {
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject singleGroup = array.getJSONObject(k);
                            if (singleGroup.getString("gid").equalsIgnoreCase(fidOrGid)) {
                                groupModel = new GroupModel();
                                groupModel = groupModel.toObject(singleGroup.toString());
                                afterGettingGroups(typeChat, fidOrGid);
                                Utility.groupModelMap.put(groupModel.getGid(), groupModel);
                                break;
                            }
                        }
                        DataManager dataManager = DataManager.getInstance(getApplicationContext());
                        dataManager.insertChatFavouritesAndGroups(null, Utility.groupModelMap);

                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.d(TAG, errorMessage);
                }

            }, APIManager.HTTP_METHOD.POST);
            createGroupApi.execute();
        }
    }

    private void afterGettingGroups(final String typeChat, final String fidOrGid) {
        try {
            if (!Utility.profileModelMap.isEmpty()) {
                Log.d(TAG, "Utility.profileModelMap exist");

                chatView(typeChat, fidOrGid);
            } else {
                APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", UserModel.getInstance(), new APIListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        Log.d(TAG, "inside onsuccess");
                    }

                    @Override
                    public void onSuccess(JSONArray array) {
                        Log.d(TAG, "Utility.profileModelMap reload");
                        try {
                            for (int k = 0; k < array.length(); k++) {
                                JSONObject object = array.getJSONObject(k);
                                Log.d("resultString", object.getString("fid"));
                                ProfileModel profileModel = new ProfileModel();
                                profileModel = profileModel.toObject(object.toString());
                                if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                    String profileModelKey = profileModel.getFid();
                                    Utility.profileModelMap.remove(profileModelKey);
                                    Utility.profileModelMap.put(profileModelKey, profileModel);
                                }
                            }
                            chatView(typeChat, fidOrGid);
                            DataManager dataManager = DataManager.getInstance(getApplicationContext());
                            dataManager.insertChatFavouritesAndGroups(Utility.profileModelMap, null);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String errorMessage, String errorCode) {
                        Log.d(TAG, errorMessage);
                    }
                });
                getFavouriteListApi.execute();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void getProfileFromFid(final String typeChat, final String fidOrGid) {
        isNotificationFrom = true;
        ProfileModel profileModel1 = new ProfileModel();
        profileModel1.setSecret_key(UserModel.getInstance().secret_key);
        profileModel1.setFid(fidOrGid);
        APIManager viewProfile = new APIManager("relationship/viewProfile", profileModel1, new APIListener() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.d(TAG, "getProfileFromFid = " + object.getString("fid"));
                    profileModel = new ProfileModel();
                    profileModel = profileModel.toObject(object.toString());
                    Utility.profileModelMap.put(profileModel.getFid(), profileModel);
                    chatView(typeChat, fidOrGid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Log.d(TAG, errorMessage);
            }
        });
        viewProfile.execute();
    }

    private void chatView(final String typeChat, final String fidOrGid) {
        try {
            Log.d(TAG, "chatView " + typeChat + " " + profileModel + " " + groupModel + " " + fidOrGid);

            recordAudioButton = findViewById(R.id.recordAudio);
            chatsendmessageButton = findViewById(R.id.chatsendmessage);
            Toolbar toolbar = findViewById(R.id.toolbartop);
            toolbar.setTitleTextColor(Color.WHITE);
            setSupportActionBar(toolbar);
            toolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (typeChat.equalsIgnoreCase("1")) {
                        if (groupModel != null) {
                            Intent intent = new Intent(getApplicationContext(), ViewGroupMembersActivity.class);
                            intent.putExtra("groupModelJson", groupModel.toJSON());
                            startActivity(intent);
                        }
                    }
                }
            });
//            toolbar.performClick();
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            if (typeChat.equalsIgnoreCase("0")) {
                marque.setText(profileModel.getDisplayname());
            } else {
                marque.setText(groupModel.getName());
            }

            chatMessageAdapter = new ChatMessageAdapter(this.getWindow().getContext(), mediaPlayer);
            listview_chatmessage = findViewById(R.id.chatactivitylistview);
            listview_chatmessage.setAdapter(chatMessageAdapter);
//        listview_chatmessage.setSelector(R.color.appColor);
//        listview_chatmessage.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

            final EditText ed = findViewById(R.id.chatactivityeditText);

            ed.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().trim().length() == 0) {
                        recordAudioButton.setVisibility(View.VISIBLE);
                        chatsendmessageButton.setVisibility(View.GONE);
                    } else {
                        recordAudioButton.setVisibility(View.GONE);
                        chatsendmessageButton.setVisibility(View.VISIBLE);
                    }
                }
            });

            chatsendmessageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utility.logFA(mFirebaseAnalytics, "chat screen", "send", "button");
                    try {
                        String contentChat = ed.getText().toString().trim();
//                    String messageString = "testString";
                        if (contentChat.equals("")) {
                            Toast.makeText(getApplicationContext(), "Kindly enter message to send", Toast.LENGTH_SHORT).show();

                        } else {
                            NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
                            if (!NetworkConnectionreceiver.isConnection) {
                                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            final ChatMessageModel chatMessageModel = new ChatMessageModel();
                            chatMessageModel.setTypeFromOrTo("to");
                            chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                            chatMessageModel.setChatType0or1(typeChat);

                            chatMessageModel.setText(contentChat);
                            chatMessageModel.setFid(fidOrGid);
                            chatMessageModel.setMessageContentType(getString(R.string.text));
                            boolean isToday = false;
                            if (chatMessageAdapter.getCount() > 0) {
                                ChatMessageModel chatMessageModel1 = (ChatMessageModel) chatMessageAdapter.getItem(chatMessageAdapter.getCount() - 1);
                                Date prevMessageDatetime = chatMessageModel1.getDatetime();
                                if (Utility.isToday(prevMessageDatetime, getApplicationContext())) {
                                    isToday = true;
                                }
                            }
                            if (!isToday || chatMessageAdapter.getCount() == 0) {
                                ChatMessageModel chatMessageModelDay = new ChatMessageModel();
                                chatMessageModelDay.setDatetime(chatMessageModel.getDatetime());
                                chatMessageAdapter.add(chatMessageModelDay);
                                chatMessageAdapter.notifyDataSetChanged();
                            }
                            ed.setText("");
                            if (chatMessageModel.getChatType0or1().equalsIgnoreCase("1")) {
                                chatMessageModel.setGid(fidOrGid);
                            }
                            ChatMessageModel tempSelectedCMModel = new ChatMessageModel();
                            if (selectedChatMessageModel != null) {
                                tempSelectedCMModel = tempSelectedCMModel.toObject(selectedChatMessageModel.toJSON(selectedChatMessageModel));
                            }
                            final ChatMessageModel newSelectedChatMessageModel = tempSelectedCMModel;
                            final JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, getApplicationContext(), newSelectedChatMessageModel, chatMessageAdapter);
                            chatMessageModel.setAcknowledgementStatus(0);
                            chatMessageModel.setTotalContent(msgJson.toString());
                            chatMessageAdapter.add(chatMessageModel);
                            chatMessageAdapter.notifyDataSetChanged();

                            if (chatMessageModel.getText().contains("abracadabra loop")) {
                                int timerPeriod = 1000;
                                if (chatMessageModel.getText().contains(",")) {
                                    timerPeriod = Integer.valueOf(chatMessageModel.getText().split(",")[1]);
                                }
                                Timer _timer = new Timer();

                                _timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        // use runOnUiThread(Runnable action)
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                chatMessageModel.setText("test string " + new Date());
                                                JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, getApplicationContext(), newSelectedChatMessageModel, chatMessageAdapter);
                                                Utility.sendChatContent(chatMessageModel, getApplicationContext(), true, newSelectedChatMessageModel, selectedChatMessageModel, listview_chatmessage, chatMessageAdapter, msgJson);
                                            }
                                        });
                                    }
                                }, timerPeriod, timerPeriod);
                            } else if (chatMessageModel.getText().contains("abracadabra delete")) {
                                DataManager dataManager = DataManager.getInstance(getApplicationContext());
                                dataManager.deleteTable("chathistory", "fid", fidOrGid);
                                Toast.makeText(getApplicationContext(), "Chat messages deleted", Toast.LENGTH_LONG).show();
                                onBackPressed();
                            } else {
                                Utility.sendChatContent(chatMessageModel, getApplicationContext(), true, newSelectedChatMessageModel, selectedChatMessageModel, listview_chatmessage, chatMessageAdapter, msgJson);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

            final ImageView chat_attach = findViewById(R.id.chat_attach);
            chat_attach.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(View v) {
                    try {
                        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
                        if (!NetworkConnectionreceiver.isConnection) {
                            Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Utility.logFA(mFirebaseAnalytics, "chat screen", "attach", "imageview");
                        PopupMenu menu = new PopupMenu(ChatMessageActivity.this, chat_attach);
                        menu.inflate(R.menu.chat_attach_menu);

                        @SuppressLint("RestrictedApi")
                        MenuPopupHelper menuHelper = new MenuPopupHelper(ChatMessageActivity.this, (MenuBuilder) menu.getMenu(), chat_attach);

                        menuHelper.setForceShowIcon(true);

                        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.ic_camera_alt_black: {
                                        try {
                                            Toast.makeText(getApplicationContext(), "Capturing image using camera", Toast.LENGTH_SHORT).show();
                                            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                            File imageFile = Utility.createImageFile(getApplicationContext());
                                            imageUri = Utility.getUriFromFile(getApplicationContext(), imageFile);
                                            takePhotoIntent.putExtra("typeChat", typeChat);
                                            takePhotoIntent.putExtra("fidOrGid", fidOrGid);

                                            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                            startActivityForResult(takePhotoIntent, CAPTURE_IMAGE_REQUEST);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return true;
                                    }

                                    case R.id.ic_attach_file_black: {
                                        Toast.makeText(getApplicationContext(), "Attach file", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                        intent.setType("image/* video/*");
                                        intent.putExtra("typeChat", typeChat);
                                        intent.putExtra("fidOrGid", fidOrGid);
                                        startActivityForResult(Intent.createChooser(intent, "Attach file"), UPLOAD_FILE_REQUEST);
                                        return true;
                                    }
                                    case R.id.ic_videocam_black: {
                                        try {
                                            Toast.makeText(getApplicationContext(), "Record video using camera", Toast.LENGTH_SHORT).show();
                                            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                            takeVideoIntent.putExtra("typeChat", typeChat);
                                            takeVideoIntent.putExtra("fidOrGid", fidOrGid);
                                            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                                                startActivityForResult(takeVideoIntent, RECORD_VIDEO_REQUEST);
                                            }
                                        } catch (Exception e) {
                                            Log.e(TAG, "ChatView = " + e.getMessage());
                                        }
                                        return true;
                                    }
                                }
                                return false;
                            }
                        });
                        //noinspection RestrictedApi
                        menuHelper.show();
                    } catch (Exception e) {
                        Log.e(TAG, "chat_attach " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });

            LinkedList<ChatMessageModel> chatHistoryWithDate = getChatHistoryWithDate(fidOrGid);
            for (final ChatMessageModel chatMessageModel1 : chatHistoryWithDate) {
                try {
                    if (chatMessageModel1.getTypeFromOrTo() != null) {
                        Log.d(TAG, chatMessageModel1.getTypeFromOrTo());
                        JSONObject chatContent = new JSONObject(chatMessageModel1.getTotalContent());
                        String chatText = chatContent.get("content").toString().trim();
                        JSONArray imagesArray = (JSONArray) chatContent.get("images");
                        JSONArray audiosArray = (JSONArray) chatContent.get("audios");
                        JSONArray videosArray = (JSONArray) chatContent.get("videos");
                        String extraType = chatContent.has("extraType") ? chatContent.getString("extraType") : "";
                        String extraString = chatContent.has("extraString") ? chatContent.getString("extraString") : "";
//                        chatMessageModel1.setIsprogress(false);
                        if (!chatText.isEmpty()) {
                            chatMessageModel1.setText(chatText);
                            chatMessageModel1.setMessageContentType("text");
                        } else if (imagesArray.length() != 0) {
                            chatMessageModel1.setImage(imagesArray.get(0).toString());
                            chatMessageModel1.setMessageContentType("image");
                        } else if (audiosArray.length() != 0) {
                            chatMessageModel1.setAudio(audiosArray.get(0).toString());
                            chatMessageModel1.setMessageContentType(getString(R.string.audio));
                        } else if (videosArray.length() != 0) {
                            chatMessageModel1.setVideo(videosArray.get(0).toString());
                            chatMessageModel1.setMessageContentType("video");
                        } else if (!extraType.isEmpty()) {
                            if (extraType.equals(getString(R.string.groupextramessage))) {
                                chatMessageModel1.setMessageContentType(getString(R.string.groupextramessage));
                                chatMessageModel1.setExtraString(extraString);
                            }
                        }
                        chatMessageModel1.setChatType0or1(typeChat);
                        if (typeChat.equalsIgnoreCase("0") && chatMessageModel1.getMessageContentType() != null) {
                            Log.d(TAG, "type0");
                            chatMessageModel1.setGroupOrProfileModel(profileModel);
                            chatMessageAdapter.add(chatMessageModel1);
                            chatMessageAdapter.notifyDataSetChanged();
                        } else if (typeChat.equalsIgnoreCase("1") && chatMessageModel1.getMessageContentType() != null) {
                            String fid = chatContent.getString("fid");
                            if (Utility.profileModelMap.containsKey(fid)) {
                                Log.d(TAG, "if " + fid);
                                chatMessageModel1.setGroupOrProfileModel(Utility.profileModelMap.get(fid));
                                chatMessageAdapter.add(chatMessageModel1);
                                chatMessageAdapter.notifyDataSetChanged();
                            } else if (fid.equals(UserModel.getInstance().getFid())) {
                                Log.d(TAG, "else if " + fid);
                                chatMessageModel1.setGroupOrProfileModel(UserModel.getInstance());
                                chatMessageAdapter.add(chatMessageModel1);
                                chatMessageAdapter.notifyDataSetChanged();
                            } else {
                                Log.d(TAG, "else " + fid);
                                try {
                                    String[] members = groupModel.getMembers();
                                    for (String member : members) {
                                        JSONObject memberJsonObject = new JSONObject(member);
                                        if (memberJsonObject.get("fid").equals(fid)) {
                                            ProfileModel profileModel1 = new ProfileModel();
                                            profileModel1.setProfileimageurl(memberJsonObject.getString("profileimage"));
                                            profileModel1.setDisplayname(memberJsonObject.getString("displayname"));
                                            chatMessageModel1.setGroupOrProfileModel(profileModel1);
                                            chatMessageAdapter.add(chatMessageModel1);
                                            chatMessageAdapter.notifyDataSetChanged();
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "else = " + e.getMessage());
                                }
                            }
                        }
                    } else {
                        Log.d(TAG, "datetime");
                        chatMessageAdapter.add(chatMessageModel1);
                        chatMessageAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            }
            if (unreadCount != null && unreadCount != 0) {
                ChatMessageModel chatMessageModel1 = new ChatMessageModel();
                chatMessageModel1.setMessageContentType(getString(R.string.groupextramessage));
                if (unreadCount == 1)
                    chatMessageModel1.setExtraString(unreadCount + " unread message");
                else
                    chatMessageModel1.setExtraString(unreadCount + " unread messages");
                chatMessageModel1.setChatType0or1(typeChat);
                int unreadposition = chatMessageAdapter.getCount() - unreadCount;
                chatMessageAdapter.add(unreadposition, chatMessageModel1);
                chatMessageAdapter.notifyDataSetChanged();
                listview_chatmessage.setSelectionFromTop(unreadposition, 150);
            }

            registerForContextMenu(listview_chatmessage);
            recordAudioButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (isRecordAudioButtonPressed) {
                            isRecordAudioButtonPressed = false;
                            if (stopRecording()) {
                                if (new File(mAudioFileName).exists())
                                    sendAudioFile();
                            }
                            return true;
                        } else {
                            Toast.makeText(getApplicationContext(), "Hold to record. Relese to send", Toast.LENGTH_SHORT).show();
                            return false;
                        }
                    }
                    return false;
                }
            });

            recordAudioButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    checkAudioRecordPermissionAndRecord();
                    return true;
                }
            });
            DataManager dataManager = DataManager.getInstance(getApplicationContext());
            dataManager.changeStatusOfUnreadMessages(fidOrGid);
        } catch (Exception e) {
            Log.e(TAG, "chatView " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void checkAudioRecordPermissionAndRecord() {
        int result_ra = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        if (result_ra == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getApplicationContext(), "onRequestPermissionsResult checkPermission = true", Toast.LENGTH_SHORT).show();
            recordAudioByPermission();
        } else {
            requestPermission();
        }
    }

    private void recordAudioByPermission() {
        Toast.makeText(getApplicationContext(), "Audio is recording... Relese to send", Toast.LENGTH_SHORT).show();
        isRecordAudioButtonPressed = true;
        startRecording();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ChatMessageActivity.this, new String[]{RECORD_AUDIO}, RA_REQUEST_CODE);
    }

    private void sendAudioFile() {
        try {
            final byte[] byteArray = Utility.convertUrlToByteArray(mAudioFileName);
            if (byteArray.length == 0) {
                Toast.makeText(getApplicationContext(), "Invalid audio. Kindly check audio permission from your mobile settings", Toast.LENGTH_SHORT).show();
                return;
            }
            LayoutInflater inflater = getLayoutInflater();
            AlertDialog.Builder chatSendConfirmationDialog = new AlertDialog.Builder(ChatMessageActivity.this);
            chatSendConfirmationDialog.setTitle("Confirm");

            View view = inflater.inflate(R.layout.chat_send_confimation, null);
            FrameLayout frameLayout = view
                    .findViewById(R.id.chat_send_confirm);
            chatSendConfirmationDialog.setView(view);

            ImageView imageView = new ImageView(getApplicationContext());
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_headset));

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(300, 300);
            layoutParams.gravity = Gravity.CENTER;
            imageView.setLayoutParams(layoutParams);
            imageView.setPadding(30, 30, 30, 30);
            frameLayout.addView(imageView);
            chatSendConfirmationDialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    dialog1.dismiss();
                    ChatMessageModel chatMessageModel = new ChatMessageModel();
                    chatMessageModel.setMessageContentType(getString(R.string.audio));
                    chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                    chatMessageModel.setAudio(mAudioFileName);
                    chatMessageModel.setTypeFromOrTo("to");
                    chatMessageModel.setGroupOrProfileModel(profileModel);
                    chatMessageModel.setChatType0or1(typeChat);
                    chatMessageModel.setFid(fidOrGid);

                    chatMessageModel.setIsprogress(true);
                    chatMessageAdapter.add(chatMessageModel);
                    chatMessageAdapter.notifyDataSetChanged();

                    uploadFile(byteArray, getString(R.string.audio), chatMessageModel, selectedChatMessageModel);
                }
            });

            chatSendConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    dialog1.cancel();
                }
            });
            chatSendConfirmationDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LinkedList<ChatMessageModel> getChatHistoryWithDate(String fidOrGid) {
        DataManager dataManager = DataManager.getInstance(getApplicationContext());
        List<ChatMessageModel> chatMessageModels = dataManager.getChatHistory(fidOrGid);

        LinkedList<ChatMessageModel> chatMessageModelLinkedList = new LinkedList<ChatMessageModel>();
        for (int k = 0; k < chatMessageModels.size(); k++) {
            ChatMessageModel chatMessageModel = chatMessageModels.get(k);
            Date currentModelDate = Utility.getDateFromDateTime(chatMessageModel.getDatetime());
            if (k == 0) {
                ChatMessageModel chatMessageModelDate = new ChatMessageModel();
                chatMessageModelDate.setDatetime(currentModelDate);
                chatMessageModelLinkedList.add(chatMessageModelDate);
            } else {
                ChatMessageModel prevChatTransModel = chatMessageModels.get(k - 1);
                Date previousModelDate = Utility.getDateFromDateTime(prevChatTransModel.getDatetime());

                if (currentModelDate.after(previousModelDate)) {
                    ChatMessageModel chatMessageModelDate = new ChatMessageModel();
                    chatMessageModelDate.setDatetime(currentModelDate);
                    chatMessageModelLinkedList.add(chatMessageModelDate);
                }
            }
            chatMessageModelLinkedList.add(chatMessageModel);
        }
        return chatMessageModelLinkedList;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private void postChatContent(final JSONObject chatContent, final Long dateLongval, final String chatType, boolean isReminder, Bundle extras, String senderFid) {
        try {
            if (chatMessageAdapter.getCount() == 0) {
                ChatMessageModel chatMessageModelDay = new ChatMessageModel();
                chatMessageModelDay.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                chatMessageAdapter.add(chatMessageModelDay);
                Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
            }
            final String chatText = chatContent.get("content").toString().trim();
            final JSONArray imagesArray = (JSONArray) chatContent.get("images");
            final JSONArray audiosArray = (JSONArray) chatContent.get("audios");
            final JSONArray videosArray = (JSONArray) chatContent.get("videos");

            final String extraType = chatContent.has("extraType") ? chatContent.getString("extraType") : "";
            final String extraString = chatContent.has("extraString") ? chatContent.getString("extraString") : "";
            final String sourceMessageForReply = chatContent.has("sourceMessageForReply") ? chatContent.getString("sourceMessageForReply") : "";

            ChatMessageModel chatMessageModel = new ChatMessageModel();
            if (isReminder) {
                chatMessageModel.setExtraString(getString(R.string.reminder));
                chatMessageModel.setReminderMid(extras.getString("reminder_mid"));
            }
            if (chatType.equals("0")) {
                chatMessageModel.setGroupOrProfileModel(profileModel);
            } else {
                for (String member : groupModel.getMembers()) {
                    JSONObject memberJsonObject = new JSONObject(member);
                    if (memberJsonObject.get("fid").equals(chatContent.getString("fid"))) {
                        ProfileModel profileModel1 = new ProfileModel();
                        profileModel1.setProfileimageurl(memberJsonObject.getString("profileimage"));
                        profileModel1.setDisplayname(memberJsonObject.getString("displayname"));
                        chatMessageModel.setGroupOrProfileModel(profileModel1);
                    }
                }
            }
            if (extraType.equals(getString(R.string.groupextramessage))) {
                chatMessageModel.setMessageContentType(getString(R.string.groupextramessage));
                chatMessageModel.setExtraString(extraString);
            } else {
                if (!chatText.isEmpty()) {
                    chatMessageModel.setText(chatText);
                    chatMessageModel.setMessageContentType("text");
                } else if (imagesArray.length() != 0) {
                    chatMessageModel.setImage(imagesArray.get(0).toString());
                    chatMessageModel.setMessageContentType(getString(R.string.image));
                } else if (audiosArray.length() != 0) {
                    chatMessageModel.setAudio(audiosArray.get(0).toString());
                    chatMessageModel.setMessageContentType(getString(R.string.audio));
                } else if (videosArray.length() != 0) {
                    chatMessageModel.setVideo(videosArray.get(0).toString());
                    chatMessageModel.setMessageContentType(getString(R.string.video));
                }
            }
            if (!isReminder && chatContent.has("isreply") && chatContent.getBoolean("isreply")) {
                chatMessageModel.setIsreply(true);
                chatMessageModel.setSourceMessageForReply(sourceMessageForReply);
            }
            if (senderFid != null && senderFid.equalsIgnoreCase(UserModel.getInstance().fid)) {
                chatMessageModel.setTypeFromOrTo("to");
            } else {
                chatMessageModel.setTypeFromOrTo("from");
            }
            chatMessageModel.setDatetime(new Date(dateLongval));
            chatMessageModel.setChatType0or1(chatType);
            String message_id = extras.containsKey("message_id") ? extras.getString("message_id") : null;
            chatMessageModel.setMessageId(message_id);
            chatMessageModel.setTotalContent(chatContent.toString());
            chatMessageModel.setFid(chatContent.getString("fid"));
            chatMessageAdapter.add(chatMessageModel);
            Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);

        } catch (Exception e) {
            Log.e(TAG, "postChatContent = " + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            super.onActivityResult(requestCode, resultCode, data);
            AlertDialog.Builder chatSendConfirmationDialog = new AlertDialog.Builder(ChatMessageActivity.this);
            chatSendConfirmationDialog.setTitle("Confirm");
            LayoutInflater inflater = getLayoutInflater();
            if (requestCode == UPLOAD_FILE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                final Uri uploadUri = data.getData();

                final String uriPath =
                        URLEncoder.encode(uploadUri.getPath(), "UTF-8");
                String extension = MimeTypeMap.getFileExtensionFromUrl(uriPath);
                ContentResolver cR = getApplicationContext().getContentResolver();
                String type = cR.getType(uploadUri);

                if (type == null)
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                if (type.contains(getString(R.string.image))) {
                    Log.d(TAG, "uriPath = " + uriPath);

                    final String compressedImagePath = Utility.compressImage(getApplicationContext(), uploadUri);
                    Log.d(TAG, "compressedImagePath = " + compressedImagePath);
                    final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);

                    final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);

                    View view = inflater.inflate(R.layout.chat_send_confimation, null);
                    FrameLayout frameLayout = view
                            .findViewById(R.id.chat_send_confirm);
                    chatSendConfirmationDialog.setView(view);

                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setImageBitmap(bitmap);

                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER;
                    imageView.setLayoutParams(layoutParams);
                    imageView.setPadding(10, 10, 10, 10);
                    frameLayout.addView(imageView);
                    chatSendConfirmationDialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.dismiss();
                            ChatMessageModel chatMessageModel = new ChatMessageModel();
                            chatMessageModel.setMessageContentType("image");
                            chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                            chatMessageModel.setImage(compressedImagePath);
                            chatMessageModel.setTypeFromOrTo("to");
                            chatMessageModel.setGroupOrProfileModel(profileModel);
                            chatMessageModel.setChatType0or1(typeChat);
                            chatMessageModel.setFid(fidOrGid);
                            byte[] byteArray = stream.toByteArray();

                            chatMessageModel.setIsprogress(true);
                            chatMessageAdapter.add(chatMessageModel);
                            chatMessageAdapter.notifyDataSetChanged();

                            uploadFile(byteArray, "image", chatMessageModel, selectedChatMessageModel);
                        }
                    });

                    chatSendConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.cancel();
                        }
                    });
                    chatSendConfirmationDialog.show();

                } else if (type.contains(getString(R.string.video))) {

                    View view = inflater.inflate(R.layout.chat_send_confimation, null);
                    FrameLayout frameLayout = view
                            .findViewById(R.id.chat_send_confirm);
                    chatSendConfirmationDialog.setView(view);

                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER;

                    VideoView videoView = new VideoView(getApplicationContext());
                    videoView.setLayoutParams(layoutParams);
                    videoView.setPadding(10, 10, 10, 10);
                    frameLayout.addView(videoView);

                    layoutParams.height = 300;
                    layoutParams.width = 300;
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setImageResource(R.drawable.playoverlay);

                    imageView.setLayoutParams(layoutParams);
                    imageView.setPadding(10, 10, 10, 10);
                    frameLayout.addView(imageView);

                    videoView.setVideoURI(uploadUri);
                    videoView.seekTo(1);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), VideoPlayerActivity.class);
                            intent.putExtra("videoUri", uploadUri.toString());
                            startActivity(intent);
                        }
                    });

                    chatSendConfirmationDialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            ChatMessageModel chatMessageModel = new ChatMessageModel();
                            chatMessageModel.setMessageContentType("video");
                            String path = Utility.getRealPathFromUri(getApplicationContext(), uploadUri);
                            byte[] byteArray = Utility.convertUrlToByteArray(path);
                            if (byteArray == null) {
                                Toast.makeText(getApplicationContext(), "Unable to upload a file", Toast.LENGTH_SHORT).show();
                                dialog1.cancel();
                            } else {
                                chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));

                                String videoFilename = Utility.getFilename(getApplicationContext(), Environment.DIRECTORY_MOVIES, ".mp4");
                                File videoFile = new File(videoFilename);
                                Utility.copy(getApplicationContext(), uploadUri, videoFile);
                                chatMessageModel.setVideo(videoFilename);

                                chatMessageModel.setTypeFromOrTo("to");
                                chatMessageModel.setGroupOrProfileModel(profileModel);
                                chatMessageModel.setChatType0or1(typeChat);
                                chatMessageModel.setFid(fidOrGid);

                                chatMessageModel.setIsprogress(true);
                                chatMessageAdapter.add(chatMessageModel);
                                chatMessageAdapter.notifyDataSetChanged();

                                uploadFile(byteArray, "video", chatMessageModel, selectedChatMessageModel);
                            }
                        }
                    });

                    chatSendConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.cancel();
                        }
                    });
                    chatSendConfirmationDialog.show();

                }
            } else if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == RESULT_OK) {

                final String compressedImagePath = Utility.compressImage(getApplicationContext(), imageUri);
                Log.d(TAG, "compressedImagePath = " + compressedImagePath);
                final Bitmap bitmap = BitmapFactory.decodeFile(compressedImagePath);

                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);

                View view = inflater.inflate(R.layout.chat_send_confimation, null);
                FrameLayout frameLayout = view
                        .findViewById(R.id.chat_send_confirm);
                chatSendConfirmationDialog.setView(view);

                ImageView imageView = new ImageView(getApplicationContext());
                imageView.setImageBitmap(bitmap);

                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER;
                imageView.setLayoutParams(layoutParams);
                imageView.setPadding(10, 10, 10, 10);
                frameLayout.addView(imageView);
                chatSendConfirmationDialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        ChatMessageModel chatMessageModel = new ChatMessageModel();
                        chatMessageModel.setImage(compressedImagePath);
                        chatMessageModel.setMessageContentType("image");
                        chatMessageModel.setTypeFromOrTo("to");
                        chatMessageModel.setChatType0or1(typeChat);
                        chatMessageModel.setFid(fidOrGid);
                        chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                        byte[] byteArray = stream.toByteArray();

                        chatMessageModel.setIsprogress(true);
                        chatMessageAdapter.add(chatMessageModel);
                        chatMessageAdapter.notifyDataSetChanged();

                        uploadFile(byteArray, "image", chatMessageModel, selectedChatMessageModel);
                    }
                });

                chatSendConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.cancel();
                    }
                });
                chatSendConfirmationDialog.show();
            } else if (requestCode == RECORD_VIDEO_REQUEST && resultCode == RESULT_OK) {
                final Uri uri = data.getData();

                View view = inflater.inflate(R.layout.chat_send_confimation, null);
                FrameLayout frameLayout = view
                        .findViewById(R.id.chat_send_confirm);
                chatSendConfirmationDialog.setView(view);

                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER;

                VideoView videoView = new VideoView(getApplicationContext());
                videoView.setLayoutParams(layoutParams);
                videoView.setPadding(10, 10, 10, 10);
                frameLayout.addView(videoView);

                layoutParams.height = 300;
                layoutParams.width = 300;
                ImageView imageView = new ImageView(getApplicationContext());
                imageView.setImageResource(R.drawable.playoverlay);

                imageView.setLayoutParams(layoutParams);
                imageView.setPadding(10, 10, 10, 10);
                frameLayout.addView(imageView);

                videoView.setVideoURI(uri);
                videoView.seekTo(1);

                chatSendConfirmationDialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        byte[] byteArray = Utility.convertUrlToByteArray(Utility.getRealPathFromUri(getApplicationContext(), uri));
                        ChatMessageModel chatMessageModel = new ChatMessageModel();
                        chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                        String videoFilename = Utility.getFilename(getApplicationContext(), Environment.DIRECTORY_MOVIES, ".mp4");
                        File videoFile = new File(videoFilename);
                        Utility.copy(getApplicationContext(), uri, videoFile);

                        chatMessageModel.setVideo(videoFilename);
                        chatMessageModel.setMessageContentType("video");
                        chatMessageModel.setTypeFromOrTo("to");
                        chatMessageModel.setGroupOrProfileModel(profileModel);
                        chatMessageModel.setChatType0or1(typeChat);
                        chatMessageModel.setFid(fidOrGid);
                        if (byteArray == null) {
                            Toast.makeText(getApplicationContext(), "Unable to upload a file", Toast.LENGTH_SHORT).show();
                        } else {
                            chatMessageModel.setIsprogress(true);
                            chatMessageAdapter.add(chatMessageModel);
                            chatMessageAdapter.notifyDataSetChanged();

                            uploadFile(byteArray, "video", chatMessageModel, selectedChatMessageModel);
                        }
                    }
                });

                chatSendConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.cancel();
                    }
                });
                chatSendConfirmationDialog.show();
            } else if (requestCode == REQUEST_RECORD_AUDIO && resultCode == RESULT_OK) {
                Log.d(TAG, "REQUEST_RECORD_AUDIO = ");
            }
        } catch (Exception e) {
            Log.e(TAG, "OnActivityResult = " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void uploadFile(byte[] byteArray, final String type, final ChatMessageModel chatMessageModel, ChatMessageModel incomingSelectedChatMessageModel) {
        ChatMessageModel tempSelectedCMModel = new ChatMessageModel();
        if (incomingSelectedChatMessageModel != null) {
            tempSelectedCMModel = tempSelectedCMModel.toObject(incomingSelectedChatMessageModel.toJSON(incomingSelectedChatMessageModel));
        }
        final ChatMessageModel newSelectedChatMessageModel = tempSelectedCMModel;

        final JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, getApplicationContext(), newSelectedChatMessageModel, chatMessageAdapter);
        chatMessageModel.setAcknowledgementStatus(0);
        chatMessageModel.setTotalContent(msgJson.toString());
        isUploadInProgress = true;
        APIManager uploadapi = new APIManager("upload", byteArray, Utility.fileFormatMap.get(type), new APIListener() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    isUploadInProgress = false;
                    Log.d("uploadFile", result.toString());
                    if (type.equalsIgnoreCase("image")) {
                        String resultUrl = result.getString("url");
                        Log.d(TAG, "uploadFile = " + result.getString("url"));
                        String fileNameAfterUpload = resultUrl.substring(resultUrl.lastIndexOf("/") + 1);

                        File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, getApplicationContext());
                        File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                        File beforeRename = new File(chatMessageModel.getImage());
                        beforeRename.renameTo(afterRename);
                        Log.d(TAG, "beforeRename = " + beforeRename);
                        Log.d(TAG, "afterRename = " + afterRename);
                        chatMessageModel.setImage(resultUrl);
                    } else if (type.equalsIgnoreCase(getString(R.string.audio))) {
                        String resultUrl = result.getString("url");
                        Log.d(TAG, "uploadFile = " + result.getString("url"));
                        String fileNameAfterUpload = resultUrl.substring(resultUrl.lastIndexOf("/") + 1);

                        File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, getApplicationContext());
                        File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                        File beforeRename = new File(chatMessageModel.getAudio());
                        beforeRename.renameTo(afterRename);
                        Log.d(TAG, "beforeRename = " + beforeRename);
                        Log.d(TAG, "afterRename = " + afterRename);
                        chatMessageModel.setAudio(resultUrl);
                    } else if (type.equalsIgnoreCase("video")) {
//                        String videosChat = result.getString("url");
//                        Log.d(TAG, "uploadFile video = " + result.getString("url"));
//                        chatMessageModel.setVideo(videosChat);

                        String resultUrl = result.getString("url");
                        Log.d(TAG, "uploadFile = " + result.getString("url"));
                        String fileNameAfterUpload = resultUrl.substring(resultUrl.lastIndexOf("/") + 1);

                        File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, getApplicationContext());
                        File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                        File beforeRename = new File(chatMessageModel.getVideo());
                        beforeRename.renameTo(afterRename);
                        Log.d(TAG, "beforeRename = " + beforeRename);
                        Log.d(TAG, "afterRename = " + afterRename);
                        chatMessageModel.setVideo(resultUrl);

                    }
                    chatMessageModel.setTypeFromOrTo("to");

                    if (chatMessageModel.getChatType0or1().equalsIgnoreCase("1")) {
                        chatMessageModel.setGid(groupModel.getGid());
                    }
                    chatMessageModel.setIsprogress(false);
                    Utility.convertChatMessageModelToMsgJsonObject(chatMessageModel, msgJson);
                    Utility.sendChatContent(chatMessageModel, getApplicationContext(), true, newSelectedChatMessageModel, selectedChatMessageModel, listview_chatmessage, chatMessageAdapter, msgJson);
                    if (chatMessageAdapter.getCount() == 0) {
                        ChatMessageModel chatMessageModelDay = new ChatMessageModel();
                        chatMessageModelDay.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                        chatMessageAdapter.add(chatMessageModelDay);
                        chatMessageAdapter.notifyDataSetChanged();
                    }
                    chatMessageAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.d(TAG, "onSuccess = " + array.length());
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                isUploadInProgress = false;
                Log.d(TAG, "onFailure = " + errorCode + " and " + errorMessage);
            }
        }, APIManager.HTTP_METHOD.FILE);
        uploadapi.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.chatactivitylistview) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            ChatMessageModel chatMessageModel = (ChatMessageModel) lv.getItemAtPosition(acmi.position);
            if (chatMessageModel.getTypeFromOrTo() != null) {
                boolean isgroupextramessage = chatMessageModel.getMessageContentType().equalsIgnoreCase(getString(R.string.groupextramessage));
                boolean isReminder = chatMessageModel.getExtraString() != null && (chatMessageModel.getExtraString().equals(getString(R.string.reminder)));
                if (!isgroupextramessage && !isReminder) {
                    menu.add(getString(R.string.reply));
                    menu.add(getString(R.string.forward));
                    menu.add(getString(R.string.share));
                    if (chatMessageModel.getMessageContentType().equals(getString(R.string.text))) {
                        menu.add(getString(R.string.copy));
                    }
                    menu.add(getString(R.string.delete));
                    if (chatMessageModel.getMessageContentType().equals(getString(R.string.text))) {
                        menu.add(getString(R.string.setreminder));
                    }
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        try {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            if (item.getTitle().equals(getString(R.string.forward))) {
                ChatMessageModel chatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                Intent forwardIntent = new Intent(getApplicationContext(), ForwardActivity.class);
                forwardIntent.putExtra("chatMessageModel", chatMessageModel.toJSON(chatMessageModel));
                forwardIntent.putExtra("typeChat", typeChat);
                forwardIntent.putExtra("fidOrGid", fidOrGid);
                startActivity(forwardIntent);
            } else if (item.getTitle().equals(getString(R.string.share))) {
                ChatMessageModel chatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                String contentType = chatMessageModel.getMessageContentType();
                shareChatContent(contentType, chatMessageModel);
            } else if (item.getTitle().equals(getString(R.string.delete))) {
                ChatMessageModel chatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                deleteChatContent(chatMessageModel, info.position);
            } else if (item.getTitle().equals(getString(R.string.copy))) {
                ChatMessageModel chatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(R.string.copy), chatMessageModel.getText());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(), "Message copied", Toast.LENGTH_SHORT).show();
            } else if (item.getTitle().equals(getString(R.string.setreminder))) {
                ChatMessageModel chatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                chooseDateAndTimeForReminder(chatMessageModel);
            } else if (item.getTitle().equals(getString(R.string.reply))) {
                EditText chatactivityeditText = findViewById(R.id.chatactivityeditText);
                if (chatactivityeditText.requestFocus()) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
                for (ChatMessageModel chatMessageModel : chatMessageAdapter.chatMessageModels) {
                    chatMessageModel.setSelected(false);
                }
                selectedChatMessageModel = (ChatMessageModel) listview_chatmessage.getItemAtPosition(info.position);
                selectedChatMessageModel.setSelected(true);
                Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private void chooseDateAndTimeForReminder(final ChatMessageModel chatMessageModel) {
        // Ask date
//        DialogFragment newFragment = new DatePickerFragment();
//        newFragment.show(getSupportFragmentManager(), "datePicker");

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int minute = c.get(Calendar.MINUTE);

        final TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
                // TODO Auto-generated method stub
                c.set(Calendar.HOUR_OF_DAY, hours);
                c.set(Calendar.MINUTE, minutes);
                c.set(Calendar.SECOND, 0);
                Log.d(TAG, c.getTime().toString());
                selectPersons(c, chatMessageModel);
            }
        };

        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                new TimePickerDialog(ChatMessageActivity.this, timeSetListener, hour, minute,
                        DateFormat.is24HourFormat(getApplicationContext())).show();
            }
        };

        new DatePickerDialog(ChatMessageActivity.this, dateSetListener, year, month, day).show();
    }

    private void selectPersons(final Calendar c, final ChatMessageModel chatMessageModel) {
        try {
            final ArrayList<ProfileModel> remindingProfiles = new ArrayList<ProfileModel>();

            if (typeChat.equalsIgnoreCase("0")) {
                ProfileModel myProfileModel = new ProfileModel();
                myProfileModel = myProfileModel.toObject(UserModel.getInstance().toJSON());
                myProfileModel.setSelected(false);
                myProfileModel.setDisplayname("Me");
                remindingProfiles.add(myProfileModel);
                profileModel.setSelected(false);
                remindingProfiles.add(profileModel);
            } else {
                String[] members = groupModel.getMembers();
                ProfileModel myProfileModel = new ProfileModel();
                myProfileModel = myProfileModel.toObject(UserModel.getInstance().toJSON());
                myProfileModel.setSelected(false);
                myProfileModel.setDisplayname("Me");
                remindingProfiles.add(myProfileModel);
                for (String member : members) {
                    ProfileModel memberProfileModel = new ProfileModel();
                    memberProfileModel = memberProfileModel.toObject(member);
                    memberProfileModel.setSelected(false);
                    if (!memberProfileModel.fid.equals(UserModel.getInstance().fid))
                        remindingProfiles.add(memberProfileModel);
                }
            }

            CreateGroupAdapter createGroupAdapter = new CreateGroupAdapter(ChatMessageActivity.this);

            LayoutInflater inflater = getLayoutInflater();
            final View view = inflater.inflate(R.layout.group_modify_members, null);
            TextView marque = (TextView) view.findViewById(R.id.marque_scrolling_text);
            marque.setTextColor(Color.WHITE);
            marque.setText("Remind to");
//            dialogToolbar.setTitle("Modify Members");
//            dialogToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
            ListView listView = (ListView) view.findViewById(R.id.list_groupmembers);
            listView.setAdapter(createGroupAdapter);

            createGroupAdapter.setProfileModelArrayList(remindingProfiles);
            createGroupAdapter.notifyDataSetChanged();

            final AlertDialog.Builder dialog = new AlertDialog.Builder(ChatMessageActivity.this);

            dialog.setView(view);
            dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
                    if (!NetworkConnectionreceiver.isConnection) {
                        Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    int tempInt = 0;
                    JSONArray remindPersons = new JSONArray();
                    for (ProfileModel profileModel : remindingProfiles) {
                        if (profileModel.isSelected()) {
                            Log.d("ViewGroupMember2", profileModel.isSelected() + "");
                            tempInt++;
                            remindPersons.put(profileModel.fid);
                        }
                    }

                    if (tempInt == 0) {
                        Toast.makeText(getApplicationContext(), "No person selected", Toast.LENGTH_SHORT).show();
                    } else {
                        setReminder(c, chatMessageModel, remindPersons);
                    }
                    dialog1.dismiss();
                }
            });

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    dialog1.cancel();
                }
            });
            dialog.show();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_group_addmember);
                    checkBox.performClick();
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception e = " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void setReminder(Calendar c, ChatMessageModel chatMessageModel, JSONArray remindPersons) {
        try {

            JSONObject object = new JSONObject();
            object.put("secret_key", UserModel.getInstance().secret_key);
            object.put("time", c.getTimeInMillis());
            object.put("remind_to", remindPersons);
            object.put("message_id", chatMessageModel.getMessageId());

            byte[] clearBadgeCountJson = object.toString().getBytes(Charset.forName("UTF-8"));

            APIManager api = new APIManager("chat/addReminder", clearBadgeCountJson, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "clearBadgeCount success");
                    Toast.makeText(getApplicationContext(), "Reminder added successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(JSONArray object) {
                    Log.e(TAG, "clearBadgeCount success");
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, "clearBadgeCount error");
                    Toast.makeText(getApplicationContext(), "Sorry. Unable to add reminder", Toast.LENGTH_SHORT).show();
                }
            }, APIManager.HTTP_METHOD.POST);

            api.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteChatContent(ChatMessageModel chatMessageModel, final int position) {
        try {
            DataManager dataManager = DataManager.getInstance(getApplicationContext());

            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dt.setTimeZone(TimeZone.getTimeZone("GMT"));
            String fidOrGid = chatMessageModel.getFid();
            boolean isMessageDeleted = dataManager.deleteChatRecord(fidOrGid, dt.format(chatMessageModel.getDatetime()));
            if (isMessageDeleted) {
                listview_chatmessage.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
                chatMessageAdapter.remove(chatMessageModel);
                Toast.makeText(getApplicationContext(), "Message deleted", Toast.LENGTH_SHORT).show();
                Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareChatContent(String contentType, ChatMessageModel chatMessageModel) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (contentType.equalsIgnoreCase(getString(R.string.text))) {
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, chatMessageModel.getText());
        } else {
            String fileUrl = null;
            File receivedDirectory = null;
            if (contentType.equalsIgnoreCase(getString(R.string.image))) {
                fileUrl = chatMessageModel.getImage();
                sendIntent.setType("image/*");
                receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, getApplicationContext());
            } else if (contentType.equalsIgnoreCase(getString(R.string.audio))) {
                fileUrl = chatMessageModel.getAudio();
                sendIntent.setType("audio/*");
                receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, getApplicationContext());
            } else if (contentType.equalsIgnoreCase(getString(R.string.video))) {
                fileUrl = chatMessageModel.getVideo();
                sendIntent.setType("video/*");
                receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, getApplicationContext());
            }
            if (fileUrl != null) {
                String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
                final File fileCheck = new File(receivedDirectory + File.separator + fileName);
                File tempShareFile = Utility.createTempShareFile(fileCheck, receivedDirectory, contentType, getApplicationContext());
                sendIntent.putExtra(Intent.EXTRA_STREAM, Utility.getUriFromFile(getApplicationContext(), tempShareFile));
            }
        }
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share)));

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("isShare")) {
            Log.d(TAG, "onBackPressed = isShare");
            finish();
        } else if (isNotificationFrom) {
            Log.d(TAG, "onBackPressed = isNotificationFrom");
            finish();
            Intent chatPage = new Intent(getApplicationContext(), BottomBarActivity.class);
            chatPage.putExtra("screen", "openchatpage");
            chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(chatPage);
        }
        if (type != null && type.equals("nearby")) {
            Intent chatPage = new Intent(getApplicationContext(), BottomBarActivity.class);
            chatPage.putExtra("screen", "nearbypage");
            chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(chatPage);
        } else if (selectedChatMessageModel != null && !isUploadInProgress) {
            selectedChatMessageModel = null;
            for (ChatMessageModel chatMessageModel : chatMessageAdapter.chatMessageModels) {
                chatMessageModel.setSelected(false);
            }
            chatMessageAdapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
        }
    }

    private void startRecording() {
        try {
            mAudioFileName = Utility.createAudioFile(getApplicationContext()).getAbsolutePath();
            if (mRecorder == null)
                mRecorder = new MediaRecorder();

            String recordaudiopermission = SharedPrefManager.getInstance(getApplicationContext()).getItem("recordaudiopermission");
            if (recordaudiopermission == null) {
                SharedPrefManager.getInstance(getApplicationContext()).setItem("recordaudiopermission", "firsttime");
                isRecordAudioButtonPressed = false;
            }
            Log.d(TAG, "recordaudiopermission = " + recordaudiopermission);
            if (recordaudiopermission != null && recordaudiopermission.equals("firsttime")) {
                SharedPrefManager.getInstance(getApplicationContext()).setItem("recordaudiopermission", "true");
                stopRecording();
                startRecording();
            } else {
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
                mRecorder.setOutputFile(mAudioFileName);
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                mRecorder.prepare();
                mRecorder.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "startRecording = " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean stopRecording() {
        try {
            Log.d(TAG, "stopRecording");
            mRecorder.stop();
            return true;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Try now", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onResume() {
        try {
            isActivityActive = true;
            Log.d(TAG, "onResume");
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
            mediaPlayer.start();
            if (fidOrGid != null) {
                clearBadgeCount();
            }
            super.onResume();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        try {
            isActivityActive = false;
            Log.d(TAG, "onPause");
            if (fidOrGid != null) {
                DataManager dataManager = DataManager.getInstance(getApplicationContext());
                dataManager.changeStatusOfUnreadMessages(fidOrGid);
                dataManager.deleteTable("unreadnotification", "fidOrGid", fidOrGid);
            }
            mediaPlayer.pause();
            super.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        try {
            Log.d(TAG, "onStop");
            mediaPlayer.pause();
            super.onStop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        Toast.makeText(getApplicationContext(), "onRequestPermissionsResult requestCode = " + requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case RA_REQUEST_CODE:
//                Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults.length = " + grantResults.length, Toast.LENGTH_SHORT).show();
                if (grantResults.length > 0) {
//                    Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults[0] = " + grantResults[0], Toast.LENGTH_SHORT).show();
                    boolean raPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (!raPermissionGranted) {
                        Toast.makeText(getApplicationContext(), "You need to grant permissions", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}