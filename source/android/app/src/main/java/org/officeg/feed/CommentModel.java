package org.officeg.feed;


import android.support.annotation.NonNull;

import java.io.Serializable;

public class CommentModel implements Serializable, Comparable<CommentModel> {
    String displayName;
    long timeCommented;
    String commentJson;
    private String profileImage;

    public CommentModel(String name, long timeCommented, String commentJson, String profileImage) {
        this.displayName = name;
        this.timeCommented = timeCommented;
        this.commentJson = commentJson;
        this.profileImage = profileImage;
    }

    public String getName() {
        return displayName;
    }

    public long getTimeCommented() {
        return timeCommented;
    }

    public String getComment() {
        return commentJson;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }


    @Override
    public int compareTo(@NonNull CommentModel commentModel) {
        return String.valueOf(this.getTimeCommented()).compareTo(String.valueOf(commentModel.getTimeCommented()));
    }
}
