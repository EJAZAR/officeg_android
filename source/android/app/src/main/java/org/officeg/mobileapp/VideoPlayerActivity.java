package org.officeg.mobileapp;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import org.officeg.api.NetworkConnectionreceiver;

/**
 * Created by techiejackuser on 09/06/17.
 */

public class VideoPlayerActivity extends AppCompatActivity {
    VideoView video_player_view;
    DisplayMetrics dm;
    MediaController media_Controller;
    private String TAG = VideoPlayerActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        checkInternetConnection();
       // NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        String videoUri = null;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            videoUri = (String) bundle.get("videoUri");
        }
        if (videoUri != null) {
            playVideo(videoUri);
        }
    }

    private void checkInternetConnection() {
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());

    }

    public void playVideo(String videoUri) {
        try {
            video_player_view = (VideoView) findViewById(R.id.video_player_view);
            media_Controller = new MediaController(this);
            dm = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int height = dm.heightPixels;
            int width = dm.widthPixels;
            video_player_view.setMinimumWidth(width);
            video_player_view.setMinimumHeight(height);
            video_player_view.setMediaController(media_Controller);
            video_player_view.setVideoPath(videoUri);
            video_player_view.start();
            video_player_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.d("video", "setOnErrorListener ");
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        video_player_view.pause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        video_player_view.pause();
    }
}