package org.officeg.mobileapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.feed.ImageLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


public class TreeFragment extends Fragment {
    WebView treeWebView;
    ImageLoader loader = null;
    private View currentView;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("My Team");

        loader = new ImageLoader(getActivity());
        currentView = inflater.inflate(R.layout.fragment_tree, container, false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        treeView();
        return currentView;
    }

    private void treeView() {
        treeWebView = currentView.findViewById(R.id.tree_webView);
        treeWebView.setWebViewClient(new TreeWebViewClient());

        WebSettings webSettings = treeWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);

        treeWebView.loadUrl("file:///android_asset/treeStructure.html");
//        treeWebView.loadUrl("file:///android_asset/html-tabs.html");

        treeWebView.addJavascriptInterface(new TreeInterface(getActivity(), treeWebView), "Relationship");

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class TreeWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals("treeStructure.html")) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view,
                                                          String url) {

            Bitmap bitmap = loader.getImage(url, true);
            if (bitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                byte[] byteArray = stream.toByteArray();
                return new WebResourceResponse("image/jpg", "UTF-8", new ByteArrayInputStream(byteArray));

            } else
                return super.shouldInterceptRequest(view, url);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_chat_id);
                    return true;
                }
                return false;
            }
        });
    }
}