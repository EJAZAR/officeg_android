package org.officeg.mobileapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.settings.BookmarksAdapter;

import java.util.ArrayList;
import java.util.List;

public class BookmarksActivity extends AppCompatActivity {
    ListView bookarkslist;
    UserModel user = null;
    ArrayList<ProfileModel> bookmarklistvalues = new ArrayList<>();
    BookmarksAdapter bookmarkListadapter;
    byte[] bookmarksbyte;
    LinearLayout emptyBookmarks;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.bookmarks);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bookarkslist = findViewById(R.id.bookarkslist);
        getBookmarks();
    }

    private void getBookmarks() {
        DataManager dataManager = DataManager.getInstance(getApplicationContext());
        List<ProfileModel> profileModels = dataManager.getBookmarks();
        bookmarklistvalues = new ArrayList<>();
        try {
            bookmarklistvalues.addAll(profileModels);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bookmarklistvalues != null && !bookmarklistvalues.isEmpty()) {
            bookmarkListadapter = new BookmarksAdapter(getWindow().getContext(), bookmarklistvalues);
            bookarkslist.setAdapter(bookmarkListadapter);
            ((BaseAdapter) bookarkslist.getAdapter()).notifyDataSetChanged();
        } else {
            emptyBookmarks = findViewById(R.id.empty_bookmarkslist);
            emptyBookmarks.setVisibility(View.VISIBLE);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBookmarks();
    }
}
