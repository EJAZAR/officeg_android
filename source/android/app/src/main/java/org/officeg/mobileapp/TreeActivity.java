package org.officeg.mobileapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.feed.ImageLoader;
import org.officeg.model.ProfileModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


public class TreeActivity extends AppCompatActivity {
    WebView treeWebView;
    ImageLoader loader = null;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ProfileModel selectedFamily;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tree_view);

        if (getIntent().hasExtra("profileModel")) {
            selectedFamily = new ProfileModel();
            selectedFamily = selectedFamily.toObject(getIntent().getStringExtra("profileModel"));
        }

        loader = new ImageLoader(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        treeView();
    }

    private void treeView() {
        treeWebView = findViewById(R.id.tree_webView);
        treeWebView.setWebViewClient(new TreeWebViewClient());

        WebSettings webSettings = treeWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);

        treeWebView.loadUrl("file:///android_asset/treeStructure.html");
//        treeWebView.loadUrl("file:///android_asset/html-tabs.html");

        treeWebView.addJavascriptInterface(new TreeViewInterface(getApplicationContext(), treeWebView, selectedFamily), "Relationship");

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class TreeWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals("treeStructure.html")) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view,
                                                          String url) {

            Bitmap bitmap = loader.getImage(url, true);
            if (bitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                byte[] byteArray = stream.toByteArray();
                return new WebResourceResponse("image/jpg", "UTF-8", new ByteArrayInputStream(byteArray));

            } else
                return super.shouldInterceptRequest(view, url);

        }
    }
}