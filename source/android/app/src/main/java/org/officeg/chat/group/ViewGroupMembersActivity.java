package org.officeg.chat.group;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.BottomBarActivity;
import org.officeg.mobileapp.ChatMessageActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static android.util.Log.d;

public class ViewGroupMembersActivity extends AppCompatActivity {
    GroupModel groupModel;
    int tempInt;
    ViewGroupMembersAdaptor viewGroupMembersAdaptor;
    LinkedList<ProfileModel> alreadyMembersProfileModelList;
    String isGroupAdmin = "false";
    private String TAG = "ViewGroupMembers";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit_id) {
            Intent editGroupIntent = new Intent(ViewGroupMembersActivity.this, CreateGroupActivity.class);
            editGroupIntent.putExtra("groupModel", groupModel.toJSON());
            startActivity(editGroupIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_group_members);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        try {
            final Object groupModelJson = getIntent().getExtras().get("groupModelJson");
            String groupModelJsonString = groupModelJson.toString();
            groupModel = new GroupModel();
            groupModel = groupModel.toObject(groupModelJsonString);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
            TextView marque = (TextView) findViewById(R.id.marque_scrolling_text);
            marque.setTextColor(Color.WHITE);
            marque.setText(groupModel.getName());
            setSupportActionBar(toolbar);

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            alreadyMembersProfileModelList = new LinkedList<ProfileModel>();
            viewGroupMembersAdaptor = new ViewGroupMembersAdaptor(ViewGroupMembersActivity.this);

            String[] adminsArray = groupModel.getAdmins();
            ArrayList<String> adminsList = new ArrayList<String>();
            for (String adminFid : adminsArray) {
                adminsList.add(adminFid);
            }
            showGroupMembers(adminsList);

            ListView listView = (ListView) findViewById(R.id.list_viewgroup);
            listView.setAdapter(viewGroupMembersAdaptor);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                        long arg3) {
                    final ProfileModel profileModel = alreadyMembersProfileModelList.get(position);
                    try {
                        String myFid = UserModel.getInstance().getFid();
                        if (!profileModel.getFid().equalsIgnoreCase(myFid)) {

                            String shortName = profileModel.getDisplayname().length() > 10 ? profileModel.getDisplayname().substring(0, 10) + "..." : profileModel.getDisplayname();

                            List<String> memberOptions = new ArrayList<String>();

                            memberOptions.add("Message " + shortName);
                            memberOptions.add("View " + shortName);

                            if (isGroupAdmin.equalsIgnoreCase("true")) {
                                memberOptions.add("Remove " + shortName);

                                if (profileModel.getIsadmin().equalsIgnoreCase("false")) {
                                    memberOptions.add("Make as admin");
                                } else {
                                    memberOptions.add("Revoke admin");
                                }
                            }
                            final CharSequence[] options = memberOptions.toArray(new CharSequence[memberOptions.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ViewGroupMembersActivity.this);
                            builder.setItems(options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int item) {
                                    if (options[item].equals("Make as admin")) {
                                        try {
                                            tempInt++;
                                            addMemberToGroup(profileModel, groupModel, "true");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (options[item].toString().contains("Message")) {
                                        try {
                                            Intent chatPage = new Intent(ViewGroupMembersActivity.this, ChatMessageActivity.class);
                                            Bundle extras = new Bundle();
                                            extras.putString("typeChat", "0");
                                            extras.putString("fidOrGid", profileModel.getFid());
                                            chatPage.putExtras(extras);
                                            chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(chatPage);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (options[item].toString().contains("Remove")) {
                                        try {
                                            tempInt++;
                                            removeMemberFromGroup(profileModel, groupModel);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (options[item].toString().contains("Revoke admin")) {
                                        try {
                                            tempInt++;
                                            revokeAdminPermission(profileModel, groupModel);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (options[item].toString().contains("View ")) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(profileModel.toJSON());
                                            Utility.showProfileDialog(jsonObject, ViewGroupMembersActivity.this);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                            builder.show();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "e = " + e.getMessage());
                    }

                }
            });

            FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.group_add_member);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!Utility.profileModelMap.isEmpty()) {
                        Log.d(TAG, "Utility.profileModelMap exist");
                        afterGettingFavouritesList();
                    } else {
                        final UserModel userModel = UserModel.getInstance();
                        APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", userModel, new APIListener() {
                            @Override
                            public void onSuccess(JSONObject result) {
                                d(TAG, "inside onsuccess");
                            }

                            @Override
                            public void onSuccess(JSONArray array) {
                                Log.d(TAG, "Utility.profileModelMap reload");

                                try {
                                    for (int k = 0; k < array.length(); k++) {
                                        JSONObject object = array.getJSONObject(k);
                                        ProfileModel profileModel = new ProfileModel();
                                        profileModel = profileModel.toObject(object.toString());

                                        if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                            String profileModelKey = profileModel.getFid();
                                            if (Utility.profileModelMap.containsKey(profileModelKey)) {
                                                Utility.profileModelMap.remove(profileModelKey);
                                            }
                                            Utility.profileModelMap.put(profileModelKey, profileModel);
                                        }
                                    }
                                    afterGettingFavouritesList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, errorMessage);
                            }
                        });
                        getFavouriteListApi.execute();
                    }
                }
            });
            Button group_exitButton = (Button) findViewById(R.id.group_exit);
            group_exitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder groupexitConfirmationDialog = new AlertDialog.Builder(ViewGroupMembersActivity.this);
                    groupexitConfirmationDialog.setTitle("Are you sure to Exit?");
                    groupexitConfirmationDialog.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.dismiss();
                            try {
                                UserModel userModel = UserModel.getInstance();
                                ProfileModel profileModel = new ProfileModel();
                                profileModel.setGid(groupModel.getGid());
                                profileModel.setSecret_key(userModel.getSecret_key());
                                profileModel.setFid(userModel.fid);
                                profileModel.setDisplayname(userModel.getDisplayname());
                                byte[] profileModelByteArray = profileModel.toJSON().getBytes(Charset.forName("UTF-8"));
                                APIManager removeMemberApi = new APIManager("chat/group/removemember", profileModelByteArray, new APIListener() {
                                    @Override
                                    public void onSuccess(JSONObject result) {
                                        d("group_exit", "inside onsuccess");
                                        Toast.makeText(getApplicationContext(), "You are no longer a participant", Toast.LENGTH_SHORT).show();
                                        Intent chatPage = new Intent(getApplicationContext(), BottomBarActivity.class);
                                        chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        chatPage.putExtra("screen", "openchatpage");
                                        startActivity(chatPage);
                                    }

                                    @Override
                                    public void onSuccess(JSONArray array) {
                                        d("group_exit", "inside onsuccess");
                                    }

                                    @Override
                                    public void onFailure(String errorMessage, String errorCode) {
                                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                        Log.e("group_exit", errorMessage + " and " + errorCode);
                                    }
                                }, APIManager.HTTP_METHOD.POST);
                                removeMemberApi.execute();
                            } catch (Exception e) {
                                Log.e("group_exit", "Exception = " + e.getMessage());
                                updateGroup(groupModel);
                            }

                        }
                    });

                    groupexitConfirmationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            dialog1.cancel();
                        }
                    });
                    groupexitConfirmationDialog.show();
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "OnCreate Exception e =" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void afterGettingFavouritesList() {
        try {
            final ArrayList<ProfileModel> remainingProfiles = new ArrayList<ProfileModel>();

            for (Map.Entry<String, ProfileModel> entry : Utility.profileModelMap.entrySet()) {
                ProfileModel profileModel = entry.getValue();
                if (!alreadyMembersProfileModelList.contains(profileModel)) {
                    remainingProfiles.add(profileModel);
                }
            }

            if (remainingProfiles.size() == 0) {
                Toast.makeText(getApplicationContext(), "You have no more favourites to add", Toast.LENGTH_SHORT).show();
                return;
            }
            final ArrayList<ProfileModel> tempProfileModels = new ArrayList<ProfileModel>();
            for (ProfileModel profileModel : remainingProfiles) {
                tempProfileModels.add((ProfileModel) profileModel.clone());
            }
            CreateGroupAdapter createGroupAdapter = new CreateGroupAdapter(ViewGroupMembersActivity.this);

            LayoutInflater inflater = getLayoutInflater();
            final View view = inflater.inflate(R.layout.group_modify_members, null);
            TextView marque = (TextView) view.findViewById(R.id.marque_scrolling_text);
            marque.setTextColor(Color.WHITE);
            marque.setText("Modify Members");
//            dialogToolbar.setTitle("Modify Members");
//            dialogToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
            ListView listView = (ListView) view.findViewById(R.id.list_groupmembers);
            listView.setAdapter(createGroupAdapter);

            createGroupAdapter.setProfileModelArrayList(remainingProfiles);
            createGroupAdapter.notifyDataSetChanged();

            final AlertDialog.Builder dialog = new AlertDialog.Builder(ViewGroupMembersActivity.this);

            dialog.setView(view);
            dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
                    if (!NetworkConnectionreceiver.isConnection) {
                        Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    tempInt = 0;

                    for (ProfileModel profileModel : remainingProfiles) {
                        for (ProfileModel tempProfileModel : tempProfileModels) {
                            Log.d("ViewGroupMember1", profileModel.isSelected() + " and " + tempProfileModel.isSelected());
                            if (profileModel.equals(tempProfileModel) && profileModel.isSelected() != tempProfileModel.isSelected()) {
                                Log.d("ViewGroupMember2", profileModel.isSelected() + " and " + tempProfileModel.isSelected());
                                tempInt++;
                            }
                        }
                    }

                    if (tempInt == 0) {
                        Toast.makeText(getApplicationContext(), "No changes in group", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (ProfileModel profileModel : remainingProfiles) {
                        for (ProfileModel tempProfileModel : tempProfileModels) {
                            Log.d("ViewGroupMember3", profileModel.isSelected() + " and " + tempProfileModel.isSelected());
                            if (profileModel.equals(tempProfileModel) && profileModel.isSelected() != tempProfileModel.isSelected()) {
                                if (profileModel.isSelected()) {
                                    Log.d("ViewGroupMember4", profileModel.isSelected() + " and " + tempProfileModel.isSelected());
                                    addMemberToGroup(tempProfileModel, groupModel, "false");
                                } else {
                                    Log.d("ViewGroupMember5", profileModel.isSelected() + " and " + tempProfileModel.isSelected());
                                    removeMemberFromGroup(tempProfileModel, groupModel);
                                }
                            }
                        }
                    }
                    Toast.makeText(getApplicationContext(), "Group details modified", Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                }
            });

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog1, int which) {
                    dialog1.cancel();
                }
            });
            dialog.show();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_group_addmember);
                    checkBox.performClick();
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception e = " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showGroupMembers(final List<String> adminList) {
        try {
            for (String member : groupModel.getMembers()) {
                JSONObject memberJsonObject = new JSONObject(member);
                ProfileModel profileModel = new ProfileModel();
                profileModel.setProfileimageurl(memberJsonObject.getString("profileimage"));
                profileModel.setDisplayname(memberJsonObject.getString("displayname"));
                profileModel.setFid(memberJsonObject.getString("fid"));
                if (adminList.contains(profileModel.getFid())) {
                    profileModel.setIsadmin("true");
                } else {
                    profileModel.setIsadmin("false");
                }
                alreadyMembersProfileModelList.add(profileModel);
                sortMembers();
                viewGroupMembersAdaptor.setProfileModelList(alreadyMembersProfileModelList);
                viewGroupMembersAdaptor.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sortMembers() {

        String myFid = UserModel.getInstance().getFid();
        for (ProfileModel profileModel : alreadyMembersProfileModelList) {
            if (profileModel.getFid().equalsIgnoreCase(myFid)) {
                isGroupAdmin = profileModel.getIsadmin();
                alreadyMembersProfileModelList.remove(profileModel);
                alreadyMembersProfileModelList.addLast(profileModel);
                break;
            }
        }
    }

    private void updateGroup(final GroupModel currGroupModel) {
        if (--tempInt == 0) {
            UserModel userModel = UserModel.getInstance();
            byte[] userModelBA = userModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager groupsApi = new APIManager("chat/groups", userModelBA, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    d(TAG, "inside onsuccess" + result.toString());
                }

                @Override
                public void onSuccess(JSONArray array) {
                    try {
                        d(TAG, "inside onsuccess" + array.toString());
                        Utility.groupModelMap = new HashMap<>();
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject singleGroup = array.getJSONObject(k);
                            GroupModel groupModel = new GroupModel();
                            groupModel = groupModel.toObject(singleGroup.toString());
                            Utility.groupModelMap.put(groupModel.getGid(), groupModel);
                            if (groupModel.getGid().equalsIgnoreCase(currGroupModel.getGid())) {
                                Intent chatPage = new Intent(getApplicationContext(), ChatMessageActivity.class);
                                Bundle extras = getIntent().getExtras();
                                extras.putString("typeChat", "1");
                                extras.putString("fidOrGid", groupModel.getGid());
                                chatPage.putExtras(extras);
                                chatPage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(chatPage);
                                Intent groupViewIntent = new Intent(getApplicationContext(), ViewGroupMembersActivity.class);
                                groupViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                groupViewIntent.putExtra("groupModelJson", groupModel.toJSON());
                                startActivity(groupViewIntent);
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, errorMessage);
                }

            }, APIManager.HTTP_METHOD.POST);
            groupsApi.execute();
        }
    }

    private void addMemberToGroup(final ProfileModel tempProfileModel, final GroupModel groupModel, final String isadmin) {
        try {
            UserModel userModel = UserModel.getInstance();

            ProfileModel profileModel = new ProfileModel();
            profileModel.setGid(groupModel.getGid());
            profileModel.setSecret_key(userModel.getSecret_key());
            profileModel.setFid(tempProfileModel.getFid());
            profileModel.setIsadmin(isadmin);

            byte[] profileModelByteArray = profileModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager addMemberApi = new APIManager("chat/group/addmember", profileModelByteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    d("addMemberToGroup", "inside onsuccess");
                    String extraString;

                    if (isadmin.equals("false")) {
                        extraString = UserModel.getInstance().getDisplayname() + " added " + tempProfileModel.getDisplayname();
                    } else {
                        extraString = tempProfileModel.getDisplayname() + " is now admin";
                    }
                    Utility.sendExtraNotification(getApplicationContext(), groupModel, getString(R.string.groupextramessage), extraString);
                    updateGroup(groupModel);
                }

                @Override
                public void onSuccess(JSONArray array) {
                    d("addMemberToGroup", "inside onsuccess");
                    updateGroup(groupModel);
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    Log.e("addMemberToGroup", errorMessage);
                    updateGroup(groupModel);
                }
            }, APIManager.HTTP_METHOD.POST);
            addMemberApi.execute();
        } catch (Exception e) {
            Log.e("addMemberToGroup", "Exception = " + e.getMessage());
            updateGroup(groupModel);
        }
    }

    private void removeMemberFromGroup(final ProfileModel tempProfileModel, final GroupModel groupModel) {
        try {
            UserModel userModel = UserModel.getInstance();

            ProfileModel profileModel = new ProfileModel();
            profileModel.setGid(groupModel.getGid());
            profileModel.setSecret_key(userModel.getSecret_key());
            profileModel.setFid(tempProfileModel.getFid());

            byte[] profileModelByteArray = profileModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager removeMemberApi = new APIManager("chat/group/removemember", profileModelByteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    d("removeMemberFromGroup", "inside onsuccess");
                    String extraString = UserModel.getInstance().getDisplayname() + " removed " + tempProfileModel.getDisplayname();
                    Utility.sendExtraNotification(getApplicationContext(), groupModel, getString(R.string.groupextramessage), extraString);
                    tempProfileModel.setSelected(false);
                    Utility.profileModelMap.remove(tempProfileModel.getFid());
                    Utility.profileModelMap.put(tempProfileModel.getFid(), tempProfileModel);
                    updateGroup(groupModel);
                }

                @Override
                public void onSuccess(JSONArray array) {
                    d("removeMemberFromGroup", "inside onsuccess");
                    updateGroup(groupModel);
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    Log.e("removeMemberFromGroup", errorMessage + " and " + errorCode);
                    updateGroup(groupModel);
                }
            }, APIManager.HTTP_METHOD.POST);
            removeMemberApi.execute();
        } catch (Exception e) {
            Log.e("removeMemberFromGroup", "Exception = " + e.getMessage());
            updateGroup(groupModel);
        }
    }


    private void revokeAdminPermission(final ProfileModel tempProfileModel, final GroupModel groupModel) {
        try {
            ProfileModel profileModel = new ProfileModel();
            profileModel.setGid(groupModel.getGid());
            profileModel.setSecret_key(UserModel.getInstance().getSecret_key());
            profileModel.setFid(tempProfileModel.getFid());

            byte[] profileModelByteArray = profileModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager revokeAdminPermissionApi = new APIManager("chat/group/revokeadminpermission", profileModelByteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    d("revokeAdminPermission", "inside onsuccess");
                    updateGroup(groupModel);
                }

                @Override
                public void onSuccess(JSONArray array) {
                    d("revokeAdminPermission", "inside onsuccess");
                    updateGroup(groupModel);
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    Log.e("revokeAdminPermission", errorMessage + " and " + errorCode);
                    updateGroup(groupModel);
                }
            }, APIManager.HTTP_METHOD.POST);
            revokeAdminPermissionApi.execute();
        } catch (Exception e) {
            Log.e("revokeAdminPermission", "Exception = " + e.getMessage());
            updateGroup(groupModel);
        }
    }
}