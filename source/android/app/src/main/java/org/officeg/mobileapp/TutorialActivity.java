package org.officeg.mobileapp;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.util.Utility;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class TutorialActivity extends FragmentActivity {

    SectionsPagerAdapter mSectionsPagerAdapter;
    ImageView zero, one, two;
    Button mSkipBtn;
    ImageView[] indicators;
    RelativeLayout layout;
    int page = 0;   //  to track page position
    Intent intent_activity;
    private AutoScrollViewPager mViewPager;
    private FirebaseAnalytics mFirebaseAnalytics;
    String[] introtitles;
    String[] introdescriptions;
    TextView introTitle, introDescription;
    private String TAG = TutorialActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tutorialscreen);
        introtitles = new String[]{getString(R.string.introtitle1), getString(R.string.introtitle2), getString(R.string.introtitle3)};
        introdescriptions = new String[]{getString(R.string.introdescription1), getString(R.string.introdescription2), getString(R.string.introdescription3)};
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        zero = findViewById(R.id.intro_indicator_0);
        one = findViewById(R.id.intro_indicator_1);
        two = findViewById(R.id.intro_indicator_2);

        introTitle = findViewById(R.id.tutorial_text1);
        introDescription = findViewById(R.id.tutorial_text2);
        layout = findViewById(R.id.main_content);

        indicators = new ImageView[]{zero, one, two};

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.startAutoScroll();
        mViewPager.setInterval(3000);
        mViewPager.setCycle(true);
        mViewPager.setStopScrollWhenTouch(true);

        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setCurrentItem(page);
        updateIndicators(page);

        mSkipBtn = findViewById(R.id.intro_btn_skip);

        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intent_activity = new Intent(TutorialActivity.this, PersonalProfileActivity.class);
                intent_activity.putExtra("type", "signup");
                startActivity(intent_activity);
            }
        });


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected");
                page = position;
                Utility.logFA(mFirebaseAnalytics, "tutorialscreen", "viewpager", "viewpager");

                updateIndicators(page);

                if (position == 0) {
//                    mSkipBtn.setText("Skip");
                    buttonsVisiblity();
                }
                if (position == 1) {
                    updateIndicators(page);
//                    mSkipBtn.setText("Skip");
                    buttonsVisiblity();
                }

                if (position == 2) {
                    updateIndicators(page);
//                    mSkipBtn.setText("Finish");
                    buttonsVisiblity();
                } else {
                    updateIndicators(page);
//                    mSkipBtn.setHint("Skip");
                    buttonsVisiblity();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void buttonsVisiblity() {
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intent_activity = new Intent(TutorialActivity.this, PersonalProfileActivity.class);
                intent_activity.putExtra("fromTutorial", "tutorialpage");
                intent_activity.putExtra("type", "signup");
                startActivity(intent_activity);
            }
        });

    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
        introTitle.setText(introtitles[position]);
        introDescription.setText(introdescriptions[position]);
    }

//      A placeholder fragment containing a simple view.

    @Override
    public void onBackPressed() {
        finish();
    }

    public static class PlaceholderFragment extends Fragment {
        //The fragment argument representing the section number for this fragment.
        private static final String ARG_SECTION_NUMBER = "section_number";

        ImageView img;

        int[] bgs = new int[]{R.drawable.intro1, R.drawable.intro2, R.drawable.intro3};

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            String TAG = TutorialActivity.class.getSimpleName();
            Log.d(TAG, "onCreateView");
            View rootView = inflater.inflate(R.layout.fragement_tutorialscreen, container, false);

            img = rootView.findViewById(R.id.bg);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            img.setImageResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}




