package org.officeg.mobileapp;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.controls.FGEdit;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

public class VerifyOTPActivity extends Activity implements FGEdit.FGEditListener {
    public static final String SHARED_PREF = "MyPrefsFile";

    String otpCode1, otpCode2, otpCode3, otpCode4, newotp, resendOTP, refCode, newrefCode, res, mobilenumber, type, receviedOtp,newMobileNo,countryCode,newCode;
    Button verify;
    ImageButton edit;
    TextView otp_err_msg, resend, display_input_mobile_number, refcodeView, otp_resend_msg;
    FGEdit otp_verifyCode1; //, otp_verifyCode2, otp_verifyCode3, otp_verifyCode4, otp_verifyCode5, otp_verifyCode6;
    ImageButton imageButton;
    UserModel user = null;
    TextView timer;
    Intent i;
    double weight_displayname, weight_firstname, weight_surname, weight_gender, weight_doi, weight_dob, weight_profileimage;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifyotp);
//        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if(!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        display_input_mobile_number = (TextView) findViewById(R.id.input_mobile_number);
        refcodeView = (TextView) findViewById(R.id.refcode_view);
        timer = (TextView) findViewById(R.id.id_timer);

        otp_verifyCode1 = (FGEdit) findViewById(R.id.otp_edText1);
        /*otp_verifyCode2 = (FGEdit) findViewById(R.id.otp_edText2);
        otp_verifyCode3 = (FGEdit) findViewById(R.id.otp_edText3);
        otp_verifyCode4 = (FGEdit) findViewById(R.id.otp_edText4);
        otp_verifyCode5 = (FGEdit) findViewById(R.id.otp_edText5);
        otp_verifyCode6 = (FGEdit) findViewById(R.id.otp_edText6);*/
        otp_err_msg = (TextView) findViewById(R.id.id_otp_err_msg);
        otp_resend_msg = (TextView) findViewById(R.id.id_resend_otp_msg);

        user = UserModel.getInstance();
        ProfileModel profile = ProfileModel.getInstance();


//        new CountDownTimer(30000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
//            }
//
//            public void onFinish() {
//                timer.setText("done!");
//            }
//        }.start();

        verifyOtp();

    }

    public void onPaste() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        String text = clipboard.getText().toString();
        if (text.length() == 6 && Character.isDigit(text.charAt(0)) && Character.isDigit(text.charAt(1)) && Character.isDigit(text.charAt(2))
                && Character.isDigit(text.charAt(3)) && Character.isDigit(text.charAt(4)) && Character.isDigit(text.charAt(5))) {
            otp_verifyCode1.setText("" + text);
//            otp_verifyCode2.setText("" + text.charAt(1));
//            otp_verifyCode3.setText("" + text.charAt(2));
//            otp_verifyCode4.setText("" + text.charAt(3));
//            otp_verifyCode5.setText("" + text.charAt(4));
//            otp_verifyCode6.setText("" + text.charAt(5));
        }
        Log.i("FamilyG", "Clip board data : " + clipboard.getText().toString());
    }

    public void verifyOtp() {


        mobilenumber = display_input_mobile_number.getText().toString();
        Log.i("FamilyG", "mob number: " + mobilenumber);

        edit = (ImageButton) findViewById(R.id.btn_edit);
        imageButton = (ImageButton) findViewById(R.id.btn_edit);
        imageButton.setImageResource(R.drawable.ic_mode_edit);

        verify = (Button) findViewById(R.id.btn_verify);
        otp_err_msg = (TextView) findViewById(R.id.id_otp_err_msg);


        otp_verifyCode1 = (FGEdit) findViewById(R.id.otp_edText1);
        /*otp_verifyCode2 = (FGEdit) findViewById(R.id.otp_edText2);
        otp_verifyCode3 = (FGEdit) findViewById(R.id.otp_edText3);
        otp_verifyCode4 = (FGEdit) findViewById(R.id.otp_edText4);
        otp_verifyCode5 = (FGEdit) findViewById(R.id.otp_edText5);
        otp_verifyCode6 = (FGEdit) findViewById(R.id.otp_edText6);*/

        otp_verifyCode1.addListener(this);
        /*otp_verifyCode2.addListener(this);
        otp_verifyCode3.addListener(this);
        otp_verifyCode4.addListener(this);
        otp_verifyCode5.addListener(this);
        otp_verifyCode6.addListener(this);*/

        otp_verifyCode1.addTextChangedListener(new CustomWatcher(otp_verifyCode1, otp_verifyCode1));
       /* otp_verifyCode2.addTextChangedListener(new CustomWatcher(otp_verifyCode2, otp_verifyCode1));
        otp_verifyCode3.addTextChangedListener(new CustomWatcher(otp_verifyCode3, otp_verifyCode2));
        otp_verifyCode4.addTextChangedListener(new CustomWatcher(otp_verifyCode4, otp_verifyCode3));
        otp_verifyCode5.addTextChangedListener(new CustomWatcher(otp_verifyCode5, otp_verifyCode4));
        otp_verifyCode6.addTextChangedListener(new CustomWatcher(otp_verifyCode6, otp_verifyCode5));*/

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "verifyotp", "edit", "button");
                i = getIntent();
                type = i.getStringExtra("type");
                newMobileNo = i.getStringExtra("NewPhoneNumber");
                countryCode = i.getStringExtra("CountryCode");
//                newCode = i.getStringExtra("oldCountryCode");
                if (type != null && type.equals("changenumber")) {
                    Log.i("FamilyG: ", "Edit new number: " + user.mobilenumber);
                    Intent intent_activity = new Intent(VerifyOTPActivity.this, ChangeNumberActivity.class);
                    intent_activity.putExtra("type", "Edit");
                    Log.i("FamilyG","edit phnumb: " +newMobileNo);
                    Log.i("FamilyG","edit cc: "+countryCode);
                    intent_activity.putExtra("enteredNewNumber",newMobileNo);
                    intent_activity.putExtra("enteredNewCountryCode",countryCode);
                    intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_activity);
                    finish();
                } else {
                    Intent intent_activity = new Intent(VerifyOTPActivity.this, RegistrationActivity.class);
                    startActivity(intent_activity);
                    finish();
                }
            }
        });

        resend = (TextView) findViewById(R.id.resendcode_btn);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "verifyotp", "resend", "button");
                otp_verifyCode1.setText("");
               /* otp_verifyCode2.setText("");
                otp_verifyCode3.setText("");
                otp_verifyCode4.setText("");
                otp_verifyCode5.setText("");
                otp_verifyCode6.setText("");*/
                otp_verifyCode1.requestFocus();

                otp_resend_msg.setText("OTP is sent to " + user.mobilenumber);
                user = UserModel.getInstance();
                i = getIntent();
                type = i.getStringExtra("type");
                if (type != null && type.equals("changenumber")) {
                    resendNewOtp();

                } else
                    resendOtp();
            }
        });

        otp_verifyCode1.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                /*if (otp_verifyCode1.length() == 1)
                    otp_verifyCode2.requestFocus();*/

                return false;
            }
        });

        /*otp_verifyCode2.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (otp_verifyCode2.length() == 1)
                    otp_verifyCode3.requestFocus();
                return false;
            }
        });

        otp_verifyCode3.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (otp_verifyCode3.length() == 1)
                    otp_verifyCode4.requestFocus();
                return false;
            }
        });

        otp_verifyCode4.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (otp_verifyCode4.length() == 1)
                    otp_verifyCode5.requestFocus();
                return false;
            }
        });

        otp_verifyCode5.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (otp_verifyCode5.length() == 1)
                    otp_verifyCode6.requestFocus();
                return false;
            }
        });

        otp_verifyCode6.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (otp_verifyCode6.length() == 1)
                    verify.requestFocus();
                return false;
            }
        });*/


        verify.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "verifyotp", "verify", "button");
                String otpCode1, otpCode2, otpCode3, otpCode4, otpCode5, otpCode6, verifyotpcode;
                otpCode1 = otp_verifyCode1.getText().toString();
                /*otpCode2 = otp_verifyCode2.getText().toString();
                otpCode3 = otp_verifyCode3.getText().toString();
                otpCode4 = otp_verifyCode4.getText().toString();
                otpCode5 = otp_verifyCode5.getText().toString();
                otpCode6 = otp_verifyCode6.getText().toString();*/

                verifyotpcode = otpCode1;// + otpCode2 + otpCode3 + otpCode4 + otpCode5 + otpCode6;
                Log.i("verify===", verifyotpcode);

                if (otpCode1.equals("") /*|| otpCode2.equals("") || otpCode3.equals("") || otpCode4.equals("") || otpCode5.equals("") || otpCode6.equals("")*/) {
                    Log.i("FC", "verification for empty or filled");
                    otp_err_msg.setError("");
                    otp_err_msg.setText(R.string.empty_otp);
                } else {

                    if (type != null && type.equals("changenumber")) {
                        user.otp = verifyotpcode;
                        verifyNewNumber();

                    } else {

                        Intent checkMobilenumber = getIntent();
                        Boolean existMobileNumber = checkMobilenumber.getBooleanExtra("ExistingNumber", false);
                        Log.i("FamilyG", "isNew: " + existMobileNumber);

                        user.otp = verifyotpcode;
                        user.isnew = existMobileNumber;

                        APIManager api = new APIManager("verify", user, new APIListener() {
                            @Override
                            public void onSuccess(JSONObject result) {
                                try {
                                    if (user.isnew) {
                                        user.secret_key = result.getString("secret_key");
                                        user.mobilenumber = mobilenumber;
                                        Log.i("FamilyG", "onsuccess verify: " + user.mobilenumber);
                                        user.fid = result.getString("fid");
                                        user.waitingrequests=result.getString("waitingrequests");
                                        Log.i("FamilyG", "waitingrequests: " + user.waitingrequests);
                                        user.save();
                                        Intent i = new Intent(VerifyOTPActivity.this, TutorialActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        //if((user.displayname.equals(""))&&(user.displayname==null)&&(user.displayname.isEmpty())){
                                        if (!result.has("displayname")) {
                                            user.secret_key = result.getString("secret_key");
                                            user.mobilenumber = mobilenumber;
                                            Log.i("FamilyG", "onsuccess verify: " + user.mobilenumber);
                                            user.fid = result.getString("fid");
                                            user.save();
                                            Log.i("FamilyG", "Displayname is empty");
                                            Intent i = new Intent(VerifyOTPActivity.this, TutorialActivity.class);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            user.setDisplayname(result.getString("displayname"));
                                            user.setFirstname(result.getString("firstname"));
                                            user.setLastname(result.has("lastname") ? result.getString("lastname") : null);
                                            user.setGender(result.getString("gender"));
                                            user.setDoi(result.getString("doi"));
                                            user.setDob(result.getString("dob"));
                                            user.setProfileimageurl(result.getString("profileimage"));
                                            user.setFid(result.getString("fid"));
                                            user.setSecret_key(result.getString("secret_key"));
                                            user.setIsprofilecreated(true);

                                            user.save();
                                            if (!result.getString("displayname").equals("")) {
                                                weight_displayname = 11.12;
                                            }
                                            if (!result.getString("firstname").equals("")) {
                                                weight_firstname = 11.12;
                                            }
                                            if (!result.getString("lastname").equals("")) {
                                                weight_surname = 11.12;
                                            }
                                            if (!result.getString("gender").equals("")) {
                                                weight_gender = 11.12;
                                            }
                                            if (!result.getString("doi").equals("")) {
                                                weight_doi = 11.12;
                                            }
                                            if (!result.getString("dob").equals("")) {
                                                weight_dob = 11.12;
                                            }
                                            if (!result.getString("profileimage").equals("")) {
                                                weight_profileimage = 11.12;
                                            }
                                            double toatalweightage = weight_profileimage + weight_displayname + weight_gender + weight_firstname + weight_surname + weight_doi + weight_dob;
                                            int toatalweightageinint = (int) toatalweightage;
                                            user.setProfilecompleteness(String.valueOf(toatalweightageinint));
                                            user.save();
                                            Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());
                                            Intent intent = new Intent(VerifyOTPActivity.this, BottomBarActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccess(JSONArray array) {

                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                otp_err_msg.setError("");
                                otp_err_msg.setText(errorMessage);
                            }
                        });
                        api.execute();
                    }
                }
            }
        });


        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void resendOtp() {
        user = UserModel.getInstance();
        i = getIntent();

        String mobileNumber = i.getStringExtra("PhoneNumber");
        String countryCode = i.getStringExtra("CountryCode");
        String token = i.getStringExtra("Token");


        user.mobilenumber = mobileNumber;
        user.countrycode = countryCode;
        user.devicetoken = token;

        APIManager api = new APIManager("register", user, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {
                    res = result.getString("refcode");
                    resendOTP = result.getString("otp");
                    refcodeView.setText(res);
                    otp_err_msg.setText(resendOTP);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                otp_err_msg.setError("");
                otp_err_msg.setText(errorMessage);

            }
        });

        api.execute();
    }

    public void resendNewOtp() {
        ProfileModel profile = ProfileModel.getInstance();

        String newMobileNumber = i.getStringExtra("NewPhoneNumber");
        String countryCode = i.getStringExtra("CountryCode");
        String secretkey = i.getStringExtra("Secretkey");

        profile.countrycode = countryCode;
        profile.mobilenumber = newMobileNumber;
        profile.secret_key = secretkey;

        String profilemodel = profile.toJSON();
        byte[] profilebytearray = profilemodel.getBytes(Charset.forName("UTF-8"));

        APIManager api = new APIManager("changenumber", profilebytearray, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {
                    res = result.getString("refcode");
                    String resendNewOtp = result.getString("otp");
                    Log.i("Ref==", res);
                    refcodeView.setText(res);
                    otp_err_msg.setText(resendNewOtp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                otp_err_msg.setError("");
                otp_err_msg.setText(errorMessage);
            }
        }, APIManager.HTTP_METHOD.POST);

        api.execute();

    }

    public void verifyNewNumber() {

        i = getIntent();

        ProfileModel profile = ProfileModel.getInstance();
        newMobileNo = i.getStringExtra("NewPhoneNumber");
        countryCode = i.getStringExtra("CountryCode");
        newCode = i.getStringExtra("oldCountryCode");

        String secretkey = i.getStringExtra("Secretkey");


        Log.i("FamilyG: ", "new..: " + newMobileNo);
        user = UserModel.getInstance();


        user.countrycode = countryCode;
        user.secret_key = secretkey;
        user.mobilenumber = newMobileNo;
        Log.i("FamilyG", "mob:" + user.mobilenumber + "cc:" + user.countrycode + "sk:" + user.secret_key + "relmob:" + user.relmobilenumber + "newotp:" + user.otp);
        user.save();
        String userprofile = user.toJSON();
        byte[] userbytearray = userprofile.getBytes(Charset.forName("UTF-8"));
        APIManager api = new APIManager("verifynewnumber", userbytearray, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {
                    res = result.getString("refcode");
                    user.mobilenumber = newMobileNo;
                    Log.i("FamilyG: ", "New mobile number: " + user.mobilenumber);
                    refcodeView.setText(res);

                    user.save();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), "Number Changed", Toast.LENGTH_LONG).show();
                Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());
                Intent intent_activity = new Intent(VerifyOTPActivity.this, BottomBarActivity.class);
                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_activity);

                finish();
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                otp_err_msg.setError("");
                otp_err_msg.setText(errorMessage);

            }
        }, APIManager.HTTP_METHOD.POST);
        api.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("log", "onresume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("log", "onstart");
        i = getIntent();

        ProfileModel profileModel = ProfileModel.getInstance();
        UserModel user = UserModel.getInstance();
        type = i.getStringExtra("type");
        Log.i("FamilyG: ", "type==: " + type);
        if (type != null && type.equals("changenumber")) {
            newrefCode = i.getStringExtra("Referencecode");
            newotp = i.getStringExtra("OTP");
            String newMobileNo = i.getStringExtra("NewPhoneNumber");
            user.refcode = newrefCode;
            user.otp = newotp;
            user.mobilenumber = profileModel.getMobilenumber();
            user.save();
            refcodeView.setText(user.getRefcode());
            display_input_mobile_number.setText(newMobileNo);
            otp_err_msg.setText(user.otp);

        } else {
            refCode = i.getStringExtra("Refcode");
            receviedOtp = i.getStringExtra("OTP");
            mobilenumber = i.getStringExtra("PhoneNumber");
            user.mobilenumber = mobilenumber;
            user.refcode = refCode;
            refcodeView.setText(user.refcode);
            display_input_mobile_number.setText(user.mobilenumber);
            otp_err_msg.setText(receviedOtp);

            user.save();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        Log.d("log", "ondestroy");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        i = getIntent();
        type = i.getStringExtra("type");
        newMobileNo = i.getStringExtra("NewPhoneNumber");
        countryCode = i.getStringExtra("CountryCode");
        if (type != null && type.equals("changenumber")) {
            Intent intent = new Intent(VerifyOTPActivity.this, ChangeNumberActivity.class);
            intent.putExtra("type", "Edit");
            Log.i("FamilyG","edit phnumb: " +newMobileNo);
            Log.i("FamilyG","edit cc: "+countryCode);
            intent.putExtra("enteredNewNumber",newMobileNo);
            intent.putExtra("enteredNewCountryCode",countryCode);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(VerifyOTPActivity.this, RegistrationActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private class CustomWatcher implements TextWatcher {

        public EditText previous;
        public EditText current;

        public CustomWatcher(EditText current, EditText previous) {
            this.current = current;
            this.previous = previous;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (current.length() == 0)
                previous.requestFocus();
        }

    }
}
