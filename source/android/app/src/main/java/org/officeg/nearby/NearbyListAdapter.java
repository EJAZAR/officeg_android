package org.officeg.nearby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.officeg.feed.ImageLoader;
import org.officeg.mobileapp.ChatMessageActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class NearbyListAdapter extends BaseAdapter {
    ArrayList listvalues = new ArrayList();
    LayoutInflater inflater;
    Context context;
    UserModel user = null;

    public NearbyListAdapter(Context context, ArrayList listvalues) {
        this.listvalues = listvalues;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public NearbyModel getItem(int position) {

        return (NearbyModel) listvalues.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("FamilyG", "creating postview: ");
        NearbyModel nearbyListModel = (NearbyModel) listvalues.get(position);
        NearbyListAdapter.MyViewHolder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.nearbylist, parent, false);
            viewholder = new NearbyListAdapter.MyViewHolder(position, convertView, nearbyListModel);
            convertView.setTag(viewholder);
            if (user.fid.equals(nearbyListModel.getFid())) {
                Log.i("fid", "same");
                convertView = inflater.inflate(R.layout.fragment_empty_nearbyrow, parent, false);

            }
        } else {
            viewholder = (NearbyListAdapter.MyViewHolder) convertView.getTag();
        }

        //viewholder.setFeedModel(blockedListModel);
        return convertView;
    }


    private class MyViewHolder {

        TextView displayname, relationship_type;
        CircleImageView profileimage;
        TextView time;
        NearbyModel itemModel;
        ImageButton chat;


        public MyViewHolder(int position, View item, final NearbyModel itemModel) {
            user = UserModel.getInstance();
            this.itemModel = itemModel;
            displayname = (TextView) item.findViewById(R.id.nearbydisplayname);
            relationship_type = (TextView) item.findViewById(R.id.relationship_type);
            profileimage = (CircleImageView) item.findViewById(R.id.nearby_image_id);
            time = (TextView) item.findViewById(R.id.nearby_time);
            chat = (ImageButton) item.findViewById(R.id.chat_nearby);

            displayname.setText(itemModel.getDisplayname());
            time.setText(itemModel.getTime());
            //relationship_type.setText(itemModel.get);
            if (itemModel.getProfileimage() != null && !itemModel.getProfileimage().trim().isEmpty()) {
                ImageLoader imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(itemModel.getProfileimage(), profileimage);

            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }

            if (itemModel.isfav) {
                chat.setVisibility(View.VISIBLE);
                chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle extras = new Bundle();
                        extras.putString("fidOrGid", itemModel.getFid());
                        extras.putString("typeChat", "0");
                        extras.putString("type", "nearby");

                        Intent chatMessageIntent = new Intent(context, ChatMessageActivity.class);
                        chatMessageIntent.putExtras(extras);
                        context.startActivity(chatMessageIntent);

                    }
                });

                displayname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                            Bundle extras = new Bundle();
                            extras.putString("fidOrGid", itemModel.getFid());
                            extras.putString("typeChat", "0");
                            extras.putString("type", "nearby");

                            Intent chatMessageIntent = new Intent(context, ChatMessageActivity.class);
                            chatMessageIntent.putExtras(extras);
                            context.startActivity(chatMessageIntent);


                    }
                });

            } else {
                chat.setVisibility(View.GONE);
            }




        }
    }
}

