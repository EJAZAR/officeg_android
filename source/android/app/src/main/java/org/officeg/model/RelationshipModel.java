package org.officeg.model;

import org.officeg.profile.RelationshipType;

public class RelationshipModel {

    String fid;
    RelationshipType relationshipType;

    public RelationshipModel(String fid, RelationshipType relationshipType) {
        this.fid = fid;
        this.relationshipType = relationshipType;
    }


    public String getFid(){
        return fid;
    }

    public RelationshipType getRelationshipType() { return relationshipType;}
}
