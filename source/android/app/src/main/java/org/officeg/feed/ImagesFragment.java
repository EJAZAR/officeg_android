package org.officeg.feed;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImagesFragment extends Fragment {
    ImageView selectedImage;
    private String TAG = ImagesFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View currentView = inflater.inflate(R.layout.images_fragment, container, false);

        Bundle extras = this.getArguments();

        FeedItemModel feedItemModel = (FeedItemModel) extras.get("itemModel");

        Gallery gallery = (Gallery) currentView.findViewById(R.id.gallery);
        selectedImage = (ImageView) currentView.findViewById(R.id.imageView);
        gallery.setSpacing(20);
        final GalleryImageAdapter galleryImageAdapter = new GalleryImageAdapter(getContext());
        galleryImageAdapter.mImageIds = feedItemModel.getImagelist();
        gallery.setAdapter(galleryImageAdapter);

        if (galleryImageAdapter.mImageIds.size() > 0) {
            String fileUrl = galleryImageAdapter.mImageIds.get(0);
            showSelectedImage(fileUrl);
        } else {
            gallery.setVisibility(View.GONE);
            selectedImage.setVisibility(View.GONE);
            TextView empty_notification = (TextView) currentView.findViewById(R.id.empty_notification);
            empty_notification.setVisibility(View.VISIBLE);
        }
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // show the selected Image
                String fileUrl = galleryImageAdapter.mImageIds.get(position);
                showSelectedImage(fileUrl);
            }
        });

        return currentView;
    }

    private void showSelectedImage(String fileUrl) {
        try {
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, getContext());

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);

            final Uri uri = Utility.getUriFromFile(getContext(), fileCheck);
            final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 200, 200, getContext());
            final ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
            selectedImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
