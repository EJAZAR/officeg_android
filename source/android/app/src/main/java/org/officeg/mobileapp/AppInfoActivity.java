package org.officeg.mobileapp;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.officeg.api.NetworkConnectionreceiver;

public class AppInfoActivity extends AppCompatActivity {

    TextView versionNumber;
    String version;
    PackageInfo packageinfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        versionNumber = (TextView) findViewById(R.id.txtview_version);
        try {
            packageinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        version = packageinfo.versionName;
        versionNumber.setText("Version " + version);
    }


}
