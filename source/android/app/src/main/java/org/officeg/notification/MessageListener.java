package org.officeg.notification;


public interface MessageListener {

    void removeMessage(MessageModel messageModel);

    void removeBlock(Object messageModel);

}