package org.officeg.mobileapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.chat.ChatAdapter;
import org.officeg.chat.group.AddMembersActivity;
import org.officeg.database.DataManager;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.ChatModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 6/9/2017.
 */

public class ChatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ChatAdapter chatAdapter;
    LinkedList<ChatModel> chatModels;
    private String TAG = ChatFragment.class.getSimpleName();


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private ViewGroup container;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean getFavouriteListApiCalled, getGroupsApiCalled;
    BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                View currentView = getActivity().getLayoutInflater().inflate(R.layout.chat_favourite_list, container, false);
                afterGettingFavouritesAndGroups(currentView);
                BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                Utility.setChatAndFeedCount(bottomNavigation, getContext(), true, false);
            } catch (Exception e) {
                Log.e(TAG, "BroadcastReceiver = " + e.getMessage());
            }
        }
    };


    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factintory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAddViaProxy.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
//        testMethod();
        this.container = container;

        getActivity().setTitle("Chat");
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getContext());
        if (!NetworkConnectionreceiver.isConnection) {
            Toast.makeText(getContext(), getString(R.string.nointernet), Toast.LENGTH_LONG).show();
        }

        View chatFavouriteListView = listFavouritesAndGroups(inflater, container, NetworkConnectionreceiver.isConnection);

        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mMessageReceiver, new IntentFilter("CHATMESSAGE_RECEIVED"));

        return chatFavouriteListView;
    }


    private void setActionForFAB(final View currentView) {

        final FloatingActionButton fab = currentView.findViewById(R.id.fab_chat);
        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(getActivity(), fab, Gravity.TOP);
                menu.inflate(R.menu.chat_add);
                //noinspection RestrictedApi

                MenuPopupHelper menuHelper = new MenuPopupHelper(getContext(), (MenuBuilder) menu.getMenu(), fab);
                //noinspection RestrictedApi

                menuHelper.setForceShowIcon(true);

                MenuItem item_addmember = menu.getMenu().findItem(R.id.ic_add_member);
                if (UserModel.getInstance().isadmin) {
                    item_addmember.setVisible(true);
                } else {
                    item_addmember.setVisible(false);
                }


                //noinspection RestrictedApi
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.ic_add_group: {
                                try {
                                    Utility.logFA(mFirebaseAnalytics, "chat viewgroup screen", "create group", "flaoting action button");
                                    Intent intent_activity = new Intent(getContext(), AddMembersActivity.class);
                                    startActivity(intent_activity);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                            case R.id.ic_add_member: {
                                Utility.logFA(mFirebaseAnalytics, "homepage", "addmember icon", "button");
                                Intent intent_activity = new Intent(getContext(), AddMemberActivity.class);
                                startActivity(intent_activity);
                                return true;
                            }
                        }
                        return false;
                    }
                });
                if (Utility.profileModelMap.isEmpty()) {
                    menu.getMenu().removeItem(R.id.ic_add_group);
                }
                menuHelper.show();
            }
        });

    }

    private View listFavouritesAndGroups(final LayoutInflater inflater, final ViewGroup container, boolean isConnection) {
        Log.d(TAG, "listFavouritesAndGroups1");
        final View currentView = inflater.inflate(R.layout.chat_favourite_list, container, false);
        chatAdapter = new ChatAdapter(getActivity());
        chatModels = new LinkedList<ChatModel>();
        final ListView listView = currentView.findViewById(R.id.chatfavouritelistview);
        listView.setAdapter(chatAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                ChatModel chatModel = chatModels.get(position);
                Bundle extras = new Bundle();
                if (chatModel.getType().equalsIgnoreCase(getString(R.string.favourite))) {
                    String profileModel = chatModel.getProfileModel().toJSON();
                    extras.putString("fidOrGid", chatModel.getProfileModel().getFid());
                    extras.putString("typeChat", "0");
                    extras.putString("profileModel", profileModel);
                    extras.putInt("unreadcount", chatModel.getUnreadCount());
                } else if (chatModel.getType().equalsIgnoreCase(getString(R.string.group))) {
                    GroupModel groupModel = Utility.groupModelMap.get(chatModel.getGroupModel().getGid());
                    String groupModelString = groupModel.toJSON();
                    extras.putString("fidOrGid", chatModel.getGroupModel().getGid());
                    extras.putString("typeChat", "1");
                    extras.putString("groupModel", groupModelString);
                    extras.putInt("unreadcount", chatModel.getUnreadCount());
                }
                Intent chatMessageIntent = new Intent(getActivity(), ChatMessageActivity.class);
                chatMessageIntent.putExtras(extras);
                startActivity(chatMessageIntent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
//                Toast.makeText(getContext(), "Long click for options", Toast.LENGTH_SHORT).show();

                String[] options = {getString(R.string.view_profile), getString(R.string.view_tree)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                builder.setTitle("Pick a color");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Object object = listView.getItemAtPosition(position);
                        Log.d(TAG, object.toString());
                        ChatModel chatModel = chatModels.get(position);
                        if (which == 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(chatModel.getProfileModel().toJSON());
                                Utility.showProfileDialog(jsonObject, getActivity());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (which == 1) {
                            Intent chatMessageIntent = new Intent(getActivity(), TreeActivity.class);
                            chatMessageIntent.putExtra("profileModel", chatModel.getProfileModel().toJSON());
                            startActivity(chatMessageIntent);
                        }
                    }
                });
                builder.show();

                return true;
            }
        });

        if (isConnection) {
            APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", UserModel.getInstance(), new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d("ChatFragment", "inside onsuccess");
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "Utility.profileModelMap reload");
                    try {
                        getFavouriteListApiCalled = true;
                        Utility.profileModelMap = new HashMap<String, ProfileModel>();
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject object = array.getJSONObject(k);
                            Log.d(TAG, "resultString  =" + object.toString());
                            ProfileModel profileModel = new ProfileModel();
                            profileModel = profileModel.toObject(object.toString());
                            if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                String profileModelKey = profileModel.getFid();
                                Utility.profileModelMap.put(profileModelKey, profileModel);
                            }
                        }

                        afterGettingFavouritesAndGroups(currentView);
                        DataManager dataManager = DataManager.getInstance(getContext());
                        dataManager.insertChatFavouritesAndGroups(Utility.profileModelMap, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        ((BottomBarActivity) getActivity()).hideProgrssDialog();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.d("ChatFragment", errorMessage);
                    Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    ((BottomBarActivity) getActivity()).hideProgrssDialog();
                }
            });
            getFavouriteListApi.execute();

            UserModel userModel = UserModel.getInstance();
            byte[] userModelBA = userModel.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager getGroupsApi = new APIManager("chat/groups", userModelBA, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "inside onsuccess" + result.toString());
                }

                @Override
                public void onSuccess(JSONArray array) {
                    try {
                        getGroupsApiCalled = true;
                        Log.d(TAG, "Utility.groupModelMap reload");
                        Utility.groupModelMap = new HashMap<String, GroupModel>();

                        for (int k = 0; k < array.length(); k++) {
                            JSONObject singleGroup = array.getJSONObject(k);
                            Log.d(TAG, "getGroupsApi  =" + singleGroup.toString());
                            GroupModel groupModel = new GroupModel();
                            groupModel.setName(singleGroup.getString("name"));
                            groupModel.setGid(singleGroup.getString("gid"));

                            if (singleGroup.has("url"))
                                groupModel.setUrl(singleGroup.getString("url"));
                            else
                                groupModel.setUrl("");

                            JSONArray membersJsonArray = singleGroup.getJSONArray("members");
                            String[] membersArray = Utility.getStringArray(membersJsonArray);
                            groupModel.setMembers(membersArray);

                            JSONArray adminsJsonArray = singleGroup.getJSONArray("admins");
                            String[] adminsArray = Utility.getStringArray(adminsJsonArray);
                            groupModel.setAdmins(adminsArray);

                            Utility.groupModelMap.put(groupModel.getGid(), groupModel);
                        }
                        afterGettingFavouritesAndGroups(currentView);
                        DataManager dataManager = DataManager.getInstance(getContext());
                        dataManager.insertChatFavouritesAndGroups(null, Utility.groupModelMap);
                        ((BottomBarActivity) getActivity()).hideProgrssDialog();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                        ((BottomBarActivity) getActivity()).hideProgrssDialog();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.d(TAG, errorMessage);
                    Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    ((BottomBarActivity) getActivity()).hideProgrssDialog();
                }

            }, APIManager.HTTP_METHOD.POST);
            getGroupsApi.execute();
        } else {
            getFavouriteListApiCalled = true;
            getGroupsApiCalled = true;
            if (Utility.profileModelMap.isEmpty()) {
                DataManager dataManager = DataManager.getInstance(getContext());
                Utility.profileModelMap = dataManager.getFavourites();
            }
            if (Utility.groupModelMap.isEmpty()) {
                DataManager dataManager = DataManager.getInstance(getContext());
                Utility.groupModelMap = dataManager.getGroups();
            }
            afterGettingFavouritesAndGroups(currentView);
            ((BottomBarActivity) getActivity()).hideProgrssDialog();
        }
        setActionForFAB(currentView);
        return currentView;
    }

    private void afterGettingFavouritesAndGroups(View currentView) {
        Log.d(TAG, "listFavouritesAndGroups2" + getFavouriteListApiCalled + " " + getGroupsApiCalled);
        try {
            Map<String, ProfileModel> tempProfileModelMap = new HashMap<String, ProfileModel>(Utility.profileModelMap);
            Map<String, GroupModel> tempGroupModelMap = new HashMap<String, GroupModel>(Utility.groupModelMap);

            DataManager dataManager = DataManager.getInstance(getActivity().getApplicationContext());
            List<ChatMessageModel> chatMessageModels = dataManager.getLastMessageEvents();
            chatModels = new LinkedList<ChatModel>();
            chatAdapter.clearAll();
            chatAdapter.notifyDataSetChanged();
            for (ChatMessageModel chatMessageModel : chatMessageModels) {
                if (Utility.profileModelMap.containsKey(chatMessageModel.getFid())) {
                    tempProfileModelMap.remove(chatMessageModel.getFid());
                    ProfileModel profileModel = Utility.profileModelMap.get(chatMessageModel.getFid());
                    ChatModel chatModel = new ChatModel();
                    chatModel.setProfileModel(profileModel);
                    chatModel.setType(getString(R.string.favourite));
                    chatModel.setUnreadCount(chatMessageModel.getUnreadCount());
                    chatModel.setMsg(chatMessageModel.getTotalContent());
                    chatModels.add(chatModel);
                    chatAdapter.add(chatModel);
                    chatAdapter.notifyDataSetChanged();
                } else if (Utility.groupModelMap.containsKey(chatMessageModel.getFid())) {
                    tempGroupModelMap.remove(chatMessageModel.getFid());
                    GroupModel groupModel = Utility.groupModelMap.get(chatMessageModel.getFid());
                    ChatModel chatModel = new ChatModel();
                    chatModel.setGroupModel(groupModel);
                    chatModel.setType(getString(R.string.group));
                    chatModel.setUnreadCount(chatMessageModel.getUnreadCount());
                    chatModel.setMsg(chatMessageModel.getTotalContent());
                    chatModels.add(chatModel);
                    chatAdapter.add(chatModel);
                    chatAdapter.notifyDataSetChanged();
                }
            }
            if (getFavouriteListApiCalled && !getGroupsApiCalled) {
                loadFavourites(tempProfileModelMap);
            }
            if (!getFavouriteListApiCalled && getGroupsApiCalled) {
                loadGroups(tempGroupModelMap);
            }
            if (getFavouriteListApiCalled && getGroupsApiCalled) {
                loadFavourites(tempProfileModelMap);
                loadGroups(tempGroupModelMap);
            }
            if (chatModels.size() == 0) {
                ListView chatfavouritelistview = currentView.findViewById(R.id.chatfavouritelistview);
                chatfavouritelistview.setVisibility(View.GONE);
                LinearLayout textview_nofavourites = currentView.findViewById(R.id.textview_nofavourites);
                textview_nofavourites.setVisibility(View.VISIBLE);
            } else {
                ListView chatfavouritelistview = currentView.findViewById(R.id.chatfavouritelistview);
                chatfavouritelistview.setVisibility(View.VISIBLE);
                LinearLayout textview_nofavourites = currentView.findViewById(R.id.textview_nofavourites);
                textview_nofavourites.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadGroups(Map<String, GroupModel> tempGroupModelMap) {
        for (Map.Entry<String, GroupModel> groupModelEntry : tempGroupModelMap.entrySet()) {
            GroupModel groupModel = groupModelEntry.getValue();
            ChatModel chatModel = new ChatModel();
            chatModel.setGroupModel(groupModel);
            chatModel.setType(getString(R.string.group));
            chatModel.setUnreadCount(0);
            chatModels.add(chatModel);
            chatAdapter.add(chatModel);
            chatAdapter.notifyDataSetChanged();
        }
    }

    private void loadFavourites(Map<String, ProfileModel> tempProfileModelMap) {
        for (Map.Entry<String, ProfileModel> profileModelEntry : tempProfileModelMap.entrySet()) {
            ProfileModel profileModel = profileModelEntry.getValue();
            ChatModel chatModel = new ChatModel();
            chatModel.setProfileModel(profileModel);
            chatModel.setType(getString(R.string.favourite));
            chatModel.setUnreadCount(0);
            chatModels.add(chatModel);
            chatAdapter.add(chatModel);
            chatAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    private void testMethod() {
        try {
            DataManager dataManager = DataManager.getInstance(getActivity().getApplicationContext());
//            Date date = new Date();
//            date.setDate(date.getDate() - 3);
//            dataManager.persistChatHistory("{content:\"My first message\",images:[],audios:[],videos:[],fid:\"1869c2\",extraType:\"text\",extraString:\"\"}", "1869c2", date);
//            date.setDate(date.getDate() - 5);
//            dataManager.persistChatHistory("{content:\"My second message\",images:[],audios:[],videos:[],fid:\"b46182\",extraType:\"text\",extraString:\"\"}", "1869c2", date);
//            date.setDate(date.getDate() - 7);
//            dataManager.persistChatHistory("{content:\"My third message\",images:[],audios:[],videos:[],fid:\"1869c2\",extraType:\"text\",extraString:\"\"}", "1869c2", date);
            dataManager.getAllMessageEvents();
        } catch (Exception e) {
            Log.e("ChatFragment", "testMethod e =" + e.getMessage());
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        if (getView() == null) {
            return;
        }
        if (getArguments() != null) {
            String screen = getArguments().containsKey("screen") ? getArguments().getString("screen") : null;
            if (screen != null) {
                Log.d(TAG, screen);
            }
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(a);
                    return true;
                }
                return false;
            }
        });

        View currentView = getActivity().getLayoutInflater().inflate(R.layout.chat_favourite_list, container, false);
        afterGettingFavouritesAndGroups(currentView);
        BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
        Utility.setChatAndFeedCount(bottomNavigation, getContext(), true, false);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        super.onPause();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_feed_id);
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}