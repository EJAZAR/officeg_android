package org.officeg.mobileapp;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.notification.MessageAdapter;
import org.officeg.notification.MessageModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class NotificationActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    ArrayList<MessageModel> listvalues = new ArrayList<>();
    MessageModel itemsmodel;
    SwipeMenuListView notificationlist;
    LinearLayout emptylist;
    UserModel user = null;
    int id;
    String type, title;
    ProfileModel profileModel;
    private FirebaseAnalytics mFirebaseAnalytics;
    public static boolean isActive = false;
    MessageAdapter adapter;
    BroadcastReceiver mMessageReceiver, mMessageReceiver1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_notifications);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        user = UserModel.getInstance();
        ProgressDialog PD = ProgressDialog.show(NotificationActivity.this, "Please Wait", "Please Wait ...", true);

        emptylist = findViewById(R.id.empty_notification);
        notificationlist = findViewById(R.id.list_notification);
        notificationlist.invalidateViews();

        Intent i = getIntent();    //from personal profile activity for first time users
        type = i.getStringExtra("waitingrequesttype");

        profileModel = new ProfileModel();
        profileModel.secret_key = UserModel.getInstance().secret_key;

        if (type != null && type.equals("1")) {   //for first time users(already invited before register)...
            profileModel.type = type;
            marque.setText("Pending requests");
        }

        mMessageReceiver1 = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("FamilyG", "received message");
                Log.i("FamilyG", "empty message " + intent.getIntExtra("emptymessage", 0));

                int listsize = intent.getIntExtra("emptymessage", 0);
                if (listsize == 0) {
                    emptylist.setVisibility(View.VISIBLE);
                }
            }
        };
        LocalBroadcastManager.getInstance(NotificationActivity.this)
                .registerReceiver(mMessageReceiver1, new IntentFilter("EMPTY_MESSAGE"));

        adapter = new MessageAdapter(NotificationActivity.this, listvalues);
        notificationlist.setAdapter(adapter);
        updatelistview(PD);
        DataManager dataManager = DataManager.getInstance(getApplicationContext());
        dataManager.deleteTable("unreadnotification", "fidOrGid", "relationship");

        mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("FamilyG", "received message");
                JSONObject post = null;
                try {
                    post = new JSONObject(intent.getBundleExtra("messageforrequest").getString("msg"));
                    MessageModel postmodel = new MessageModel(post);
                    listvalues.add(0, postmodel);
                    Log.i("FamilyG", "LISTVALUES: " + listvalues.toString());
                    if (listvalues != null) {
                        adapter.updateList(listvalues);
                        emptylist.setVisibility(View.GONE);
                        Log.i("FamilyG", intent.getBundleExtra("messageforrequest").getString("msg"));
                        //((BaseAdapter)feedlist.getAdapter()).notifyDataSetChanged();
                    }
                    if (listvalues == null && listvalues.isEmpty()) {
                        emptylist.setVisibility(View.VISIBLE);
                        //adapter.updateList(listvalues);
                        Log.i("FamilyG", "listis empty");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        LocalBroadcastManager.getInstance(NotificationActivity.this)
                .registerReceiver(mMessageReceiver, new IntentFilter("MESSAGE_RECEIVED"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Clear All").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 1) {
            clearAllNotifications();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updatelistview(final ProgressDialog PD) {

        APIManager api = new APIManager("getnotifications", profileModel, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {

            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.i("check", String.valueOf(array));
                try {
                    if (array.length() == 0) {
                        Toolbar toolbar = findViewById(R.id.toolbartop);
                        toolbar.getMenu().findItem(1).setVisible(false);
                    } else {
                        for (int index = 0; index < array.length(); index++) {
                            JSONObject notificationObject = array.getJSONObject(index);
                            itemsmodel = new MessageModel(notificationObject);
                            listvalues.add(itemsmodel);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (listvalues != null) {

                    adapter = new MessageAdapter(NotificationActivity.this, listvalues);
                    notificationlist.setAdapter(adapter);
                  /*  SharedPreferences bb = getSharedPreferences("max_id", 0);//getting maximum id for displaying read and unread messages
                    if (bb.getInt("maxid", 0) != 0) {
                        int count = bb.getInt("maxid", 0);
                        id = count;
                        Log.i("FamilyG", "Max_id is " + id);
                        user.setMaximum_id(id);
                    }*/

                    Log.i("listnotnull", String.valueOf(notificationlist.getAdapter().getCount()));

                    SwipeMenuCreator creator = new SwipeMenuCreator() {

                        @Override
                        public void create(SwipeMenu menu) {
                            //create an action that will be showed on swiping an item in the list
                            SwipeMenuItem item1 = new SwipeMenuItem(
                                    getApplicationContext());
                            item1.setBackground(new ColorDrawable(Color.DKGRAY));
                            // set width of an option (px)
                            item1.setWidth(200);
                            item1.setTitle("Action 1");
                            item1.setTitleSize(18);
                            item1.setTitleColor(Color.WHITE);
                            menu.addMenuItem(item1);

                            SwipeMenuItem item2 = new SwipeMenuItem(
                                    getApplicationContext());
                            // set item background
                            item2.setBackground(new ColorDrawable(Color.RED));
                            item2.setWidth(200);
                            item2.setTitle("Action 2");
                            item2.setTitleSize(18);
                            item2.setTitleColor(Color.WHITE);
                            menu.addMenuItem(item2);
                        }
                    };
                    //set MenuCreator
                    notificationlist.setMenuCreator(creator);


             /*       // set SwipeListener
                    notificationlist.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

                        @Override
                        public void onSwipeStart(int position) {
                           Log.i("FamilyG","onSwipeStart");
                        }

                        @Override
                        public void onSwipeEnd(int position) {
                            Log.i("FamilyG","onSwipeEnd");
                        }
                    });

                    notificationlist.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                            MessageModel value = adapter.getItem(position);
                            switch (index) {
                                case 0:
                                    Toast.makeText(getApplicationContext(), "Action 1 for "+ value , Toast.LENGTH_SHORT).show();
                                    break;
                                case 1:
                                    Toast.makeText(getApplicationContext(), "Action 2 for "+ value , Toast.LENGTH_SHORT).show();
                                    break;
                            }
                            return false;
                        }});
*/

                    //adapter.notifyDataSetChanged();
                }
                if (listvalues.isEmpty()) {
                    emptylist.setVisibility(View.VISIBLE);
                    Log.i("list", "no data");
                }
                PD.dismiss();
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                PD.dismiss();
            }
        });

        api.execute();
    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0) {
//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if (locationAccepted) {
//                    } else {
//                        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//                            showMessageOKCancel("You need to allow access to both the permissions",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
//                                                    PERMISSION_REQUEST_CODE);
//                                        }
//                                    });
//                            return;
//                        }
//
//                    }
//                }
//                break;
//        }
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(NotificationActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent(NotificationActivity.this, BottomBarActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);

    }

    @Override
    public void onResume() {
        super.onResume();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;

    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false;

    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(NotificationActivity.this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(NotificationActivity.this).unregisterReceiver(mMessageReceiver1);

        super.onDestroy();
        isActive = false;

    }

    public void clearAllNotifications() {
        ProfileModel profileModel = new ProfileModel();

        profileModel.secret_key = UserModel.getInstance().secret_key;
        profileModel.type = "all";

        APIManager api = new APIManager("notifications/delete", profileModel, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                Log.i("FamilyG", "delete inside onsuccess");
                Toast.makeText(getApplicationContext(), getString(R.string.notifications_cleared), Toast.LENGTH_LONG).show();
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }

            @Override
            public void onSuccess(JSONArray object) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
            }
        });

        api.execute();
    }
}