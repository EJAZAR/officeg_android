package org.officeg.notification;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.feed.ImageLoader;
import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.profile.RelationshipType;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends BaseAdapter implements MessageListener {

    public int id;
    ArrayList messageList = new ArrayList();
    LayoutInflater inflater;
    Context context;
    ProfileModel profileModel = null;
    String senderid;
    UserModel user = null;
    //ArrayList listvalues = new ArrayList();
    private FirebaseAnalytics mFirebaseAnalytics;
    private String TAG = MessageAdapter.class.getSimpleName();


    public MessageAdapter(Context context, ArrayList messageList) {
        this.messageList = messageList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public MessageModel getItem(int position) {
        Log.i("FamilyG", "getitem : " + position);
        return (MessageModel) messageList.get(position);
    }

    public void updateList(ArrayList messageList) {
        Log.i("FamilyG", "updating the message list");
        this.messageList = messageList;
        this.notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position)

    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageModel messageModel = (MessageModel) messageList.get(position);

        Log.i("FamilyG", "getview : " + position);

        final MessageAdapter.MyViewHolder viewholder;
        if (convertView == null)
            convertView = inflater.inflate(R.layout.notification_list_item, parent, false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        viewholder = new MessageAdapter.MyViewHolder(convertView, messageModel);
        viewholder.addListener(this);
        //convertView.setTag(viewholder);
        Log.i("FamilyG", "getviewnew : " + position);

        return convertView;
    }

    @Override
    public void removeMessage(MessageModel messageModel) {

        messageList.remove(messageModel);
        notifyDataSetChanged();
    }

    @Override
    public void removeBlock(Object messageModel) {

    }

    private class MyViewHolder {
        TextView notificationmessage, time, view_profile;
        CircleImageView profileimage;
        ImageButton btnaccept, btnreject, btnconnect, btnignore, btnmerge;
        MessageModel messageModel;
        ImageLoader imageLoader;
        SwipeLayout linearforreject;
        ImageView iv;
        View item_label;


        private List<MessageListener> listeners = new ArrayList<MessageListener>();

        public MyViewHolder(final View item, final MessageModel messageModel) {
            user = UserModel.getInstance();

            this.messageModel = messageModel;

            notificationmessage = item.findViewById(R.id.title);
            time = item.findViewById(R.id.active_days);
            view_profile = item.findViewById(R.id.view_profile);
            profileimage = item.findViewById(R.id.profile_image);
            btnaccept = item.findViewById(R.id.btn_accept);
            btnreject = item.findViewById(R.id.btn_reject);
            btnconnect = item.findViewById(R.id.btn_connect);
            btnignore = item.findViewById(R.id.btn_ignore);
            btnmerge = item.findViewById(R.id.btn_merge);
            // unread = (TextView) item.findViewById(R.id.unread);
            linearforreject = item.findViewById(R.id.linearforreject);
            iv = item.findViewById(R.id.image_view);

            // leftswipeignoreandreject();

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("FamilyG", "iv clicked ");

                    ignoreAPI();

                }
            });

            /*String id = messageModel.getId();
            msg_idd = Integer.parseInt(id);
            Log.i("msg_idd ", "" + msg_idd);*/
            Log.i("FamilyG", "senderid " + senderid);
            // type = 1 mean request for accepting relationship
            // type = 2 mean information message
            // type = 3 mean request for accepting favourite
            // type = 5 mean suggestion for connecting relationship
            Log.i("FamilyG", "notificationType " + messageModel.getType());
            if (messageModel.getType().equalsIgnoreCase("2")) {
                profileimage.setVisibility(View.GONE);
            } else {
                profileimage.setVisibility(View.VISIBLE);
            }
            if ((!messageModel.getType().equalsIgnoreCase("1")) && (!messageModel.getType().equalsIgnoreCase("3"))) {
                btnaccept.setVisibility(View.GONE);
                btnreject.setVisibility(View.GONE);
                view_profile.setVisibility(View.GONE);
            } else {
                btnaccept.setVisibility(View.VISIBLE);
                btnreject.setVisibility(View.VISIBLE);
                view_profile.setVisibility(View.VISIBLE);
            }

            if (messageModel.getType().equalsIgnoreCase("5")) {
                btnconnect.setVisibility(View.VISIBLE);
                btnignore.setVisibility(View.VISIBLE);
                // view_profile.setVisibility(View.VISIBLE);

                btnconnect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //unread.setVisibility(View.GONE);

                        Utility.logFA(mFirebaseAnalytics, "notification screen", "accept", "button");
                        Log.i("FamilyG", messageModel.getSenderid());
                        senderid = messageModel.getSenderid();
                        profileModel = new ProfileModel();
                        profileModel.fid = senderid;
                        profileModel.secret_key = UserModel.getInstance().secret_key;
                        profileModel.type = messageModel.getType();
                        profileModel.notification_id = messageModel.getId();

                        Log.i("FamilyG", "senderid" + profileModel.fid);
                        Log.i("FamilyG", "sec key" + profileModel.secret_key);
                        APIManager api = new APIManager("relationship/approve", profileModel, new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {
                                raiseRemoveEvent();
                            }

                            @Override
                            public void onSuccess(JSONArray object) {

                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                raiseRemoveEvent();
                                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                            }
                        });

                        api.execute();

                    }
                });

                // leftswipeignoreandreject();

                btnignore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ignoreAPI();


                    }
                });

            } else if (messageModel.getType().equalsIgnoreCase("6")) {
                btnmerge.setVisibility(View.VISIBLE);
                btnignore.setVisibility(View.VISIBLE);
                // view_profile.setVisibility(View.VISIBLE);

                btnmerge.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utility.logFA(mFirebaseAnalytics, "notification screen", "merge", "button");

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("secret_key", UserModel.getInstance().secret_key);
                            jsonObject.put("mergefid", messageModel.getMergefid());
                            jsonObject.put("anotherfid", messageModel.getAnotherfid());
                            jsonObject.put("mode", "accept");
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }

                        byte[] mergeJson = jsonObject.toString().getBytes(Charset.forName("UTF-8"));
                        APIManager api = new APIManager("relationship/merge", mergeJson, new APIListener() {

                            @Override
                            public void onSuccess(JSONObject result) {
                                try {
                                    UserModel.getInstance().loadMembers();
                                    Log.d(TAG, result.toString());
                                    raiseRemoveEvent();
                                    Toast.makeText(context, "Merged successfully", Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    Log.e(TAG, e.getMessage());
                                }
                            }

                            @Override
                            public void onSuccess(JSONArray array) {
                            }

                            @Override
                            public void onFailure(String errorMessage, String errorCode) {
                                Log.e(TAG, errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                            }
                        }, APIManager.HTTP_METHOD.POST);
                        api.execute();

                    }
                });

                btnignore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ignoreAPI();
                    }
                });

            } else {
                btnconnect.setVisibility(View.GONE);
                btnignore.setVisibility(View.GONE);
                btnmerge.setVisibility(View.GONE);
            }

            notificationmessage.setText(messageModel.getNotificationmessage());
            time.setText(messageModel.getTime());
            if (messageModel.getProfileimage() != null && !messageModel.getProfileimage().trim().isEmpty()) {
                ImageLoader imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(messageModel.getProfileimage(), profileimage);
            } else {
                profileimage.setImageResource(R.drawable.default_profile_image);
            }

            view_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    senderid = messageModel.getSenderid();
                    Log.i("FamilyG sender id is ", messageModel.getSenderid());
                    profileModel = new ProfileModel();
                    profileModel.fid = senderid;
                    profileModel.secret_key = UserModel.getInstance().secret_key;
                    profileModel.notification_id = messageModel.getId();

                    APIManager viewProfile = new APIManager("relationship/viewProfile", profileModel, new APIListener() {
                        @Override
                        public void onSuccess(JSONObject object) {
                            Log.i("FamilyG", "view profile " + object);
                            final Dialog dialog = new Dialog(context);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.view_profile);
                            CircleImageView imageview = dialog.findViewById(R.id.profileview_image_id);
                            TextView displayname = dialog.findViewById(R.id.dispaly_name);
                            TextView firstname = dialog.findViewById(R.id.first_name);
                            TextView surname = dialog.findViewById(R.id.surname);
                            TextView gender = dialog.findViewById(R.id.gender);
                            TextView dob = dialog.findViewById(R.id.birth);
                            TextView mobile = dialog.findViewById(R.id.mobile);
                            Button ok = dialog.findViewById(R.id.dialog_ok);
                            TextView relationpath = dialog.findViewById(R.id.relationship);
                            LinearLayout linear_relationship = dialog.findViewById(R.id.linear_relationship);


                            try {
                                if ((object.getString("profileimage") != null) && (!object.getString("profileimage").isEmpty())) {
                                    ImageLoader imageLoader = new ImageLoader(context);
                                    imageLoader.DisplayImage(object.getString("profileimage"), imageview);
                                } else {

                                    profileimage.setImageResource(R.drawable.default_profile_image);

                                }
                                if ((object.getString("displayname") != null) && (!object.getString("displayname").isEmpty())) {
                                    displayname.setText(object.getString("displayname"));
                                } else {
                                    displayname.setText("N/A");
                                }

                                if ((object.getString("firstname") != null) && (!object.getString("firstname").isEmpty())) {
                                    firstname.setText(object.getString("firstname"));
                                } else {
                                    firstname.setText("N/A");
                                }
                                if ((object.getString("lastname") != null) && (!object.getString("lastname").isEmpty())) {
                                    surname.setText(object.getString("lastname"));
                                } else {
                                    surname.setText("N/A");
                                }
                                if ((object.getString("gender") != null) && (!object.getString("gender").isEmpty())) {
                                    gender.setText(object.getString("gender"));
                                } else {
                                    gender.setText("N/A");
                                }
                                if ((object.getString("dob") != null) && (!object.getString("dob").isEmpty())) {
                                    dob.setText(object.getString("dob"));
                                } else {
                                    dob.setText("N/A");
                                }
                                if ((object.getString("mobilenumber") != null) && (!object.getString("mobilenumber").isEmpty())) {
                                    mobile.setText(object.getString("mobilenumber"));
                                } else {
                                    mobile.setText("N/A");
                                }

                                if ((messageModel.getRelationpath() != null) && (!messageModel.getRelationpath().isEmpty())) {
                                    Log.i("FamilyG", "if" + messageModel.getRelationpath());

                                    relationpath.setText(messageModel.getRelationpath());
                                } else {
                                    Log.i("FamilyG", "else" + messageModel.getRelationpath());
                                    // relationpath.setText("N/A");
                                    linear_relationship.setVisibility(View.GONE);
                                }


                                /*firstname.setText(object.getString("firstname"));
                                surname.setText(object.getString("surname"));
                                gender.setText(object.getString("gender"));
                                dob.setText(object.getString("dob"));
                                mobile.setText(object.getString("mobilenumber"));*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.show();
                            Window window = dialog.getWindow();
                            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });

                        }

                        @Override
                        public void onSuccess(JSONArray array) {

                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            Log.d("FamilyG", "Errormessage " + errorMessage);
                        }
                    });
                    viewProfile.execute();

                }
            });


            btnaccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //unread.setVisibility(View.GONE);
                    if (messageModel.getRelation() != null && messageModel.getRelation().equalsIgnoreCase("RELATION")) {
                        final List<String> relationsList = new ArrayList<String>();

                        for (int k = 0; k < RelationshipType.length(); k++) {
                            RelationshipType relationship = RelationshipType.values()[k];
                            relationsList.add(String.valueOf(relationship));
                        }
                        CharSequence relationsArray[] = new CharSequence[relationsList.size()];
                        relationsList.toArray(relationsArray);

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Choose a relationship");
                        builder.setItems(relationsArray, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                // the user clicked on colors[which]
                                approveNotification(relationsList.get(i));

                            }
                        });
                        builder.show();
                    } else {
                        approveNotification(null);
                    }
                }
            });

            //leftswipeignoreandreject();

            btnreject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // unread.setVisibility(View.GONE);
                    rejectAPI();
                }
            });
        }

        private void approveNotification(String relationship) {
                           /* if(NotificationActivity.notificationJson != null) {
                    // itreate through JSONArray
                    // for each object check obj[index].senderId == user.fid {
                        // remove object from notificationJson array
                    //}
                }*/
            //  unread.setVisibility(View.GONE);

            Utility.logFA(mFirebaseAnalytics, "notification screen", "accept", "button");
            Log.i("FamilyG", messageModel.getSenderid());
            senderid = messageModel.getSenderid();
            profileModel = new ProfileModel();
            profileModel.fid = senderid;
            profileModel.secret_key = UserModel.getInstance().secret_key;
            profileModel.type = messageModel.getType();
            if (relationship != null) {
                profileModel.relationship = relationship.toUpperCase();
            }
            profileModel.notification_id = messageModel.getId();

            Log.i("FamilyG", "senderid" + profileModel.fid);
            Log.i("FamilyG", "sec key" + profileModel.secret_key);
            APIManager api = new APIManager("relationship/approve", profileModel, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    raiseRemoveEvent();
                    Utility.updateFavouritesList(context);
                }

                @Override
                public void onSuccess(JSONArray object) {
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    raiseRemoveEvent();
                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();


                }
            });

            api.execute();

        }

        public void addListener(MessageListener toAdd) {
            listeners.add(toAdd);
        }

        public void raiseRemoveEvent() {
            Log.i("FamilyG", "delete ");

            //animation effect while reject and ignore..
            TranslateAnimation animate = new TranslateAnimation(0, -linearforreject.getWidth(), 0, 0);
            animate.setDuration(500);
            animate.setFillAfter(false);
            linearforreject.startAnimation(animate);

            // Notify everybody that may be interested.
            for (MessageListener hl : listeners)
                hl.removeMessage(messageModel);
        }

        public void leftswipeignoreandreject() {

            linearforreject.setOnTouchListener(new OnSwipeTouchListener(context) {


                public void onSwipeLeft() {
                    ignoreAPI();
                }


                public boolean onTouch(View v, MotionEvent event) {
                    return gestureDetector.onTouchEvent(event);
                }

            });

        }

        public void rejectAPI() {
            profileModel = new ProfileModel();
            Utility.logFA(mFirebaseAnalytics, "notification screen", "accept", "button");
            Log.i("FamilyG", messageModel.getSenderid());
            senderid = messageModel.getSenderid();
            profileModel = new ProfileModel();
            profileModel.fid = senderid;
            profileModel.secret_key = UserModel.getInstance().secret_key;
            Log.i("FamilyG", "senderid" + profileModel.fid);
            Log.i("FamilyG", "sec key" + profileModel.secret_key);
            profileModel.type = messageModel.getType();
            profileModel.notification_id = messageModel.getId();

            APIManager api = new APIManager("relationship/reject", profileModel, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {

                    raiseRemoveEvent();
                    Log.i("FamilyG", "list size is: " + messageList.size());
                    Intent broadcastintent = new Intent("EMPTY_MESSAGE");
                    broadcastintent.putExtra("emptymessage", messageList.size());
                    LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastintent);

                }

                @Override
                public void onSuccess(JSONArray object) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    raiseRemoveEvent();
                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                }
            });

            api.execute();
        }

        public void ignoreAPI() {
            ProfileModel profileModel = new ProfileModel();
            profileModel.notification_id = messageModel.getId();
            profileModel.type = "single";

            profileModel.secret_key = UserModel.getInstance().secret_key;

            APIManager api = new APIManager("notifications/delete", profileModel, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    Log.i("FamilyG", "delete inside onsuccess");

                    raiseRemoveEvent();

                    Log.i("FamilyG", "list size is: " + messageList.size());
                    Intent broadcastintent = new Intent("EMPTY_MESSAGE");
                    broadcastintent.putExtra("emptymessage", messageList.size());
                    LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastintent);

                }

                @Override
                public void onSuccess(JSONArray object) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    raiseRemoveEvent();
                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();


                }
            });

            api.execute();
        }

    }
}
