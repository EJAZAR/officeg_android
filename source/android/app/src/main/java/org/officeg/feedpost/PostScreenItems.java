package org.officeg.feedpost;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.officeg.mobileapp.R;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class PostScreenItems extends BaseAdapter {

    public  Bitmap bitmap;
    public static byte[] byteArray;
    public ArrayList<PostScreenItemsmodel> postScreenItemsmodelList;
    public String image;
    Activity superActivity;
    private LayoutInflater inflater = null;

    public PostScreenItems(Activity activity) {
        superActivity = activity;
        postScreenItemsmodelList = new ArrayList<PostScreenItemsmodel>();
        inflater = (LayoutInflater) activity
                .getSystemService(LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return postScreenItemsmodelList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        try {
            PostScreenItemsmodel postScreenItemsmodel = postScreenItemsmodelList.get(position);
            if (postScreenItemsmodel.getMessageType().equalsIgnoreCase("image")) {
                vi = inflater.inflate(R.layout.postscreen_image, null);

                try {
                    final ImageView imageView = (ImageView) vi.findViewById(R.id.post_imageview);
                    final Button btn_cancelimage = (Button) vi.findViewById(R.id.btn_cancelimage);
                    btn_cancelimage.setTag(position);
                    bitmap = (Bitmap) postScreenItemsmodel.getObject();
                    imageView.setImageBitmap(bitmap);


                    Log.i("sepimagesbytearray", String.valueOf(byteArray));

                    Log.i("sepimages", String.valueOf(bitmap));



                   /* DisplayMetrics dm = new DisplayMetrics();
                    superActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    int width = dm.widthPixels;
                    int height = width * bitmap.getHeight() / bitmap.getWidth();
                    if (width > height) {
                        height = (height * 2) / 3;
                        width = (width * 2) / 3;
                    } else {
                        height = height / 2;
                        width = width / 2;
                    }
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(width, height));*/
                    /*btn_cancelimage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Integer index = (Integer) view.getTag();
                            postScreenItemsmodelList.remove(position);
                            notifyDataSetChanged();
                        }
                    });*/
                    Log.i("list", String.valueOf(postScreenItemsmodelList));

                } catch (Exception e) {
                    Toast.makeText(superActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            } else if (postScreenItemsmodel.getMessageType().equalsIgnoreCase("video")) {
                vi = inflater.inflate(R.layout.postscreen_video, null);
                try {
                    //Uri uri = (Uri) postScreenItemsmodel.getObject();

                    ImageView mVideoView = (ImageView) vi.findViewById(R.id.post_videoview);

                    bitmap = (Bitmap) postScreenItemsmodel.getObject();
                    mVideoView.setImageBitmap(bitmap);
//                    final Button btn_cancelvideo = (Button) vi.findViewById(R.id.btn_cancelvideo);
//                    btn_cancelvideo.setTag(position);
                   /* mVideoView.setVideoURI(uri);
                    mVideoView.seekTo(1);*/

                  /*  Display display = superActivity.getWindowManager().getDefaultDisplay();
                    int width = display.getWidth();
                    int height = display.getHeight();
                    mVideoView.setLayoutParams(new FrameLayout.LayoutParams(width,height));*/

                   /* DisplayMetrics dm = new DisplayMetrics();
                    superActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    int width = dm.widthPixels;
                    int height = width * dm.heightPixels / dm.widthPixels;
                    if (width > height) {
                        height = (height * 2) / 3;
                        //width = (width * 2) ;
                    } else {
                        height = height / 2;
                        width = width ;
                    }
                    RelativeLayout.LayoutParams frameparams=new  RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_LEFT ,RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_TOP ,RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT ,RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM ,RelativeLayout.TRUE);

                    mVideoView.setLayoutParams(frameparams);*/
//                    btn_cancelvideo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            Integer index = (Integer) view.getTag();
//                            postScreenItemsmodelList.remove(position);
//                            notifyDataSetChanged();
//                        }
//                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }


    public void add(PostScreenItemsmodel postScreenItemsmodel) {

        postScreenItemsmodelList.add(postScreenItemsmodel);

    }


}