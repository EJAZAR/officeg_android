package org.officeg.feed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class LikesAndCommentsPagerAdapter extends FragmentPagerAdapter {

    private String title[] = {"Likes", "Comments"};
    private Bundle extras;

    public LikesAndCommentsPagerAdapter(FragmentManager manager, Bundle extras) {
        super(manager);
        this.extras = extras;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            LikesFragment likesFragment = new LikesFragment();
            likesFragment.setArguments(extras);
            return likesFragment;
        } else {
            CommentsFragment commentsFragment = new CommentsFragment();
            commentsFragment.setArguments(extras);
            return commentsFragment;
        }
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}