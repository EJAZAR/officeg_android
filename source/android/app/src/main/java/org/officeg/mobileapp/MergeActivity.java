package org.officeg.mobileapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.model.UserModel;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.Charset;

public class MergeActivity extends AppCompatActivity {
    private String TAG = MergeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge);
        String displayName = getIntent().getStringExtra("displayname");
        EditText ed_displayname = (EditText) findViewById(R.id.ed_displayname);
        ed_displayname.setText(displayName);
        ed_displayname.setFocusable(false);
        Button merge_button = (Button) findViewById(R.id.btn_merge);
        final EditText ed_merge_person = (EditText) findViewById(R.id.ed_merge_person);
        merge_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog PD = ProgressDialog.show(MergeActivity.this, "Please Wait", "Please Wait ...", true);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("secret_key", UserModel.getInstance().secret_key);
                    jsonObject.put("mergefid", getIntent().getStringExtra("fid"));
                    jsonObject.put("anotherfid", ed_merge_person.getText().toString());
                    jsonObject.put("mode", "request");
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }

                byte[] mergeJson = jsonObject.toString().getBytes(Charset.forName("UTF-8"));
                APIManager api = new APIManager("relationship/merge", mergeJson, new APIListener() {

                    @Override
                    public void onSuccess(JSONObject result) {
                        try {
                            UserModel.getInstance().loadMembers();
                            Log.d(TAG, result.toString());
                            Intent intent_activity = new Intent(getApplicationContext(), AddMemberActivity.class);
                            startActivity(intent_activity);
                            PD.dismiss();
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }

                    @Override
                    public void onSuccess(JSONArray array) {
                    }

                    @Override
                    public void onFailure(String errorMessage, String errorCode) {
                        Log.e(TAG, errorMessage);
                        PD.dismiss();
                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }, APIManager.HTTP_METHOD.POST);
                api.execute();
            }
        });

        TextView marque = (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        String marqueText = "Merging " + displayName + " with another person";
        marque.setText(marqueText);

    }
}
