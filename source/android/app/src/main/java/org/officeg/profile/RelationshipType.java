package org.officeg.profile;

public enum RelationshipType {
    BOSS,
    PEER,
    JUNIOR;

    //String relationshiptype;

    //private Relationship(String relationshiptype) {
    //    this.relationshiptype = relationshiptype;
    //}

    //@Override
    //public String toString(){
    //    return relationshiptype;
    //}

    public static int length() {
        return RelationshipType.values().length;
    }
}

