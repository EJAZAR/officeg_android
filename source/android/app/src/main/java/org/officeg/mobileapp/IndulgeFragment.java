package org.officeg.mobileapp;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.model.UserModel;

import de.hdodenhof.circleimageview.CircleImageView;

public class IndulgeFragment extends Fragment {
    TextView backup, changenumber, help, faq, privacyPolicy, displayname, mobilenumber, blockedList, fid, percentage, appinfo, mySharedData;
    ImageButton btnEdit;
    UserModel user = null;
    CircleImageView imageView;
    SeekBar seekbar;
    RelativeLayout appColor;
    WebView pedigree_webview;
    private View currentView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String TAG = IndulgeFragment.class.getSimpleName();
    ProgressDialog PD;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.indulge);
        currentView = inflater.inflate(R.layout.activity_indulge, container, false);
        container.removeAllViews();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        checkInternetConnection();

        user = UserModel.getInstance();
        pedigree_webview = currentView.findViewById(R.id.pedigree_webview);
        PD = ProgressDialog.show(getActivity(), "Please Wait", "Please Wait ...", true);

        pedigreeView();
        return currentView;
    }

    private void pedigreeView() {
//        pedigree_webview.setWebViewClient(new PedigreeViewActivity.PedigreeeWebViewClient());
        WebSettings webSettings = pedigree_webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        //pedigree_webview.loadUrl("http://www.familyg.org/faq.html");
        Log.i("FamilyG", "doi " + UserModel.getInstance().doi);
        Log.i("FamilyG", "secret_key " + user.secret_key);

        pedigree_webview.loadUrl("http://www.familyg.org/blog/");
        Log.d(TAG, "URL  = http://www.familyg.org/blog/");
        pedigree_webview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                PD.dismiss();
            }
        });
    }

    private void checkInternetConnection() {
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getActivity());
        if (!networkCheck)
            Toast.makeText(getActivity(), APIManager.errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_feed_id);
                    return true;
                }
                return false;
            }
        });
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}

