package org.officeg.mobileapp;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import org.officeg.api.APIManager;
import org.officeg.model.UserModel;

public class PedigreeViewActivity extends AppCompatActivity {
    WebView pedigree_webview;
    UserModel user = null;
    private String TAG = PedigreeViewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indulge);

        user = UserModel.getInstance();
        pedigree_webview = findViewById(R.id.pedigree_webview);

        Toolbar toolbar = findViewById(R.id.toolbartop);
        TextView marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_pedigreeView);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PedigreeViewActivity.this, BottomBarActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        pedigreeView();
    }


    private void pedigreeView() {

//        pedigree_webview.setWebViewClient(new PedigreeViewActivity.PedigreeeWebViewClient());

        WebSettings webSettings = pedigree_webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        //pedigree_webview.loadUrl("http://www.familyg.org/faq.html");
        Log.i("FamilyG", "doi " + UserModel.getInstance().doi);
        Log.i("FamilyG", "secret_key " + user.secret_key);

        pedigree_webview.loadUrl(APIManager.link + "pedigreetree?doi=" + user.doi + "&secret_key=" + user.secret_key);
        Log.d(TAG, "URL  = " + APIManager.link + "pedigreetree?doi=" + user.doi + "&secret_key=" + user.secret_key);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

//    private class PedigreeeWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (Uri.parse(url).getHost().equals("treeStructure.html")) {
//                return false;
//            }
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            view.getContext().startActivity(intent);
//            return true;
//        }
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(PedigreeViewActivity.this, BottomBarActivity.class);
        startActivity(i);
        finish();

    }
}