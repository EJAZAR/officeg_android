package org.officeg.feed;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.officeg.chat.DownloadFileTask;
import org.officeg.mobileapp.R;
import org.officeg.mobileapp.VideoPlayerActivity;
import org.officeg.util.Utility;

import java.io.File;

public class VideosFragment extends Fragment {
    LinearLayout videoViewLL;
    LayoutInflater inflater;
    private String TAG = VideosFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View currentView = inflater.inflate(R.layout.videos_fragment, container, false);
        this.inflater = inflater;
        Bundle extras = this.getArguments();

        FeedItemModel feedItemModel = (FeedItemModel) extras.get("itemModel");

        Gallery gallery = (Gallery) currentView.findViewById(R.id.gallery);
        videoViewLL = (LinearLayout) currentView.findViewById(R.id.videoView);
        gallery.setSpacing(20);
        final GalleryVideoAdapter galleryVideoAdapter = new GalleryVideoAdapter(getContext());
        galleryVideoAdapter.mVideoIds = feedItemModel.getVideolist();
        gallery.setAdapter(galleryVideoAdapter);

        if (galleryVideoAdapter.mVideoIds.size() > 0) {
            String fileUrl = galleryVideoAdapter.mVideoIds.get(0);
            showSelectedImage(fileUrl);
        } else {
            gallery.setVisibility(View.GONE);
            videoViewLL.setVisibility(View.GONE);
            TextView empty_notification = (TextView) currentView.findViewById(R.id.empty_notification);
            empty_notification.setVisibility(View.VISIBLE);
        }

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // show the selected Image
                String fileUrl = galleryVideoAdapter.mVideoIds.get(position);
                showSelectedImage(fileUrl);
            }
        });

        return currentView;
    }

    private void showSelectedImage(String fileUrl) {
        try {
            View videoView = inflater.inflate(R.layout.feed_video, null);

            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, getContext());
            ImageView imageView = (ImageView) videoView.findViewById(R.id.chat_bubble_imageview);

            DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = width * metrics.heightPixels / metrics.widthPixels;
            if (width > height) {
                height = (height * 2) / 3;
                width = metrics.widthPixels;
            } else {
                height = height / 2;
                width = width;
            }

            imageView.getLayoutParams().height = height;
            imageView.getLayoutParams().width = width;
            imageView.requestLayout();

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d("VideoPost = ", "if" + fileCheck.getAbsolutePath());
                DownloadFileTask downloadFileTask = new DownloadFileTask(getContext(), videoView, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, getContext().getResources().getString(R.string.video));
            } else {
                Log.d("VideoPost = ", "else" + fileCheck.getAbsolutePath());
                final Uri uri = Utility.getUriFromFile(getContext(), fileCheck);

                //String mp4filename = fileName.replace(".png", ".mp4");
                File afterRename = new File(receivedDirectory, fileName);
                //Utility.copy(superActivity, uri, afterRename);
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getAbsolutePath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);
                if (bitmap == null) {
                    Log.d("createVideoThumbnail", "null and " + uri.getPath() + " and " + afterRename.getAbsolutePath());
                }
                imageView.setImageBitmap(bitmap);
            }
            videoViewLL.removeAllViews();
            videoViewLL.addView(videoView);
            videoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), VideoPlayerActivity.class);
                    Uri uri = Utility.getUriFromFile(getContext(), fileCheck);
                    intent.putExtra("videoUri", uri.toString());
                    getContext().startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
