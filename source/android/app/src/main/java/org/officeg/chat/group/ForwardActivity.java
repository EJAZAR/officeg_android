package org.officeg.chat.group;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.ChatModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import static android.util.Log.d;

public class ForwardActivity extends AppCompatActivity {
    ArrayList<ChatModel> chatModels = new ArrayList<ChatModel>();
    TextView marque;
    private String TAG = ForwardActivity.class.getSimpleName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Send").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 1) {
            NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
            if (!NetworkConnectionreceiver.isConnection) {
                Toast.makeText(getApplicationContext(), getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }
            final LinkedList<String> fids = new LinkedList<String>();
            final LinkedList<String> gids = new LinkedList<String>();

            String chatMessageModelStr = getIntent().getStringExtra("chatMessageModel");
            ChatMessageModel chatMessageModel = new ChatMessageModel();
            chatMessageModel = chatMessageModel.toObject(chatMessageModelStr);

            for (int k = 0; k < chatModels.size(); k++) {
                ChatModel chatModel = chatModels.get(k);
                if (chatModel.isSelected()) {
                    if (chatModel.getType().equals(getString(R.string.group))) {
                        gids.add(chatModel.getGroupModel().getGid());
                    } else {
                        fids.add(chatModel.getProfileModel().getFid());
                    }
                }
            }
            if (fids.size() == 0 && gids.size() == 0) {
                Toast.makeText(getApplicationContext(), "No recipient to forward", Toast.LENGTH_LONG).show();
            } else {
                for (String fid : fids) {
                    chatMessageModel.setFid(fid);
                    chatMessageModel.setChatType0or1("0");
                    JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, getApplicationContext(), null, null);
                    Utility.sendChatContent(chatMessageModel, getApplicationContext(), false, null, null, null, null, msgJson);
                }
                for (String gid : gids) {
                    chatMessageModel.setFid(gid);
                    chatMessageModel.setGid(gid);
                    chatMessageModel.setChatType0or1("1");
                    JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, getApplicationContext(), null, null);
                    Utility.sendChatContent(chatMessageModel, getApplicationContext(), false, null, null, null, null, msgJson);
                }

//                Intent chatPage = new Intent(getApplicationContext(), ChatMessageActivity.class);
//                Bundle extras = getIntent().getExtras();
////                if (fids.size() != 0) {
////                    extras.putString("typeChat", "0");
////                    extras.putString("fidOrGid", fids.get(0));
////                } else {
////                    extras.putString("typeChat", "1");
////                    extras.putString("fidOrGid", gids.get(0));
////                }
//                chatPage.putExtras(extras);
//                chatPage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                finish();
//                startActivity(chatPage);

                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.members_listview);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        marque = (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            marque.setText(R.string.share);
            Bundle extras = getIntent().getExtras();
            extras.putBoolean("isShare", true);
            getIntent().putExtras(extras);

            if ("text/plain".equals(type)) {
                handleSendText(intent);
            } else if (type.startsWith("image/")) {
                handleSendFile(intent, getString(R.string.image));
            } else if (type.startsWith("audio/")) {
                handleSendFile(intent, getString(R.string.audio));
            } else if (type.startsWith("video/")) {
                handleSendFile(intent, getString(R.string.video));
            }
        } else {
            marque.setText(R.string.forward);

        }
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ForwardAdapter forwardAdapter = new ForwardAdapter(this.getWindow().getContext());
        ListView listView = (ListView) findViewById(R.id.list_creategroup);
        listView.setAdapter(forwardAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox = view.findViewById(R.id.checkbox_group_addmember);
                checkBox.performClick();
            }
        });


        if (!Utility.profileModelMap.isEmpty()) {
            Log.d(TAG, "Utility.profileModelMap exist");
            afterGettingFavouritesList(forwardAdapter);
        } else {
            final UserModel userModel = UserModel.getInstance();
            APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", userModel, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "inside onsuccess");
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "Utility.profileModelMap reload");
                    try {
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject object = array.getJSONObject(k);
                            ProfileModel profileModel = new ProfileModel();
                            profileModel = profileModel.toObject(object.toString());

                            if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                String profileModelKey = profileModel.getFid();
                                if (Utility.profileModelMap.containsKey(profileModelKey)) {
                                    Utility.profileModelMap.remove(profileModelKey);
                                }
                                Utility.profileModelMap.put(profileModelKey, profileModel);
                            }
                        }
                        afterGettingFavouritesList(forwardAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, errorMessage);
                }
            });
            getFavouriteListApi.execute();
        }

        UserModel userModel = UserModel.getInstance();
        byte[] userModelBA = userModel.toJSON().getBytes(Charset.forName("UTF-8"));
        if (!Utility.groupModelMap.isEmpty()) {
            Log.d(TAG, "Utility.groupModelMap exist");
            afterGettingGroups(forwardAdapter);
        } else {
            APIManager getGroupsApi = new APIManager("chat/groups", userModelBA, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    d("ForwardActivity", "inside onsuccess" + result.toString());
                }

                @Override
                public void onSuccess(JSONArray array) {
                    try {
                        Log.d(TAG, "Utility.groupModelMap reload");
                        for (int k = 0; k < array.length(); k++) {
                            JSONObject singleGroup = array.getJSONObject(k);
                            GroupModel groupModel = new GroupModel();
                            groupModel.setName(singleGroup.getString("name"));
                            groupModel.setGid(singleGroup.getString("gid"));

                            if (singleGroup.has("url"))
                                groupModel.setUrl(singleGroup.getString("url"));
                            else
                                groupModel.setUrl("");

                            JSONArray membersJsonArray = singleGroup.getJSONArray("members");
                            String[] membersArray = Utility.getStringArray(membersJsonArray);
                            groupModel.setMembers(membersArray);

                            JSONArray adminsJsonArray = singleGroup.getJSONArray("admins");
                            String[] adminsArray = Utility.getStringArray(adminsJsonArray);
                            groupModel.setAdmins(adminsArray);

                            Utility.groupModelMap.put(groupModel.getGid(), groupModel);
                        }
                        afterGettingGroups(forwardAdapter);
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    d("ForwardActivity", errorMessage);
                }

            }, APIManager.HTTP_METHOD.POST);
            getGroupsApi.execute();
        }
    }

    void handleSendText(Intent intent) {
        ProgressDialog PD = ProgressDialog.show(ForwardActivity.this, "Please Wait", "Please Wait ...", true);
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            ChatMessageModel chatMessageModel = new ChatMessageModel();
            chatMessageModel.setText(sharedText);
            chatMessageModel.setMessageContentType(getString(R.string.text));
            chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
            Bundle extras = getIntent().getExtras();
            extras.putString("chatMessageModel", chatMessageModel.toJSON(chatMessageModel));
            getIntent().putExtras(extras);
        }
        PD.dismiss();
    }

    void handleSendFile(Intent intent, final String type) {
        Uri fileUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (fileUri != null) {
            final String filePath = Utility.getRealPathFromUri(getApplicationContext(), fileUri);
            byte[] byteArray = Utility.convertUrlToByteArray(filePath);
            final ChatMessageModel chatMessageModel = new ChatMessageModel();

            final ProgressDialog PD = ProgressDialog.show(ForwardActivity.this, "Please Wait", "Please Wait ...", true);

            APIManager uploadapi = new APIManager("upload", byteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        Log.d("uploadFile", result.toString());
                        if (type.equalsIgnoreCase("image")) {
                            String resultUrl = result.getString("url");
                            Log.d(TAG, "uploadFile = " + result.getString("url"));
                            String fileNameAfterUpload = resultUrl.substring(resultUrl.lastIndexOf("/") + 1);

                            File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, getApplicationContext());
                            File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                            File beforeRename = new File(filePath);
                            beforeRename.renameTo(afterRename);
                            Log.d(TAG, "beforeRename = " + beforeRename);
                            Log.d(TAG, "afterRename = " + afterRename);
                            chatMessageModel.setImage(resultUrl);
                        } else if (type.equalsIgnoreCase(getString(R.string.audio))) {
                            String resultUrl = result.getString("url");
                            Log.d(TAG, "uploadFile = " + result.getString("url"));
                            String fileNameAfterUpload = resultUrl.substring(resultUrl.lastIndexOf("/") + 1);

                            File receivedDirectoryByType = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, getApplicationContext());
                            File afterRename = new File(receivedDirectoryByType, fileNameAfterUpload);

                            File beforeRename = new File(filePath);
                            beforeRename.renameTo(afterRename);
                            Log.d(TAG, "beforeRename = " + beforeRename);
                            Log.d(TAG, "afterRename = " + afterRename);
                            chatMessageModel.setAudio(resultUrl);
                        } else if (type.equalsIgnoreCase("video")) {
                            String videosChat = result.getString("url");
                            Log.d(TAG, "uploadFile video = " + result.getString("url"));
                            chatMessageModel.setVideo(videosChat);
                        }
                        chatMessageModel.setMessageContentType(type);
                        chatMessageModel.setDatetime(new Date(Utility.getNetworkTime(getApplicationContext())));
                        Bundle extras = new Bundle();
                        extras.putString("chatMessageModel", chatMessageModel.toJSON(chatMessageModel));
                        getIntent().putExtras(extras);
                        PD.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PD.dismiss();
                    }
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "onSuccess = " + array.length());
                    PD.dismiss();
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.d(TAG, "onFailure = " + errorCode + " and " + errorMessage);
                    PD.dismiss();
                }
            }, APIManager.HTTP_METHOD.FILE);
            uploadapi.execute();

        }
    }

    private void afterGettingGroups(ForwardAdapter forwardAdapter) {
        try {
            for (Map.Entry<String, GroupModel> entry : Utility.groupModelMap.entrySet()) {
                if (!getIntent().getExtras().getString("fidOrGid").equalsIgnoreCase(entry.getKey())) {
                    GroupModel groupModel = entry.getValue();
                    ChatModel chatModel = new ChatModel();
                    chatModel.setGroupModel(groupModel);
                    chatModel.setType(getString(R.string.group));
                    chatModels.add(chatModel);
                    forwardAdapter.add(chatModel);
                    forwardAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            Log.e("ForwardActivity", e.getMessage());
        }
    }

    private void afterGettingFavouritesList(ForwardAdapter forwardAdapter) {

        for (Map.Entry<String, ProfileModel> entry : Utility.profileModelMap.entrySet()) {
            if (!getIntent().getExtras().getString("fidOrGid").equalsIgnoreCase(entry.getKey())) {
                ChatModel chatModel = new ChatModel();
                chatModel.setProfileModel(entry.getValue());
                chatModel.setType(getString(R.string.favourite));
                chatModels.add(chatModel);
                forwardAdapter.add(chatModel);
                forwardAdapter.notifyDataSetChanged();
            }
        }
    }
}
