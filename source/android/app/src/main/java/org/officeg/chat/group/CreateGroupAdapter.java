package org.officeg.chat.group;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.util.Utility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class CreateGroupAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<ProfileModel> profileModelArrayList;
    private Context context;

    public CreateGroupAdapter(Context context) {
        profileModelArrayList = new ArrayList<ProfileModel>();
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return profileModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return profileModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(ProfileModel profileModel) {
        profileModelArrayList.add(profileModel);
    }

    public void setProfileModelArrayList(ArrayList<ProfileModel> profileModelArrayList) {
        this.profileModelArrayList = profileModelArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.group_addmember_adaptor, null);

        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_group_addmember);

        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ProfileModel profileModel = (ProfileModel) cb.getTag();
                profileModel.setSelected(cb.isChecked());
            }
        });
        ProfileModel profileModel = profileModelArrayList.get(position);
//        checkBox.setText(profileModel.getDisplayname());
        checkBox.setChecked(profileModel.isSelected());
        checkBox.setTag(profileModel);

        TextView name = (TextView) convertView.findViewById(R.id.name_group_addmember);
        name.setText(profileModel.getDisplayname());

        CircleImageView circleImageView = (CircleImageView) convertView.findViewById(R.id.circle_group_addmember);
        if (profileModel.getProfileimageurl() != null && !profileModel.getProfileimageurl().trim().isEmpty()) {
            Utility.showProfilePicture(profileModel.getProfileimageurl(), context, circleImageView, profileModel.getDisplayname(), true);
        } else {
            circleImageView.setImageResource(R.drawable.ic_person);
        }
        return convertView;
    }
}
