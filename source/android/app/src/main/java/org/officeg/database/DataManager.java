package org.officeg.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.feed.FeedItemModel;
import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.nearby.NearbyModel;
import org.officeg.notification.MessageModel;
import org.officeg.util.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class DataManager extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "FamilyGUserDatabase";
    public static final String TABLE_NAME = "usertable";
    private static final int DATABASE_VERSION = 1;
    static Context ctx;
    private static DataManager datamanager = null;
    String queries;
    SQLiteDatabase db;
    List<HashMap<String, String>> profileList;
    private String TAG = DataManager.class.getSimpleName();

    private DataManager(Context applicationcontext) {
        super(applicationcontext, DATABASE_NAME, null, DATABASE_VERSION);
        try {
            ctx = applicationcontext;
            db = getWritableDatabase();
            onCreate(db);
            Log.d("User Database: ", "Created");
        } catch (Exception e) {
            Toast.makeText(applicationcontext, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static DataManager getInstance(Context context) {

        if (datamanager == null) {
            if (context != null)
                ctx = context;
            datamanager = new DataManager(ctx);
            Log.i("FamilyG", "Initiated data manager class");
        }
        return datamanager;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String createRelationTable = "CREATE  TABLE IF NOT EXISTS " + "relationships" + " (" + "KEY_ID"
                + " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "
                + "FamilyGID TEXT NOT NULL , "
                + "DisplayName TEXT, "
                + "FirstName TEXT, "
                + "LastName TEXT, "
                + "DOB TEXT, "
                + "Gender TEXT, "
                + "MobileNumber TEXT, "
                + "DOI TEXT, "
                + "Deathdate TEXT, "
                + "IsOwner TEXT, "
                + "IsFavourite TEXT, "
                + "ImageUrl TEXT, "
                + "ownerfid TEXT, "
                + "IsAlive TEXT, "
                + "IsFirstDegree TEXT "
                + ")";

        String createRelationMappingTable = "CREATE  TABLE IF NOT EXISTS " + "relationshipMapping" + " ("
                + "FamilyGID TEXT NOT NULL , "
                + "RelationFamilyGID TEXT, "
                + "RelationType TEXT, "
                + "Status TEXT "
                + ")";

        //Reading app.sql file from assets folder
        StringBuffer sb = new StringBuffer();
        BufferedReader buffererdeader = null;
        try {
            buffererdeader = new BufferedReader(new InputStreamReader(ctx.getAssets().open(
                    "app.sql"), Charset.forName("UTF-8")));
            String temp;
            while ((temp = buffererdeader.readLine()) != null)
                sb.append(temp);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert buffererdeader != null;
                buffererdeader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        queries = sb.toString();
        Log.i("QUERY==", queries);
        database.execSQL(queries);
//        database.execSQL("DROP TABLE unreadnotification;");
        database.execSQL("CREATE TABLE IF NOT EXISTS unreadnotification(fidOrGid TEXT PRIMARY KEY, content TEXT, postid TEXT, datetime TEXT, type TEXT, count TEXT);");
        database.execSQL("CREATE TABLE IF NOT EXISTS chatfavouritesandgroups(fidOrGid TEXT PRIMARY KEY, name TEXT, url TEXT, mode TEXT, mobilenumber TEXT);");
        database.execSQL("CREATE TABLE IF NOT EXISTS posts(postcontent TEXT PRIMARY KEY);");
        database.execSQL("CREATE TABLE IF NOT EXISTS nearbydata(nearbydatacontent TEXT PRIMARY KEY);");

        database.execSQL(createRelationTable);
        database.execSQL(createRelationMappingTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
//        String query;
//        query = ("DROP TABLE IF EXISTS usertable");
//        query = ("DROP TABLE IF EXISTS chathistory");
//        query = ("DROP TABLE IF EXISTS relationships");
        database.execSQL("DROP TABLE IF EXISTS usertable");
        database.execSQL("DROP TABLE IF EXISTS chathistory");
        database.execSQL("DROP TABLE IF EXISTS relationships");
        database.execSQL("DROP TABLE IF EXISTS relationshipMapping");
        onCreate(database);
    }

    public void delete(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(tableName, null, null);
        db.close();
    }

    //similar to select query
    public Cursor Execute(String sql) {
        SQLiteDatabase db = getWritableDatabase();
        return db.rawQuery(sql, null);
    }

    //insert or update query
    public Boolean ExecuteNonQuery(String tablename, Object param, String condition) {
        Class paramClass = param.getClass();
        Field[] fields = paramClass.getFields();
        ContentValues values = new ContentValues();

        for (int index = 0; index < fields.length; index++) {
            String fieldName = fields[index].getName();
            Log.i("checking", fieldName);
            try {
                Object value = fields[index].get(param);
                if (value == null)
                    continue;

                values.put(fieldName, value.toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        SQLiteDatabase db = getWritableDatabase();
        if (condition == null || condition.isEmpty()) {
            // Directly do insert
            db.insert(tablename, null, values);

        } else {
            String selectQuery = "SELECT  * FROM " + tablename + " " + condition;
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.getCount() == 0) {
                db.insert(tablename, null, values);
            }
            cursor.close();
        }
        return true;
    }

    public Boolean ExecuteKeyValueNonQuery(String tablename, Object param, String condition) {
        try {
            Class paramClass = param.getClass();
            Field[] fields = paramClass.getFields();
            ContentValues values = new ContentValues();
            SQLiteDatabase db = getWritableDatabase();
            for (int index = 0; index < fields.length; index++) {
                String fieldName = fields[index].getName();
                try {
                    if (fieldName.equals("profileMemberModel"))
                        continue;
                    Object value = fields[index].get(param);
                    if (value == null)
                        continue;
                    values.put("Key", fieldName);
                    if (value instanceof byte[])
                        values.put("Value", (byte[]) value);
                    else if (value instanceof Integer)
                        values.put("Value", (Integer) value);
                    else if (value instanceof Boolean)
                        values.put("Value", (Boolean) value);
                    else
                        values.put("Value", String.valueOf(value));
                    Log.i("FamilyG", (index + 1) + ". Key=" + fieldName + ", Value= " + String.valueOf(value));
                    db.insert(tablename, null, values);
                } catch (Exception e) {
                    Log.i("FamilyG", "Failed in saving user : " + e.getMessage());
                    Log.i("FamilyG", e.getMessage());
                }
            }

            if (condition == null || condition.isEmpty()) {
                db.insert(tablename, null, values);
            } else {
                String selectQuery = "SELECT  * FROM " + tablename + " " + condition;
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.getCount() == 0) {
                    db.insert(tablename, null, values);
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.d("DataManager", "ExecuteKeyValueNonQuery = " + e.getMessage());
        }
        return true;
    }

    public void createTableForChatHistory() {
        SQLiteDatabase database = getWritableDatabase();
//        database.execSQL("DROP TABLE IF EXISTS chathistory");
        database.execSQL("CREATE TABLE IF NOT EXISTS chathistory(messageId TEXT, fid TEXT, msg TEXT, mode TEXT, datetime TEXT, readstatus INTEGER, fileStatus INTEGER, acknowledgementStatus INTEGER)");
        database.execSQL("CREATE TABLE IF NOT EXISTS bookmarks(fid TEXT, displayname TEXT, profileimage TEXT)");
    }

    public void insertTable(String tablename, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        Long data = db.insert(tablename, null, values);
        Log.i("FamilyG", "Result for insert records in '" + tablename + " : " + String.valueOf(data));
    }

    public void updateTable(String tablename, ContentValues values, String fieldname, String fid) {   // to update
        SQLiteDatabase db = getWritableDatabase();
        Long data = Long.valueOf(db.update(tablename, values, fieldname + " ='" + fid + "'", null));
        Log.i("FamilyG", "Result for update of '" + tablename + "' : " + String.valueOf(data));
    }

    public void updateMapping(ContentValues values, String srcFid, String desFid) {
        SQLiteDatabase db = getWritableDatabase();
        Long data = Long.valueOf(db.update("relationshipMapping", values, "FamilyGID='" + srcFid + "' and RelationFamilyGID='" + desFid + "'", null));
        Log.i("FamilyG", "Result for updating 'relationshipmapping'  : " + String.valueOf(data));
    }

    public void deleteTable(String tablename, String fieldname, String value) {   // to delete particular row
        SQLiteDatabase db = getWritableDatabase();
        Long data = Long.valueOf(db.delete(tablename, fieldname + " = ?", new String[]{value}));
        Log.i("FamilyG", "return data delete : " + String.valueOf(data));
    }

    public boolean isUserExists(String fid) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from relationships where FamilyGID='" + fid + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean CheckIsDataAlreadyInDBorNot(String tableName,
                                               String dbfield, String fieldValue) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from " + tableName + " where " + dbfield + " =  '" + fieldValue + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public List<ChatMessageModel> getChatHistory(String fid) {
        List<ChatMessageModel> chatMessageModels = new ArrayList<ChatMessageModel>();
        try {
            SQLiteDatabase db = getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM chathistory WHERE fid='" + fid +
                    "' order by datetime(datetime) ASC", null);
            if (c.moveToFirst()) {
                do {
                    String column1 = c.getString(0);
                    String column2 = c.getString(1);
                    String column3 = c.getString(2);
                    String column4 = c.getString(3);
                    String column5 = c.getString(4);
                    String column6 = c.getString(5);
                    Integer column7 = c.getInt(6);
                    Integer column8 = c.getInt(7);
                    ChatMessageModel chatMessageModel = new ChatMessageModel();
                    chatMessageModel.setMessageId(column1);
                    chatMessageModel.setFid(column2);
                    chatMessageModel.setTotalContent(column3);
                    chatMessageModel.setTypeFromOrTo(column4);
                    chatMessageModel.setDatetime(Utility.getLocalDate(column5));
                    chatMessageModel.setAcknowledgementStatus(column8);
                    if (column7 == 0)
                        chatMessageModel.setIsprogress(true);
                    else
                        chatMessageModel.setIsprogress(false);

                    JSONObject reminderJsonObject = new JSONObject(column3);
                    if (reminderJsonObject.has("reminder_mid")) {
                        try {
                            ChatMessageModel chatMessageModel1 = getMessage(reminderJsonObject.getString("reminder_mid"));
                            chatMessageModel1.setExtraString(ctx.getString(R.string.reminder));
                            chatMessageModel1.setDatetime(chatMessageModel.getDatetime());
                            chatMessageModel1.setMessageId(chatMessageModel.getMessageId());
                            chatMessageModel1.setTypeFromOrTo(chatMessageModel.getTypeFromOrTo());
                            chatMessageModel1.setReminderMid(reminderJsonObject.getString("reminder_mid"));
                            chatMessageModels.add(chatMessageModel1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        chatMessageModels.add(chatMessageModel);
                    }
                    ChatMessageModel tempChatMessageModel = new ChatMessageModel();
                    tempChatMessageModel = tempChatMessageModel.toObject(column3);
                    if (tempChatMessageModel != null && tempChatMessageModel.isreply()) {
                        chatMessageModel.setSourceMessageForReply(tempChatMessageModel.getSourceMessageForReply());
                        chatMessageModel.setIsreply(tempChatMessageModel.isreply());
                    }

                    String chatdatetime = chatMessageModel.getDatetime() != null ? chatMessageModel.getDatetime().toString() : "";
                    Log.d("getDBValues", column1 + " and " + column2 + " and " + column3 + " and " + column4 + " and " + chatdatetime + " and " + column5 + " and " + column6 + " and " + column7);
                } while (c.moveToNext());
            }
            Log.i("FamilyG", "arraylist chat is" + String.valueOf(chatMessageModels));
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chatMessageModels;
    }

    public List<HashMap<String, String>> getProfiles(String fid) {
        JSONArray jArray = null;
        Log.i("FamilyG", "GETPROFILES: " + fid);

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT FamilyGID, displayname FROM relationships", null);

        while (c.moveToNext()) {
            Log.i("FamilyG", c.toString());
            Log.i("FamilyG", " Row : FGID = " + c.getString(0) + " & RFGID = " + c.getString(1));
        }

        c = db.rawQuery("SELECT * FROM relationships INNER JOIN relationshipMapping ON " +
                "relationshipMapping.RelationFamilyGID = relationships.FamilyGID WHERE relationshipMapping.FamilyGID = '" + fid + "' ", null);
        profileList = new ArrayList<>();
        if (c.moveToFirst()) {

            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("fid", c.getString(1));
                map.put("displayname", c.getString(2));
                map.put("firstname", c.getString(3));
                map.put("lastname", c.getString(4));
                map.put("dob", c.getString(5));
                map.put("gender", c.getString(6));
                map.put("mobilenumber", c.getString(7));
                map.put("doi", c.getString(8));
                map.put("deceaseddate", c.getString(9));
                map.put("isowner", c.getString(10));
                map.put("isfav", c.getString(11));
                map.put("profileimage", c.getString(12));
                map.put("ownerfid", c.getString(13));
                map.put("isalive", c.getString(14));
                map.put("isfirstdegree", c.getString(15));

                map.put("relationtype", c.getString(18));
                map.put("status", c.getString(19));

                profileList.add(map);

                //Log.d("getProfile", column1 + " " + column2 + " " + column3 + " " + column4 + " " +
                //        column5 + " " + column6 + " " + column7 + " " + column8 + " " + column9 + " " +
                //        column10 + " " + column11 + " " + column12 + " " + column13 + " " + column14 + " " + column15);
            } while (c.moveToNext());
        }
        Log.i("FamilyG", "Relationship list for " + fid + " is : " + String.valueOf(profileList));
        c.close();
        return profileList;
    }

    public void persistChatHistory(String msg, String fid, Context context, int fileStatus, String messageId, Integer acknowledgementStatus) {
        try {
            if (fileStatus == 0) {
                Log.d(TAG, "messageId = " + messageId);
                ContentValues row = new ContentValues();
                row.put("fid", fid);
                row.put("msg", msg);
                row.put("mode", "to");
                row.put("messageId", messageId);
                row.put("filestatus", fileStatus);
                row.put("readstatus", "0");
                row.put("acknowledgementStatus", acknowledgementStatus);
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dt.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date date = new Date(Utility.getNetworkTime(context));
                Log.d(TAG, "googlesenttime = " + date.toString());
                row.put("datetime", dt.format(date));
                insertTable("chathistory", row);
                getChatHistory(fid);
            } else {
                ContentValues row = new ContentValues();
                row.put("filestatus", fileStatus);
                row.put("fid", fid);
                row.put("msg", msg);
                row.put("mode", "to");
                row.put("filestatus", fileStatus);
                row.put("readstatus", "0");
                row.put("acknowledgementStatus", acknowledgementStatus);
                updateTable("chathistory", row, "messageId", messageId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isMappingExists(String srcFid, String desFid) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from relationshipMapping where FamilyGID='" + srcFid + "' and   RelationFamilyGID='" + desFid + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean deleteChatRecord(String fidOrGid, String chatDateTime) {
        try {
            SQLiteDatabase database = getWritableDatabase();
            String deleteQuery = "DELETE FROM chathistory WHERE fid='" + fidOrGid +
                    "' and datetime='" + chatDateTime + "'";
            Log.d("deleteChatRecord", deleteQuery);
            database.execSQL(deleteQuery);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<ChatMessageModel> getLastMessageEvents() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT ch.fid, ch.msg, ch.mode, ch.datetime, (select count(*) from chathistory ch1 where ch1.readstatus=1 and ch.fid=ch1.fid) FROM chathistory ch group by ch.fid order by datetime(ch.datetime) desc", null);
        List<ChatMessageModel> chatMessageModels = new ArrayList<ChatMessageModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                String column4 = c.getString(3);
                Integer column5 = c.getInt(4);
                ChatMessageModel chatMessageModel = new ChatMessageModel();
                chatMessageModel.setFid(column1);
                chatMessageModel.setTotalContent(column2);
                chatMessageModel.setTypeFromOrTo(column3);
                chatMessageModel.setDatetime(Utility.getLocalDate(column4));
                chatMessageModel.setUnreadCount(column5);
                chatMessageModels.add(chatMessageModel);
                Log.d("getLastMessageEvents", column1 + " and " + column2 + " and " + column3 + " and " + column4 + " and " + column5);
            } while (c.moveToNext());
        }
        c.close();
        return chatMessageModels;
    }

    public Integer getUnreadMessageCount(String fidORgid) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("select count(*) from chathistory  where readstatus=1 and fid='" + fidORgid + "'", null);
        if (c.moveToFirst()) {
            do {
                Integer column2 = c.getInt(0);
                Log.d("getLastMessageEvents", column2 + "");
                c.close();
                return column2;
            } while (c.moveToNext());
        }
        c.close();
        return 0;
    }

    public List<ChatMessageModel> getAllMessageEvents() {
        SQLiteDatabase db = getWritableDatabase();
//        Cursor c = db.rawQuery("SELECT * FROM chathistory group by fid order by datetime(datetime) desc", null);
//        messageId TEXT, fid TEXT, msg TEXT, mode TEXT, datetime TEXT, readstatus INTEGER, fileStatus INTEGER, acknowledgementStatus INTEGER
        Cursor c = db.rawQuery("SELECT * FROM chathistory order by fid desc", null);
        List<ChatMessageModel> chatMessageModels = new ArrayList<ChatMessageModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                String column4 = c.getString(3);
                String column5 = c.getString(4);
                String column6 = c.getString(5);
                Integer column7 = c.getInt(6);
                Integer column8 = c.getInt(7);
                ChatMessageModel chatMessageModel = new ChatMessageModel();
                chatMessageModel.setMessageId(column1);
                chatMessageModel.setFid(column2);
                chatMessageModel.setTotalContent(column3);
                chatMessageModel.setTypeFromOrTo(column4);
                chatMessageModel.setDatetime(Utility.getLocalDate(column5));
                chatMessageModel.setAcknowledgementStatus(column8);
                chatMessageModels.add(chatMessageModel);
                String chatdatetime = chatMessageModel.getDatetime() != null ? chatMessageModel.getDatetime().toString() : "";
                Log.d("getAllMessageEvents", column1 + " and " + column2 + " and " + column3 + " and " + column4 + " and " + chatdatetime + " and " + column5 + " and " + column6 + " and " + column7);
            } while (c.moveToNext());
        }
        c.close();
        return chatMessageModels;
    }

    public void changeStatusOfUnreadMessages(String fidOrGid) {
        try {
            ContentValues rowformapping = new ContentValues();
            rowformapping.put("readstatus", "0");
            updateTable("chathistory", rowformapping, "fid", fidOrGid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void isUnreadMessageExist(String fidOrGid) {
        deleteTable("unreadnotification", "fidOrGid", fidOrGid);
    }

    public List<MessageModel> getUnreadMessages() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select * from unreadnotification order by datetime(datetime) ASC LIMIT 7", null);
        List<MessageModel> unreadMessages = new ArrayList<MessageModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                int column3 = c.getInt(2);
                String column4 = c.getString(3);
                String column5 = c.getString(4);

                MessageModel messageModel = new MessageModel();
                messageModel.setSenderid(column1);
                messageModel.setNotificationmessage(column2);
                messageModel.setPostid(column3);
                messageModel.setNotificationTime(Utility.getLocalDate(column4));
                messageModel.setType(column5);
                unreadMessages.add(messageModel);
                Log.d("getUnreadMessages", column1 + " and " + column2 + " and " + column3);
            } while (c.moveToNext());
        }
        c.close();
        return unreadMessages;
    }

    public void insertChatFavouritesAndGroups(Map<String, ProfileModel> profileModelMap, Map<String, GroupModel> groupModelMap) {
        try {
            if (profileModelMap != null) {
                deleteTable("chatfavouritesandgroups", "mode", "0");
                for (Map.Entry<String, ProfileModel> entry : profileModelMap.entrySet()) {
                    ProfileModel profileModel = entry.getValue();
                    ContentValues row = new ContentValues();
                    row.put("fidOrGid", profileModel.getFid());
                    row.put("name", profileModel.getDisplayname());
                    row.put("url", profileModel.getProfileimageurl());
                    row.put("mode", "0");
                    row.put("mobilenumber", profileModel.getMobilenumber());
                    insertTable("chatfavouritesandgroups", row);
                }
            }
            if (groupModelMap != null) {
                deleteTable("chatfavouritesandgroups", "mode", "1");
                for (Map.Entry<String, GroupModel> entry : groupModelMap.entrySet()) {
                    GroupModel groupModel = entry.getValue();
                    ContentValues row = new ContentValues();
                    row.put("fidOrGid", groupModel.getGid());
                    row.put("name", groupModel.getName());
                    row.put("url", groupModel.getUrl());
                    row.put("mode", "1");
                    insertTable("chatfavouritesandgroups", row);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map<String, ProfileModel> getFavourites() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select * from chatfavouritesandgroups where mode ='0'", null);
        Map<String, ProfileModel> favourites = new HashMap<String, ProfileModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                String column4 = c.getString(4);

                ProfileModel profileModel = new ProfileModel();
                profileModel.setFid(column1);
                profileModel.setDisplayname(column2);
                profileModel.setProfileimageurl(column3);
                profileModel.setMobilenumber(column4);
                favourites.put(column1, profileModel);
                Log.d("getFavourites", column1 + " and " + column2 + " and " + column3 + " and " + column4);
            } while (c.moveToNext());
        }
        c.close();
        return favourites;
    }

    public Map<String, GroupModel> getGroups() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select * from chatfavouritesandgroups where mode ='1'", null);
        Map<String, GroupModel> groups = new HashMap<String, GroupModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);

                GroupModel groupModel = new GroupModel();
                groupModel.setGid(column1);
                groupModel.setName(column2);
                groupModel.setUrl(column3);
                groups.put(column1, groupModel);
                Log.d("getFavourites", column1 + " and " + column2 + " and " + column3);
            } while (c.moveToNext());
        }
        c.close();
        return groups;
    }

    public void insertPosts(JSONArray array) {
        try {
            delete("posts");
            for (int index = 0; index < array.length(); index++) {
                JSONObject postobject = array.getJSONObject(index);
                ContentValues row = new ContentValues();
                row.put("postcontent", postobject.toString());
                insertTable("posts", row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<FeedItemModel> getPosts() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select * from posts", null);
        List<FeedItemModel> feedItemModels = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                try {
                    JSONObject jsonObject = new JSONObject(column1);
                    FeedItemModel feedItemModel = new FeedItemModel(jsonObject);
                    feedItemModels.add(feedItemModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("getPosts", column1);
            } while (c.moveToNext());
        }
        c.close();
        return feedItemModels;
    }

    public void insertNearbyData(JSONArray array) {
        try {
            delete("nearbydata");
            for (int index = 0; index < array.length(); index++) {
                JSONObject nearbydataobject = array.getJSONObject(index);
                ContentValues row = new ContentValues();
                row.put("nearbydatacontent", nearbydataobject.toString());
                insertTable("nearbydata", row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<NearbyModel> getNearByData() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select * from nearbydata", null);
        List<NearbyModel> nearbyModels = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                try {
                    JSONObject jsonObject = new JSONObject(column1);
                    NearbyModel nearbyModel = new NearbyModel(jsonObject);
                    nearbyModels.add(nearbyModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("getNearByData", column1);
            } while (c.moveToNext());
        }
        c.close();
        return nearbyModels;
    }

    public int getUnreadChatMessagesCount() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from unreadnotification where type = '1' or type = '0'", null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getUnreadFeedMessagesCount() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select count from unreadnotification where type = '2'", null);
        if (c.moveToFirst()) {
            do {
                Integer column2 = c.getInt(0);
                Log.d("unreadFeedMessagesCount", column2 + "");
                c.close();
                return column2;
            } while (c.moveToNext());
        }
        c.close();
        return 0;
    }

    public int getUnreadRelationMessagesCount() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("Select count from unreadnotification where type = '3'", null);
        if (c.moveToFirst()) {
            do {
                Integer column2 = c.getInt(0);
                Log.d("unreadRelationMsgCount", column2 + "");
                c.close();
                return column2;
            } while (c.moveToNext());
        }
        c.close();
        return 0;
    }

    public List<ProfileModel> getBookmarks() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM bookmarks", null);
        List<ProfileModel> profileModels = new ArrayList<ProfileModel>();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                ProfileModel profileModel = new ProfileModel();
                profileModel.setFid(column1);
                profileModel.setDisplayname(column2);
                profileModel.setProfileimageurl(column3);
                profileModels.add(profileModel);
            } while (c.moveToNext());
        }
        c.close();
        return profileModels;
    }

    public void bookmarkThis(String fid, String displayname, String profileimage, Context mContext) {
        ContentValues row = new ContentValues();
        row.put("fid", fid);
        row.put("displayname", displayname);
        row.put("profileimage", profileimage);
        String selectQuery = "SELECT * FROM bookmarks where fid = '" + fid + "'";
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() == 0) {
            insertTable("bookmarks", row);
            Toast.makeText(mContext, "Bookmark added", Toast.LENGTH_SHORT).show();
        } else {
            deleteTable("bookmarks", "fid", fid);
            Toast.makeText(mContext, "Bookmark removed", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
    }

    public ChatMessageModel getMessage(String messageId) {

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM chathistory WHERE messageId='" + messageId +
                "'", null);
        ChatMessageModel chatMessageModel = new ChatMessageModel();
        if (c.moveToFirst()) {
            do {
                String column1 = c.getString(0);
                String column2 = c.getString(1);
                String column3 = c.getString(2);
                String column4 = c.getString(3);
                String column5 = c.getString(4);
                String column6 = c.getString(5);
                Integer column7 = c.getInt(6);
                chatMessageModel.setMessageId(column1);
                chatMessageModel.setFid(column2);
                chatMessageModel.setTotalContent(column3);
                chatMessageModel.setTypeFromOrTo(column4);
                chatMessageModel.setDatetime(Utility.getLocalDate(column5));
                if (column7 == 0)
                    chatMessageModel.setIsprogress(true);
                else
                    chatMessageModel.setIsprogress(false);
                String chatdatetime = chatMessageModel.getDatetime() != null ? chatMessageModel.getDatetime().toString() : "";
                Log.d("getMessage", column1 + " and " + column2 + " and " + column3 + " and " + column4 + " and " + chatdatetime + " and " + column5 + " and " + column6 + " and " + column7);
                c.close();
                return chatMessageModel;
            } while (c.moveToNext());
        }
        c.close();
        return null;
    }
}