package org.officeg.feed;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.officeg.mobileapp.R;
import org.officeg.mobileapp.VideoPlayerActivity;
import org.officeg.util.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadPostVideoTask extends AsyncTask<String, String, String> {
    //ProgressDialog PD;
    View vi;
    Activity activity;
    LinearLayout contentView1;

    public DownloadPostVideoTask(Activity activity, View vi) {
        this.activity = activity;
        this.vi = vi;
    }


    /*@Override
    protected void onPreExecute() {
        PD = ProgressDialog.show(activity, "Please Wait", "Please Wait ...", true);
        PD.setCancelable(true);
    }*/

    @Override
    protected String doInBackground(String... urls) {
        String fileURL = urls[0];
        String fileName = urls[1];
        downloadFile(fileURL, fileName);
//        downloadFile("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_30mb.mp4", "Sample.mp4");
        return "done";

    }

    protected void onPostExecute(Boolean result) {
        //
        // PD.dismiss();
    }

    private void downloadFile(String fileURL, final String fileName) {
        FileOutputStream f = null;
        try {
            File moviesDirectory = null;

            if (android.os.Environment.getExternalStorageState().equals(
                    android.os.Environment.MEDIA_MOUNTED))
                moviesDirectory = new File(
                        android.os.Environment.getExternalStorageDirectory(),
                        "FamilyG/TempVideos");

            else
                moviesDirectory = activity.getCacheDir();
            if (!moviesDirectory.exists())
                moviesDirectory.mkdirs();
            String rootDir = moviesDirectory
                    + File.separator + "Received";
            File rootFile = new File(rootDir);
            rootFile.mkdir();
            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.connect();
            final File fileFinal = new File(rootFile,
                    fileName);
            f = new FileOutputStream(fileFinal);
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            //PD.dismiss();
            Log.d("fileFinal = ", fileFinal.getAbsolutePath());

            final File finalMoviesDirectory = moviesDirectory;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    contentView1 = (LinearLayout) vi.findViewById(R.id.viewgrouplinear1);

                    Log.d("runONUiThread", "");
                    final Uri uri = Utility.getUriFromFile(activity, fileFinal);

                    ImageView imageview = null;
                    imageview = new ImageView(activity);
                    // String mp4filename = fileName.replace(".png", ".mp4");
                    File afterRename = new File(finalMoviesDirectory, fileName);
                    // Utility.copy(activity, uri, afterRename);
                    Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(afterRename.getPath(),
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    imageview.setImageBitmap(bitmap);
                    imageview.setImageBitmap(bitmap);
                    imageview.setAdjustViewBounds(true);
                    imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
                    int width = metrics.widthPixels;
                    int height = width * metrics.heightPixels / metrics.widthPixels;
                    if (width > height) {
                        height = (height * 2) / 3;
                        width = metrics.widthPixels;
                    } else {
                        height = height / 2;
                    }
                    LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
                    buttonLayoutParams.setMargins(0, 7, 0, 0);
                    imageview.setLayoutParams(buttonLayoutParams);

                    RelativeLayout feedRelativeLayout = new RelativeLayout(activity);
                    LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linearParams.setMargins(0, 30, 0, 0);
                    feedRelativeLayout.setLayoutParams(linearParams);
                    feedRelativeLayout.addView(imageview);


                    /*VideoView video = new VideoView(activity);
                    video.setVideoURI(uri);
                    DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
                    int width = metrics.widthPixels;
                    int height = width * metrics.heightPixels / metrics.widthPixels;
                    if (width > height) {
                        height = (height * 2) / 3;
                        width = metrics.widthPixels;
                    } else {
                        height = height / 2;
                        width = width;
                    }
                    RelativeLayout.LayoutParams frameparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_TOP, RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    frameparams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

                    video.setLayoutParams(frameparams);                    // video.setZOrderOnTop(true);*//*                    // video.setZOrderOnTop(true);
                    video.seekTo(1);


                    video.requestFocus();*/
                    //contentView1.addView(imageview);
                   /* video.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("video", "setOnErrorListener ");
                            return true;
                        }
                    });*/
                    ImageView playicon = new ImageView(activity);
                    RelativeLayout.LayoutParams playLayoutParams = new RelativeLayout.LayoutParams(300, 300);
                    //playLayoutParams.gravity= Gravity.CENTER;
                    playLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);

                    playicon.setLayoutParams(playLayoutParams);
                    playicon.setImageResource(R.drawable.playoverlay);
                    feedRelativeLayout.addView(playicon);

                    contentView1.addView(feedRelativeLayout);


                    playicon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(activity, VideoPlayerActivity.class);
                            intent.putExtra("videoUri", uri.toString());
                            activity.startActivity(intent);
                        }
                    });
                }
            });

        } catch (Exception e) {
            //  Log.e("Error....", e.toString());
            // PD.dismiss();
        } finally {
            try {
                if (f != null)
                    f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}