package org.officeg.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.FileProvider;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.chat.ChatMessageAdapter;
import org.officeg.chat.DownloadProfilePicture;
import org.officeg.database.DataManager;
import org.officeg.feed.ImageLoader;
import org.officeg.mobileapp.ChatMessageActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.profile.RelationshipType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class Utility {

    public static Map<String, ProfileModel> profileModelMap = new HashMap<String, ProfileModel>();
    public static Map<String, GroupModel> groupModelMap = new HashMap<String, GroupModel>();
    public static String selectedFid;
    public static String selectedSource;
    public static RelationshipType selectedRelationType;
    private static String TAG = Utility.class.getSimpleName();

    public static Map<String, String> fileFormatMap = new HashMap<String, String>();

    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("audio", "fileName.aac");
        aMap.put("image", "fileName.png");
        aMap.put("video", "fileName.mp4");
        fileFormatMap = Collections.unmodifiableMap(aMap);
    }

    public static String ElapsedTime(Long time) {
        String elapsedtime = null;
        Date date = new Date(time);
        SimpleDateFormat sfddate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String datee = sfddate.format(date);

        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        Log.i("currentdateandtime", formattedDate);

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(datee);
            d2 = format.parse(formattedDate);

            // in milliseconds
            long diff = d2.getTime() - d1.getTime();
            Log.i("diff", String.valueOf(diff));
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);


            if (diffHours > 0) {

                Log.i("hour", String.valueOf(diffHours));
                elapsedtime = String.valueOf(diffHours) + "h";

                if (diffDays > 0) {
                    Log.i("days", String.valueOf(diffDays));
                    elapsedtime = String.valueOf(diffDays) + "d";
                }


            } else {
                Log.i("mns", String.valueOf(diffMinutes));
                elapsedtime = String.valueOf(diffMinutes) + "mi";
                if (diffMinutes < 1) {
                    elapsedtime = "now";
                }
            }

            Log.i("time", " " + diffDays + " " + diffHours + " " + diffMinutes);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return elapsedtime;
    }

    public static Phonenumber.PhoneNumber getCountrycodewithnumber(Context context, String phonenumber) throws NumberParseException {

        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String usersCountryISOCode = manager.getNetworkCountryIso();
        Log.i("FamilyG", "Country code : " + usersCountryISOCode);
        //Log.i("FamilyG", "Original number : " + phoneNo);
        Phonenumber.PhoneNumber phone = PhoneNumberUtil.getInstance().parseAndKeepRawInput(phonenumber, usersCountryISOCode.toUpperCase());


        return phone;
    }

    public static byte[] convertUrlToByteArray(String path) {
        try {
            Log.d("Utility", "path = " + path);
            FileInputStream fis = new FileInputStream(path);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];

            for (int readNum; (readNum = fis.read(b)) != -1; ) {
                bos.write(b, 0, readNum);
            }

            byte[] bytes = bos.toByteArray();

            return bytes;
        } catch (Exception e) {
            Log.e("Utility", "convertUrlToByteArray = " + e.getMessage());
            return null;
        } catch (OutOfMemoryError e) {
            Log.e("Utility", "convertUrlToByteArray OutOfMemoryError = " + e.getMessage());
            return null;
        }
    }

    public static String currentLocationtoJSON(double latitude, double longitude, String secret_key) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("secret_key", secret_key);
            jsonObject.put("lat", latitude);
            jsonObject.put("lon", longitude);
            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

    public static Bitmap decodeSampledBitmapFromResource(Uri uploadUri, int reqWidth, int reqHeight, Context context) {
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(context.getContentResolver()
                    .openInputStream(uploadUri), null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize1(options, reqWidth, reqHeight);
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(context.getContentResolver()
                    .openInputStream(uploadUri), null, options);
        } catch (Exception e) {
            Log.e("Utility", "decodeSampledBitmapFromResource E = " + e.getMessage());
            return null;
        }

    }

    public static int calculateInSampleSize1(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String[] getStringArray(JSONArray jsonArray) {
        ArrayList<String> stringArrayList = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                stringArrayList.add(jsonArray.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String[] stringArray = new String[stringArrayList.size()];
        stringArrayList.toArray(stringArray);
        return stringArray;
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUri(final Context context, final Uri uri) {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.parseLong(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } catch (Exception e) {
            Log.e("Utility", "getDataColumn Exception " + e.getMessage());
            e.printStackTrace();
            return getFilePathFromURI(context, uri);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static Date getLocalDate(String ourDate) {
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date value = dateFormatter.parse(ourDate);
            dateFormatter.setTimeZone(TimeZone.getDefault());
            return dateFormatter.parse(dateFormatter.format(value));
        } catch (Exception e) {
            return null;
        }
    }

    public static Date getDateFromDateTime(Date dateTime) {
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormatter.parse(dateFormatter.format(dateTime));
        } catch (Exception e) {
            return null;
        }
    }

    public static String getDayFromDate(Date thatDay, Context context) {
        String day = "";
        try {
            Date today = new Date(getNetworkTime(context));
            long diff = today.getTime() - thatDay.getTime();

            int dayDiff = (int) diff / (24 * 60 * 60 * 1000);
            System.out.println("diff = " + dayDiff);
            if (dayDiff == 0) {
                day = "Today";
            } else if (dayDiff == 1) {
                day = "Yesterday";
            } else if (dayDiff < 7) {
                SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
                day = simpleDateformat.format(thatDay);
            } else {
                String dateFormat = "dd/MM/yyyy";
                SimpleDateFormat dt = new SimpleDateFormat(dateFormat);
                day = dt.format(thatDay);
            }
        } catch (Exception e) {
            Log.e("Utility", "getDayFromDate = " + e.getMessage());
        }
        return day;
    }

    public static void createAppPackages(Context activity) {
        File extDirectory = Environment.getExternalStorageDirectory();
        String appName = activity.getResources().getString(R.string.app_name);

        // Creating appDirectory
        File directoryAppName = new File(extDirectory + File.separator + appName);
        createDirectoryIfNotExist(directoryAppName);

        String cameratemp = activity.getResources().getString(R.string.cameratemp);
        // Creating CameraTemp Directory
        File directoryCameraTemp = new File(directoryAppName + File.separator + cameratemp);
        createDirectoryIfNotExist(directoryCameraTemp);

        // Creating Pictures Directory
        File directoryPictures = new File(directoryAppName + File.separator + Environment.DIRECTORY_PICTURES);
        createDirectoryIfNotExist(directoryPictures);
        // Creating Movies Directory
        File directoryMovies = new File(directoryAppName + File.separator + Environment.DIRECTORY_MOVIES);
        createDirectoryIfNotExist(directoryMovies);
        // Creating Music Directory
        File directoryMusic = new File(directoryAppName + File.separator + Environment.DIRECTORY_MUSIC);
        createDirectoryIfNotExist(directoryMusic);

        String receivedStr = activity.getResources().getString(R.string.received);
        String profilepictures = activity.getResources().getString(R.string.profilepictures);

        // Creating Pictures/Received Directory
        File directoryPicturesReceived = new File(directoryPictures + File.separator + receivedStr);
        createDirectoryIfNotExist(directoryPicturesReceived);
        // Creating Pictures/ProfilePictures Directory
        File directoryProfilePictures = new File(directoryPictures + File.separator + profilepictures);
        createDirectoryIfNotExist(directoryProfilePictures);
        // Creating Movies/Received Directory
        File directoryMoviesReceived = new File(directoryMovies + File.separator + receivedStr);
        createDirectoryIfNotExist(directoryMoviesReceived);
        // Creating Music/Received Directory
        File directoryMusicReceived = new File(directoryMusic + File.separator + receivedStr);
        createDirectoryIfNotExist(directoryMusicReceived);
    }

    public static File getReceivedDirectoryByType(String type, Context context) {
        File extDirectory = Environment.getExternalStorageDirectory();
        String appName = context.getResources().getString(R.string.app_name);
        String receivedStr = context.getResources().getString(R.string.received);
        return new File(extDirectory + File.separator + appName + File.separator + type + File.separator + receivedStr);
    }

    public static File getProfilePicturesDirectory(Context context) {
        File extDirectory = Environment.getExternalStorageDirectory();
        String appName = context.getResources().getString(R.string.app_name);
        String profilepictures = context.getResources().getString(R.string.profilepictures);
        return new File(extDirectory + File.separator + appName + File.separator + Environment.DIRECTORY_PICTURES + File.separator + profilepictures);
    }


    public static File getCameraFolderTemp(Context context) {
        File extDirectory = Environment.getExternalStorageDirectory();
        String appName = context.getResources().getString(R.string.app_name);
        String cameratemp = context.getResources().getString(R.string.cameratemp);
        File directory = new File(extDirectory + File.separator + appName + File.separator + cameratemp);
        return directory;
    }

    public static void createDirectoryIfNotExist(File givenDirectory) {
        if (!givenDirectory.exists()) {
            givenDirectory.mkdir();
        }
    }

    public static File createImageFile(Context context) {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File cameraFolderTemp = Utility.getCameraFolderTemp(context);
            File imageFile = new File(cameraFolderTemp + File.separator + timeStamp + ".jpg");
            return imageFile;
        } catch (Exception e) {
            Log.e("Utility", "createImageFile " + e.getMessage());
            return null;
        }
    }

    public static void logFA(FirebaseAnalytics fa, String id, String name, String type) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("name", name);
        bundle.putString("type", type);
        fa.logEvent("action", bundle);
    }


//image compression code starts here

    public static String compressImage(Context context, Uri imageUri) {
        try {
            String filePath = getRealPathFromUri(context, imageUri);
            Bitmap scaledBitmap = null, bmp = null;

            BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            options.inSampleSize = 2;
            BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

//      setting inSampleSize value allows to load a scaled down version of the original image

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
//          load the bitmap from its path
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
                return null;
            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
                return null;
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                        true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            FileOutputStream out = null;
            String filename = getFilename(context, Environment.DIRECTORY_PICTURES, ".png");
            try {
                out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Log.d("filename", filename);

            return filename;
        } catch (Exception e) {
            Log.e("Utility", "compressImage: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static String getFilename(Context context, String type, String fileExtension) {
        File receivedDirectory = Utility.getReceivedDirectoryByType(type, context);
        return (receivedDirectory.getAbsolutePath() + "/" + System.currentTimeMillis() + fileExtension);

    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    //image compression code ends here


    public static String getFilePathFromURI(Context context, Uri contentUri) {
        String fileType = getMimeTypeFromFile(context, contentUri);
        return createTempShareFile1(contentUri, fileType, context).getAbsolutePath();

    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        OutputStream out = null;
        try {
            InputStream in = context.getContentResolver().openInputStream(srcUri);
            if (in == null) return;
            out = new FileOutputStream(dstFile);


            byte[] buffer = new byte[1024];
            int len = in.read(buffer);
            while (len != -1) {
                out.write(buffer, 0, len);
                len = in.read(buffer);
            }
            in.close();
            out.close();
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String surroundWithDoubleQuotes(String givenString, boolean evenEmpty) {
        if (!givenString.isEmpty() || evenEmpty) {
            givenString = "\"" + givenString + "\"";
        }
        return givenString;
    }

    public static void sendChatContent(final ChatMessageModel chatMessageModel, final Context context, final boolean isReload, ChatMessageModel newSelectedChatMessageModel, ChatMessageModel selectedChatMessageModel, final ListView listview_chatmessage, final ChatMessageAdapter chatMessageAdapter, final JSONObject msgJson) {
        try {

            if (selectedChatMessageModel != null && selectedChatMessageModel.isSelected()) {
                selectedChatMessageModel.setSelected(false);
            }

            String GCM_SENDER_ID = context.getApplicationContext().getString(R.string.gcm_defaultSenderId);
            Log.i("GCM_SENDER_ID", "GCM_SENDER_ID " + GCM_SENDER_ID);

            int randomNum = (int) (Math.random() * 50000);

            String id = Integer.toString(randomNum);

            Log.i(TAG, "sendChatContent " + id + " and " + String.valueOf(msgJson) + " and " + chatMessageModel.getChatType0or1() + " and " + chatMessageModel.getFid() + " and " + UserModel.getInstance().getSecret_key());

            JSONObject chatJsonObject = new JSONObject();
            chatJsonObject.put("type", chatMessageModel.getChatType0or1());
            chatJsonObject.put("key", UserModel.getInstance().getSecret_key());
            chatJsonObject.put("to", chatMessageModel.getFid());
            chatJsonObject.put("mode", context.getString(R.string.app_name));
            chatJsonObject.put("msg", msgJson);
            chatJsonObject.put("messageId", chatMessageModel.getMessageId());

            APIManager sendChatApi = new APIManager("chat/send", chatJsonObject.toString().getBytes(Charset.forName("UTF-8")), new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        Log.d(TAG, "chat message sent " + result.toString() + " and " + chatMessageModel.getFid());
                        DataManager dataManager = DataManager.getInstance(context.getApplicationContext());
                        dataManager.persistChatHistory(String.valueOf(msgJson), result.getString("to"), context, 1, result.getString("messageId"), 1);
//                        chatMessageModel.setAcknowledgementStatus(1);
//                        Utility.notifyDataSetChangedWithoutScroll(listview_chatmessage, chatMessageAdapter);
                        if (!msgJson.getString("exstraType").equals(context.getString(R.string.text)) && isReload) {
                            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                            if (cn.getShortClassName().equals(".ChatMessageActivity")) {
                                ChatMessageActivity.chatMessageActivity.finish();
                                ChatMessageActivity.chatMessageActivity.overridePendingTransition(0, 0);
                                ChatMessageActivity.chatMessageActivity.startActivity(ChatMessageActivity.chatMessageActivity.getIntent());
                                ChatMessageActivity.chatMessageActivity.overridePendingTransition(0, 0);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "chat message sent " + array.length());
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.d(TAG, errorMessage);
                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                }
            }, APIManager.HTTP_METHOD.POST);
            sendChatApi.execute();

//            FirebaseMessaging.getInstance().send(new RemoteMessage.Builder(GCM_SENDER_ID + "@gcm.googleapis.com").setMessageId(id)
//                    .addData("type", chatMessageModel.getChatType0or1())
//                    .addData("key", UserModel.getInstance().getSecret_key())
//                    .addData("to", chatMessageModel.getFid())
//                    .addData("mode", "fcm")
//                    .addData("msg", msgJson.toString()).setTtl(86400)
//                    .build());

        } catch (Exception e) {
            Log.e("ChatFragment", "sendChatContent = " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void forceCloseKeyboard(Context context, View currentFocus) {
        if (currentFocus != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    public static File createAudioFile(Context context) {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File musicDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, context);
            File musicFile = new File(musicDirectory + File.separator + timeStamp + ".aac");
            return musicFile;
        } catch (Exception e) {
            Log.e("Utility", "createAudioFile " + e.getMessage());
            return null;
        }
    }

    public static void showProfilePicture(String fileUrl, final Context context, ImageView imageView, final String title, boolean showProfileWhenClick) {

        try {
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File directory = Utility.getProfilePicturesDirectory(context);

            final File fileCheck = new File(directory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d(TAG, "showProfilePicture if " + fileCheck.getAbsolutePath());
                DownloadProfilePicture downloadFileTask = new DownloadProfilePicture(context, fileCheck, imageView, showProfileWhenClick);
                downloadFileTask.execute(fileUrl);
            } else {
                Log.d("ImagePost = ", "showProfilePicture else " + fileCheck.getAbsolutePath());
                final Uri uri = Utility.getUriFromFile(context, fileCheck);
                final Bitmap bitmap = Utility.decodeSampledBitmapFromResource(uri, 50, 50, context);
                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 3, stream);
                imageView.setImageBitmap(bitmap);
                Log.d("heightwidth", imageView.getHeight() + " and " + imageView.getWidth() + " and " + bitmap.getHeight() + " and " + bitmap.getWidth());
                if (showProfileWhenClick) {
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (fileCheck.exists()) {
                                    Dialog imageDialog = new Dialog(context, android.R.style.Theme_Dialog);
                                    ImageView imageview = new ImageView(context);
                                    Bitmap myBitmap = BitmapFactory.decodeFile(fileCheck.getAbsolutePath());
                                    imageview.setImageBitmap(myBitmap);
                                    imageDialog.setContentView(imageview);
                                    imageDialog.setTitle(title);
                                    imageDialog.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateFavouritesList(final Context context) {
        APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", UserModel.getInstance(), new APIListener() {
            @Override
            public void onSuccess(JSONObject result) {
                Log.d("ChatFragment", "inside onsuccess");
            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.d(TAG, "Utility.profileModelMap reload");
                try {
                    Utility.profileModelMap = new HashMap<String, ProfileModel>();
                    for (int k = 0; k < array.length(); k++) {
                        JSONObject object = array.getJSONObject(k);
                        Log.d("resultString", object.getString("fid"));
                        ProfileModel profileModel = new ProfileModel();
                        profileModel = profileModel.toObject(object.toString());
                        if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                            String profileModelKey = profileModel.getFid();
                            Utility.profileModelMap.put(profileModelKey, profileModel);
                        }
                    }
                    DataManager dataManager = DataManager.getInstance(context);
                    dataManager.insertChatFavouritesAndGroups(Utility.profileModelMap, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Log.d(TAG, errorMessage);
            }
        });
        getFavouriteListApi.execute();
    }

    public static Uri getUriFromFile(Context context, File imageFile) {
        //Requires - devazar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(context, "org.officeg.mobileapp.fileprovider", imageFile);
        } else {
            return Uri.fromFile(imageFile);
        }
    }

    public static Uri getUriFromFileForShareVideo(Context context, File imageFile) {
        return Uri.fromFile(imageFile);
    }

    public static File createTempShareFile(File fileCheck, File receivedDirectory, String contentType, Context context) {
        try {
            if (contentType.equalsIgnoreCase(context.getString(R.string.image))) {
                return fileCheck;
            } else if (contentType.equalsIgnoreCase(context.getString(R.string.audio))) {
                File tempShareFile = new File(receivedDirectory, "Audio.aac");
                tempShareFile.createNewFile();
                Utility.copy(context, Utility.getUriFromFile(context, fileCheck), tempShareFile);
                return tempShareFile;
            } else if (contentType.equalsIgnoreCase(context.getString(R.string.video))) {
                File tempShareFile = new File(receivedDirectory, "Video.mp4");
                tempShareFile.createNewFile();
                Utility.copy(context, Utility.getUriFromFile(context, fileCheck), tempShareFile);
                return tempShareFile;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File createTempShareFile1(Uri uri, String contentType, Context context) {
        try {
            if (contentType.equalsIgnoreCase(context.getString(R.string.image))) {
                File receivedDirectory = getReceivedDirectoryByType(Environment.DIRECTORY_PICTURES, context);
                File tempShareFile = new File(receivedDirectory, "Image.png");
                tempShareFile.createNewFile();
                Utility.copy(context, uri, tempShareFile);
                return tempShareFile;
            } else if (contentType.equalsIgnoreCase(context.getString(R.string.audio))) {
                File receivedDirectory = getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, context);
                File tempShareFile = new File(receivedDirectory, "Audio.aac");
                tempShareFile.createNewFile();
                Utility.copy(context, uri, tempShareFile);
                return tempShareFile;
            } else if (contentType.equalsIgnoreCase(context.getString(R.string.video))) {
                File receivedDirectory = getReceivedDirectoryByType(Environment.DIRECTORY_MOVIES, context);
                File tempShareFile = new File(receivedDirectory, "Video.mp4");
                tempShareFile.createNewFile();
                Utility.copy(context, uri, tempShareFile);
                return tempShareFile;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMimeTypeFromFile(Context context, Uri uri) {
        try {
            final String uriPath =
                    URLEncoder.encode(uri.getPath(), "UTF-8");
            String extension = MimeTypeMap.getFileExtensionFromUrl(uriPath);
            ContentResolver cR = context.getContentResolver();
            String type = cR.getType(uri);

            if (type == null)
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

            if (type.contains(context.getString(R.string.image))) {
                return context.getString(R.string.image);
            } else if (type.contains(context.getString(R.string.audio))) {
                return context.getString(R.string.audio);
            } else if (type.contains(context.getString(R.string.video))) {
                return context.getString(R.string.video);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void sendExtraNotification(Context context, GroupModel groupModel, String extraType, String extraString) {
        try {
            ChatMessageModel chatMessageModel = new ChatMessageModel();
            chatMessageModel.setMessageContentType(extraType);
            chatMessageModel.setExtraString(extraString);
            chatMessageModel.setChatType0or1("1");
            chatMessageModel.setFid(groupModel.getGid());
            chatMessageModel.setGid(groupModel.getGid());
            JSONObject msgJson = Utility.saveInProgressChatMessage(chatMessageModel, context, null, null);
            Utility.sendChatContent(chatMessageModel, context, false, null, null, null, null, msgJson);
        } catch (Exception e) {
            Log.e(TAG, "sendExtraNotification = " + e.getMessage());
        }
    }

    public static float getFilesize(String imagepath) {
        File file = new File(imagepath);
        Long fileSizeInBytes = file.length();
        float fileSizeInKB = fileSizeInBytes / 1024;
        Log.i("FamilyG", "fileSize in KB " + fileSizeInKB);
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        float fileSizeInMB = fileSizeInKB / 1024;
        //String filesizeinMB=Float.toString(fileSizeInMB);
        Log.i("FamilyG", "fileSize in MB " + fileSizeInMB);
        return fileSizeInMB;
    }

    public static void showProfileDialog(JSONObject object, Activity context) {
        Log.i("FamilyG", "view profile " + object);
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.view_profile);
        CircleImageView imageview = dialog.findViewById(R.id.profileview_image_id);
        TextView displayname = dialog.findViewById(R.id.dispaly_name);
        TextView firstname = dialog.findViewById(R.id.first_name);
        TextView surname = dialog.findViewById(R.id.surname);
        TextView gender = dialog.findViewById(R.id.gender);
        TextView dob = dialog.findViewById(R.id.birth);
        TextView mobile = dialog.findViewById(R.id.mobile);
        Button ok = dialog.findViewById(R.id.dialog_ok);
        LinearLayout relationShipLL = dialog.findViewById(R.id.linear_relationship);
        relationShipLL.setVisibility(View.GONE);
        try {
            if ((object.getString("profileimage") != null) && (!object.getString("profileimage").isEmpty())) {
                ImageLoader imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(object.getString("profileimage"), imageview);
            } else {

                imageview.setImageResource(R.drawable.default_profile_image);

            }
            if ((object.getString("displayname") != null) && (!object.getString("displayname").isEmpty())) {
                displayname.setText(object.getString("displayname"));
            } else {
                displayname.setText("N/A");
            }

            if ((object.getString("firstname") != null) && (!object.getString("firstname").isEmpty())) {
                firstname.setText(object.getString("firstname"));
            } else {
                firstname.setText("N/A");
            }
            if ((object.getString("lastname") != null) && (!object.getString("lastname").isEmpty())) {
                surname.setText(object.getString("lastname"));
            } else {
                surname.setText("N/A");
            }
            if ((object.getString("gender") != null) && (!object.getString("gender").isEmpty())) {
                gender.setText(object.getString("gender"));
            } else {
                gender.setText("N/A");
            }
            if ((object.getString("dob") != null) && (!object.getString("dob").isEmpty())) {
                dob.setText(object.getString("dob"));
            } else {
                dob.setText("N/A");
            }
            if ((object.getString("mobilenumber") != null) && (!object.getString("mobilenumber").isEmpty())) {
                mobile.setText(object.getString("mobilenumber"));
            } else {
                mobile.setText("N/A");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public static long getNetworkTime(Context context) {
        try {
            if (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME) == 1) {
                return new Date().getTime();
            } else {
                String[] hosts = new String[]{
                        "ntp02.oal.ul.pt", "ntp04.oal.ul.pt",
                        "ntp.xs4all.nl"};
                NTPUDPClient client = new NTPUDPClient();
                // We want to timeout if a response takes longer than 5 seconds
                client.setDefaultTimeout(5000);
                for (String host : hosts) {
                    try {
                        InetAddress hostAddr = InetAddress.getByName(host);
                        TimeInfo info = client.getTime(hostAddr);
                        return info.getMessage().getReceiveTimeStamp().getTime();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                client.close();
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean isToday(Date dateUnderTest, Context context) {
        boolean isToday = false;
        Date today = new Date(getNetworkTime(context));
        if (removeTime(today).equals(removeTime(dateUnderTest)))
            isToday = true;
        return isToday;
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static void setChatAndFeedCount(BottomNavigationView bottomNavigation, Context context, boolean isChatActive, boolean isFeedActive) {
        BottomNavigationMenuView mbottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigation.getChildAt(0);

        View view = mbottomNavigationMenuView.getChildAt(1);
        BottomNavigationItemView itemView = (BottomNavigationItemView) view;

        View cart_badge = LayoutInflater.from(context)
                .inflate(R.layout.bottombar_badge,
                        mbottomNavigationMenuView, false);

        TextView tv_badge = cart_badge.findViewById(R.id.tv_badge);

        if (isChatActive) {
            tv_badge.setBackgroundResource(R.drawable.badge_background_red);
        } else {
            tv_badge.setBackgroundResource(R.drawable.badge_background_grey);
        }

        DataManager dataManager = DataManager.getInstance(context);
        int unreadCount = dataManager.getUnreadChatMessagesCount();

        tv_badge.setText(String.valueOf(unreadCount));

        if (itemView.getChildAt(2) != null) {
            itemView.removeViewAt(2);
        }
        if (unreadCount != 0) {
            itemView.addView(cart_badge, 2);
        }

        //for feed
        BottomNavigationMenuView mbottomNavigationMenuViewFeed =
                (BottomNavigationMenuView) bottomNavigation.getChildAt(0);

        View viewFeed = mbottomNavigationMenuViewFeed.getChildAt(0);
        BottomNavigationItemView itemViewFeed = (BottomNavigationItemView) viewFeed;

        View cart_badgeFeed = LayoutInflater.from(context)
                .inflate(R.layout.bottombar_badge,
                        mbottomNavigationMenuViewFeed, false);

        TextView tv_badgeFeed = cart_badgeFeed.findViewById(R.id.tv_badge);

        if (isFeedActive) {
            tv_badgeFeed.setBackgroundResource(R.drawable.badge_background_red);
        } else {
            tv_badgeFeed.setBackgroundResource(R.drawable.badge_background_grey);
        }

        int unreadCountFeed = dataManager.getUnreadFeedMessagesCount();

        tv_badgeFeed.setText("");

        if (itemViewFeed.getChildAt(2) != null) {
            itemViewFeed.removeViewAt(2);
        }
        if (unreadCountFeed != 0) {
            itemViewFeed.addView(cart_badgeFeed, 2);
        }

    }

    public static JSONArray listToJsonArray(List<HashMap<String, String>> list) {
        try {
            JSONObject jsonobjdetails = null;
            Log.i("FamilyG", "listsie " + list.size());
            JSONArray jArray = new JSONArray();

            for (int i = 0; i < list.size(); i++) {
                jsonobjdetails = new JSONObject();
                jsonobjdetails.put("fid", list.get(i).get("fid"));
                jsonobjdetails.put("displayname", list.get(i).get("displayname"));
                jsonobjdetails.put("firstname", list.get(i).get("firstname"));
                jsonobjdetails.put("lastname", list.get(i).get("lastname"));
                jsonobjdetails.put("dob", list.get(i).get("dob"));
                jsonobjdetails.put("gender", list.get(i).get("gender"));
                jsonobjdetails.put("mobilenumber", list.get(i).get("mobilenumber"));
                jsonobjdetails.put("doi", list.get(i).get("doi"));
                jsonobjdetails.put("deceaseddate", list.get(i).get("deceaseddate"));
                if (list.get(i).get("isowner").equals("1")) {
                    jsonobjdetails.put("isowner", true);
                } else {
                    jsonobjdetails.put("isowner", false);
                }
                jsonobjdetails.put("isfav", list.get(i).get("isfav"));
                jsonobjdetails.put("ownerfid", list.get(i).get("ownerfid"));
                jsonobjdetails.put("profileimage", list.get(i).get("profileimage"));
                jsonobjdetails.put("status", list.get(i).get("status"));
                jsonobjdetails.put("relationtype", list.get(i).get("relationtype"));
                if (list.get(i).get("isalive").equals("1")) {
                    jsonobjdetails.put("isalive", true);
                } else {
                    jsonobjdetails.put("isalive", false);
                }
                if (list.get(i).get("isfirstdegree").equals("1")) {
                    jsonobjdetails.put("isfirstdegree", true);
                } else {
                    jsonobjdetails.put("isfirstdegree", false);
                }
                jArray.put(jsonobjdetails);
            }

            Log.i("FamilyG", "JSONARRAY is" + jArray);
            return jArray;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public static void insertBadgeIntoDB(String fidOfGid, String content, String postid, String type, int count, Context context) {
        DataManager dataManager = DataManager.getInstance(context);
        ContentValues row = new ContentValues();
        row.put("fidOrGid", fidOfGid);
        row.put("content", content);
        row.put("postid", postid);
        row.put("type", type);
        row.put("count", count);

        Date date = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dt.setTimeZone(TimeZone.getTimeZone("GMT"));
        row.put("datetime", dt.format(date));
        dataManager.isUnreadMessageExist(fidOfGid);
        dataManager.insertTable("unreadnotification", row);
    }

    public static String getRandomHexString(int numchars) {
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }

    public static JSONObject saveInProgressChatMessage(ChatMessageModel chatMessageModel, Context context, ChatMessageModel newSelectedChatMessageModel, ChatMessageAdapter chatMessageAdapter) {

        try {
            String messageId = Utility.getUUID();
            chatMessageModel.setMessageId(messageId);
            JSONObject msgJson = new JSONObject();
            convertChatMessageModelToMsgJsonObject(chatMessageModel, msgJson);

            if (newSelectedChatMessageModel != null && newSelectedChatMessageModel.isSelected()) {
                newSelectedChatMessageModel.setSelected(false);
                chatMessageModel.setIsreply(true);
                chatMessageModel.setSourceMessageForReply(newSelectedChatMessageModel.toJSON(newSelectedChatMessageModel));
                if (chatMessageAdapter != null)
                    chatMessageAdapter.notifyDataSetChanged();
                msgJson.put("isreply", true);
                if (newSelectedChatMessageModel.getSourceMessageForReply() != null) {
//                    JSONObject sourceMessageForReplyCMM = new JSONObject(newSelectedChatMessageModel.getSourceMessageForReply());
//                    sourceMessageForReplyCMM.put("totalContent", "");
//                    sourceMessageForReplyCMM.put("sourceMessageForReply", "");
                    newSelectedChatMessageModel.setSourceMessageForReply(null);
                }

                if (newSelectedChatMessageModel.getTotalContent() != null) {
                    JSONObject totalContent = new JSONObject(newSelectedChatMessageModel.getTotalContent());
                    if (totalContent.has("sourceMessageForReply")) {
                        totalContent.remove("sourceMessageForReply");
                    }
                    newSelectedChatMessageModel.setTotalContent(totalContent.toString());
                }
                msgJson.put("sourceMessageForReply", newSelectedChatMessageModel.toJSON(newSelectedChatMessageModel));

            }
            msgJson.put("datetimesent", getGMTTime());

            DataManager dataManager = DataManager.getInstance(context);
            dataManager.persistChatHistory(String.valueOf(msgJson), chatMessageModel.getFid(), context, 0, chatMessageModel.getMessageId(), 0);
            return msgJson;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void convertChatMessageModelToMsgJsonObject(ChatMessageModel chatMessageModel, JSONObject msgJson) {
        try {

            String text = chatMessageModel.getText() == null ? "" : chatMessageModel.getText();
            String image = chatMessageModel.getImage() == null ? "" : chatMessageModel.getImage();
            String video = chatMessageModel.getVideo() == null ? "" : chatMessageModel.getVideo();
            String audio = chatMessageModel.getAudio() == null ? "" : chatMessageModel.getAudio();
            String extraType = chatMessageModel.getMessageContentType() == null ? "" : chatMessageModel.getMessageContentType();
            String extraString = chatMessageModel.getExtraString() == null ? "" : chatMessageModel.getExtraString();

            msgJson.put("content", text);

            JSONArray jsonImagesArray = new JSONArray();
            if (!image.isEmpty())
                jsonImagesArray.put(image);

            msgJson.put("images", jsonImagesArray);

            JSONArray jsonAudiosArray = new JSONArray();
            if (!audio.isEmpty())
                jsonAudiosArray.put(audio);

            msgJson.put("audios", jsonAudiosArray);

            JSONArray jsonVideosArray = new JSONArray();
            if (!video.isEmpty())
                jsonVideosArray.put(video);

            msgJson.put("videos", jsonVideosArray);

            msgJson.put("fid", UserModel.getInstance().getFid());
            if (chatMessageModel.getChatType0or1().equalsIgnoreCase("1")) {
                msgJson.put("gid", chatMessageModel.getGid());
            }
            msgJson.put("extraType", extraType);
            msgJson.put("extraString", extraString);
        } catch (Exception e) {
            Log.e(TAG, "convertChatMessageModelToMsgJsonObject = " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static JSONArray sortJsonArray(JSONArray jsonArray, Context context) {
        try {
            JSONArray sortedJsonArray = new JSONArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject row = jsonArray.getJSONObject(i);

                switch (row.getString("relationtype")) {
                    case "MOTHER": {
                        row.put("relationtype", "AAA");
                        break;
                    }
                    case "FATHER": {
                        row.put("relationtype", "BBB");
                        break;
                    }
                    case "SISTER": {
                        row.put("relationtype", "CCC");
                        break;
                    }
                    case "BROTHER": {
                        row.put("relationtype", "DDD");
                        break;
                    }
                    case "WIFE": {
                        row.put("relationtype", "EEE");
                        break;
                    }
                    case "HUSBAND": {
                        row.put("relationtype", "FFF");
                        break;
                    }
                    case "DAUGHTER": {
                        row.put("relationtype", "GGG");
                        break;
                    }
                    case "SON": {
                        row.put("relationtype", "HHH");
                        break;
                    }
                    case "BOSS": {
                        row.put("relationtype", "III");
                        break;
                    }
                    case "PEER": {
                        row.put("relationtype", "JJJ");
                        break;
                    }
                    case "JUNIOR": {
                        row.put("relationtype", "KKK");
                        break;
                    }
                }
            }
            List<JSONObject> jsonList = new ArrayList<JSONObject>();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonList.add(jsonArray.getJSONObject(i));
            }
            Collections.sort(jsonList, new Comparator<JSONObject>() {

                public int compare(JSONObject a, JSONObject b) {
                    String valA = "";
                    String valB = "";

                    try {
                        valA = (String) a.get("relationtype");
                        valB = (String) b.get("relationtype");
                    } catch (JSONException e) {
                        //do something
                    }

                    return valA.compareTo(valB);
                }
            });
            for (int i = 0; i < jsonArray.length(); i++) {
                sortedJsonArray.put(jsonList.get(i));
            }
            for (int i = 0; i < sortedJsonArray.length(); i++) {
                JSONObject row = sortedJsonArray.getJSONObject(i);

                switch (row.getString("relationtype")) {
                    case "AAA": {
                        row.put("relationtype", "MOTHER");
                        break;
                    }
                    case "BBB": {
                        row.put("relationtype", "FATHER");
                        break;
                    }
                    case "CCC": {
                        row.put("relationtype", "SISTER");
                        break;
                    }
                    case "DDD": {
                        row.put("relationtype", "BROTHER");
                        break;
                    }
                    case "EEE": {
                        row.put("relationtype", "WIFE");
                        break;
                    }
                    case "FFF": {
                        row.put("relationtype", "HUSBAND");
                        break;
                    }
                    case "GGG": {
                        row.put("relationtype", "DAUGHTER");
                        break;
                    }
                    case "HHH": {
                        row.put("relationtype", "SON");
                        break;
                    }
                    case "III": {
                        row.put("relationtype", "BOSS");
                        break;
                    }
                    case "JJJ": {
                        row.put("relationtype", "PEER");
                        break;
                    }
                    case "KKK": {
                        row.put("relationtype", "JUNIOR");
                        break;
                    }
                }
            }
            return sortedJsonArray;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public static void notifyDataSetChangedWithoutScroll(final ListView listview_chatmessage, ChatMessageAdapter chatMessageAdapter) {
        listview_chatmessage.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
        chatMessageAdapter.notifyDataSetChanged();
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(200);
                    listview_chatmessage.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static long getGMTTime() {
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        return now;
    }
}