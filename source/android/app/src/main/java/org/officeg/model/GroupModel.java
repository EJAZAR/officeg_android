package org.officeg.model;

import android.util.Log;

import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;

public class GroupModel implements Serializable {
    private String fid;
    private String secret_key;
    private String name;
    private String gid;
    private String url;
    private String[] members;
    private String[] admins;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String[] getAdmins() {
        return admins;
    }

    public void setAdmins(String[] admins) {
        this.admins = admins;
    }

    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("secret_key", getSecret_key());
            jsonObject.put("name", getName());
            jsonObject.put("gid", getGid());
            jsonObject.put("fid", getFid());
            jsonObject.put("url", getUrl());
            if (getMembers() != null) {
                JSONArray members = new JSONArray(Arrays.asList(getMembers()));
                jsonObject.put("members", members);
            }
            if (getAdmins() != null) {
                JSONArray admins = new JSONArray(Arrays.asList(getAdmins()));
                jsonObject.put("admins", admins);
            }
            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

    public GroupModel toObject(String jsonString) {

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            String name = jsonObject.has("name") ? jsonObject.getString("name") : null;
            setName(name);

            String gid = jsonObject.has("gid") ? jsonObject.getString("gid") : null;
            setGid(gid);

            String url = jsonObject.has("url") ? jsonObject.getString("url") : "";
            setUrl(url);

            JSONArray membersString = jsonObject.has("members") ? jsonObject.getJSONArray("members") : null;
            setMembers(Utility.getStringArray(membersString));

            JSONArray adminsString = jsonObject.has("admins") ? jsonObject.getJSONArray("admins") : null;
            setAdmins(Utility.getStringArray(adminsString));

            return this;
        } catch (Exception e) {
            Log.e("GroupModel", "toObject e = " + e.getMessage());
            return null;
        }

    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getMembers() {
        return members;
    }

    public void setMembers(String[] members) {
        this.members = members;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}