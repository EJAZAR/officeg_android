package org.officeg.mobileapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.firebase.SharedPrefManager;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

import static org.officeg.mobileapp.VerifyOTPActivity.SHARED_PREF;

public class RegistrationActivity extends Activity implements AdapterView.OnItemSelectedListener {

    String phoneNumber, mobileNumberPattern, country, code, token;
    Button btnSendOtp;
    TextView error, country_code_view;
    String[] codes_array, country_array, codes_array_split;
    SearchableSpinner selected_country;
    int index_code;
    UserModel user = null;
    String res, otp;
    String CountryZipCode = "";
    int check = 0;
    Boolean existingNumber;
    int phoneLength;
    EditText edtxtphnumb;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
//        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        if (!networkCheck)
            Toast.makeText(getApplicationContext(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        edtxtphnumb = (EditText) findViewById(R.id.ph_numb);
        error = (TextView) findViewById(R.id.id_mobile_number_err_msg);

//         Toast.makeText(getApplicationContext(), "network available", Toast.LENGTH_LONG).show();
        SharedPreferences settings = getSharedPreferences(SHARED_PREF, 0);

        if (settings.getString("Secret key", "").equals("")) {
            Log.i("FamilyG", "Enter number");
        } else {
            //Go to login activity
            Intent i = new Intent(RegistrationActivity.this, BottomBarActivity.class);
            startActivity(i);
            finish();
        }
        codes_array = getResources().getStringArray(R.array.country_codes);


        country_array = getResources().getStringArray(R.array.country_arrays);

        selected_country = (SearchableSpinner) findViewById(R.id.spinner_country);
        selected_country.setTitle("Select Country");
        country = selected_country.getSelectedItem().toString();

        index_code = Arrays.asList(country_array).indexOf(country);

        code = Arrays.asList(codes_array).get(index_code);
        Log.i("newcountryCode", code);
        country_code_view = (TextView) findViewById(R.id.ccode_view);


        String CountryID = "";
        String Country = "";


        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        Locale loc = new Locale("", CountryID);
        String displayName = loc.getDisplayCountry();
        Log.d("FamilyG", "User is from " + displayName);
        selected_country.setPrompt(displayName);


        String[] country_name = this.getResources().getStringArray(R.array.country_arrays);
        for (int i = 0; i < country_name.length; i++) {
            String g = country_name[i];
            if (g.equals(displayName)) {
                Country = g;
                Log.i("FamilyG", "Country : " + Country);
                selected_country.setSelection(i);
                break;
            }
        }

        //String[] rl=this.getResources().getStringArray(R.array.country_codes);
        for (int i = 0; i < codes_array.length; i++) {
            String[] g = codes_array[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                Log.i("FamilyG", "CountryZip code : " + CountryZipCode);
                break;
            }
        }
        country_code_view.setText("+" + CountryZipCode);

        selected_country.setOnItemSelectedListener(this);
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try {
            int number = Integer.parseInt(CountryZipCode.replaceAll("\\s+", ""));
            Log.i("FamilyG", "" + number);
            String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(number);
            String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
            phoneLength = exampleNumber.length();
            Log.i("FamilyG", "length " + phoneLength);
        } catch (Exception e) {
            e.printStackTrace();
        }

        edtxtphnumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtxtphnumb.setCursorVisible(true);
                error.setError(null);
                error.setText(null);
            }
        });

        sendOTP();
    }

    public void sendOTP() {

        btnSendOtp = (Button) findViewById(R.id.btn_otp);
        btnSendOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                edtxtphnumb.setCursorVisible(false);


                Utility.logFA(mFirebaseAnalytics, "Registration", "btnSendOtp", "button");

                token = SharedPrefManager.getInstance(RegistrationActivity.this).getItem("tagtoken");

                /*if (token == null) {
                    Toast.makeText(RegistrationActivity.this, "Token not generated", Toast.LENGTH_LONG).show();
                    return;
                }*/

                mobileNumberPattern = (getString(R.string.mobile_number_pattern));

                codes_array = getResources().getStringArray(R.array.country_codes);
                country_array = getResources().getStringArray(R.array.country_arrays);

                selected_country = (SearchableSpinner) findViewById(R.id.spinner_country);

                country = selected_country.getSelectedItem().toString();

                index_code = Arrays.asList(country_array).indexOf(country);

                code = Arrays.asList(codes_array).get(index_code);
                Log.i("newcountryCode", code);

                phoneNumber = ((EditText) findViewById(R.id.ph_numb)).getText().toString();
                while ((phoneNumber.length() > 1) && (phoneNumber.charAt(0) == '0') && (code.equals("91")))
                    phoneNumber = phoneNumber.substring(1);
                Log.i("replace num", "" + phoneNumber.length());

                Log.i("country", country);
                if (phoneNumber.equals("")) {
                    error.setError("");
                    error.setText(R.string.empty_mobile_number);
                    edtxtphnumb.clearFocus();
                } else if (phoneNumber.replaceAll("[\\D]", "").length() != phoneLength) {
                    error.setError("");
                    error.setText(R.string.invalid_mobile_number);

                } else {
                    Log.i("FG", "calling thread");
                    user = UserModel.getInstance();
                    user.mobilenumber = phoneNumber.replaceAll("[\\D]", "");
                    user.countrycode = code.replaceAll("[a-zA-Z,]", "");
                    user.devicetoken = token;

                    APIManager api = new APIManager("register", user, new APIListener() {

                        @Override
                        public void onSuccess(JSONObject result) {

                            Intent intent_activity = new Intent(RegistrationActivity.this, VerifyOTPActivity.class);
                            Log.i("FC", phoneNumber);
                            Log.i("Firebasetoken", token);
                            try {
                                res = result.getString("refcode");
                                otp = result.getString("otp");
                                existingNumber = result.getBoolean("isnew");
                                Log.i("Ref==", res);
                                user.save();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            intent_activity.putExtra("PhoneNumber", user.mobilenumber);
                            intent_activity.putExtra("CountryCode", user.countrycode);
                            intent_activity.putExtra("Token", user.devicetoken);
                            intent_activity.putExtra("Refcode", res);
                            intent_activity.putExtra("OTP", otp);
                            intent_activity.putExtra("ExistingNumber", existingNumber);
                            startActivity(intent_activity);
                            finish();
                        }

                        @Override
                        public void onSuccess(JSONArray array) {

                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            error.setError("");
                            error.setText(errorMessage);
                        }
                    });

                    api.execute();
                }

            }


        });


        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent,
                               View view, int pos, long id) {
        if (++check > 1) {
            country_code_view = (TextView) findViewById(R.id.ccode_view);
            String countrySelected = parent.getItemAtPosition(pos).toString();
            Log.i("selcountry", country);
            country = selected_country.getSelectedItem().toString();
            index_code = Arrays.asList(country_array).indexOf(countrySelected);
            code = codes_array[index_code];
            Log.i("FamilyG", "code inside onItemclick" + code);
            String[] str = code.split(",");
            country_code_view.setText("+" + str[0]);
            try {
                String number = str[0].replaceAll("\\s+", "");
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(number));
                String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                phoneLength = exampleNumber.length();
                Log.i("FamilyG", "length inside " + phoneLength);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.logFA(mFirebaseAnalytics, "Registration", "CountrySelection", "Spinner");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {


    }


}
