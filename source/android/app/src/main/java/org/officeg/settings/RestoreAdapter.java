package org.officeg.settings;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.mobileapp.R;
import org.officeg.model.UserModel;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class RestoreAdapter extends BaseAdapter {

    LinkedList<String> backupsModels;
    Activity superActivity;
    ProgressDialog PD;
    private LayoutInflater inflater = null;

    public RestoreAdapter(Activity activity) {
        superActivity = activity;
        backupsModels = new LinkedList<String>();
        inflater = (LayoutInflater) activity
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return backupsModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        try {
            String jsonString = backupsModels.get(position);
            final JSONObject jsonObject = new JSONObject(jsonString);
            vi = inflater.inflate(R.layout.backup_list_adaptor, null);
            Long timeStampLong = Long.parseLong(jsonObject.getString("time"));
            TextView textView_restore_date = vi.findViewById(R.id.textview_restore_date);
            Date date = new Date(timeStampLong);
            String timeStamp = new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(date);
            textView_restore_date.setText(timeStamp);

            Button btn_restore = vi.findViewById(R.id.btn_restore);
            btn_restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        PD = ProgressDialog.show(superActivity, "Please Wait", "Please Wait ...", true);
                        PD.setCancelable(true);
                        final String url = jsonObject.getString("url");
                        Thread mThread = new Thread() {
                            @Override
                            public void run() {
                                downloadFile(url, "FamilyGUserDatabase");
                            }
                        };
                        mThread.start();
                    } catch (Exception e) {
                        Log.e("RestoreAdapter", e.getMessage());
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

    public void add(String date) {
        backupsModels.add(date);
    }

    private void downloadFile(String fileURL, String fileName) {
        FileOutputStream f = null;
        try {

            File fileFinal = superActivity.getApplicationContext().getDatabasePath("FamilyGUserDatabase");

            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.connect();
            f = new FileOutputStream(fileFinal);
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            Log.d("fileFinal = ", fileFinal.getAbsolutePath());

            superActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PD.dismiss();
                    Toast.makeText(superActivity, "Backup Restored", Toast.LENGTH_LONG).show();
                    UserModel.getInstance().save();
                }
            });

        } catch (Exception e) {
            Log.e("RestoreAdapter", "downloadFile" + e.getMessage());
            superActivity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(superActivity, "Failed to restore", Toast.LENGTH_LONG).show();
                    PD.dismiss();
                }
            });
        } finally {
            try {
                if (f != null)
                    f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}