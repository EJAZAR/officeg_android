package org.officeg.mobileapp;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.model.UserModel;
import org.officeg.nearby.GPSTracker;
import org.officeg.nearby.NearbyListAdapter;
import org.officeg.nearby.NearbyModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

public class NearByFragment extends Fragment {
    private static final int REQUEST_CODE_PERMISSION = 2;
    private final int REQUEST_LOCATION = 200;
    private final int REQUEST_CHECK_SETTINGS = 300;
    private final int REQUEST_GOOGLE_PLAY_SERVICE = 400;
    public Button checkin;
    UserModel user = null;
    double lat, lon;
    // GPSTracker class
    GPSTracker gps;
    NearbyListAdapter nearbyListAdapter;
    byte[] nearbyListbyte;
    ArrayList<NearbyModel> nearbyarrayList = new ArrayList<>();
    NearbyModel itemsmodel;
    ListView nearbyList;
    private View currentView;
    private OnFragmentInteractionListener mListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    LinearLayout empty_Nearby_List;
    ProgressDialog PD;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Nearby");
        currentView = inflater.inflate(R.layout.fragment_nearby, container, false);
        PD = ProgressDialog.show(getContext(), "Please Wait", "Please Wait ...", true);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        container.removeAllViews();
        nearbyList = currentView.findViewById(R.id.list_nearby);

        checkin = currentView.findViewById(R.id.checkin);
        empty_Nearby_List = currentView.findViewById(R.id.empty_nearby);

        user = UserModel.getInstance();

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            getNearbyPeople();

        } else {
            Log.i("FamilyG", "location permission already granted");
            getNearbyPeople();
        }

        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.logFA(mFirebaseAnalytics, "nearby screen", "share location", "button");
                updateLocation();
            }
        });

        // Inflate the layout for this fragment
        return currentView;
    }

    private void updateLocation() {

        checkin.setVisibility(View.GONE);
        String secret_key = UserModel.getInstance().secret_key;

        gps = new GPSTracker(getActivity());

        // check if GPS enabled
        if (gps.canGetLocation()) {

            lat = gps.getLatitude();
            lon = gps.getLongitude();

            Toast.makeText(getActivity(), "Location shared", Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        String checkinn = Utility.currentLocationtoJSON(lat, lon, secret_key);
        byte[] checkinBytes = checkinn.getBytes(Charset.forName("UTF-8"));
        Log.i("checkin", checkinn);
        user.latitude = lat;
        user.longitude = lon;
        user.save();
        Log.i("lat and long", String.valueOf(user.latitude + " " + user.longitude));

        APIManager api = new APIManager("location/update", checkinBytes, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {

                // checkin.setVisibility(View.GONE);

            }

            @Override
            public void onSuccess(JSONArray object) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                // Toast.makeText(getActivity(),errorMessage,Toast.LENGTH_LONG).show();

            }
        }, APIManager.HTTP_METHOD.POST);

        api.execute();
    }

    private void getNearbyPeople() {
        gps = new GPSTracker(getActivity());
        // check if GPS enabled
        if (gps.canGetLocation()) {

            lat = gps.getLatitude();
            lon = gps.getLongitude();

            /*Toast.makeText(getActivity(), "Your Location is - \nLat: "
                    + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();*/
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        JSONObject object = new JSONObject();
        try {
            object.put("secret_key", UserModel.getInstance().secret_key);
            object.put("lat", lat);
            object.put("lon", lon);
            object.put("doi", UserModel.getInstance().doi);


            Log.i("jobjblock", String.valueOf(object));
            nearbyListbyte = object.toString().getBytes(Charset.forName("UTF-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getContext());
        if (NetworkConnectionreceiver.isConnection) {
            APIManager api = new APIManager("location/nearby", nearbyListbyte, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.i("check", String.valueOf(array));
                    try {
                        for (int index = 0; index < array.length(); index++) {
                            JSONObject nearByPerson = array.getJSONObject(index);
                            itemsmodel = new NearbyModel(nearByPerson);
                            nearbyarrayList.add(itemsmodel);
                        }
                        DataManager dataManager = DataManager.getInstance(getContext());
                        dataManager.insertNearbyData(array);
                        afterGettingNearbyData();
                    } catch (JSONException e) {
                        PD.dismiss();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    PD.dismiss();
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                    Log.i("FamilyG", "errorMessage" + errorMessage);
                }
            }, APIManager.HTTP_METHOD.POST);

            api.execute();
        } else {
            Toast.makeText(getContext(), getString(R.string.nointernet), Toast.LENGTH_LONG).show();
            try {
                DataManager dataManager = DataManager.getInstance(getContext());
                nearbyarrayList = new ArrayList<>(dataManager.getNearByData());
                afterGettingNearbyData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void afterGettingNearbyData() {
        PD.dismiss();
        if (nearbyarrayList != null && !nearbyarrayList.isEmpty()) {
            nearbyListAdapter = new NearbyListAdapter(getActivity(), nearbyarrayList);
            nearbyList.setAdapter(nearbyListAdapter);
            ((BaseAdapter) nearbyList.getAdapter()).notifyDataSetChanged();
        } else {
            empty_Nearby_List.setVisibility(View.VISIBLE);
            nearbyList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_chat_id);
                    return true;
                }
                return false;
            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}

