package org.officeg.feed;


import android.support.annotation.NonNull;

import java.io.Serializable;

public class LikeModel implements Serializable, Comparable<LikeModel> {

    private final long timeCommented;
    private String displayName;
    private String fid;
    private String profileImage;

    public LikeModel(String displayName, long timeCommented, String fid, String profileImage) {
        this.displayName = displayName;
        this.timeCommented = timeCommented;
        this.fid = fid;
        this.profileImage = profileImage;
    }

    public String getName() {
        return displayName;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public long getTimeCommented() {
        return timeCommented;
    }

    @Override
    public int compareTo(@NonNull LikeModel commentModel) {
        return String.valueOf(this.getTimeCommented()).compareTo(String.valueOf(commentModel.getTimeCommented()));
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LikeModel) {
            LikeModel likeModel = (LikeModel) o;
            return (this.fid == likeModel.fid);
        }
        return false;
    }
}
