package org.officeg.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ProfileModel implements Serializable, Cloneable {
    private static ProfileModel profile = new ProfileModel();
    public String mobilenumber;
    public String countrycode;
    public String displayname;
    public String firstname;
    public String lastname;
    public String dob;
    public String gender;
    public String relationship;
    public String fid;
    public String devicetoken;
    public String gid;
    public String isadmin = "false";
    public String tagging = "";
    public String geolocationaccess = "";
    public String pushnotification = "";
    public byte[] profileimage;
    public String profileimageurl = "";
    public String secret_key;
    public boolean isowner;
    public String isfavourite;
    public String deceaseddate;
    public String doi;
    public Boolean isalive;
    public String ownerfid;
    public String status;
    public String type;
    public String notification_id;
    public String relationshipType;
    public Boolean isfirstdegree;
    public String relationpath;
    boolean selected = false;
    public String platform;
    public String mode;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public ProfileModel() {

    }

    public static ProfileModel getInstance() {
        return profile;
    }

    public String getRelationpath() {
        return relationpath;
    }

    public void setRelationpath(String relationpath) {
        this.relationpath = relationpath;
    }

    public Boolean getIsfirstdegree() {
        return isfirstdegree;
    }

    public void setIsfirstdegree(Boolean isfirstdegree) {
        this.isfirstdegree = isfirstdegree;
    }

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return displayname;
    }

    public String getOwnerfid() {
        return ownerfid;
    }

    public void setOwnerfid(String ownerfid) {
        this.ownerfid = ownerfid;
    }

    public Boolean getIsalive() {
        return isalive;
    }

    public void setIsalive(Boolean isalive) {
        this.isalive = isalive;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getIsfavourite() {
        return isfavourite;
    }

    public void setIsfavourite(String isfavourite) {
        this.isfavourite = isfavourite;
    }

    public String getDeceaseddate() {
        return deceaseddate;
    }

    public void setDeceaseddate(String deceaseddate) {
        this.deceaseddate = deceaseddate;
    }

    public Boolean getIsowner() {
        return isowner;
    }

    public void setIsowner(Boolean isowner) {
        this.isowner = isowner;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getProfileimageurl() {
        return profileimageurl;
    }

    public void setProfileimageurl(String profileimageurl) {
        this.profileimageurl = profileimageurl;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public byte[] getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(byte[] profileimage) {
        this.profileimage = profileimage;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ProfileModel) {
            ProfileModel profileModel = (ProfileModel) o;
            return this.getFid().equalsIgnoreCase(profileModel.getFid());
        }
        return false;
    }


    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(String isadmin) {
        this.isadmin = isadmin;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobilenumber", getMobilenumber());
            jsonObject.put("countrycode", getCountrycode());
            jsonObject.put("secret_key", getSecret_key());
            jsonObject.put("displayname", getDisplayname());
            jsonObject.put("firstname", getFirstname());
            jsonObject.put("lastname", getLastname());
            jsonObject.put("dob", getDob());
            jsonObject.put("doi", getDoi());
            jsonObject.put("profileimage", getProfileimageurl());
            jsonObject.put("geolocationaccess", "");
            jsonObject.put("pushnotification", "");
            jsonObject.put("tagging", "");
            jsonObject.put("gender", getGender());
            jsonObject.put("relationship", getRelationship());
            jsonObject.put("fid", getFid());
            jsonObject.put("gid", getGid());
            jsonObject.put("isadmin", getIsadmin());
            jsonObject.put("devicetoken", getDevicetoken());
            jsonObject.put("deceaseddate", getDeceaseddate());
            jsonObject.put("isowner", getIsowner());
            jsonObject.put("ownerfid", getOwnerfid());
            jsonObject.put("isalive", getIsalive());
            jsonObject.put("type", getType());
            jsonObject.put("platform", getPlatform());

            return jsonObject.toString();
        } catch (JSONException e) {
            Log.e("ProfileModel", "e = " + e.getMessage());
            return "";
        }

    }

    public ProfileModel toObject(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            setSecret_key(jsonObject.has("secret_key") ? jsonObject.getString("secret_key") : null);
            setMobilenumber(jsonObject.has("mobilenumber") ? jsonObject.getString("mobilenumber") : null);
            setCountrycode(jsonObject.has("countrycode") ? jsonObject.getString("countrycode") : null);
            setDisplayname(jsonObject.has("displayname") ? jsonObject.getString("displayname") : null);
            setFirstname(jsonObject.has("firstname") ? jsonObject.getString("firstname") : null);
            setLastname(jsonObject.has("lastname") ? jsonObject.getString("lastname") : null);
            setDob(jsonObject.has("dob") ? jsonObject.getString("dob") : null);
            setGender(jsonObject.has("gender") ? jsonObject.getString("gender") : null);
            setRelationship(jsonObject.has("relationship") ? jsonObject.getString("relationship") : null);
            setFid(jsonObject.has("fid") ? jsonObject.getString("fid") : null);
            setGid(jsonObject.has("gid") ? jsonObject.getString("gid") : null);
            setIsadmin(jsonObject.has("isadmin") ? jsonObject.getString("isadmin") : null);
            setProfileimageurl(jsonObject.has("profileimage") ? jsonObject.getString("profileimage") : null);
            setPlatform(jsonObject.has("platform") ? jsonObject.getString("platform") : null);
            return this;
        } catch (Exception e) {
            Log.e("GroupModel", "toObject e = " + e.getMessage());
            return null;
        }

    }

}
