package org.officeg.mobileapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.feed.ImageLoader;

public class ProfilePicFullscreenActivity extends AppCompatActivity {
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilepic_fullview);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        image = (ImageView) findViewById(R.id.fullview_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        TextView marque= (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.title_profile_photo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Bundle bundle = getIntent().getExtras();
        String imageurl = null;
        if (bundle != null) {
            imageurl = (String) bundle.get("bitmap");
            ImageLoader imageLoader = imageLoader = new ImageLoader(this);
            imageLoader.DisplayImage(imageurl, image);

        }


    }
}
