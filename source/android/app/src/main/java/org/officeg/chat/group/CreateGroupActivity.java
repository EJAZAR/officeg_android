package org.officeg.chat.group;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.BottomBarActivity;
import org.officeg.mobileapp.R;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateGroupActivity extends AppCompatActivity {
    private static final int UPLOAD_IMAGE_REQUEST = 1881;
    private static final int CAPTURE_IMAGE_REQUEST = 1882;
    Activity activity;
    ArrayList<ProfileModel> profileModels = new ArrayList<ProfileModel>();
    Uri imageUri;
    CircleImageView groupIcon;
    byte[] byteArray;
    ProgressDialog PD;
    GroupModel groupModel;
    TextView marque;
    private String TAG = CreateGroupActivity.class.getSimpleName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("groupModel")) {
            menu.add(0, 2, 0, "Ok").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        } else {
            menu.add(0, 1, 0, "Create").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        EditText groupNameET = (EditText) findViewById(R.id.edittext_creategroup);
        final String groupName = groupNameET.getText().toString().trim();
        if (id == 1) {
            if (groupName.equals("")) {
                Toast.makeText(getApplicationContext(), "Enter Group Name", Toast.LENGTH_SHORT).show();
            } else {
                if (byteArray != null) {
                    PD = ProgressDialog.show(CreateGroupActivity.this, "Please Wait", "Please Wait...");
                    APIManager uploadapi = new APIManager("upload", byteArray, new APIListener() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                Log.d(TAG, "uploadFile" + result.toString());
                                String url = result.getString("url");
                                createGroup(url, groupName);
                            } catch (Exception e) {
                                e.printStackTrace();
                                PD.dismiss();
                            }
                        }

                        @Override
                        public void onSuccess(JSONArray array) {
                            PD.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            PD.dismiss();
                        }
                    }, APIManager.HTTP_METHOD.FILE);
                    uploadapi.execute();

                } else {
                    createGroup("", groupName);
                }
            }
        } else if (id == 2) {
            if (groupName.equals(groupModel.getName()) && byteArray == null) {
                Toast.makeText(activity, "No change in group details", Toast.LENGTH_SHORT).show();
                Intent chatPage = new Intent(activity, BottomBarActivity.class);
                chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chatPage.putExtra("screen", "openchatpage");
                startActivity(chatPage);
            } else if (groupName.trim().isEmpty()) {
                Toast.makeText(activity, "Enter valid name", Toast.LENGTH_SHORT).show();
            } else {
                final String groupOldName = groupModel.getName();
                groupModel.setName(groupName);
                if (byteArray != null) {
                    PD = ProgressDialog.show(CreateGroupActivity.this, "Please Wait", "Please Wait...");
                    APIManager uploadapi = new APIManager("upload", byteArray, new APIListener() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                Log.d(TAG, "uploadFile " + result.toString());
                                String url = result.getString("url");
                                groupModel.setUrl(url);
                                String groupMessage = UserModel.getInstance().getDisplayname() + " changed this group's icon";
                                Utility.sendExtraNotification(getApplicationContext(), groupModel, getString(R.string.groupextramessage), groupMessage);
                                editGroupDetails(groupOldName);
                            } catch (Exception e) {
                                e.printStackTrace();
                                PD.dismiss();
                            }
                        }

                        @Override
                        public void onSuccess(JSONArray array) {
                            PD.dismiss();
                        }

                        @Override
                        public void onFailure(String errorMessage, String errorCode) {
                            PD.dismiss();
                        }
                    }, APIManager.HTTP_METHOD.FILE);
                    uploadapi.execute();

                } else {
                    editGroupDetails(groupOldName);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void editGroupDetails(final String groupOldName) {
        try {
            UserModel userModel = UserModel.getInstance();
            final GroupModel groupModel1 = new GroupModel();
            groupModel1.setGid(groupModel.getGid());
            groupModel1.setName(groupModel.getName());
            groupModel1.setUrl(groupModel.getUrl());
            groupModel1.setSecret_key(userModel.getSecret_key());

            Log.d(TAG, "toJSON = " + groupModel1.toJSON());
            byte[] groupModelByteArray = groupModel1.toJSON().getBytes(Charset.forName("UTF-8"));
            APIManager createGroupApi = new APIManager("chat/group/changename", groupModelByteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "changename inside onsuccess" + result.toString());
                    Toast.makeText(activity, "Group details changed", Toast.LENGTH_SHORT).show();
                    if (!groupOldName.equals(groupModel.getName())) {
                        String groupMessage = "The group name changed from '" + groupOldName + "' to '" + groupModel.getName() + "'";
                        Utility.sendExtraNotification(getApplicationContext(), groupModel, getString(R.string.groupextramessage), groupMessage);
                    }
                    Intent chatPage = new Intent(activity, BottomBarActivity.class);
                    chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    chatPage.putExtra("screen", "openchatpage");
                    startActivity(chatPage);
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d(TAG, "changename inside onsuccess");
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, "changename" + errorMessage + " and " + errorCode);
                }

            }, APIManager.HTTP_METHOD.POST);
            createGroupApi.execute();
        } catch (Exception e) {
            Log.e(TAG, "editGroupDetails Error = " + e.getMessage());
        }
    }

    private void createGroup(String url, String groupName) {
        try {
            PD = ProgressDialog.show(CreateGroupActivity.this, "Please Wait", "Please Wait...");
            String fids = getIntent().getExtras().getString("fids");
            fids = fids.replace("[", "").replace("]", "").replaceAll(" ", "");
            final List<String> fidList = new ArrayList<String>(Arrays.asList(fids.split(",")));

            //to remove duplicates
            Set<String> hs = new HashSet<>();
            hs.addAll(fidList);
            fidList.clear();
            fidList.addAll(hs);

            UserModel userModel = UserModel.getInstance();
            final GroupModel groupModel = new GroupModel();
            groupModel.setFid(userModel.getFid());
            groupModel.setName(groupName);
            groupModel.setUrl(url);
            groupModel.setSecret_key(userModel.getSecret_key());

            String[] fidArray = new String[fidList.size()];
            fidList.toArray(fidArray);
            groupModel.setMembers(fidArray);

            String[] adminArray = new String[0];
            groupModel.setAdmins(adminArray);

            Log.d(TAG, "createGroup json" + groupModel.toJSON());
            byte[] groupModelByteArray = groupModel.toJSON().getBytes(Charset.forName("UTF-8"));

            APIManager createGroupApi = new APIManager("chat/creategroup", groupModelByteArray, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d("CreateGroup", "inside onsuccess" + result.toString());
                    try {
                        groupModel.setGid(result.getString("gid"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String extraString = "** " + UserModel.getInstance().getDisplayname() + " created group " + groupModel.getName() + " **";
                    Utility.sendExtraNotification(getApplicationContext(), groupModel, getString(R.string.groupextramessage), extraString);
                    Toast.makeText(activity, "Group Created", Toast.LENGTH_SHORT).show();
                    Intent chatPage = new Intent(activity, BottomBarActivity.class);
                    chatPage.putExtra("screen", "openchatpage");
                    chatPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(chatPage);
                    PD.dismiss();
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.d("CreateGroup", "inside onsuccess");
                    Toast.makeText(activity, "Group Created", Toast.LENGTH_SHORT).show();
                    PD.dismiss();
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e("CreateGroup", errorMessage + " and " + errorCode);
                    Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
                    PD.dismiss();
                }
            }, APIManager.HTTP_METHOD.POST);
            createGroupApi.execute();
        } catch (Exception e) {
            Log.e("CreateGroup", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        activity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartop);
        marque = (TextView) findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        groupIcon = (CircleImageView) findViewById(R.id.icon_creategroup);
        EditText groupNameET = (EditText) findViewById(R.id.edittext_creategroup);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("groupModel")) {
            GroupModel groupModel = new GroupModel();
            groupModel.toObject(extras.getString("groupModel"));
            this.groupModel = groupModel;
            marque.setText(R.string.title_edit_group);

            if (groupModel.getUrl() != null && !groupModel.getUrl().trim().isEmpty()) {
                Utility.showProfilePicture(groupModel.getUrl(), getApplicationContext(), groupIcon, groupModel.getName(), false);
            }
            groupNameET.setText(groupModel.getName());
            int pos = groupNameET.getText().length();
            groupNameET.setSelection(pos);
        } else {
            marque.setText(R.string.title_create_group);
        }
        groupNameET.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        groupIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] options = {"Take Photo", "Choose from Gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(CreateGroupActivity.this);
                builder.setTitle("Add Photo!");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            try {
                                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File imageFile = Utility.createImageFile(CreateGroupActivity.this);
                                imageUri = Utility.getUriFromFile(getApplicationContext(), imageFile);

                                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                startActivityForResult(takePhotoIntent, CAPTURE_IMAGE_REQUEST);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (options[item].equals("Choose from Gallery")) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), UPLOAD_IMAGE_REQUEST);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == UPLOAD_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                imageUri = data.getData();
                Bitmap bitmap = Utility.decodeSampledBitmapFromResource(imageUri, 200, 200, CreateGroupActivity.this);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                groupIcon.setImageBitmap(bitmap);
                byteArray = stream.toByteArray();
            }
            if (requestCode == CAPTURE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
                Bitmap bitmap = Utility.decodeSampledBitmapFromResource(imageUri, 200, 200, CreateGroupActivity.this);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                groupIcon.setImageBitmap(bitmap);
                byteArray = stream.toByteArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
