package org.officeg.mobileapp;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.database.DataManager;
import org.officeg.feed.FeedAdapter;
import org.officeg.feed.FeedItemModel;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.util.ArrayList;

public class FeedFragment extends Fragment {

    public View currentView;
    FeedAdapter feedAdapter;
    ListView feedlist;
    TextView emptyfeedlist;
    //    Button status_post;
    ArrayList<FeedItemModel> listvalues = new ArrayList<>();
    UserModel user = null;
    LinearLayout feed, withoutfeed, postLayout;
    private OnFragmentInteractionListener mListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    BroadcastReceiver mMessageReceiver;
    private String TAG = FeedFragment.class.getSimpleName();
    ProgressBar progressBar;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("FamilyG", "creating fragment view");
        getActivity().setTitle("Feed");
        ((BottomBarActivity) getActivity()).showProgrssDialog();

        currentView = inflater.inflate(R.layout.fragment_feed, container, false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        container.removeAllViews();
        user = UserModel.getInstance();
        feed = currentView.findViewById(R.id.linearfeed);
        withoutfeed = currentView.findViewById(R.id.linearwithoutfeed);
        feedlist = currentView.findViewById(R.id.list_feed);
        feedAdapter = new FeedAdapter(getContext(), listvalues);
        feedlist.setAdapter(feedAdapter);
        feedAdapter.notifyDataSetChanged();
        progressBar = currentView.findViewById(R.id.progressBar);
        postLayout = currentView.findViewById(R.id.linearforpost);
        if (user.isadmin()) {
            postLayout.setVisibility(View.VISIBLE);
        } else {
            postLayout.setVisibility(View.GONE);
        }
//        if (checkPermission()) {
//            Log.i("permission", "Already granted");
//        } else {
//            requestPermission();
//        }

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("FamilyG", "received post");
                JSONObject post;
                try {
                    post = new JSONObject(intent.getBundleExtra("post").getString("msg"));
                    FeedItemModel feedItemModel = new FeedItemModel(post);
                    if (listvalues != null) {
                        feedAdapter.add(feedItemModel);
                        Log.i("FamilyG", intent.getBundleExtra("post").getString("msg"));
                        ((BaseAdapter) feedlist.getAdapter()).notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mMessageReceiver, new IntentFilter("POST_RECEIVED"));
        emptyfeedlist = currentView.findViewById(R.id.empty_feed);
        emptyfeedlist.setVisibility(View.GONE);

        ImageView camera_post = currentView.findViewById(R.id.camera_post);
        ImageView send_post = currentView.findViewById(R.id.send_post);
        Button ed_post = currentView.findViewById(R.id.ed_post);

        camera_post.setOnClickListener(new AddOnClickListener());
        send_post.setOnClickListener(new AddOnClickListener());
        ed_post.setOnClickListener(new AddOnClickListener());
//        updateFeed(feedlist, emptyfeedlist, feedAdapter, listvalues, getContext());

        ed_post.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
                Utility.logFA(mFirebaseAnalytics, "feed screen", "link to post", "button");
                Intent poststatusintent = new Intent(getActivity(), PostScreenActivity.class);
                startActivity(poststatusintent);
            }
        });

        if (!Utility.profileModelMap.isEmpty()) {
            Log.d("FamilyG", "Utility.profileModelMap exist " + Utility.profileModelMap.size());
            afterGettingFavouritesList();
        } else {
            NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getContext());
            if (NetworkConnectionreceiver.isConnection) {
                final UserModel userModel = UserModel.getInstance();
                APIManager getFavouriteListApi = new APIManager("relationship/getFavouriteList", userModel, new APIListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        Log.d("FamilyG", "inside onsuccess");
                    }

                    @Override
                    public void onSuccess(JSONArray array) {
                        Log.d("FamilyG", "Utility.profileModelMap reload");
                        try {
                            for (int k = 0; k < array.length(); k++) {
                                JSONObject object = array.getJSONObject(k);
                                ProfileModel profileModel = new ProfileModel();
                                profileModel = profileModel.toObject(object.toString());

                                if (profileModel.getMobilenumber() != null && !profileModel.getMobilenumber().trim().equalsIgnoreCase("")) {
                                    String profileModelKey = profileModel.getFid();
                                    Utility.profileModelMap.remove(profileModelKey);
                                    Utility.profileModelMap.put(profileModelKey, profileModel);
                                }
                            }
                            afterGettingFavouritesList();
                            DataManager dataManager = DataManager.getInstance(getContext());
                            dataManager.insertChatFavouritesAndGroups(Utility.profileModelMap, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String errorMessage, String errorCode) {
                        Log.e("FamilyG", errorMessage);
                    }
                });
                getFavouriteListApi.execute();

            } else {
                DataManager dataManager = DataManager.getInstance(getContext());
                Utility.profileModelMap = dataManager.getFavourites();
                afterGettingFavouritesList();
            }
        }

        return currentView;
    }

    class AddOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Utility.logFA(mFirebaseAnalytics, "feed screen", "link to post", "button");
            Intent poststatusintent = new Intent(getActivity(), PostScreenActivity.class);
            startActivity(poststatusintent);
        }
    }

    private void afterGettingFavouritesList() {
        if (!Utility.profileModelMap.isEmpty()) {
            withoutfeed.setVisibility(View.GONE);
            feed.setVisibility(View.VISIBLE);
            Log.i("FamilyG", "feed fragment" + "true");
            LongOperation longOperation = new LongOperation(progressBar, listvalues, user, feedlist, emptyfeedlist, feedAdapter, getContext());
            longOperation.execute();
        } else {
            withoutfeed.setVisibility(View.VISIBLE);
            Log.i("FamilyG", "user without relation" + "true");
            ((BottomBarActivity) getActivity()).hideProgrssDialog();
        }
    }

    private void updateFeed(ProgressBar progressBar, ListView feedlist, TextView emptyfeedlist, FeedAdapter feedAdapter, ArrayList listvalues, Context context) {

        final DataManager dataManager = DataManager.getInstance(context);
        listvalues = new ArrayList<>(dataManager.getPosts());
        afterGettingPostlist(progressBar, false, feedlist, emptyfeedlist, feedAdapter, listvalues, context);
        BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
        Utility.setChatAndFeedCount(bottomNavigation, context, false, true);
        dataManager.deleteTable("unreadnotification", "fidOrGid", "feed");
    }

    private void afterGettingPostlist(ProgressBar progressBar, boolean isAPi, ListView feedlist, TextView emptyfeedlist, FeedAdapter feedAdapter, ArrayList listvalues, Context context) {
        if (listvalues != null) {
            Log.i("FamilyG", "list not null");
            Log.i("FamilyG", "list not null" + listvalues.size());
            feedlist.setVisibility(View.VISIBLE);
            emptyfeedlist.setVisibility(View.GONE);
            feedAdapter = new FeedAdapter(context, listvalues);
            feedlist.setAdapter(feedAdapter);
            feedAdapter.notifyDataSetChanged();
            if (!isAPi)
                getFeedFromApi(progressBar, listvalues, user, feedlist, emptyfeedlist, feedAdapter, context);
        } else {
            feedlist.setVisibility(View.GONE);
            emptyfeedlist.setVisibility(View.VISIBLE);
            emptyfeedlist.setText("Oops..\n There's nothing 'ere, yet.");
        }
    }

    private void getFeedFromApi(final ProgressBar progressBar, final ArrayList listvalues, UserModel user, final ListView feedlist, final TextView emptyfeedlist, final FeedAdapter feedAdapter, final Context context) {
        try {
            NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(context);
            if (NetworkConnectionreceiver.isConnection) {
                APIManager api = new APIManager("post/getPostList", user, new APIListener() {

                    @Override
                    public void onSuccess(JSONObject result) {

                    }

                    @Override
                    public void onSuccess(JSONArray array) {
                        ((BottomBarActivity) getActivity()).hideProgrssDialog();
                        progressBar.setVisibility(View.GONE);
                        Log.i("check", String.valueOf(array));
                        try {
                            listvalues.clear();
                            for (int index = 0; index < array.length(); index++) {
                                JSONObject postobject = array.getJSONObject(index);
                                FeedItemModel itemsmodel = new FeedItemModel(postobject);
                                listvalues.add(itemsmodel);
                            }
                            DataManager dataManager = DataManager.getInstance(context);
                            dataManager.insertPosts(array);
                            afterGettingPostlist(progressBar, true, feedlist, emptyfeedlist, feedAdapter, listvalues, context);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(String errorMessage, String errorCode) {
                        Log.e(TAG, errorMessage);
                        ((BottomBarActivity) getActivity()).hideProgrssDialog();
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

                    }
                });
                api.execute();
            } else {
                Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_LONG).show();
                ((BottomBarActivity) getActivity()).hideProgrssDialog();
                progressBar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroyView();
    }
   /* @Override
    public void onResume() {
        super.onResume();
        UpdateFeed();
    }*/

//    private boolean checkPermission() {
//        int result = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
//        return result == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermission() {
//        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0) {
//
//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//
//                    if (locationAccepted) {
//
//                    } else {
//                        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//                            showMessageOKCancel("You need to allow access to both the permissions",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
//                                                    PERMISSION_REQUEST_CODE);
//                                        }
//                                    });
//                            return;
//                        }
//
//                    }
//                }
//                break;
//        }
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onResume() {
        Log.i("FamilyG", "onResume");

        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        super.onResume();
        if (getView() == null) {
            return;
        }
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
    }

    @Override
    public void onStart() {
        super.onStart();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    BottomNavigationView bottomNavigation = getActivity().findViewById(R.id.bottom_nav);
                    bottomNavigation.setSelectedItemId(R.id.ic_chat_id);

                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        DataManager dataManager = DataManager.getInstance(getContext());
        dataManager.deleteTable("unreadnotification", "fidOrGid", "feed");
        super.onPause();
        Log.i("FamilyG", "onPause");
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class LongOperation extends AsyncTask<String, Void, String> {
        ArrayList listvalues;
        UserModel user;
        ListView feedlist;
        TextView emptyfeedlist;
        FeedAdapter feedAdapter;
        Context context;
        ProgressBar progressBar;

        LongOperation(ProgressBar progressBar, ArrayList listvalues, UserModel user, ListView feedlist, TextView emptyfeedlist, FeedAdapter feedAdapter, Context context) {
            this.progressBar = progressBar;
            this.listvalues = listvalues;
            this.user = user;
            this.feedlist = feedlist;
            this.emptyfeedlist = emptyfeedlist;
            this.feedAdapter = feedAdapter;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
//            getFeedFromApi( listvalues, user, feedlist, emptyfeedlist, feedAdapter, context);
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateFeed(progressBar, feedlist, emptyfeedlist, feedAdapter, listvalues, context);
                }
            });
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}