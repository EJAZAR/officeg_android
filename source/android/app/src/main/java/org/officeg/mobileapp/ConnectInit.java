package org.officeg.mobileapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.util.LinkedList;

public class ConnectInit extends Activity {
    private static final int REQUEST_PICK_CONTACT_FIRSTBOXRELATION = 91343;
    private static final int REQUEST_PICK_CONTACT_SECONDBOXRELATION = 91347;
    ProgressDialog progDailog;
    TextInputLayout til_contactnumber_secondboxrelation;
    TextInputLayout til_contactnumber_firstboxrelation;
    TextInputLayout til_name_secondboxrelation;
    TextInputLayout til_name_firstboxrelation;
    private String TAG = ConnectInit.class.getSimpleName();
    String[][] relationArray;
    int connectInitPosition = 0;
    EditText ed_contact_connect_init_secondboxrelation, ed_contact_connect_init_firstboxrelation, ed_contact_connect_init_secondboxrelation_name, ed_contact_connect_init_firstboxrelation_name;
    TextView tv_secondboxrelation, tv_firstboxrelation;
    LinkedList<ProfileModel> profileModels = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_init);
        relationArray = new String[][]{
                {getString(R.string.boss), getString(R.string.peer)},
                {getString(R.string.junior)}
        };
        progDailog = new ProgressDialog(ConnectInit.this);
        Utility.selectedFid = UserModel.getInstance().fid;
        til_contactnumber_secondboxrelation = (TextInputLayout) findViewById(R.id.til_contactnumber_secondboxrelation);
        til_contactnumber_firstboxrelation = (TextInputLayout) findViewById(R.id.til_contactnumber_firstboxrelation);
        til_name_secondboxrelation = (TextInputLayout) findViewById(R.id.til_name_secondboxrelation);
        til_name_firstboxrelation = (TextInputLayout) findViewById(R.id.til_name_firstboxrelation);

        ed_contact_connect_init_firstboxrelation = (EditText) findViewById(R.id.ed_contact_connect_init_firstboxrelation);
        ed_contact_connect_init_firstboxrelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllErrors();
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, REQUEST_PICK_CONTACT_FIRSTBOXRELATION);
            }
        });
        ed_contact_connect_init_secondboxrelation = (EditText) findViewById(R.id.ed_contact_connect_init_secondboxrelation);
        ed_contact_connect_init_secondboxrelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllErrors();
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, REQUEST_PICK_CONTACT_SECONDBOXRELATION);
            }
        });

        ed_contact_connect_init_firstboxrelation_name = (EditText) findViewById(R.id.ed_contact_connect_init_firstboxrelation_name);
        ed_contact_connect_init_secondboxrelation_name = (EditText) findViewById(R.id.ed_contact_connect_init_secondboxrelation_name);

        tv_firstboxrelation = (TextView) findViewById(R.id.tv_firstboxrelation);
        tv_secondboxrelation = (TextView) findViewById(R.id.tv_secondboxrelation);

        TextView tv_connect_init_skip = (TextView) findViewById(R.id.tv_connect_init_skip);
        tv_connect_init_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickingSkip();
            }
        });

        LinearLayout ll_connect_init_next = (LinearLayout) findViewById(R.id.ll_connect_init_next);
        ll_connect_init_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickingNext();
            }

        });
        setNextSetOfRelations();
        final EditText ed_contact_connect_init_firstboxrelation_name = (EditText) findViewById(R.id.ed_contact_connect_init_firstboxrelation_name);
        ed_contact_connect_init_firstboxrelation_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllErrors();
            }
        });
        final EditText ed_contact_connect_init_secondboxrelation_name = (EditText) findViewById(R.id.ed_contact_connect_init_secondboxrelation_name);
        ed_contact_connect_init_secondboxrelation_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAllErrors();
            }
        });

        connectInitPosition++;
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(TAG, "error " + e.getMessage());
        }
    }

    private void onClickingSkip() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        if (connectInitPosition == 2 && profileModels.size() > 0) {
            afterAddingRelations();
        } else {
            clearAllFields();
            if (connectInitPosition == 2) {
                Intent intent_activity = new Intent(ConnectInit.this, BottomBarActivity.class);
                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_activity);
                finish();
            } else {
                clearAllErrors();
                setNextSetOfRelations();
                connectInitPosition++;
            }
        }
    }

    private void setNextSetOfRelations() {
        String[] currentSetOfRelations = relationArray[connectInitPosition];
        String currentRelation = currentSetOfRelations[0].substring(0, 1).toUpperCase() + currentSetOfRelations[0].substring(1);
        tv_firstboxrelation.setText(currentRelation);

        if (connectInitPosition == 2) {
            LinearLayout ll_secondboxrelation = (LinearLayout) findViewById(R.id.ll_secondboxrelation);
            ll_secondboxrelation.setVisibility(View.INVISIBLE);
            tv_secondboxrelation.setText("");
        } else {
            LinearLayout ll_secondboxrelation = (LinearLayout) findViewById(R.id.ll_secondboxrelation);
            ll_secondboxrelation.setVisibility(View.VISIBLE);
            currentRelation = currentSetOfRelations[1].substring(0, 1).toUpperCase() + currentSetOfRelations[1].substring(1);
            tv_secondboxrelation.setText(currentRelation);
        }
        for (int k = 0; k < 2; k++) {
            String idadd = "iv_step_" + String.valueOf(k + 1);
            ImageView imageView_step = (ImageView) findViewById(getResources().getIdentifier(idadd, "id", getPackageName()));
            if (connectInitPosition == k) {
                imageView_step.setImageDrawable(getResources().getDrawable(R.drawable.step_current));
            } else {
                imageView_step.setImageDrawable(getResources().getDrawable(R.drawable.step_new));
            }
        }
    }

    public void onClickingNext() {

        if (connectInitPosition == 2 && profileModels.size() > 0) {
            afterAddingRelations();
            return;
        }

        ed_contact_connect_init_secondboxrelation.setCursorVisible(false);
        ed_contact_connect_init_firstboxrelation.setCursorVisible(false);

        String namepattern = getString(R.string.name_pattern);

        String secondboxrelationNumber = ed_contact_connect_init_secondboxrelation.getText().toString().trim();
        String firstboxrelationNumber = ed_contact_connect_init_firstboxrelation.getText().toString().trim();
        String secondboxrelationName = ed_contact_connect_init_secondboxrelation_name.getText().toString().trim();
        String firstboxrelationName = ed_contact_connect_init_firstboxrelation_name.getText().toString().trim();

        clearAllErrors();

        if (secondboxrelationNumber.isEmpty() && firstboxrelationNumber.isEmpty()) {
            til_contactnumber_secondboxrelation.setErrorEnabled(true);
            til_contactnumber_firstboxrelation.setErrorEnabled(true);
            til_contactnumber_secondboxrelation.setError("Please provide a mobile number");
            til_contactnumber_firstboxrelation.setError("Please provide a mobile number");
        } else if (secondboxrelationNumber.isEmpty() && !secondboxrelationName.isEmpty()) {
            til_contactnumber_secondboxrelation.setErrorEnabled(true);
            til_contactnumber_secondboxrelation.setError("Please provide a mobile number");
        } else if (firstboxrelationNumber.isEmpty() && !firstboxrelationName.isEmpty()) {
            til_contactnumber_firstboxrelation.setErrorEnabled(true);
            til_contactnumber_firstboxrelation.setError("Please provide a mobile number");
        } else if (secondboxrelationName.isEmpty() && !secondboxrelationNumber.isEmpty()) {
            til_name_secondboxrelation.setErrorEnabled(true);
            til_name_secondboxrelation.setError("Please provide a name");
        } else if (firstboxrelationName.isEmpty() && !firstboxrelationNumber.isEmpty()) {
            til_name_firstboxrelation.setErrorEnabled(true);
            til_name_firstboxrelation.setError("Please provide a name");
        } else if (!secondboxrelationName.isEmpty() && !secondboxrelationName.matches(namepattern)) {
            Log.i("FamilyG", "pattern mismatched");
            ed_contact_connect_init_secondboxrelation_name.requestFocus();
            til_name_secondboxrelation.setErrorEnabled(true);
            til_name_secondboxrelation.setError("Please provide a valid name");
        } else if (!firstboxrelationName.isEmpty() && !firstboxrelationName.matches(namepattern)) {
            Log.i("FamilyG", "pattern mismatched");
            ed_contact_connect_init_firstboxrelation_name.requestFocus();
            til_name_firstboxrelation.setErrorEnabled(true);
            til_name_firstboxrelation.setError("Please provide a valid name");
        } else {

            clearAllErrors();

            String[] currentSetOfRelations = relationArray[connectInitPosition - 1];

            if (!firstboxrelationNumber.isEmpty()) {
                String currentRelation = currentSetOfRelations[0].substring(0, 1).toUpperCase() + currentSetOfRelations[0].substring(1);
                if (currentRelation.equalsIgnoreCase(getString(R.string.spouse))) {
                    String gender = UserModel.getInstance().gender;
                    if (gender.equalsIgnoreCase(getString(R.string.male))) {
                        currentRelation = getString(R.string.wife);
                    } else {
                        currentRelation = getString(R.string.husband);
                    }
                }
                ProfileModel profileModel = new ProfileModel();
                profileModel.setRelationship(currentRelation);
                profileModel.setDisplayname(firstboxrelationName);
                profileModel.setMobilenumber(firstboxrelationNumber);
                profileModels.add(profileModel);
            }
            if (!secondboxrelationNumber.isEmpty()) {
                String currentRelation = currentSetOfRelations[1].substring(0, 1).toUpperCase() + currentSetOfRelations[1].substring(1);
                ProfileModel profileModel = new ProfileModel();
                profileModel.setRelationship(currentRelation);
                profileModel.setDisplayname(secondboxrelationName);
                profileModel.setMobilenumber(secondboxrelationNumber);
                profileModels.add(profileModel);
            }
            clearAllFields();
            if (connectInitPosition == 2) {
                afterAddingRelations();
            } else {
                setNextSetOfRelations();
            }
            connectInitPosition++;
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                Log.e(TAG, "error " + e.getMessage());
            }
        }
    }

    private void afterAddingRelations() {
        progDailog.setTitle("Please Wait");
        progDailog.setMessage("Loading...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(false);
        progDailog.show();
        ConnectionRequest connectionRequest = new ConnectionRequest();
        for (ProfileModel profileModel : profileModels) {
            connectionRequest.connectWithRelation(getApplicationContext(), profileModel.getRelationship(), profileModel.getDisplayname(), profileModel.getMobilenumber(), profileModels.size(), progDailog, TAG);
        }
    }

    private void clearAllFields() {
        ed_contact_connect_init_secondboxrelation_name.setText("");
        ed_contact_connect_init_secondboxrelation.setText("");
        ed_contact_connect_init_firstboxrelation_name.setText("");
        ed_contact_connect_init_firstboxrelation.setText("");
    }

    private void clearAllErrors() {
        til_contactnumber_secondboxrelation.setError(null);
        til_contactnumber_firstboxrelation.setError(null);
        til_name_secondboxrelation.setError(null);
        til_name_firstboxrelation.setError(null);
        til_contactnumber_secondboxrelation.setErrorEnabled(false);
        til_contactnumber_firstboxrelation.setErrorEnabled(false);
        til_name_secondboxrelation.setErrorEnabled(false);
        til_name_firstboxrelation.setErrorEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_PICK_CONTACT_FIRSTBOXRELATION && resultCode == RESULT_OK) {
            afterPickingContact(intent, "firstboxrelation");
        } else if (requestCode == REQUEST_PICK_CONTACT_SECONDBOXRELATION && resultCode == RESULT_OK) {
            afterPickingContact(intent, "secondboxrelation");
        }
    }

    private void afterPickingContact(Intent intent, String relation) {
        Cursor cursor;
        try {
            Uri uri = intent.getData();
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String phoneNo = cursor.getString(phoneIndex).replaceAll("[()\\s-]+", "");

            TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            String usersCountryISOCode = manager.getSimCountryIso().toUpperCase();

            Log.i("FamilyG", "Country code : " + usersCountryISOCode);
            Log.i("FamilyG", "Original number : " + phoneNo);

            Phonenumber.PhoneNumber phone = PhoneNumberUtil.getInstance().parseAndKeepRawInput(phoneNo, usersCountryISOCode.toUpperCase());

            Log.i("FamilyG", "Extracted number : " + phone.getCountryCode());

            String name = cursor.getString(nameIndex);
            if (relation.equals("firstboxrelation")) {
                ed_contact_connect_init_firstboxrelation.setText(PhoneNumberUtil.getInstance().format(phone, PhoneNumberUtil.PhoneNumberFormat.E164));
                ed_contact_connect_init_firstboxrelation_name.setText(name);
            } else {
                ed_contact_connect_init_secondboxrelation.setText(PhoneNumberUtil.getInstance().format(phone, PhoneNumberUtil.PhoneNumberFormat.E164));
                ed_contact_connect_init_secondboxrelation_name.setText(name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}