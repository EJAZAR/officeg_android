package org.officeg.chat.group;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatModel;
import org.officeg.model.GroupModel;
import org.officeg.model.ProfileModel;
import org.officeg.util.Utility;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */


public class ForwardAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<ChatModel> chatModelArrayList;
    private Context context;

    public ForwardAdapter(Context context) {
        chatModelArrayList = new ArrayList<ChatModel>();
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chatModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(ChatModel chatModel) {
        chatModelArrayList.add(chatModel);
    }

    public void setChatModelArrayList(ArrayList<ChatModel> chatModelArrayList) {
        this.chatModelArrayList = chatModelArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.group_addmember_adaptor, null);

        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_group_addmember);

        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ChatModel chatModel = (ChatModel) cb.getTag();
                chatModel.setSelected(cb.isChecked());
            }
        });
        ChatModel chatModel = chatModelArrayList.get(position);
        checkBox.setChecked(chatModel.isSelected());
        checkBox.setTag(chatModel);

        String name, imageUrl = "";


        if (chatModel.getType().equals(context.getString(R.string.favourite))) {
            ProfileModel profileModel = chatModel.getProfileModel();
            name = profileModel.getDisplayname();
            if (profileModel.getProfileimageurl() != null && !profileModel.getProfileimageurl().trim().isEmpty()) {
                imageUrl = profileModel.getProfileimageurl();
            }
        } else {
            GroupModel groupModel = chatModel.getGroupModel();
            name = groupModel.getName();
            if (groupModel.getUrl() != null && !groupModel.getUrl().trim().isEmpty()) {
                imageUrl = groupModel.getUrl();
            }
        }

        TextView nameTextView = (TextView) convertView.findViewById(R.id.name_group_addmember);
        nameTextView.setText(name);

        CircleImageView circleImageView = (CircleImageView) convertView.findViewById(R.id.circle_group_addmember);
        if (!imageUrl.isEmpty()) {
            Utility.showProfilePicture(imageUrl, context, circleImageView, name, false);
        } else {
            if (chatModel.getType().equals(context.getString(R.string.favourite))) {
                circleImageView.setImageResource(R.drawable.ic_person);
            } else {
                circleImageView.setImageResource(R.drawable.ic_group);
            }
        }
        return convertView;
    }
}
