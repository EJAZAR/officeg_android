package org.officeg.feed;

import android.util.Log;

import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class FeedItemModel implements Serializable {

    String content, postid, displayname, totalcomments, profileimage, fid;
    Long time;
    List<String> imagelist = new ArrayList<String>();
    List<String> videolist = new ArrayList<String>();
    List<CommentModel> commentList = new ArrayList<CommentModel>();
    List<LikeModel> likeList = new ArrayList<LikeModel>();
    int totallikes, i, comments;
    boolean isLikedByMe = false, isfav, isfavByMe = false, isfirst;
    byte[] imagebytearray;

    public String getFileNameBeforeRename() {
        return fileNameBeforeRename;
    }

    public void setFileNameBeforeRename(String fileNameBeforeRename) {
        this.fileNameBeforeRename = fileNameBeforeRename;
    }

    private String fileNameBeforeRename;

    public byte[] getImagebytearray() {
        return imagebytearray;
    }

    public void setImagebytearray(byte[] imagebytearray) {
        this.imagebytearray = imagebytearray;
    }


    public FeedItemModel() {

    }

    public FeedItemModel(JSONObject post) {

        try {
            Log.i("FamilyG", "FeedItemModel " + "calling");
            postid = post.getString("postid");
            displayname = post.getString("displayname");
            content = post.getString("content");
            time = post.getLong("time");
            Log.i("FamilyG", "postid is " + postid);
            if (!(post.getString("profileimage").equals(""))) {
                profileimage = post.getString("profileimage");
            }
            if (post.has("images")) {
                JSONArray imageurls = post.getJSONArray("images");
                Log.i("imagesarray", String.valueOf(imageurls));

                for (int indeximageurl = 0; indeximageurl < imageurls.length(); indeximageurl++) {
                    imagelist.add((String) imageurls.get(indeximageurl));
                }
            }
            if (post.has("videos")) {
                JSONArray videourls = post.getJSONArray("videos");
                Log.i("videosrray", String.valueOf(videourls));
                for (int indexvideourls = 0; indexvideourls < videourls.length(); indexvideourls++) {
                    videolist.add((String) videourls.get(indexvideourls));
                }
            }
            //fid = post.getString("fid");
            fid = post.getString("fid");
            Log.i("FamilyG", "fid is " + fid);
            isfav = post.getBoolean("isfav");
            isfirst = post.has("isfirst") && post.getBoolean("isfirst");

            totallikes = post.getInt("likes");
            JSONArray comments = post.getJSONArray("comments");
            JSONArray likingpersons = post.getJSONArray("likingpersons");
            totalcomments = String.valueOf(comments.length());
            i = Integer.parseInt(totalcomments);


            for (int index = 0; index < comments.length(); index++) {
                JSONObject comment = (JSONObject) comments.get(index);
                commentList.add(new CommentModel(comment.getString("displayname"), comment.getLong("time"), comment.getString("comment"), comment.getString("profileimage")));
            }

            for (int index = 0; index < likingpersons.length(); index++) {
                JSONObject likingperson = (JSONObject) likingpersons.get(index);
                likeList.add(new LikeModel(likingperson.getString("displayname"), likingperson.getLong("time"), likingperson.getString("fid"), likingperson.getString("profileimage")));
            }

            isLikedByMe = post.getBoolean("likebyme");
            // isfav=post.getBoolean("isfav");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public int getTotallikes() {
        return totallikes;
    }

    public int incrementLikeCount() {
        return ++totallikes;
    }

    public int decrementLikeCount() {
        return --totallikes;
    }

   /* public boolean addfav() {
        isfavByMe = true;
        return isfav;
    }*/

    public void addComment(CommentModel commentModel) {
        commentList.add(commentModel);
    }

    public void addLike(String name, long timeCommented, String fid, String profileimage) {
        likeList.add(new LikeModel(name, timeCommented, fid, profileimage));
    }

    public void removeLike(String fid) {
        for (LikeModel likeModel : likeList) {
            if (likeModel.getFid().equals(fid))
                likeList.remove(likeModel);
        }
    }

    public int getTotalComments() {
        Log.i("FamilyG", "Total comments count is " + totalcomments);
        return commentList.size();
    }

    public List<CommentModel> getCommentList() {
        return commentList;
    }

    public String getPostid() {
        return postid;
    }

    public String getFid() {
        return fid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getTime() {
        return Utility.ElapsedTime(time);
    }

    public String getProfileimage() {
        return profileimage;
    }

    public String getContent() {
        return content;
    }

    public List<String> getImagelist() {
        return imagelist;
    }

    public List<String> getVideolist() {
        return videolist;
    }

    public boolean getisfav() {
        return isfav;
    }

    public List<LikeModel> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<LikeModel> likeList) {
        this.likeList = likeList;
    }

}
