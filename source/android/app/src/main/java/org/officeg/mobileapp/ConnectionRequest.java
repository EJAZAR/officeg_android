package org.officeg.mobileapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.nio.charset.Charset;

public class ConnectionRequest {
    int k = 0;

    public void connectWithRelation(Context applicationContext, String relation, String relationName, String relationNumber, int k, ProgressDialog progDailog, String TAG) {
        if (this.k == 0) {
            this.k = k;
        }
        ProfileModel profile = new ProfileModel();
        profile.isalive = true;

        profile.setType("1");
        profile.fid = "";
        profile.isowner = true;

        profile.secret_key = UserModel.getInstance().secret_key;
        profile.displayname = relationName;
        Log.i("FamilyG", "display name after editing " + relationName);
        profile.ownerfid = Utility.selectedFid;
        profile.secret_key = UserModel.getInstance().secret_key;
        profile.type = "1";
        profile.doi = "";
        profile.status = applicationContext.getString(R.string.pending);


        Log.i("FamilyG", "Selected Gender: " + profile.gender + "Selected DOI: " + profile.doi + "Alive: " + profile.isalive + "Type: " + profile.type);
        try {
            TelephonyManager manager = (TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
            String usersCountryISOCode = manager.getSimCountryIso();
            Phonenumber.PhoneNumber number = null;
            PhoneNumberUtil util = PhoneNumberUtil.getInstance();
            if (relationNumber != null) {
                number = util.parseAndKeepRawInput(relationNumber, usersCountryISOCode.toUpperCase());
                profile.setMobilenumber("" + number.getNationalNumber());
            } else {
                profile.mobilenumber = relationNumber;
            }

            profile.setRelationship(relation.toUpperCase());

            for (String r : util.getSupportedRegions()) {
                try {
                    // check if it's a possible number
                    boolean isValid = util.isPossibleNumber(relationNumber, r);
                    if (isValid) {
                        number = util.parse(relationNumber, r);
                        // check if it's a valid number for the given region
                        isValid = util.isValidNumberForRegion(number, r);
                        if (isValid)
                            Log.i("countryCode", number.getCountryCode() + ", " + number.getNationalNumber());
                    }
                } catch (NumberParseException e) {
                    e.printStackTrace();
                }
            }

            profile.setCountrycode("" + number.getCountryCode());
            profile.ownerfid = UserModel.getInstance().fid;
            Log.i("FamilyG", "Relationship : " + profile.getRelationship());
            createProfile(applicationContext, profile, progDailog, TAG);
        } catch (Exception e) {
            Log.i("FamilyG", "Error : " + e.getMessage());
        }
    }

    public void createProfile(final Context applicationContext, final ProfileModel profile, final ProgressDialog progDailog, final String TAG) {

        String userprofile = profile.toJSON();

        byte[] profileModel = userprofile.getBytes(Charset.forName("UTF-8"));
        Log.i("FamilyG", "profileformimage" + userprofile);
        final APIManager api = new APIManager("relationship/connect", profileModel, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {
                    profile.fid = result.getString("fid");
                    Log.i("FamilyG fid", profile.fid);

                    UserModel.getInstance().profileMemberModel.AddMember(Utility.selectedFid, profile);
                    if (--k == 0) {
                        Toast.makeText(applicationContext, applicationContext.getString(R.string.request_has_been_sent), Toast.LENGTH_LONG).show();
                        progDailog.dismiss();
                        Class activity;
                        if (TAG.equalsIgnoreCase(ConnectInit.class.getSimpleName())) {
                            activity = BottomBarActivity.class;
                        } else {
                            activity = AddMemberActivity.class;
                        }
                        Intent intent_activity = new Intent(applicationContext, activity);
                        intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent_activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        applicationContext.startActivity(intent_activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONArray array) {
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG).show();
                progDailog.dismiss();

            }
        }, APIManager.HTTP_METHOD.POST);

        api.execute();
    }
}
