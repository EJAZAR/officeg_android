package org.officeg.mobileapp;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.database.DataManager;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

public class TreeInterface {

    Context mContext;
    UserModel user = null;
    WebView treeWebView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final String TAG = TreeInterface.class.getSimpleName();


    /**
     * Instantiate the interface and set the context
     */
    TreeInterface(Context c, WebView treeWebView) {
        mContext = c;
        this.treeWebView = treeWebView;
        user = UserModel.getInstance();
        user.load(mContext);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);

    }

    public static void longToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void disconnectRelation(String srcFid, final String desFid) {
        ProfileDisconnectRequest disconnectRequest = new ProfileDisconnectRequest();
        disconnectRequest.desfid = desFid;
        disconnectRequest.srcfid = srcFid;
        disconnectRequest.secret_key = UserModel.getInstance().secret_key;
        APIManager getrelationapi = new APIManager("relationship/disconnect", disconnectRequest.toJSON().getBytes(Charset.forName("UTF-8")), new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                Toast.makeText(mContext, "Request to disconnect the relationship is successful.", Toast.LENGTH_LONG).show();
                DataManager dm = DataManager.getInstance(null);
                dm.deleteTable("relationships", "FamilyGID", desFid);
                dm.deleteTable("relationshipMapping", "RelationFamilyGID", desFid);

                Log.i("FamilyG", "desfid Relationships " + desFid);
                //treeWebView.reload();
                treeWebView.evaluateJavascript("reload()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });
                UserModel.getInstance().profileMemberModel.RemoveMember(desFid);
                List<ProfileModel> ownedProfileList = UserModel.getInstance().ownedProfileList;
                ProfileModel desFidprofileModel = new ProfileModel();
                desFidprofileModel.fid = desFid;
                ownedProfileList.remove(desFidprofileModel);
            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.i("Relation Array", String.valueOf(array));
                Toast.makeText(mContext, "Request to disconnect the relationship is successful.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Log.i("tree", errorMessage);
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
                //TreeInterface.longToast(mContext,"");

            }
        }, APIManager.HTTP_METHOD.POST);

        getrelationapi.execute();
    }

    @JavascriptInterface
    public void getRelationship(final String fid) {
        Utility.logFA(mFirebaseAnalytics, "tree", "get relationship", "tree");
        ProfileModel model = new ProfileModel();
        if (fid.equalsIgnoreCase(user.getFid())) // if the logged in user fid and requested fid is same then need to send fid as empty ("")
            model.setFid("");
        else
            model.setFid(fid);
        model.secret_key = user.secret_key;

        //final String finalFid = fid;
        APIManager getrelationapi = new APIManager("relationship/getrelation", model, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
            }

            @Override
            public void onSuccess(JSONArray array) {
                Log.i("Relation Array", String.valueOf(array));
                JSONArray sortedArray = Utility.sortJsonArray(array, mContext);
                Log.i("sortedArray", String.valueOf(sortedArray));
                treeWebView.evaluateJavascript("onRelationshipReceived(" + String.valueOf(sortedArray) + ")", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });

                DataManager dm = DataManager.getInstance(null);
                ContentValues row = new ContentValues();
                try {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject person = (JSONObject) array.get(i);
                        ContentValues rowformapping = new ContentValues();


                        ProfileModel member = new ProfileModel();
                        if (fid.equals("")) {
                            rowformapping.put("FamilyGID", user.fid);
                            rowformapping.put("RelationFamilyGID", person.getString("fid"));
                        } else {
                            rowformapping.put("FamilyGID", fid);
                            rowformapping.put("RelationFamilyGID", person.getString("fid"));
                        }
                        if (person.has("fid")) {
                            row.put("FamilyGID", person.getString("fid"));
                            // rowformapping.put("FamilyGID", person.getString("fid"));

                            member.setFid(person.getString("fid"));
                        }

                        if (person.has("displayname")) {
                            row.put("DisplayName", person.getString("displayname"));
                            member.setDisplayname(person.getString("displayname"));
                        }

                        if (person.has("firstname")) {
                            row.put("FirstName", person.getString("firstname"));
                            member.setFirstname(person.getString("firstname"));
                        }

                        if (person.has("lastname")) {
                            row.put("LastName", person.getString("lastname"));
                            member.setLastname(person.getString("lastname"));
                        }

                        if (person.has("dob")) {
                            row.put("DOB", person.getString("dob"));
                            member.setDob(person.getString("dob"));
                        }

                        if (person.has("gender")) {
                            row.put("Gender", person.getString("gender"));
                            member.setGender(person.getString("gender"));
                        }

                        if (person.has("mobilenumber")) {
                            row.put("MobileNumber", person.getString("mobilenumber"));
                            member.setMobilenumber(person.getString("mobilenumber"));
                        }

                        if (person.has("deceaseddate")) {
                            row.put("Deathdate", person.getString("deceaseddate"));
                            member.setDeceaseddate(person.getString("deceaseddate"));
                        }

                        if (person.has("isowner")) {
                            row.put("IsOwner", person.getBoolean("isowner"));
                            member.setIsowner(person.getBoolean("isowner"));
                        }

                        if (person.has("isfav")) {
                            row.put("IsFavourite", person.getString("isfav"));
                            member.setIsfavourite(person.getString("isfav"));
                        }

                        if (person.has("doi")) {
                            row.put("doi", person.getString("doi"));
                            member.setDoi(person.getString("doi"));
                        }
                        if (person.has("profileimage")) {
                            row.put("ImageUrl", person.getString("profileimage"));
                            member.setProfileimageurl(person.getString("profileimage"));
                        }

                        if (person.has("ownerfid")) {
                            row.put("ownerfid", person.getString("ownerfid"));
                            member.setOwnerfid(person.getString("ownerfid"));
                        }

                        if (person.has("status")) {

                            rowformapping.put("Status", person.getString("status"));
                            member.setStatus(person.getString("status"));
                        }

                        if (person.has("relationtype")) {
                            rowformapping.put("RelationType", person.getString("relationtype"));
                            member.setRelationship(person.getString("relationtype"));
                        }

                        if (person.has("isfirstdegree")) {
                            row.put("IsFirstDegree", person.getBoolean("isfirstdegree"));
                            member.setIsfirstdegree(person.getBoolean("isfirstdegree"));
                        }
                        if (person.has("isalive")) {
                            row.put("IsAlive", person.getBoolean("isalive"));
                            member.setIsalive(person.getBoolean("isalive"));
                        } else {
                            row.put("IsAlive", false);
                            member.setIsalive(false);
                        }

                        String sourceFid = fid;
                        if (sourceFid.equalsIgnoreCase(""))
                            sourceFid = user.fid;

                        if (!dm.isUserExists(person.getString("fid")))
                            dm.insertTable("relationships", row);   //insert "relationships" row
                        else
                            dm.updateTable("relationships", row, "FamilyGID", person.getString("fid"));

                        if ((dm.isMappingExists(sourceFid, person.getString("fid")))) {  //checking particular row is alread present in database or not
                            //dm.updateTable("relationships", row, "FamilyGID", person.getString("fid"));  //update "relationships" row
                            dm.updateMapping(rowformapping, sourceFid, person.getString("fid"));  //update "relationships" row

                        } else {
                            Log.i("FamilyG", "inserting new data");
                            dm.insertTable("relationshipMapping", rowformapping);  //insert "relationships" row

                        }

                          /*  profileMemberModel.AddMember(user.getFid(), member, Enum.valueOf(RelationshipType.class, person.getString("relationtype")));


                            if (profileModelList.indexOf(member) == -1) {
                                Log.i("FamilyG", "Userfid in tree " + user.getFid());


                                profileModelList.add(member);
                                Log.i("FamilyG", "Sqlite Relationships " + row);
                                dm.insertTable("relationships", row);
                                dm.insertTable("relationshipMapping", rowformapping);
                                Log.i("FamilyG", "Sqlite Relationships mapping " + rowformapping);
                                //dm.getProfiles(user.getFid());
                                Log.i("FamilyG", "Userfid " + user.getFid());


                            }
*/
                        /*if (!member.isowner && member.ownerfid.equalsIgnoreCase(fid)) { // Check if the profile is owned by logged in user
                            if (ownedProfileList.indexOf(member) == -1) {
                                ownedProfileList.add(member);
                                loadMembers(member.fid);
                            }
                        }*/
                    }
                } catch (Exception e) {
                    Log.i("FamilyG", "error in inserting: " + e);
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Log.i("tree", errorMessage);
                //retrieving relation from sqlite in offline
                DataManager dm = DataManager.getInstance(null);
                List<HashMap<String, String>> list = null;
                if (fid.equalsIgnoreCase("")) {
                    list = dm.getProfiles(user.fid);
                    Log.i("FamilyG", "Login user list in tree is " + list);
                } else {
                    list = dm.getProfiles(fid);
                    Log.i("FamilyG", "list in tree is " + list);
                }
                try {
                    JSONArray jArray = Utility.listToJsonArray(list);

                    if (jArray != null && jArray.length() > 0) {

                        treeWebView.evaluateJavascript("onRelationshipReceived(" + jArray + ")", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                //store / process result received from executing Javascript.
                            }
                        });

                    } else {
                        Toast.makeText(mContext, "Unable to get the user.Please connect internet to get the user.", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getrelationapi.execute();

    }

    @JavascriptInterface
    public void getPedigreeTree() {
        Utility.logFA(mFirebaseAnalytics, "tree", "getPedigreeTree", "tree");
        try {
            ProfileModel model = new ProfileModel();
            model.secret_key = user.secret_key;
            model.doi = user.doi;
            model.mode = "api";

            //final String finalFid = fid;
            APIManager getrelationapi = new APIManager("treeancestry", model, new APIListener() {

                @Override
                public void onSuccess(JSONObject result) {
                    Log.e(TAG, "onSuccess");
                }

                @Override
                public void onSuccess(JSONArray array) {
                    Log.i("Relation Array", String.valueOf(array));
                    treeWebView.evaluateJavascript("loadNetworkCallBack(" + String.valueOf(array) + ")", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                        }
                    });
                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Log.e(TAG, errorMessage);
                }
            });
            getrelationapi.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void addFavourite(String fid, String relationlink) {
        String link = relationlink.replaceAll("<.*?>", "");
        String l = link.replaceFirst("You", user.displayname);
        Log.i("FamilyG", "without html tags RelationLink: " + link);
        // Log.i("FamilyG","without html tags RelationLink without You: " +l);
        ProfileModel model = new ProfileModel();
        model.secret_key = user.secret_key;
        model.setFid(fid);
        model.relationpath = l;

        Log.i("FamilyG", "ProfileModel in addFavourite: " + model);
        Utility.logFA(mFirebaseAnalytics, "tree", "add favorite", "tree");
        APIManager addfavouriteapi = new APIManager("relationship/addFavourite", model, new APIListener() {

            @Override
            public void onSuccess(JSONObject object) {
//                Toast.makeText(mContext,"Added to Favourite",Toast.LENGTH_LONG).show();

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText("Request has been sent to add to favourite");

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
                treeWebView.evaluateJavascript("onAddFavourite()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });

                treeWebView.evaluateJavascript("closePopup()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {

                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();

            }
        });
        addfavouriteapi.execute();
    }

    @JavascriptInterface
    public void removeFavourite(String fid) {

        final ProfileModel model = new ProfileModel();
        model.setFid(fid);
        model.secret_key = user.secret_key;
        Utility.logFA(mFirebaseAnalytics, "tree", "remove favorite", "tree");

        APIManager removefavouriteapi = new APIManager("relationship/removeFavourite", model, new APIListener() {


            @Override
            public void onSuccess(JSONObject object) {
                model.setIsfavourite("false");
                Toast.makeText(mContext, "Removed from Favourite", Toast.LENGTH_LONG).show();
                treeWebView.evaluateJavascript("onRemoveFavourite()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });

                treeWebView.evaluateJavascript("reload()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //store / process result received from executing Javascript.
                    }
                });
            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();

            }
        });
        removefavouriteapi.execute();
    }

    @JavascriptInterface
    public JSONArray onRelationshipArray(JSONArray arr) {
        Utility.logFA(mFirebaseAnalytics, "tree", "get relationshiparray", "tree");

        JSONArray relArray = arr;
        return relArray;

    }


    @JavascriptInterface
    public String getMyName() {
        Utility.logFA(mFirebaseAnalytics, "tree", "get name", "tree");

        Log.i("FamilyG", "My name " + user.displayname);
        return user.displayname;
    }

    @JavascriptInterface
    public void logEvent() {
        Utility.logFA(mFirebaseAnalytics, "tree", "view profile", "popup");

    }

    @JavascriptInterface
    public String getMyPhoto() {
        Utility.logFA(mFirebaseAnalytics, "tree", "get photo", "tree");

        if (user.profileimageurl == null || user.profileimageurl.isEmpty()) {
            Log.i("FamilyG", "My photo if null: " + user.gender);
            String gender = user.gender;
            if (gender == null)
                gender = "Male";
            return (gender.equals("Female") ? "female_default_profileimage.png" : "male_default_profileimage.png");
        }
//        String profileimageString = Base64.encodeToString(user.profileimage, Base64.NO_WRAP);
//        Log.i("Base64",profileimageString);
        return user.profileimageurl;
    }

    @JavascriptInterface
    public String getMyFID() {
        return user.fid;
    }

    @JavascriptInterface
    public String getPedigreeLink() {
        String pedigreeLink = APIManager.link + "pedigreetree?doi=" + user.doi + "&secret_key=" + user.secret_key;
        return pedigreeLink;
    }

    @JavascriptInterface
    public void showToast(String toastContent) {
        Toast.makeText(mContext, toastContent, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void logThis(String logContent) {
        Log.d(TAG, logContent);
    }

    @JavascriptInterface
    public void moveAddMemberActivity() {
        Intent intent_activity = new Intent(mContext, AddMemberActivity.class);
        mContext.startActivity(intent_activity);
    }

    @JavascriptInterface
    public void editProfile(String fid, final String Lfid, final String isconnected, final String editDisplayname, final String relationType) {
        ProfileModel profileModel;
        profileModel = new ProfileModel();
        profileModel.fid = fid;
        profileModel.secret_key = UserModel.getInstance().secret_key;

        Log.i("FamilyG", "Selected relations fid: " + profileModel.fid);
        Log.i("FamilyG", "LFID: " + Lfid);
        Log.i("FamilyG", "isconnected: " + isconnected);
        Log.i("FamilyG", "editDispname: " + editDisplayname);
        Log.i("FamilyG", "relationType: " + relationType);

        APIManager viewProfile = new APIManager("relationship/viewProfile", profileModel, new APIListener() {

            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Intent i = new Intent(mContext, PersonalProfileActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("type", "profileView");
                    if (isconnected.equals("true"))
                        extras.putString("displayname", editDisplayname);
                    else
                        extras.putString("displayname", object.getString("displayname"));

                    extras.putString("firstname", object.getString("firstname"));
                    extras.putString("lastname", object.getString("lastname"));
                    extras.putString("profileimage", object.getString("profileimage"));
                    extras.putString("doi", object.getString("doi"));
                    extras.putString("dob", object.getString("dob"));
                    extras.putString("gender", object.getString("gender"));
                    extras.putString("mobilenumber", object.getString("mobilenumber"));
                    if (object.has("ownerfid")) {
                        extras.putString("ownerfid", object.getString("ownerfid"));

                    }
                    extras.putString("fid", object.getString("fid"));
                    extras.putString("connected", isconnected);
                    extras.putBoolean("isalive", object.getBoolean("isalive"));
                    extras.putString("deceaseddate", object.getString("deceaseddate"));

                    extras.putString("LFID", Lfid);
                    extras.putString("relationship", relationType);

                    Log.i("FamilyG", "CCCCONN: " + isconnected);
                    i.putExtras(extras);
                    mContext.startActivity(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onSuccess(JSONArray array) {

            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {

            }
        });
        viewProfile.execute();
    }

    @JavascriptInterface
    public String getMyCurrentName() {
        return "You";
    }

    @JavascriptInterface
    public void bookmarkThis(String fid, String displayname, String profileimage) {
        DataManager dataManager = DataManager.getInstance(mContext);
        try {
            dataManager.bookmarkThis(fid, displayname, profileimage, mContext);
        } catch (Exception e) {
            Log.d("insertChatIntoDB", e.getMessage());
        }
    }

    @JavascriptInterface
    public boolean isAlreadyBookmark(String fid) {
        DataManager dataManager = DataManager.getInstance(mContext);
        try {
            return dataManager.CheckIsDataAlreadyInDBorNot("bookmarks", "fid", fid);
        } catch (Exception e) {
            Log.d("insertChatIntoDB", e.getMessage());
            return false;
        }
    }

    @JavascriptInterface
    public boolean isMyTree() {
        return true;
    }

    class ProfileDisconnectRequest {
        String srcfid;
        String desfid;
        public String secret_key;

        public String toJSON() {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("srcfid", srcfid);
                jsonObject.put("desfid", desfid);
                jsonObject.put("secret_key", secret_key);


                return jsonObject.toString();
            } catch (JSONException e) {
                Log.e("ProfileModel", "e = " + e.getMessage());
                return "";
            }

        }
    }
}





