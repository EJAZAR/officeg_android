package org.officeg.mobileapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.officeg.api.APIListener;
import org.officeg.api.APIManager;
import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.feed.ImageLoader;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonalProfileActivity extends AppCompatActivity {

    static final int DATE_DIALOG_ID = 1;
    static final int DATE_DIALOG_ID2 = 2;
    private static final int PERMISSION_REQUEST_CODE = 200;
    static int DOI;
    final Context context = this;
    String txtDisplayName, txtFirstName, txtLastname, namepattern, txtMobilenumber, dob, gender, doi, leftSidePersonFid, txtCountryCode;
    Button btnNext;
    TextView txtErrMessage, marque, spinnerview, gendertxtview;
    EditText edtxtdate, edtxtdisplayname, edtxtfirstname, edtxtlastname, edtxtmobilenumber, edtxtfid, edtxtdeathdate, ed_countrycode;
    CircleImageView ImgageViewProfile;
    String type2, type3, typePage, fromTutorialpage;
    ImageButton pickImage, btnDisplayNameInfo, btnFirstNameInfo, btnLastNameInfo, btnDOBInfo, btnGenderInfo, btnDOIInfo, btnLivingInfo, btnDeaceaseddateInfo;
    byte[] byteArray, profile, decodedByte, storeByteArray;
    Spinner doiSpinner, spinner_relationship;
    ProfileModel profileModel = null;
    UserModel user = null;
    RadioGroup radioGroup;
    ProgressDialog progDailog;
    RadioButton male, female;
    Bundle bundle;
    String currentFid;
    LinearLayout mobileNumberLayout;
    RelativeLayout firstNameLayout;
    RelativeLayout lastNameLayout;
    RelativeLayout DOBLayout;
    RelativeLayout DOILayout;
    RelativeLayout deathdate;
    RelativeLayout rl_relationship;
    RelativeLayout gender_rl;
    double weight_displayname, weight_profileimage, weight_genderr, total_weightage, weight_firstname, weight_lastname, weight_dob, weight_doi;
    Toolbar toolbar;
    Bitmap bitmap = null;
    ByteArrayOutputStream stream;
    String isconnected;
    TextInputLayout til;
    Switch livingSwitch;
    int cur = 0, setImageFlag = 0, doiPosition;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int year, month, day;
    private Uri imageUri;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (cur == DATE_DIALOG_ID) {
                edtxtdate.setText(new StringBuilder().append(day).append("/")
                        .append(month + 1).append("/").append(year));
            } else {
                edtxtdeathdate.setText(new StringBuilder().append(day).append("/")
                        .append(month + 1).append("/").append(year));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        user = UserModel.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_profile);
        bundle = getIntent().getExtras();

        Boolean networkCheck = NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        Log.i("FamilyG", "Network Connection in personal profile: " + networkCheck);
        if (!networkCheck)
            Toast.makeText(getApplication(), APIManager.errorMessage, Toast.LENGTH_LONG).show();

        profileModel = new ProfileModel();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        if (checkPermission()) {
//            Log.i("Family", "permissions granted granted");
//        } else
//            requestPermission();

        ImgageViewProfile = findViewById(R.id.profile_image_id);
        pickImage = findViewById(R.id.imageButton_picture);
        radioGroup = findViewById(R.id.gender_radiogp);

        edtxtdate = findViewById(R.id.date_id);
        edtxtdisplayname = findViewById(R.id.display_name_id);
        edtxtfirstname = findViewById(R.id.firstname_id);
        edtxtlastname = findViewById(R.id.lastname_id);
        edtxtmobilenumber = findViewById(R.id.mobilenumber_id);
        ed_countrycode = findViewById(R.id.ed_countrycode);
        edtxtfid = findViewById(R.id.fid_id);
        doiSpinner = findViewById(R.id.degree_of_interest_id);
        spinner_relationship = findViewById(R.id.spinner_relationship);

        gendertxtview = findViewById(R.id.gender_textview);
        til = findViewById(R.id.input_layout_displayname);
        livingSwitch = findViewById(R.id.living_switch);
        edtxtdeathdate = findViewById(R.id.death_date_id);
        deathdate = findViewById(R.id.reldeathdate);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);

        male = findViewById(R.id.male_radio_btn);
        female = findViewById(R.id.female_radio_btn);
        mobileNumberLayout = findViewById(R.id.mobilenumber_layout);
        firstNameLayout = findViewById(R.id.relFirstname);
        lastNameLayout = findViewById(R.id.relLastname);
        DOBLayout = findViewById(R.id.relDob);
        DOILayout = findViewById(R.id.relDoi);
        rl_relationship = findViewById(R.id.rl_relationship);

        currentFid = user.fid;
        leftSidePersonFid = "";
        isconnected = String.valueOf(false);

        spinnerview = findViewById(R.id.spinner_textview);
        spinnerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doiSpinner.performClick();
            }
        });
        gender_rl = findViewById(R.id.gender_rl);

        toolbar = findViewById(R.id.toolbartop);
        marque = findViewById(R.id.marque_scrolling_text);
        marque.setTextColor(Color.WHITE);
        marque.setText(R.string.your_profile_title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profileImage();

        profileVerification();
        edtxtdisplayname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtxtdisplayname.setCursorVisible(true);
                til.setError(null);

            }
        });

        livingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Utility.logFA(mFirebaseAnalytics, "profileform screen", "livingswitch", "switch");
                // do something, the isChecked will be true if the switch is in the On position
                if (isChecked) {
                    deathdate.setVisibility(View.GONE);
                    profileModel.setIsalive(true);
                } else {
                    deathdate.setVisibility(View.VISIBLE);
                    edtxtdeathdate.setText("");
                    profileModel.setIsalive(false);
                }
            }
        });
        setCurrentDateOnView();
        addListenerOnButton();
        Log.i("FamilyG", "calling onCreate");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                TextView gender_error = findViewById(R.id.gender_error);
                gender_error.setVisibility(View.GONE);
            }
        });
    }

    public void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        edtxtdeathdate.setText(edtxtdate.getText().toString());
    }

    public void addListenerOnButton() {
        edtxtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        edtxtdeathdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID2);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case DATE_DIALOG_ID:
                System.out.println("onCreateDialog  : " + id);
                cur = DATE_DIALOG_ID;
                // set date picker as current date
                DatePickerDialog datePickerDialog = new DatePickerDialog(PersonalProfileActivity.this, R.style.MyTheme, datePickerListener, year, month, day);

                //to set the dialog to display in center of page
                Window window = datePickerDialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                return datePickerDialog;

            case DATE_DIALOG_ID2:
                cur = DATE_DIALOG_ID2;
                System.out.println("onCreateDialog2  : " + id);
                // set date picker as current date
                DatePickerDialog datePickerDialog1 = new DatePickerDialog(PersonalProfileActivity.this, R.style.MyTheme, datePickerListener, year, month, day);

                //to set the dialog to display in center of page
                Window window1 = datePickerDialog1.getWindow();
                window1.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                window1.setGravity(Gravity.CENTER);
                return datePickerDialog1;

        }
        return null;
    }

    public void profileVerification() {
        btnNext = findViewById(R.id.btn_next);
        btnDisplayNameInfo = findViewById(R.id.btn_info_dispname);
        btnFirstNameInfo = findViewById(R.id.btn_info_firstname);
        btnLastNameInfo = findViewById(R.id.btn_info_surname);
        btnDOBInfo = findViewById(R.id.btn_info_date);


        btnGenderInfo = findViewById(R.id.btn_info_gender);
        btnDOIInfo = findViewById(R.id.btn_info_doi);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("CutPasteId")
            public void onClick(View v) {
                edtxtdisplayname.setCursorVisible(false);
                Utility.logFA(mFirebaseAnalytics, "personal profile", "next", "button");
                progDailog = new ProgressDialog(PersonalProfileActivity.this);
                progDailog.setTitle("Please Wait");
                progDailog.setMessage("Loading...");
                progDailog.setIndeterminate(false);
                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDailog.setCancelable(true);
                progDailog.show();

                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(selectedId);

                txtDisplayName = edtxtdisplayname.getText().toString();
                txtFirstName = edtxtfirstname.getText().toString();
                txtLastname = edtxtlastname.getText().toString();
                txtMobilenumber = edtxtmobilenumber.getText().toString();
                txtCountryCode = ed_countrycode.getText().toString();
                dob = ((EditText) findViewById(R.id.date_id)).getText().toString();
                doi = ((Spinner) findViewById(R.id.degree_of_interest_id)).getSelectedItem().toString().replaceAll("[Selct DgrofIns:()\\s-]+", "");

//                user.dob = ((EditText) findViewById(R.id.date_id)).getText().toString();
//
//                user.gender = radioButton.getText().toString();
//
                int doiIndex = ((Spinner) findViewById(R.id.degree_of_interest_id)).getSelectedItemPosition();
                Log.i("FamilyG", "DOI Index: " + doiIndex);
                user.doi = ((Spinner) findViewById(R.id.degree_of_interest_id)).getSelectedItem().toString().replaceAll("[Selct DgrofIns:()\\s-]+", "");

                Log.i("FamilyG", "from starting of reg: " + "gender:" + user.gender + "DEGREE:" + user.doi);

                namepattern = (getString(R.string.name_pattern));
                TextView gender_error = findViewById(R.id.gender_error);
                gender_error.setVisibility(View.GONE);

                if (txtDisplayName.equals("") || txtDisplayName.trim().length() == 0) {
                    Log.i("FamilyG", txtDisplayName);
                    edtxtdisplayname.requestFocus();

                    til.setError("Please provide display name");
                    progDailog.hide();

                } else if ((!(txtDisplayName.matches(namepattern))) || (txtDisplayName.trim().length() == 0)) {
                    Log.i("FamilyG", "pattern mismatched");
                    edtxtdisplayname.requestFocus();

                    til.setError("Please provide a valid name");
                    progDailog.hide();
                } else if (radioButton == null) {
                    Log.i("FamilyG", "gender not selected");

                    gender_error.setVisibility(View.VISIBLE);
                    progDailog.hide();
                } else {
                    gender_error.setVisibility(View.GONE);

                    gender = radioButton.getText().toString();
                    Log.i("FamilyG", "currentFid " + currentFid);
                    if (typePage != null && typePage.equals("EditFromSettings")) {
                        Log.i("FamilyG", "Inside EditFromSettings: " + user.profilecompleteness);
                        profileModel.secret_key = UserModel.getInstance().secret_key;
                        profileModel.profileimage = user.getProfileimage();
                        Log.i("FamilyG", "profileimageurl " + profileModel.profileimageurl);
                        if ((user.profileimageurl != null) && (!user.profileimageurl.isEmpty())) {
                            Log.i("FamilyG", "profileimageurl inside" + profileModel.profileimageurl);
                            weight_profileimage = 11.12;
                        } else {
                            weight_profileimage = 0.0;

                        }
                        user.dob = profileModel.dob = dob;
                        user.doi = profileModel.doi = doi;
                        Log.i("FamilyG", "dob " + profileModel.dob);
                        if (profileModel.dob != null && !profileModel.dob.trim().isEmpty()) {
                            weight_dob = 11.12;
                        }
                        profileModel.mobilenumber = UserModel.getInstance().mobilenumber;
                        profileModel.gender = radioButton.getText().toString();

                        Log.i("FamilyG", "secret_key " + profileModel.secret_key);
                    } else if (typePage != null && typePage.equals("profileView")) {
                        profileModel.profileimage = profileModel.getProfileimage();
                        profileModel.mobilenumber = txtMobilenumber;
                        profileModel.countrycode = txtCountryCode;
                        profileModel.dob = edtxtdate.getText().toString();
                        String gender = UserModel.getInstance().gender;
                        profileModel.gender = gender;
                    } else {
                        profileModel.profileimage = user.getProfileimage();
                        profileModel.mobilenumber = UserModel.getInstance().mobilenumber;
                        profileModel.dob = "";
                        profileModel.gender = radioButton.getText().toString();
                        user.gender = radioButton.getText().toString();
                    }

                    profileModel.secret_key = UserModel.getInstance().secret_key;
                    profileModel.displayname = txtDisplayName.trim();
                    weight_displayname = 11.12;
                    profileModel.deceaseddate = edtxtdeathdate.getText().toString();
                    weight_genderr = 11.12;
                    if (txtFirstName.equals("") || txtFirstName.isEmpty()) {
                        profileModel.firstname = "";
                        weight_firstname = 0.0;
                    } else {
                        profileModel.firstname = txtFirstName;
                        if (typePage != null && typePage.equals("EditFromSettings")) {
                            weight_firstname = 11.12;
                        }
                    }
                    if (txtLastname.equals("") || txtLastname.isEmpty()) {
                        profileModel.lastname = "";
                        weight_lastname = 0.0;
                    } else {
                        profileModel.lastname = txtLastname;
                        if (typePage != null && typePage.equals("EditFromSettings")) {
                            weight_lastname = 11.12;
                        }
                    }
                    if (doiIndex == 0) {
                        profileModel.doi = "";
                        weight_doi = 0.0;
                    } else {
                        profileModel.doi = ((Spinner) findViewById(R.id.degree_of_interest_id)).getSelectedItem().toString().replaceAll("[Selct DgrofIns:()\\s-]+", "");
                        if (typePage != null && typePage.equals("EditFromSettings")) {
                            weight_doi = 11.12;
                        }
                    }

                    profileModel.tagging = "";
                    profileModel.geolocationaccess = "";
                    profileModel.pushnotification = "";

                    if (bundle != null && bundle.containsKey("relationship")) {
                        String relation = bundle.getString("relationship");
                        if (relation != null && !relation.trim().isEmpty() && spinner_relationship.getSelectedItem() != null) {
                            String relationShipType = String.valueOf(spinner_relationship.getSelectedItem()).toUpperCase();
                            if (!relation.equalsIgnoreCase(relationShipType)) {
                                profileModel.relationshipType = relationShipType;
                                profileModel.relationship = relationShipType;
                            }
                        }
                    }
                    if (profileModel.profileimage != null) {
                        uploadProfileImage();
                    } else {
                        updateProfile();
                    }
                }

                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        });


        btnDisplayNameInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "displayname info", "button");
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_displayname);
                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        btnFirstNameInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "firstname info", "button");

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_firstname);

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        btnLastNameInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "lastname info", "button");

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_lastname);

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        btnDOBInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "dob info", "button");

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_dob);

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        btnGenderInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "gender info", "button");

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_gender);

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        btnDOIInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "doi info", "button");

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert);

                // set the custom_alert dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(R.string.info_doi);

                Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom_alert dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        doiSpinner.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                return false;
            }
        });
    }

    private void updateProfile() {
        try {
            if (typePage != null && typePage.equals("profileView") && setImageFlag == 0) {
                // Log.i("FamilyG", "bytearray viewprofile on cancel: " + byteArray);
                if (bundle.getString("profileimage") != null && !bundle.getString("profileimage").isEmpty()) {
                    profileModel.profileimageurl = bundle.getString("profileimage");
                    Log.i("FamilyG", "updateProfile profileimage url: " + profileModel.profileimageurl);
                }
            } else if (typePage != null && typePage.equals("EditFromSettings")) {
                if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {
                    user.profileimageurl = user.getProfileimageurl();
                    profileModel.profileimageurl = user.getProfileimageurl();
                    Log.i("FamilyG", "updateProfile user image url: " + user.profileimageurl);
                }

            }

            final byte[] profile = profileModel.toJSON().getBytes(Charset.forName("UTF-8"));
            Utility.forceCloseKeyboard(getApplicationContext(), getCurrentFocus());

            Log.i("FamilyG", "isconnected: " + isconnected);
            Log.i("FamilyG", "lfid: " + leftSidePersonFid);
            Log.d("profile", "profile?isowner=" + user.fid.equals(currentFid) + "&fid=" + currentFid
                    + "&isconnected=" + isconnected + "&lfid=" + leftSidePersonFid);
            Log.d("profile", profileModel.toJSON());

            String mode = "";
            if (profileModel.mobilenumber != null && !profileModel.mobilenumber.isEmpty() && !profileModel.mobilenumber.equals(bundle.getString("mobilenumber")) && (typePage != null && typePage.equals("profileView"))) {
                mode = "mobilenumber";
            }

            APIManager api = new APIManager("profile?isowner=" + user.fid.equals(currentFid) + "&fid=" + currentFid
                    + "&isconnected=" + isconnected + "&lfid=" + leftSidePersonFid + "&mode=" + mode, profile, new APIListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    user.setIsprofilecreated(true);

                    if (!(typePage != null && typePage.equals("profileView")) && (!(typePage != null && typePage.equals("EditFromSettings")))) {
                        double toatalweightage = weight_profileimage + weight_displayname + weight_genderr + weight_firstname + weight_lastname + weight_doi + weight_dob;
                        int toatalweightageinint = (int) toatalweightage;

                        user.setProfilecompleteness(String.valueOf(toatalweightageinint));
                        Log.i("FamilyG", "total weight-age from register is " + user.profilecompleteness);
                        Log.i("FamilyG", "total from register  is " + "image: " + weight_profileimage + " " + "displayname: " + weight_displayname + " "
                                + "gender: " + weight_genderr + " " + "firstname: " + weight_firstname + " " + "lastname: " + weight_lastname + " "
                                + " " + "doi: " + weight_doi + " " + "dob: " + weight_dob);
                        user.save();
                    }

                    if (user.fid.equals(currentFid)) {
                        Log.i("FamilyG", "currentfid block");
                        user.displayname = txtDisplayName;
                        user.firstname = txtFirstName;
                        user.lastname = txtLastname;
                        user.dob = profileModel.dob;
                        user.profileimage = user.getProfileimage();
                        UserModel.getInstance().save();

                    }
                    if (typePage != null && typePage.equals("EditFromSettings")) {
                        double toatalweightage = weight_profileimage + weight_displayname + weight_genderr + weight_firstname + weight_lastname + weight_doi + weight_dob;
                        int toatalweightageinint = (int) toatalweightage;

                        user.setProfilecompleteness(String.valueOf(toatalweightageinint));
                        Log.i("FamilyG", "total weight-age is " + user.profilecompleteness);
                        Log.i("FamilyG", "total  is " + "image: " + weight_profileimage + " " + "displayname: " + weight_displayname + " "
                                + "gender: " + weight_genderr + " " + "firstname: " + weight_firstname + " " + "lastname: " + weight_lastname + " "
                                + "doi: " + weight_doi + " " + "dob: " + weight_dob);
                        user.save();
                        Intent intent = new Intent(PersonalProfileActivity.this, BottomBarActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Toast.makeText(PersonalProfileActivity.this, "Profile edited", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        finish();
                    } else if (typePage != null && typePage.equals("profileView")) {
                        if (profileModel.relationship != null && !profileModel.relationship.trim().isEmpty()) {
                            UserModel.getInstance().loadMembers();
                        }

                        Log.i("FamilyG", "displayname is " + profileModel.getDisplayname());
                        Intent intent = new Intent(PersonalProfileActivity.this, BottomBarActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Toast.makeText(PersonalProfileActivity.this, "Profile edited successfully", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        finish();
                    } else {
                        UserModel.getInstance().save();
                        int waitingrequests;
                        try {
                            waitingrequests = Integer.parseInt(user.waitingrequests);

                            if (waitingrequests != 0) {
                                Log.i("FamilyG", "if part");//for first time users(already invited before register)...
                                Log.i("FamilyG", "waitingrequest " + user.waitingrequests);
                                Intent intent_activity = new Intent(PersonalProfileActivity.this, NotificationActivity.class);
                                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent_activity.putExtra("waitingrequesttype", "1");

                                startActivity(intent_activity);
                                finish();
                            } else {
                                Log.i("FamilyG", "else part");
                                Log.i("FamilyG", "waitingrequest " + user.waitingrequests);
//                                Intent intent_activity = new Intent(PersonalProfileActivity.this, ConnectInit.class);
//                                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent_activity);
//                                finish();
                                Intent intent_activity = new Intent(PersonalProfileActivity.this, BottomBarActivity.class);
                                intent_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent_activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent_activity);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    UserModel.getInstance().save();
                }

                @Override
                public void onSuccess(JSONArray array) {

                }

                @Override
                public void onFailure(String errorMessage, String errorCode) {
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                    progDailog.dismiss();
                }
            }, APIManager.HTTP_METHOD.POST);
            api.execute();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void uploadProfileImage() {
        Log.i("FamilyG", "upload from personal profile");
//        Log.i("FamilyG", "profileImage:" + user.profileimage);

        APIManager uploadapi = new APIManager("upload", profileModel.profileimage, new APIListener() {

            @Override
            public void onSuccess(JSONObject result) {
                try {

                    if (user.fid.equals(currentFid)) {
                        user.profileimageurl = result.getString("url");
                        weight_profileimage = 11.12;
                        user.profileimage = byteArray;
                        UserModel.getInstance().save();

                    }

                    profileModel.profileimageurl = result.getString("url");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                updateProfile();
            }

            @Override
            public void onSuccess(JSONArray array) {
            }

            @Override
            public void onFailure(String errorMessage, String errorCode) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                progDailog.dismiss();

            }
        }, APIManager.HTTP_METHOD.FILE);
        uploadapi.execute();
    }

    private void showDate(int year, int month, int day) {
        edtxtdate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    public void profileImage() {

        ImgageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "pickimage", "button");
                selectImage();
            }
        });

        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "pickimage", "button");
                selectImage();
            }
        });
    }

    private void selectImage() {

//        if (Build.VERSION.SDK_INT >= 24) {
//            try {
//                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
//                m.invoke(null);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(PersonalProfileActivity.this);
        builder.setTitle("Add Profile Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
//                Log.i("FamilyG","profileModel.profileimage on open of popup: " +profileModel.profileimage);
                if (options[item].equals("Take Photo")) {
                    try {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        File f = Utility.createImageFile(PersonalProfileActivity.this);
                        imageUri = Utility.getUriFromFile(getApplicationContext(), f);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Choose from Gallery"), 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            Bitmap scaled = null;
            final Dialog dialog = new Dialog(PersonalProfileActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
            dialog.setContentView(R.layout.profilepic_confimation);
            Button dialogBlockButtonOk = dialog.findViewById(R.id.dialogbuttonok);
            Button dialogBlockButtonCancel = dialog.findViewById(R.id.dialogbuttoncancel);
            ImageView image = dialog.findViewById(R.id.confirm_image);
            if (requestCode == 1) {
                try {
                    bitmap = Utility.decodeSampledBitmapFromResource(imageUri, 200, 200, PersonalProfileActivity.this);
                    stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    image.setImageBitmap(scaled);
                    final Bitmap finalScaled = scaled;
                    dialogBlockButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setImageFlag = 1;
                            ImgageViewProfile.setImageBitmap(finalScaled);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                            byteArray = stream.toByteArray();
                            user.profileimage = byteArray;
                            UserModel.getInstance().save();
                            profileModel.profileimage = byteArray;
                            dialog.dismiss();
                        }
                    });

                    dialogBlockButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setImageFlag = 0;
                            if (typePage != null && typePage.equals("profileView")) {
                                if (bundle.getString("profileimage") != null && !bundle.getString("profileimage").isEmpty()) {
                                    profileModel.profileimageurl = bundle.getString("profileimage");
                                    Log.i("FamilyG", "viewprofile on camera cancel: " + profileModel.profileimageurl);
                                }

                            } else if (typePage != null && typePage.equals("EditFromSettings")) {
                                if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {
                                    user.profileimageurl = user.getProfileimageurl();
                                    user.save();
                                    profileModel.profileimageurl = user.getProfileimageurl();
                                    Log.i("FamilyG", "edit from settings on camera cancel: " + user.profileimageurl);
                                }
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
//                    byteArray = stream.toByteArray();
//                    user.profileimage = byteArray;
//                    UserModel.getInstance().save();
//                    profileModel.profileimage = byteArray;

                    Log.i("FamilyG", "Image size : " + byteArray.length);
                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                Log.i("FamilyG", "selectedImage is " + "calling");
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(imageStream);
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    Log.i("FamilyG", "bitmap is " + scaled);
                    image.setImageBitmap(scaled);
                    final Bitmap finalScaled = scaled;
                    dialogBlockButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setImageFlag = 1;
                            ImgageViewProfile.setImageBitmap(finalScaled);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                            byteArray = stream.toByteArray();
                            user.profileimage = byteArray;
                            UserModel.getInstance().save();
                            profileModel.profileimage = byteArray;
                            dialog.dismiss();
                        }
                    });
                    dialogBlockButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setImageFlag = 0;
                            if (typePage != null && typePage.equals("profileView")) {
                                //Log.i("FamilyG","bytearray viewprofile on cancel: "+byteArray);
                                if (bundle.getString("profileimage") != null && !bundle.getString("profileimage").isEmpty()) {
                                    profileModel.profileimageurl = bundle.getString("profileimage");
                                    Log.i("FamilyG", "viewprofile on gallery cancel: " + profileModel.profileimageurl);
                                }

                            } else if (typePage != null && typePage.equals("EditFromSettings")) {
                                if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {
                                    user.profileimageurl = user.getProfileimageurl();
                                    user.save();
                                    profileModel.profileimageurl = user.getProfileimageurl();
                                    Log.i("FamilyG", "edit from settings on gallery cancel: " + user.profileimageurl);
                                }
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

//    private boolean checkPermission() {
//        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
//        return result == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermission() {
//        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0) {
//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if (locationAccepted) {
//                    } else {
//                        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//                            showMessageOKCancel("You need to allow access to both the permissions",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
//                                                    PERMISSION_REQUEST_CODE);
//                                        }
//                                    });
//                            return;
//                        }
//                    }
//                }
//                break;
//        }
//    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener
            okListener) {
        new android.support.v7.app.AlertDialog.Builder(PersonalProfileActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {

        Intent i = getIntent();
        typePage = i.getStringExtra("type");
        fromTutorialpage = i.getStringExtra("fromTutorial");
        if (typePage != null && typePage.equals("EditFromSettings")) {
            Intent intent = new Intent(PersonalProfileActivity.this, BottomBarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else if (typePage != null && typePage.equals("profileView")) {
            super.onBackPressed();
        } else if (fromTutorialpage != null && fromTutorialpage.equals("tutorialpage")) {
            Intent intent = new Intent(PersonalProfileActivity.this, TutorialActivity.class);
            startActivity(intent);
            finish();
        } else
            finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
//
//        profileImage();
        Log.i("FamilyG", "calling onStart");
        Intent i = getIntent();
        bundle = getIntent().getExtras();
        type2 = i.getStringExtra("type2");
        type3 = i.getStringExtra("type3");
        typePage = i.getStringExtra("type");

        final ArrayAdapter<String> spinnerdoiAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, getResources().getStringArray(R.array.doi)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }

                return view;
            }
        };
        spinnerdoiAdapter.setDropDownViewResource(R.layout.spinner_item);
        doiSpinner.setAdapter(spinnerdoiAdapter);

        doiSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utility.logFA(mFirebaseAnalytics, "personal profile", "doispinner", "spinner");
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection First item is disable and it is used for hint
                DOI = position;
                Log.i("FamilyG: ", "position: " + position);
                Log.i("FamilyG: ", "DOI in itemselected: " + DOI);
//                if (position > 0) {
//                    // Notify the selected item text
//                }

                if (typePage != null && typePage.equals("EditFromSettings")) {
                    user.doi = String.valueOf(position);
                    user.save();
                } else if (typePage != null && typePage.equals("profileView")) {
                    profileModel.doi = String.valueOf(position);
                }
                user.doi = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (typePage != null && typePage.equals("EditFromSettings")) {
            firstNameLayout.setVisibility(View.VISIBLE);
            lastNameLayout.setVisibility(View.VISIBLE);
//            DOILayout.setVisibility(View.VISIBLE);
            DOBLayout.setVisibility(View.VISIBLE);

            Log.i("FamilyG", "Insie editform settigs");
            user = UserModel.getInstance();
            currentFid = user.fid;

            leftSidePersonFid = "";
            if (user.displayname != null) {
                edtxtdisplayname.setText(user.displayname);
                edtxtdisplayname.setSelection(edtxtdisplayname.getText().length());
            }
            if (user.firstname != null)
                edtxtfirstname.setText(user.firstname);
            if (user.lastname != null)
                edtxtlastname.setText(user.lastname);
            if (user.dob != null)
                edtxtdate.setText(user.dob);
            else
                edtxtdate.setText("");
            /*if (user.profileimage != null) {
                Bitmap profileimagebitmap = BitmapFactory.decodeByteArray(user.profileimage, 0, user.profileimage.length);
                int nh = (int) (profileimagebitmap.getHeight() * (512.0 / profileimagebitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(profileimagebitmap, 512, nh, true);
                ImgageViewProfile.setImageBitmap(scaled);
                UserModel.getInstance().save();
            }*/

            if (user.profileimageurl != null && !user.profileimageurl.trim().isEmpty()) {

                ImageLoader imageLoader = imageLoader = new ImageLoader(context);
                imageLoader.DisplayImage(user.profileimageurl, ImgageViewProfile);
                user.save();

            } else {
                ImgageViewProfile.setImageResource(R.drawable.default_profile_image);
            }

            if (user.gender != null && (user.gender).equals("Male")) {
                male.setChecked(true);
                female.setChecked(false);
            } else if (user.gender != null && (user.gender).equals("Female")) {
                female.setChecked(true);
                male.setChecked(false);
            } else {
                male.setChecked(true);
                female.setChecked(false);
            }

            if (doiPosition == 0 || doiPosition < 0) {
                doiSpinner.setSelection(3);
            }

            if (user.doi != null) {
                try {
                    doiPosition = Integer.parseInt(user.doi);

                    Log.i("FamilyG: ", "DOI from itemselected: " + DOI);
                    Log.i("FamilyG: ", "compareValue: " + doiPosition);
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.doi, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    doiSpinner.setAdapter(adapter);
                    if (doiPosition == 0) {
                        doiSpinner.setSelection(3);
                        user.doi = String.valueOf(doiPosition);
                    } else if (doiPosition == 1 || doiPosition == 2 || doiPosition == 3 || doiPosition == 4 || doiPosition == 5 || doiPosition == 6 || doiPosition == 7) {
                        doiSpinner.setSelection(doiPosition);
                        user.doi = String.valueOf(doiPosition);
                    }
                    UserModel.getInstance().save();

                } catch (NumberFormatException e) {
                    e.printStackTrace();

                }

            } else {
                Log.i("FamilyG: ", "DOI from itemselected: " + DOI);

                int doiPosition = DOI;
                Log.i("FamilyG: ", "DOI from itemselected: " + DOI);
                Log.i("FamilyG: ", "compareValue: " + doiPosition);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.doi, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                doiSpinner.setAdapter(adapter);
                if (doiPosition == 0) {
                    doiSpinner.setSelection(3);
                    user.doi = String.valueOf(doiPosition);
                } else if (doiPosition == 1 || doiPosition == 2 || doiPosition == 3 || doiPosition == 4 || doiPosition == 5 || doiPosition == 6 || doiPosition == 7) {
                    doiSpinner.setSelection(doiPosition);
                    user.doi = String.valueOf(doiPosition);
                }
                UserModel.getInstance().save();
            }
            Log.i("FamilyG", "EditFrom settings:" + "gender:" + user.gender + "DEGREE:" + user.doi);
            UserModel.getInstance().save();

        }

        if (typePage != null && typePage.equals("profileView")) {

            firstNameLayout.setVisibility(View.VISIBLE);
            lastNameLayout.setVisibility(View.VISIBLE);
            DOBLayout.setVisibility(View.VISIBLE);
            mobileNumberLayout.setVisibility(View.VISIBLE);
            rl_relationship.setVisibility(View.VISIBLE);

            String relation = bundle.getString("relationship");
            if (relation != null && !relation.trim().isEmpty()) {
                relation = relation.substring(0, 1).toUpperCase() + relation.substring(1).toLowerCase();

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.relationship_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                int spinnerPosition = getIndex(spinner_relationship, relation);
                spinner_relationship.setSelection(spinnerPosition);

//                String relationship_spinner = (String) spinner_relationship.getSelectedItem();
//                Log.d("PersonalProfileActivity", relationship_spinner);
            }

            Log.i("FamilyG", "Insie view profile");
            currentFid = bundle.getString("fid");
            isconnected = bundle.getString("connected");

            Log.i("FamilyG", "viewprofile: " + bundle.getString("connected"));
            leftSidePersonFid = bundle.getString("LFID");

            marque.setText((bundle.getString("displayname")) + " profile ");

            //created relation persons details
            if (bundle.getString("ownerfid") != null && bundle.getString("ownerfid").equals(user.getFid())) {
                livingSwitch.setVisibility(View.VISIBLE);
                Log.i("FamilyG", "profileView inside if");
                Log.i("FamilyG", "Isaliveeee: " + bundle.getBoolean("isalive"));
                edtxtdisplayname.setText(bundle.getString("displayname"));
                edtxtdisplayname.setSelection(edtxtdisplayname.getText().length());

                if (bundle.getString("firstname") != null && !bundle.getString("firstname").isEmpty())
                    edtxtfirstname.setText(bundle.getString("firstname"));
                Log.i("FamilyG: ", "firstname from bundle: " + bundle.getString("firstname"));

                if (bundle.getString("lastname") != null && !bundle.getString("lastname").isEmpty())
                    edtxtlastname.setText(bundle.getString("lastname"));

                if (bundle.getBoolean("isalive")) {
                    livingSwitch.setChecked(true);
                    profileModel.isalive = true;
                    if (bundle.getString("mobilenumber") != null && !bundle.getString("mobilenumber").isEmpty()) {
                        edtxtmobilenumber.setText(bundle.getString("mobilenumber"));
                        ed_countrycode.setText(bundle.containsKey("countrycode") ? bundle.getString("countrycode") : "");
                        Log.i("FamilyG: ", "number from bundle: " + bundle.getString("mobilenumber"));
                    }
                } else {
                    livingSwitch.setChecked(false);
                    profileModel.isalive = false;
                    mobileNumberLayout.setVisibility(View.GONE);
                    edtxtdeathdate.setVisibility(View.VISIBLE);
                    if (bundle.getString("deceaseddate") != null && !bundle.getString("deceaseddate").isEmpty()) {
                        edtxtdeathdate.setText(bundle.getString("deceaseddate"));
                        Log.i("FamilyG: ", "death date from bundle: " + bundle.getString("deceaseddate"));
                    } else
                        edtxtdeathdate.setText("");
                }
                if (bundle.getString("profileimage") != null && !bundle.getString("profileimage").isEmpty()) {
                    ImageLoader imageLoader = imageLoader = new ImageLoader(getApplicationContext());
                    imageLoader.DisplayImage(bundle.getString("profileimage"), ImgageViewProfile);
                /*String imageurl=bundle.getString("profileimage");
                byte[] imageFromViewProfile = imageurl.getBytes();
                profileModel.profileimage=imageFromViewProfile;*/
                } else {
                    ImgageViewProfile.setImageResource(R.drawable.default_profile_image);
                }

                if (bundle.getString("dob") != null && !bundle.getString("dob").isEmpty())
                    edtxtdate.setText(bundle.getString("dob"));
                else
                    edtxtdate.setText("");

                if (bundle.getString("gender") != null && !bundle.getString("gender").isEmpty()) {
                    if ((bundle.getString("gender")).equals("Male")) {
                        male.setChecked(true);
                        female.setChecked(false);
                        female.setEnabled(false);
                    } else if ((bundle.getString("gender")).equals("Female")) {
                        female.setChecked(true);
                        male.setChecked(false);
                        male.setEnabled(false);
                    }
                }
            } else {
                Log.i("FamilyG", "profileView inside else");
                edtxtfirstname.setEnabled(false);
                edtxtlastname.setEnabled(false);
                edtxtmobilenumber.setEnabled(false);
                ed_countrycode.setEnabled(false);
                ImgageViewProfile.setEnabled(false);
                ImgageViewProfile.setAlpha(.5f);
                pickImage.setEnabled(false);
                pickImage.setAlpha(.5f);
                doiSpinner.setEnabled(false);
                spinnerview.setEnabled(false);
                male.setEnabled(false);
                female.setEnabled(false);
                edtxtdate.setEnabled(false);
                gendertxtview.setEnabled(false);

                edtxtdisplayname.setText(bundle.getString("displayname"));
                edtxtdisplayname.setSelection(edtxtdisplayname.getText().length());

                if (bundle.getString("firstname") != null && !bundle.getString("firstname").isEmpty())
                    edtxtfirstname.setText(bundle.getString("firstname"));
                Log.i("FamilyG: ", "firstname from bundle: " + bundle.getString("firstname"));

                if (bundle.getString("lastname") != null && !bundle.getString("lastname").isEmpty())
                    edtxtlastname.setText(bundle.getString("lastname"));


                if (bundle.getString("mobilenumber") != null && !bundle.getString("mobilenumber").isEmpty()) {

                    edtxtmobilenumber.setText(bundle.getString("mobilenumber"));
                    ed_countrycode.setText(bundle.containsKey("countrycode") ? bundle.getString("countrycode") : "");
                    Log.i("FamilyG: ", "number from bundle: " + bundle.getString("mobilenumber"));
                }

                if (bundle.getString("profileimage") != null && !bundle.getString("profileimage").isEmpty()) {
                    ImageLoader imageLoader = imageLoader = new ImageLoader(getApplicationContext());
                    imageLoader.DisplayImage(bundle.getString("profileimage"), ImgageViewProfile);
                /*String imageurl=bundle.getString("profileimage");
                byte[] imageFromViewProfile = imageurl.getBytes();
                profileModel.profileimage=imageFromViewProfile;*/
                } else {
                    ImgageViewProfile.setImageResource(R.drawable.default_profile_image);
                }

                if (bundle.getString("doi") == null) {
                    doiSpinner.setSelection(3);
                } else {
                    int doiPosition = DOI;
                    Log.i("FamilyG: ", "DOI from itemselected: " + DOI);
                    Log.i("FamilyG: ", "compareValue: " + doiPosition);
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.doi, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    doiSpinner.setAdapter(adapter);
                    if (doiPosition == 0)
                        doiSpinner.setSelection(3);

                    else if (doiPosition < 1) {
                        doiSpinner.setSelection(doiPosition);
                    }
                }

                if (bundle.getString("dob") != null && !bundle.getString("dob").isEmpty())
                    edtxtdate.setText(bundle.getString("dob"));
                else
                    edtxtdate.setText("");


                if (bundle.getString("gender") != null && !bundle.getString("gender").isEmpty()) {
                    if ((bundle.getString("gender")).equals("Male")) {
                        male.setChecked(true);
                        female.setChecked(false);
                    } else if ((bundle.getString("gender")).equals("Female")) {
                        female.setChecked(true);
                        male.setChecked(false);
                    }
                }
            }
        }
        if (typePage != null && typePage.equals("signup")) {
            gender_rl.setVisibility(View.VISIBLE);
        }
    }

    private int getIndex(Spinner spinner, String givenRelation) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (String.valueOf(spinner.getItemAtPosition(i)).equalsIgnoreCase(givenRelation)) {
                return i;
            }
        }
        return index;
    }
}

