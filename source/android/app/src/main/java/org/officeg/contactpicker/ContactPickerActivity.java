package org.officeg.contactpicker;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.officeg.api.NetworkConnectionreceiver;
import org.officeg.mobileapp.ConnectionRequest;
import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;

import static android.Manifest.permission.READ_CONTACTS;

public class ContactPickerActivity extends AppCompatActivity {
    int tempInt;
    ContactPickerAdaptor contactPickerAdaptor;
    LinkedList<ProfileModel> phoneContactList;
    private String TAG = ContactPickerActivity.class.getSimpleName();
    private static final int RC_REQUEST_CODE = 100;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact_request, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.send_request_id) {
            onRequestContact();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_picker);
        contactPickerAdaptor = new ContactPickerAdaptor(ContactPickerActivity.this);

        checkPermission();
    }

    public LinkedList<ProfileModel> getAllContacts(ContentResolver cr) {
        LinkedList<ProfileModel> phoneContactList = new LinkedList<ProfileModel>();
        try {

            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
            };

            Cursor phones = cr.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null,
                    null, ContactsContract.Contacts.SORT_KEY_PRIMARY + " ASC");

            if (phones != null) {
//                Toast.makeText(getApplicationContext(), "getAllContacts phones not null", Toast.LENGTH_SHORT).show();
                try {
                    HashSet<String> normalizedNumbersAlreadyFound = new HashSet<>();
                    int indexOfNormalizedNumber = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
//                    Toast.makeText(getApplicationContext(), "getAllContacts indexOfNormalizedNumber = " + indexOfNormalizedNumber, Toast.LENGTH_SHORT).show();

                    while (phones.moveToNext()) {
                        String normalizedNumber = phones.getString(indexOfNormalizedNumber);

                        if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
                            String name = phones
                                    .getString(phones
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            String phoneNumber = phones
                                    .getString(phones
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            ProfileModel profileModel = new ProfileModel();
                            profileModel.setDisplayname(name);
                            profileModel.setMobilenumber(phoneNumber);
                            phoneContactList.add(profileModel);
                        }
                    }
                } finally {
                    phones.close();
                }
            }
            Log.d("getAllContacts", phoneContactList.size() + " is size");
//            Toast.makeText(getApplicationContext(), "getAllContacts phoneContactList" + phoneContactList.size(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "getAllContacts Exception = " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return phoneContactList;
    }


    private void showPhoneContats(LinkedList<ProfileModel> adminList) {
        try {
            contactPickerAdaptor.setPhoneContactList(adminList);
            contactPickerAdaptor.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onRequestContact() {
        final LinkedList<ProfileModel> selectedContacts = new LinkedList<>();
        for (ProfileModel contactSelected : phoneContactList) {
            if (contactSelected.isSelected()) {
                selectedContacts.add(contactSelected);
                Log.d(TAG, contactSelected.mobilenumber);
            }
        }
        if (selectedContacts.size() > 0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(ContactPickerActivity.this);
            alert.setTitle("Connection Request");
            alert.setMessage("This will send connection request to selected contacts");
            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    final ProgressDialog progDailog = ProgressDialog.show(ContactPickerActivity.this, "Loading", "Please Wait ...", true);
                    dialog.dismiss();
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                afterPickingContact(selectedContacts, progDailog);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
            });

            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alert.show();
        }
    }


    private void afterPickingContact(LinkedList<ProfileModel> pickedContact, ProgressDialog progDailog) {
        try {
            ConnectionRequest connectionRequest = new ConnectionRequest();
            for (ProfileModel profileModel : pickedContact) {
                String phoneNumber = profileModel.getMobilenumber();
                phoneNumber = phoneNumber.replaceAll("[()\\s-]+", "");

                TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                String usersCountryISOCode = manager.getSimCountryIso().toUpperCase();

                Log.i("FamilyG", "Country code : " + usersCountryISOCode);
                Log.i("FamilyG", "Original number : " + phoneNumber);

                Phonenumber.PhoneNumber phone = PhoneNumberUtil.getInstance().parseAndKeepRawInput(phoneNumber, usersCountryISOCode.toUpperCase());

                Log.i("FamilyG", "Extracted number : " + phone.getCountryCode());

                phoneNumber = PhoneNumberUtil.getInstance().format(phone, PhoneNumberUtil.PhoneNumberFormat.E164);

                connectionRequest.connectWithRelation(getApplicationContext(), getString(R.string.relation), profileModel.getDisplayname(), phoneNumber, pickedContact.size(), progDailog, TAG);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkPermission() {
        int result_rc = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        if (result_rc == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getApplicationContext(), "onRequestPermissionsResult checkPermission = true", Toast.LENGTH_SHORT).show();
            afterPermissionGranted();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ContactPickerActivity.this, new String[]{READ_CONTACTS}, RC_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        Toast.makeText(getApplicationContext(), "onRequestPermissionsResult requestCode = " + requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case RC_REQUEST_CODE:
//                Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults.length = " + grantResults.length, Toast.LENGTH_SHORT).show();
                if (grantResults.length > 0) {
//                    Toast.makeText(getApplicationContext(), "onRequestPermissionsResult grantResults[0] = " + grantResults[0], Toast.LENGTH_SHORT).show();
                    boolean wesPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (wesPermissionGranted) {
                        afterPermissionGranted();
                    } else {
                        Toast.makeText(getApplicationContext(), "You need to grant permissions", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }
                break;
        }
    }

    private void afterPermissionGranted() {
        NetworkConnectionreceiver.isConnection = NetworkConnectionreceiver.isConnected(getApplicationContext());
        try {

            Toolbar toolbar = findViewById(R.id.toolbartop);
            TextView marque = findViewById(R.id.marque_scrolling_text);
            marque.setTextColor(Color.WHITE);
            marque.setText(getString(R.string.contacts));
            setSupportActionBar(toolbar);

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            phoneContactList = getAllContacts(getContentResolver());
            showPhoneContats(phoneContactList);

            ListView listView = findViewById(R.id.list_viewgroup);
            listView.setAdapter(contactPickerAdaptor);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long arg3) {
//                    final ProfileModel profileModel = phoneContactList.get(position);
                    try {
                        CheckBox checkBox = view.findViewById(R.id.checkbox_group_addmember);
                        checkBox.performClick();
                    } catch (Exception e) {
                        Log.e(TAG, "e = " + e.getMessage());
                    }

                }
            });

            final EditText search_contact_picker = findViewById(R.id.search_contact_picker);
            search_contact_picker.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    String text = arg0.toString().toLowerCase(Locale.getDefault());
                    contactPickerAdaptor.filter(text);
                    Log.d(TAG, arg0.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                    // TODO Auto-generated method stub
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "OnCreate Exception e =" + e.getMessage());
            e.printStackTrace();
        }
    }

}