package org.officeg.chat;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import org.officeg.mobileapp.R;
import org.officeg.model.ChatMessageModel;
import org.officeg.util.Utility;

import java.io.File;

import static org.officeg.mobileapp.R.string.audio;

/**
 * Created by techiejackuser on 09/06/17.
 */

public class AudioPost {
    Handler handler;
    MediaPlayer mediaPlayer;
    private View vi;
    private Context context;
    private boolean mStartPlaying = true;
    private String TAG = AudioPost.class.getSimpleName();
    ChatMessageAdapter chatMessageAdapter;

    public AudioPost(View view, Context context, MediaPlayer mediaPlayer, ChatMessageAdapter chatMessageAdapter) {
        vi = view;
        this.context = context;
        this.mediaPlayer = mediaPlayer;
        this.chatMessageAdapter = chatMessageAdapter;
    }

    public void postAudio(final ChatMessageModel chatMessageModel) {
        try {
            String fileUrl = chatMessageModel.getAudio();
            String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);

            File receivedDirectory = Utility.getReceivedDirectoryByType(Environment.DIRECTORY_MUSIC, context);
//            ImageView imageView = (ImageView) vi.findViewById(R.id.chat_bubble_imageview);
            ImageView imageView = vi.findViewById(R.id.audiodialog_play);
            LinearLayout bubble_layout_parent = vi.findViewById(R.id.bubble_layout_parent);
            bubble_layout_parent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            final File fileCheck = new File(receivedDirectory + File.separator + fileName);
            if (!fileCheck.exists()) {
                Log.d(TAG, "if " + fileCheck.getAbsolutePath());
                DownloadFileTask downloadFileTask = new DownloadFileTask(context, vi, imageView, false);
                downloadFileTask.execute(fileUrl, fileName, context.getString(audio));
            } else {
                Log.d(TAG, "else " + fileCheck.getAbsolutePath());
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (fileCheck.exists()) {
                            invertPlay(fileCheck.getAbsolutePath());
                            handler = new Handler();
                            LayoutInflater inflater = LayoutInflater.from(context);
                            View audioDialogView = inflater.inflate(R.layout.audiodialog, null);
                            final AlertDialog.Builder audioDialogBuilder = new AlertDialog.Builder(context);
                            audioDialogBuilder.setTitle("Playing audio");
                            audioDialogBuilder.setView(audioDialogView);
                            final SeekBar seekBar = audioDialogView.findViewById(R.id.audiodialog_seekbar);

                            final SeekBar seekBarlv = vi.findViewById(R.id.audiodialog_seekbar);

                            final Runnable moveSeekBarThread = new Runnable() {

                                public void run() {
                                    try {
                                        int mediaPos_new = mediaPlayer.getCurrentPosition();
                                        int mediaMax_new = mediaPlayer.getDuration();
                                        seekBar.setMax(mediaMax_new);
                                        seekBar.setProgress(mediaPos_new);

                                        seekBarlv.setMax(mediaMax_new);
                                        seekBarlv.setProgress(mediaPos_new);

                                        handler.postDelayed(this, 100); //Looping the thread after 0.1 second
                                    } catch (Exception e) {
                                        Log.e(TAG, "moveSeekBarThread");
                                    }
                                }
                            };


                            int mediaPos = mediaPlayer.getCurrentPosition();
                            int mediaMax = mediaPlayer.getDuration();

                            seekBar.setMax(mediaMax); // Set the Maximum range of the
                            seekBar.setProgress(mediaPos);// set current progress to song's

                            seekBarlv.setMax(mediaMax);
                            seekBarlv.setProgress(mediaPos);


                            handler.removeCallbacks(moveSeekBarThread);
                            handler.postDelayed(moveSeekBarThread, 100);


                            final AlertDialog audioDialog = audioDialogBuilder.create();
                            audioDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    handler.removeCallbacks(moveSeekBarThread);
                                    stopPlaying();
                                    mStartPlaying = true;
                                    seekBarlv.setProgress(0);
                                }
                            });
                            audioDialog.show();
                            audioDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    handler.removeCallbacks(moveSeekBarThread);
                                    stopPlaying();
                                    mStartPlaying = true;
                                    seekBarlv.setProgress(0);
                                }
                            });
                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    audioDialog.dismiss();
                                    handler.removeCallbacks(moveSeekBarThread);
                                    stopPlaying();
                                    mStartPlaying = true;
                                    seekBarlv.setProgress(0);
                                }
                            });

                            final ImageView audiodialog_play = audioDialogView.findViewById(R.id.audiodialog_play);
                            final ImageView audiodialog_pause = audioDialogView.findViewById(R.id.audiodialog_pause);
                            audiodialog_play.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    audiodialog_pause.setVisibility(View.VISIBLE);
                                    audiodialog_play.setVisibility(View.GONE);
                                    handler.postDelayed(moveSeekBarThread, 100);
                                    mediaPlayer.start();
                                }
                            });
                            audiodialog_pause.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    audiodialog_play.setVisibility(View.VISIBLE);
                                    audiodialog_pause.setVisibility(View.GONE);
                                    handler.removeCallbacks(moveSeekBarThread);
                                    mediaPlayer.pause();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    View parent = (View) v.getParent();
                    parent.performLongClick();
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invertPlay(String absolutePath) {
        try {
            onPlay(mStartPlaying, absolutePath);
            mStartPlaying = !mStartPlaying;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onPlay(boolean start, String mAudioFileName) {
        if (start) {
            startPlaying(mAudioFileName);
        } else {
            stopPlaying();
        }
    }

    private void startPlaying(String mAudioFileName) {
        try {
            mediaPlayer.setDataSource(mAudioFileName);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            Log.e(TAG, "startPlaying() failed");
        }
    }

    private void stopPlaying() {
        try {
            mediaPlayer.reset();
        } catch (Exception e) {
            Log.e(TAG, "stopPlaying() failed");
        }
    }
}