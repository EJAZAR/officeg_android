package org.officeg.feedpost;

public class PostScreenItemsmodel {

    private String messageString, messageType;
    private Object object;

    public PostScreenItemsmodel(String messageString,
                                String messageType, Object object) {
        this.messageString = messageString;
        this.messageType = messageType;
        this.object = object;
    }

    public String getMessageString() {
        return messageString;
    }

    public String getMessageType() {
        return messageType;
    }

    public Object getObject() {
        return object;
    }

}
