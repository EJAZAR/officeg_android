package org.officeg.chat.group;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.officeg.mobileapp.R;
import org.officeg.model.ProfileModel;
import org.officeg.model.UserModel;
import org.officeg.util.Utility;

import java.util.LinkedList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by techiejackuser on 06/06/17.
 */

public class ViewGroupMembersAdaptor extends BaseAdapter {

    private LayoutInflater inflater;
    private LinkedList<ProfileModel> profileModelList;
    private Activity superActivity;

    public ViewGroupMembersAdaptor(Activity activity) {
        profileModelList = new LinkedList<ProfileModel>();
        superActivity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return profileModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return profileModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.chat_favourite_list_adaptor, null);

        ProfileModel profileModel = profileModelList.get(position);

        CircleImageView circleImageView = (CircleImageView) convertView.findViewById(R.id.chat_fav_list_profile);
        if (profileModel.getProfileimageurl() != null && !profileModel.getProfileimageurl().trim().isEmpty()) {
            Utility.showProfilePicture(profileModel.getProfileimageurl(), superActivity, circleImageView, profileModel.getDisplayname(), true);
        } else {
            circleImageView.setImageResource(R.drawable.ic_person);
        }

        String memberName = profileModel.getDisplayname();
        String myFid = UserModel.getInstance().getFid();
        if (profileModel.getFid().equalsIgnoreCase(myFid)) {
            memberName = "Me";
        }
        if (profileModel.getIsadmin().equalsIgnoreCase("true"))
            memberName += " (admin)";
        TextView msg = (TextView) convertView.findViewById(R.id.chat_bubble_textview);
        msg.setText(memberName);
        return convertView;
    }

    public void setProfileModelList(LinkedList<ProfileModel> profileModelList) {
        this.profileModelList = profileModelList;
    }
}