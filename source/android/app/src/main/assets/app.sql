CREATE TABLE IF NOT EXISTS userDetails(Key TEXT PRIMARY KEY, Value TEXT );

CREATE TABLE IF NOT EXISTS relationships(fid TEXT PRIMARY KEY, DisplayName TEXT,ProfileImage BLOB,ParentID TEXT,RelationType TEXT);

CREATE TABLE IF NOT EXISTS notifications(SenderID TEXT PRIMARY KEY, Message TEXT,ProfileImage BLOB,Date and Time TEXT);

CREATE TABLE IF NOT EXISTS chathistory(fid TEXT, msg TEXT, mode TEXT, datetime TEXT);

CREATE TABLE IF NOT EXISTS unreadnotification(fidOrGid TEXT PRIMARY KEY, content TEXT, postid TEXT, datetime TEXT);

CREATE TABLE IF NOT EXISTS chatfavouritesandgroups(fidOrGid TEXT PRIMARY KEY, name TEXT, url TEXT, mode TEXT, mobilenumber TEXT);

CREATE TABLE IF NOT EXISTS posts(postcontent TEXT PRIMARY KEY);

CREATE TABLE IF NOT EXISTS nearbydata(nearbydatacontent TEXT PRIMARY KEY);