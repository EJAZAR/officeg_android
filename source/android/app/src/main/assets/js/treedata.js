var g_link ="";
    function showRelationship(index) {
        //selectedItem = navigationStack[index];

        navigationStack.length = index + 1;
        updateNavigationTree();
        root=selectedItem = navigationStack[index];
        update(selectedItem);
        Relationship.getRelationship(navigationStack[index].fid);
    }

    function updateNavigationTree() {
        var link = "";
        for(var index = 0; index < navigationStack.length; index++) {
            link += "<a onclick='showRelationship(" + index + ")'>" + navigationStack[index].displayname + "</a> &gt;";
        }
        g_link = document.getElementById("navTree").innerHTML = link;
    }

//    function openPopup(person){
//        Relationship.logThis('treedata openPopup');
//        document.getElementById("popupProfileImage").src = (person.profileimage !== "" && person.profileimage !== null && person.profileimage !== undefined) ? person.profileimage : (person.gender == "Female") ? "female_default_profileimage.png" : "male_default_profileimage.png";
//        document.getElementById("displayname").innerHTML = getDisplayValue(person.displayname);
//        document.getElementById("firstname").innerHTML = getDisplayValue(person.firstname);
//        document.getElementById("surname").innerHTML = getDisplayValue(person.surname);
//        document.getElementById("gender").innerHTML = getDisplayValue(person.gender);
//        document.getElementById("dob").innerHTML = getDisplayValue(person.dob);
//        document.getElementById("mobilenumber").innerHTML = getDisplayValue(person.mobilenumber);
//        document.getElementById("relationship").innerHTML = g_link;
//        document.getElementById("showprofile").style.display = "block";
//    }

    function getDisplayValue(value) {
        if(value == "" || value == undefined || value == null)
            return "N/A";
        return value;
    }

    function closePopup(){
        document.getElementById("showprofile").style.display = "none";
    }

    var m = [20, 120, 20, 120],
    w = window.innerWidth - m[1] - m[3],
    h = window.innerHeight - 30 - m[0] - m[2],
    i = 0,
    root;
    var navigationStack = [{displayname:Relationship.getMyName(), fid:"", profileimage:Relationship.getMyPhoto(), children:[]}];
    updateNavigationTree();
    var tree = d3.layout.tree().size([h, w]);

    var diagonal = d3.svg.diagonal().projection(function (d) { return [d.y, d.x]; });

    var vis = d3.select("#treeView").append("svg:svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .append("svg:g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

    var cp = vis.append("svg:clipPath");
    cp.attr("id", "clipCircle");
    cp.append("svg:circle").attr("r", 30).attr("cx",-30).attr("cy", 0);
    console.log("profile image"+Relationship.getMyPhoto());

    root = navigationStack[0];
    selectedItem = root;
    Relationship.getRelationship("");
    root.x0 = h / 2;
    root.y0 = 0;

    function toggleAll(d) {
        if (d.children) {
            d.children.forEach(toggleAll);
            toggle(d);
        }
    }
     update(root);

    function update(source) {
        var duration = d3.event && d3.event.altKey ? 5000 : 500;

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse();

        // Normalize for fixed-depth.
        nodes.forEach(function (d) { d.y = d.depth * 180; });

        // Update the nodes
        var node = vis.selectAll("g.node")
            .data(nodes, function (d) { return d.id || (d.id = ++i); });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("svg:g")
            .attr("class", "node")
            .attr("transform", function (d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
            .on("click", function (d) {
            if(selectedItem == d)
                return ;
            root = d;
            toggle(root);
            update(root);

        });

        nodeEnter.append("svg:circle")
            .attr("r", 1e-6)
            .style("fill", function (d) { return d._children ? "lightsteelblue" : "#fff"; });

        //displayName text
        nodeEnter.append("svg:text")
            .attr("x", -50)//function (d) { return d.children || d._children ? -10 : -10; })
            .attr("y", 50)
            .attr("dy", ".35em")
            .attr("id", "display_name")
            .attr("text-anchor", function (d) { return d.children || d._children ? "end" : "start"; })
            .text(function (d) { return d.displayname; })
            .on("click", function(d) {
            d3.event.preventDefault();
//            openPopup(d);
           });

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function (d) { return "translate(" + d.y + "," + d.x + ")"; });

        //circle profile image
        nodeEnter.append("svg:image")
        .attr('x', -75)
        .attr('y', -45)
        .attr('width', 90)
        .attr('height', 90)
        .attr('clip-path', 'url(#clipCircle)')
        .attr("xlink:href", function (d) {

           return (d.profileimage !== "" && d.profileimage !== null && d.profileimage !== undefined) ?  d.profileimage : (d.gender === "Female") ? "female_default_profileimage.png" : "male_default_profileimage.png" ;
        }).on("click", function (person) {
            if(selectedItem == person)
                return;
            console.log("onclick:"+person.displayname);
            navigationStack.push(person);
            updateNavigationTree();
            root = selectedItem = person;
            Relationship.getRelationship(person.fid);
        }
        );

         //star image
        nodeEnter.append("svg:image")
        .attr("x", -15)
        .attr("y", 15)
        .attr('width', 20)
        .attr('height', 20)
        .attr("xlink:href", function (d) {
            if(d === navigationStack[0])
                return "";
            if(d.isfirstdegree)
                return "yellow_star.png";
            return (d.isfav) ? "yellow_star.png" : "grey_star.png";
        }).on("click", function (person) {
            if(person.isfav){
                Relationship.removeFavourite(person.fid);
                <!--return("grey_star.png");-->
            }
            else{
                Relationship.addFavourite(person.fid);
            }

        }
        );

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function (d) { return "translate(" + source.y + "," + source.x + ")"; })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links
       var link = vis.selectAll("path.link")
            .data(tree.links(nodes), function (d) { return d.target.id; });

        // Enter any new links at the parent's previous position.
        link.enter().insert("svg:path", "g")
            .attr("class", "link")
            .attr("d", function (d) {
                var o = { x: source.x0, y: source.y0 };
                return diagonal({ source: o, target: o });
            })
          .transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function (d) {
                var o = { x: source.x, y: source.y };
                return diagonal({ source: o, target: o });
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    // Toggle children.
    function toggle(d) {

        if (d.children) {

            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
    }

    function onRelationshipReceived(array) {
        console.log("From javascript:" +JSON.stringify(array));
        selectedItem.children = array;
        update(selectedItem);

    }

    function addFavouriteStar(starimage){
        console.log("yellowstar: "+starimage);
    }

    function removeFavouriteStar(starimage){

    }