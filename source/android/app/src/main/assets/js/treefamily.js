    var g_link = "";
    var g_showAll = false;
    var connected = false;
    var connectedDisplayname = "";

    function edit(){
        console.log("Calling edit method");
        console.log("popup of selectedItem.displayname:" +selectedItem.displayname);
        console.log("popup of selectedItem.displayname:" +selectedItem.displayname);
        console.log("selected person fid: " + selectedItem.fid);
        console.log("Active person fid: " +g_activePerson.fid);
        console.log("Active person ownerfid g_activePerson.ownerfid: " +g_activePerson.ownerfid);
        console.log("selected person displayname: " +selectedItem.displayname);
        console.log("active person displayname: " +g_activePerson.displayname);

        if(g_activePerson.ownerfid === undefined || g_activePerson.ownerfid === "" )
        {
          connected = true;
          connectedDisplayname = g_activePerson.displayname;
        }
        else if(selectedItem.fid == g_activePerson.ownerfid){
            connected = false;
            }

            Relationship.editProfile(g_activePerson.fid, selectedItem.fid, connected, connectedDisplayname, g_activePerson.relationtype);
            console.log("javascript connected : " +connected);

    }
    function showRelationship(index) {
        root = selectedItem = g_navigationStack.showFrom(index);
        update(selectedItem);
        g_navigationStack.updateLegend(selectedItem);
        Relationship.getRelationship(selectedItem.fid);
    }

    function showAllMembers() {
        g_showAll = !g_showAll;
        g_navigationStack.updateLegend(selectedItem);
        Relationship.getRelationship(selectedItem.fid);
    }

    function bookmarkThis(){
        Relationship.logThis('bookmarkThis');
        Relationship.bookmarkThis(selectedItem.fid, selectedItem.displayname, selectedItem.profileimage);
    }

    function updateRelationshipLink(person) {
        var relationlink = '';
        relationlink += "<label class='navlink popuprelationshipfont'>" + g_navigationStack.get(0).displayname + '-' + "</label>"

        for (var index = 1; index < g_navigationStack.stack.length; index++) {
            relationlink += "<label class='navlink popuprelationshipfont' onclick='showRelationship(" + index + ")'>{" + g_navigationStack.get(index).relationtype + "}->" + g_navigationStack.get(index).displayname + "</label>";
        }
        relationlink += "<label class='navlink popuprelationshipfont'>-{" + person.relationtype + "}->" + person.displayname + "</label>"
        g_link = relationlink;
    }

    function onDisconnectRelation(fid) {
        var srcFid = selectedItem.fid;
        var desFid = fid;
        console.log("Disconnect source fid : " + srcFid + " desFid " + desFid);
        Relationship.disconnectRelation(srcFid, desFid);
    }

    var g_activePerson = null;
    function openPopup(person) {
        Relationship.logThis('treefamily openPopup');
        Relationship.logEvent();
        updateRelationshipLink(person);
        g_activePerson = person;

        document.getElementById("popupProfileImage").src = (person.profileimage !== "" && person.profileimage !== null && person.profileimage !== undefined) ? person.profileimage : (person.gender == "Female") ? "female_default_profileimage.png" : "male_default_profileimage.png";
        document.getElementById("displayname").innerHTML = getDisplayValue(person.displayname);
        document.getElementById("firstname").innerHTML = getDisplayValue(person.firstname);
        document.getElementById("lastname").innerHTML = getDisplayValue(person.lastname);
        document.getElementById("gender").innerHTML = getDisplayValue(person.gender);
        document.getElementById("dob").innerHTML = getDisplayValue(person.dob);
        document.getElementById("fgid").innerHTML = getDisplayValue(person.fid);
        document.getElementById("relationship").innerHTML = g_link;
        document.getElementById("showprofile").style.display = "block";
        document.getElementById('removefav').style.display = 'none';
        document.getElementById('addfav').style.display = 'none';
        document.getElementById('withdraw').style.display = 'none';
        document.getElementById('remove').style.display = 'none';

        console.log("ownerfid in popup person.ownerfid: " + person.ownerfid);
        console.log("ownerfid in popup selectedItem.ownerfid: " +selectedItem.ownerfid);
        console.log("ownerfid in popup selectedItem.fid: " +selectedItem.fid);
        console.log("ownerfid in popup g_activePerson.ownerfid: " +g_activePerson.ownerfid);
        console.log("ownerfid in isfirstdegree: " +person.isfirstdegree);
        Relationship.logThis(person.isfirstdegree + ' and ' + selectedItem.fid + ' and ' + g_activePerson.ownerfid + ' and ' + selectedItem.fid + ' and ' + selectedItem.isalive + ' and ' + person.fid + ' and ' + person.isalive);
        if(!person.isalive){
            document.getElementById("addfav").style.display = 'none';
            document.getElementById('removefav').style.display = 'none'
        }
        else if (person.isfirstdegree || g_navigationStack.isLoggedInUser(selectedItem.ownerfid) ) {

            if(person.status  === "approved")
                document.getElementById('remove').style.display = 'block';
            else
                document.getElementById('withdraw').style.display = 'block';
        }
        else if(!person.isfirstdegree &&  (selectedItem.fid == g_activePerson.ownerfid)){
            document.getElementById("addfav").style.display = 'none';
            document.getElementById('removefav').style.display = 'none'
        }
        else if (!person.isfirstdegree || (person.ownerfid == "" || person.ownerfid == undefined)) {
            if (person.isfav) {
                if (document.getElementById('removefav').style.display == 'none') {
                    document.getElementById("removefav").style.display = "block";
                }
            }
            else {
                 if( person.status.toLowerCase() == "pending")
                document.getElementById("addfav").style.display = "none";
            else
                document.getElementById("addfav").style.display = "block";
            }
        }

        console.log("editprofile:" + "first: " + person.isfirstdegree);
        console.log("editprofile:" + "looggg: " + g_navigationStack.isLoggedInUser(selectedItem.ownerfid));
        console.log("editprofile:" + "ownerfid: " + person.ownerfid);
        console.log("editprofile:" + "fiddd: " + person.fid);

           if (person.isfirstdegree || g_navigationStack.isLoggedInUser(selectedItem.ownerfid) ) {
               if(person.status.toLowerCase() == "pending"){
                    document.getElementById("editProfile").style.display = "none";
               }
               else
                    document.getElementById("editProfile").style.display = "block";
           }
           else if(!person.isfirstdegree || selectedItem.ownerfid == undefined || person.ownerfid == undefined || g_navigationStack.isLoggedInUser(selectedItem.ownerfid) ) {
            if(document.getElementById('addfav').style.display == 'block'){
                    document.getElementById("editProfile").style.display = "none";
               }
               else if(document.getElementById('remove').style.display == 'block')
                    document.getElementById("editProfile").style.display = "block";
           }

    }

    function getDisplayValue(value) {
        if (value == "" || value == undefined || value == null)
            return "N/A";
        return value;
    }

    function closePopup() {
        document.getElementById("showprofile").style.display = "none";
    }

    function NavigationStack() {
        this.isMoreVisible = false;
        this.visibleItemCount = parseInt((window.innerWidth - 30) / 73);
        document.getElementById("navList").style.height = (window.innerHeight - 30) + "px"
        this.stack = [];
        this.push = function (person) {
            var ret = true;
            this.stack.push(person);
            for (var index = 0; index < this.stack.length; index++) {
                if (this.stack[index].fid == person.fid) {
                    this.stack.length = index + 1;
                    ret = false;
                }
            }

            this.updateLegend(person);
            this.updateView();
            return ret;
        }

        this.updateLegend = function (person) {
            if ((this.isLoggedInUser(person.fid) || this.isLoggedInUser(person.ownerfid)) && Relationship.isMyTree()) {
                document.getElementById("showall").style.display = 'block';
                document.getElementById("bookmarkthisdiv").style.display = 'none';
            } else {
                document.getElementById("showall").style.display = 'none';
                document.getElementById("bookmarkthisdiv").style.display = 'block';
                var isAlreadyBookmark = Relationship.isAlreadyBookmark(person.fid);
                Relationship.logThis('isAlreadyBookmark = '+ isAlreadyBookmark);
                if(Relationship.isAlreadyBookmark(person.fid)){
                    document.getElementById("bookmarkthis").checked = true;
                } else{
                    document.getElementById("bookmarkthis").checked = false;
                }
            }
        }

        this.showFrom = function (index) {
            this.stack.length = index + 1;
            this.updateView();
            return this.stack[index];
        }

        this.updateView = function () {
            var start = 0;
            if (this.stack.length > this.visibleItemCount)
                start = this.stack.length - this.visibleItemCount;

            var link = "<div class='backbtn' onclick='g_navigationStack.onMore()'></div>";
            for (var index = start; index < this.stack.length; index++) {
                link += "<div class='navlink' onclick='showRelationship(" + index + ")'>" + this.stack[index].displayname + "</div>";
            }
            document.getElementById("navTree").innerHTML = link;

            if (start > 0) {
                var link = "";
                for (var index = start - 1; index >= 0; index--) {
                    link += "<div class='navitem' onclick='showRelationship(" + index + ")'>" + this.stack[index].displayname + "</div>";
                }
                document.getElementById("navList").innerHTML = link;

            }
            this.isMoreVisible = false;
            document.getElementById("navList").style.display = 'none';
        }

        this.onMore = function () {

            if (this.stack.length <= this.visibleItemCount)
                return;
            document.getElementById("navList").style.display = (this.isMoreVisible) ? 'none' : 'block';

            this.isMoreVisible = !this.isMoreVisible;
        }

        this.isLoggedInUser = function (fid) {
            //console.log("Logged in user fid : " + this.stack[0].fid + " , current fid : " + fid);
            return this.stack[0].fid == fid;
        }

        this.get = function (index) {
            return this.stack[index];
        }
    }

    var g_navigationStack = new NavigationStack();
    var lastFavouritePerson = null;
    var m = [20, 0, 20, 50],
    w = window.innerWidth - m[1] - m[3],
    h = window.innerHeight - 30 - m[0] - m[2],
    i = 0,
    root;
    g_navigationStack.push({ displayname: Relationship.getMyCurrentName(), fid: Relationship.getMyFID(), profileimage: Relationship.getMyPhoto(), children: [] });

    var tree = d3.layout.tree().size([h, w]);

    var diagonal = d3.svg.diagonal().projection(function (d) { return [d.y, d.x]; });

    var vis = d3.select("#treeView").append("svg:svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .append("svg:g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

    var cp = vis.append("svg:clipPath");
    cp.attr("id", "clipCircle");
    cp.append("svg:circle").attr("r", 18).attr("cx", 0).attr("cy", 0);

    root = g_navigationStack.get(0);
    selectedItem = root;
    Relationship.getRelationship("");
    root.x0 = h / 2;
    root.y0 = 0;

    function toggleAll(d) {
        if (d.children) {
            d.children.forEach(toggleAll);
            toggle(d);
        }
    }
    //update(root);

    var profileSize = 40;
    function update(source) {
        // resize the tree height
        var tempH = source.children.length * profileSize;
        tempH = (h < tempH) ? tempH : h;
        tree.size([tempH, w]);
        d3.select("svg").attr('height', (tempH + 20));

        var duration = d3.event && d3.event.altKey ? 5000 : 500;

        vis.selectAll("tspan.rel").remove();
        vis.selectAll("tspan.dn").attr("x", 5).attr("dy", 42).attr("text-anchor", "middle");

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse();

        // Normalize for fixed-depth.
        nodes.forEach(function (d) { d.y = d.depth * 180; });

        // Update the nodes
        var node = vis.selectAll("g.node")
            .data(nodes, function (d) { return d.id || (d.id = ++i); });

        //var nodeUpdate = node.update().
        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("svg:g")
            .attr("class", "node")
            .attr("transform", function (d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
            .on("click", function (d) {
                if (selectedItem == d)
                    return;
                root = d;
                //toggle(root);
                update(root);
            });


        //circle border
        nodeEnter.append("svg:circle")
            .attr("r", 20)
            .style("stroke-width", "1")
            .style('stroke', function (d) {
                    return '#BDC3C7';
            });

        //displayName text
        nodeEnter.append("svg:text")
            .attr('class', 'relationshipfont')
            .attr("x", -10)
            .attr("y", -10)
            .attr("dy", ".35em")
            .attr("id", "display_name")
            .attr("text-anchor", function (d) { return d.children || d._children ? "middle" : "start"; })
            .on("click", function (d) {
                // Do not show profile dialog for logged in user.
                Relationship.logThis('treefamily = ' + d.children + ' and ' + d._children + ' and ' + (d.children || d._children ? "middle" : "start"));
                if (g_navigationStack.isLoggedInUser(d.fid))
                    return;
                if(d.children || d._children)
                    return;
                d3.event.preventDefault();
                openPopup(d);
            })
            .append('svg:tspan')
            .attr('class', 'dn')
            .attr("x", function (d) {
                return (d.fid == root.fid) ? 5 : 30;
            })
            .attr("dy", function (d) { return (d.fid == root.fid) ? 42 : 5; })
            .text(function (d) { return d.displayname.substring(0, 15); })
            .append('svg:tspan')
            .attr('class', 'dn')
            .attr("x", function (d) {
                return (d.fid == root.fid) ? 5 : 30;
            })
            .attr("dy", function (d) { return ((d.displayname.length > 15 ) ? 10 : 0) ; })
            .text(function (d) { return d.displayname.substring(15); })
            .append('svg:tspan')
            .attr('class', 'rel')
            .attr("x", 30)
            .attr("y", function (d) { return ((d.displayname.length > 15 ) ? 10 : 0) + ((d.fid == root.fid) ? 50 : 12); })
            .text(function (d) {
                return ((d.fid == root.fid) || (d.fid == source.fid)) ? "" : "[" + d.relationtype + "]";
            });

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function (d) { return "translate(" + d.y + "," + d.x + ")"; });

        //circle profile image
        nodeEnter.append("svg:image")
        .attr('x', -20)
        .attr('y', -20)
        .attr('width', 40)
        .attr('height', 40)
        .attr('clip-path', 'url(#clipCircle)')
        .attr("xlink:href", function (d) {
            return (d.profileimage !== "" && d.profileimage !== null && d.profileimage !== undefined) ? d.profileimage : (d.gender === "Female") ? "female_default_profileimage.png" : "male_default_profileimage.png";
        }).on("click", function (person) {
            if (selectedItem == person)
                return;
            if(person.status != "approved")
                return ;
            console.log("onclick:" + person.displayname);
            g_navigationStack.push(person);
            root = selectedItem = person;
            Relationship.getRelationship(person.fid);
        });


         // request status indicator image
        nodeEnter.append("svg:image")
        .attr('x', 9)
        .attr('y', 9)
        .attr('width', 10)
        .attr('height', 10)
        .attr("xlink:href", function (d) {
         if (g_navigationStack.isLoggedInUser(d.fid))
            return;

          if(d.status.toLowerCase() == "pending")
            return "pending.png";
          else if(d.mobilenumber === "" && d.isalive)
                return "without_number.png";
          else if(!d.isalive)
                return "expired.png";

          if (!d.isfirstdegree) {
                <!--if(d.status.toLowerCase() == "pending")-->
                    <!--return "pending.png";-->

            return (d.isfav) ? "favourite.png" : "";

         }


           if(d.isfirstdegree){
            if(d.mobilenumber === "" && d.isalive)
                return "without_number.png";
            else if(!d.isalive)
                return "expired.png";
            else
                return "favourite.png";
         }

         if(!d.isowner && d.isfirstdegree){
            if(d.mobilenumber === "" && d.isalive)
                return "without_number.png";
            else if(!d.isalive)
                return "expired.png";
            else
                return "favourite.png";
         }
         else{
             return "favourite.png";
         }
        });


        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function (d) { return "translate(" + source.y + "," + source.x + ")"; })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links
        var link = vis.selectAll("path.link")
             .data(tree.links(nodes), function (d) { return d.target.id; });

        // Enter any new links at the parent's previous position.
        link.enter().insert("svg:path", "g")
            .attr("class", "link")
            .attr("d", function (d) {
                var o = { x: source.x0, y: source.y0 };
                return diagonal({ source: o, target: o });
            })
          .transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function (d) {
                var o = { x: source.x, y: source.y };
                return diagonal({ source: o, target: o });
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    // Toggle children.
    function toggle(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
    }

    function onRelationshipReceived(array) {
        if (array.length == 0) {
            document.getElementById("norelationmessage").style.display = "block";
            document.getElementById("legend").style.display = "none";
            document.getElementById("navTree").style.display = "none";
            return;
        }
        document.getElementById("norelationmessage").style.display = "none";
        document.getElementById("legend").style.display = "block";

        var relationArray = new Array();
        console.log("Show all : " + g_showAll);
        //console.log("From javascript:" + JSON.stringify(array));
        for (var i = 0; i < array.length; i++) {
            var relation = array[i];
            var relationType = relation["relationtype"];
//            if (relationType === "FATHER" || relationType === "MOTHER" || relationType === "BROTHER" ||
//            relationType === "SISTER" || relationType === "SON" || relationType === "DAUGHTER" || relationType === "HUSBAND" ||
//            relationType === "WIFE" || relationType === "RELATION" || (g_showAll && g_navigationStack.isLoggedInUser(selectedItem.fid))) {
//                relationArray.push(relation);
//            }
            if (relationType === "BOSS" || relationType === "PEER" || relationType === "JUNIOR" || (g_showAll && g_navigationStack.isLoggedInUser(selectedItem.fid))) {
                relationArray.push(relation);
            }

            if (g_navigationStack.isLoggedInUser(relation.fid))
                relation.displayname = "You";
            //console.log("type: " + JSON.stringify(relationType));
            //console.log("relationship: " + JSON.stringify(relation));
        }
        //console.log("Relation  Array: " + JSON.stringify(relationArray));
        selectedItem.children = relationArray;
        update(selectedItem);

    }

    function onAddFavourite() {
        console.log("lastfavorite: " + lastFavouritePerson.isfav);
        lastFavouritePerson.isfav = true;
        update(lastFavouritePerson);
    }
    function onRemoveFavourite() {
        lastFavouritePerson.isfav = false;
        update(selectedItem);
    }

    function reload() {
        closePopup();
        window.setTimeout( function() {
            selectedItem = root = g_navigationStack.get(0);
           Relationship.getRelationship("");
        }, 100);
    }
