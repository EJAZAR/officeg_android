var networkdata;
var elements = [], root, levels = [], levelMap = [],
    networktree = document.getElementById('networktree'),
    startTop, startLeft, gap = 32, size = 64, sizeHeight = 24;
    startTop = (window.innerHeight / 2) - (sizeHeight);
    startLeft = (window.innerWidth / 2) - (size);

function loadnetwork(){
    //console.log('loadnetwork');
     Relationship.getPedigreeTree();
//var array = [{"id":"114269","parents":["9680"],"children":[],"siblings":[],"spouses":[],"level":2,"name":"AzarJ"},{"id":"9680","parents":[],"children":["114269"],"siblings":[],"spouses":[],"level":3,"name":"Azar Idea"}];
//var array = [{"id":"6906","parents":["6904"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Shantikumar"},{"id":"6908","parents":["6904"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Pradip"},{"id":"6825","parents":["6812"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Prabhodh"},{"id":"6814","parents":["6812"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Anusuya"},{"id":"6835","parents":["6812"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Nilam"},{"id":"6850","parents":["6812"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Navmalika"},{"id":"6927","parents":["6925"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Urmi"},{"id":"6979","parents":["6943"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Tushar"},{"id":"6980","parents":["6943"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Archana"},{"id":"6939","parents":["6937"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Satish"},{"id":"6940","parents":["6937"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Uma"},{"id":"6941","parents":["6937"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Keerti"},{"id":"6948","parents":["6946"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Kedar"},{"id":"6949","parents":["6946"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Kush"},{"id":"6952","parents":["6946"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Ashish"},{"id":"6950","parents":["6946"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Asha"},{"id":"6953","parents":["6946"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Arti"},{"id":"6956","parents":["6954"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Ram"},{"id":"6957","parents":["6954"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Krishna"},{"id":"6958","parents":["6954"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sonali"},{"id":"6963","parents":["6961"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Anand"},{"id":"6964","parents":["6961"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sanjay"},{"id":"6971","parents":["6968"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Devadatta"},{"id":"6970","parents":["6968"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Supriya"},{"id":"6974","parents":["6972"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Leela"},{"id":"6977","parents":["6975"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Divya"},{"id":"6978","parents":["6975"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Amrita"},{"id":"6967","parents":["6965"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Vinayak"},{"id":"6966","parents":["6965"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sukanya"},{"id":"6904","parents":["6339","6427"],"children":["6906","6908"],"siblings":["6812","6924","6925"],"spouses":["6905"],"level":1,"name":"Kantilal"},{"id":"6905","parents":[],"children":[],"siblings":[],"spouses":["6904"],"level":1,"name":"Saraswati"},{"id":"6812","parents":["6339","6427"],"children":["6825","6814","6835","6850"],"siblings":["6904","6924","6925"],"spouses":["6813"],"level":1,"name":"Rami"},{"id":"6813","parents":[],"children":[],"siblings":[],"spouses":["6812"],"level":1,"name":"Kunverji Parekh"},{"id":"6924","parents":["6339","6427"],"children":[],"siblings":["6904","6925","6812"],"spouses":[],"level":1,"name":"Rasik"},{"id":"6925","parents":["6339","6427"],"children":["6927"],"siblings":["6904","6812","6924"],"spouses":["6926"],"level":1,"name":"Manuben"},{"id":"6926","parents":[],"children":[],"siblings":[],"spouses":["6925"],"level":1,"name":"Surendra"},{"id":"6943","parents":["6428","6429"],"children":["6979","6980"],"siblings":["6937","6946"],"spouses":["6945"],"level":1,"name":"Arun"},{"id":"6945","parents":[],"children":[],"siblings":[],"spouses":["6943"],"level":1,"name":"Sunanda"},{"id":"6937","parents":["6428","6429"],"children":["6939","6940","6941"],"siblings":["6943","6946"],"spouses":["6938"],"level":1,"name":"Sita"},{"id":"6938","parents":[],"children":[],"siblings":[],"spouses":["6937"],"level":1,"name":"Shashikant"},{"id":"6946","parents":["6428","6429"],"children":["6948","6949","6952","6950","6953"],"siblings":["6943","6937"],"spouses":["6947"],"level":1,"name":"Ila"},{"id":"6947","parents":[],"children":[],"siblings":[],"spouses":["6946"],"level":1,"name":"Ramgovin Mevalal"},{"id":"6959","parents":["6495","6804"],"children":[],"siblings":["6954","6961"],"spouses":["6960"],"level":1,"name":"Kahandas"},{"id":"6960","parents":[],"children":[],"siblings":[],"spouses":["6959"],"level":1,"name":"Shivalaxmi"},{"id":"6954","parents":["6495","6804"],"children":["6956","6957","6958"],"siblings":["6959","6961"],"spouses":["6955"],"level":1,"name":"Sumitra"},{"id":"6955","parents":[],"children":[],"siblings":[],"spouses":["6954"],"level":1,"name":"Gajanan"},{"id":"6961","parents":["6495","6804"],"children":["6963","6964"],"siblings":["6959","6954"],"spouses":["6962"],"level":1,"name":"Usha"},{"id":"6962","parents":[],"children":[],"siblings":[],"spouses":["6961"],"level":1,"name":"Harish Gokani"},{"id":"6968","parents":["6810","6811"],"children":["6971","6970"],"siblings":["6972","6975","6965"],"spouses":["6969"],"level":1,"name":"Rajmohan"},{"id":"6969","parents":[],"children":[],"siblings":[],"spouses":["6968"],"level":1,"name":"Usha"},{"id":"6972","parents":["6810","6811"],"children":["6974"],"siblings":["6968","6975","6965"],"spouses":["6973"],"level":1,"name":"Ramachandra"},{"id":"6973","parents":[],"children":[],"siblings":[],"spouses":["6972"],"level":1,"name":"Indu"},{"id":"6975","parents":["6810","6811"],"children":["6977","6978"],"siblings":["6968","6972","6965"],"spouses":["6976"],"level":1,"name":"Gopalakrishna"},{"id":"6976","parents":[],"children":[],"siblings":[],"spouses":["6975"],"level":1,"name":"Tara"},{"id":"6965","parents":["6810","6811"],"children":["6967","6966"],"siblings":["6968","6972","6975"],"spouses":[],"level":1,"name":"Tara"},{"id":"6339","parents":["6316","6317"],"children":["6904","6812","6924","6925"],"siblings":["6428","6495","6810"],"spouses":["6427"],"level":2,"name":"Harilal"},{"id":"6427","parents":[],"children":["6904","6812","6924","6925"],"siblings":[],"spouses":["6339"],"level":2,"name":"Gulabben"},{"id":"6428","parents":["6317","6316"],"children":["6943","6937","6946"],"siblings":["6339","6495","6810"],"spouses":["6429"],"level":2,"name":"Manilal"},{"id":"6429","parents":[],"children":["6943","6937","6946"],"siblings":[],"spouses":["6428"],"level":2,"name":"Sushilaben"},{"id":"6495","parents":["6317","6316"],"children":["6959","6954","6961"],"siblings":["6428","6339","6810"],"spouses":["6804"],"level":2,"name":"Ramdas"},{"id":"6804","parents":[],"children":["6959","6954","6961"],"siblings":[],"spouses":["6495"],"level":2,"name":"Nirmalaben"},{"id":"6810","parents":["6317","6316"],"children":["6968","6972","6975","6965"],"siblings":["6428","6495","6339"],"spouses":["6811"],"level":2,"name":"Devdas"},{"id":"6811","parents":[],"children":["6968","6972","6975","6965"],"siblings":[],"spouses":["6810"],"level":2,"name":"Lakshmi"},{"id":"6316","parents":["6321","6322"],"children":["6339","6428","6495","6810"],"siblings":["6318","6320"],"spouses":["6317"],"level":3,"name":"Bapu"},{"id":"6317","parents":[],"children":["6339","6428","6495","6810"],"siblings":[],"spouses":["6316"],"level":3,"name":"Kasturba"},{"id":"6318","parents":["6321","6322"],"children":[],"siblings":["6316","6320","6319"],"spouses":[],"level":3,"name":"Laxmidas"},{"id":"6320","parents":["6321","6322"],"children":[],"siblings":["6318","6316"],"spouses":[],"level":3,"name":"Karsandas"},{"id":"6319","parents":["6321","6322"],"children":[],"siblings":["6318","6316","6320"],"spouses":[],"level":3,"name":"Raliatben"},{"id":"6321","parents":["6323"],"children":["6316","6318","6320","6319"],"siblings":["6325"],"spouses":["6322","6326","6327"],"level":4,"name":"Karamchand"},{"id":"6327","parents":[],"children":[],"siblings":[],"spouses":["6321"],"level":4,"name":"Pankunvarben"},{"id":"6326","parents":[],"children":[],"siblings":[],"spouses":["6321"],"level":4,"name":"Muliben"},{"id":"6322","parents":[],"children":["6318","6316","6320","6319"],"siblings":[],"spouses":["6321"],"level":4,"name":"Putlima"},{"id":"6325","parents":["6323","6324"],"children":[],"siblings":["6321"],"spouses":[],"level":4,"name":"Tulsidas"},{"id":"6334","parents":["6323"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Vallbhji"},{"id":"6335","parents":["6323"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Ratanji"},{"id":"6336","parents":["6323"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Pitamber"},{"id":"6337","parents":["6323"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Premkunver"},{"id":"6338","parents":["6323"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Jivanlal"},{"id":"6323","parents":["6331"],"children":["6325","6334","6335","6336","6337","6338","6321"],"siblings":[],"spouses":["6324","6333"],"level":5,"name":"Uttam"},{"id":"6333","parents":[],"children":[],"siblings":[],"spouses":["6323"],"level":5,"name":"Kadavima"},{"id":"6324","parents":[],"children":["6325"],"siblings":[],"spouses":[],"level":5,"name":"Laxmima"},{"id":"6331","parents":[],"children":["6323"],"siblings":[],"spouses":[],"level":6,"name":"Harjivan"}];
//var array = [{"id":"101659","parents":["101661","101686"],"children":[],"siblings":["101676","101667"],"spouses":["101660"],"level":3,"name":"Saifudeen Khan"},{"id":"101660","parents":[],"children":[],"siblings":[],"spouses":["101659"],"level":3,"name":"Shireen Fatima"},{"id":"101676","parents":["101661","101686"],"children":[],"siblings":["101659","101667"],"spouses":["101663"],"level":3,"name":"Rafiudeen Khan"},{"id":"101663","parents":[],"children":[],"siblings":[],"spouses":["101676"],"level":3,"name":"Asima Rafi"},{"id":"101667","parents":["101661","101686"],"children":[],"siblings":["101676","101659"],"spouses":["101685"],"level":3,"name":"Moinudeen Khan"},{"id":"101685","parents":[],"children":[],"siblings":[],"spouses":["101667"],"level":3,"name":"Yasmeen"},{"id":"101670","parents":["101673"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Jezima"},{"id":"101672","parents":["101673"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Razeem"},{"id":"101678","parents":["101683"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Fara"},{"id":"101677","parents":["101683"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Sama"},{"id":"101661","parents":[],"children":["101667","101676","101659"],"siblings":["101673","101664","101683","101680","101671"],"spouses":["101686"],"level":4,"name":"Najma Banu"},{"id":"101686","parents":[],"children":["101667","101676","101659"],"siblings":[],"spouses":["101661"],"level":4,"name":"Zaheerudeen Khan"},{"id":"101673","parents":[],"children":["101670","101672"],"siblings":["101661","101664","101683","101680","101671"],"spouses":["101688"],"level":4,"name":"Naseema Banu"},{"id":"101688","parents":[],"children":[],"siblings":[],"spouses":["101673"],"level":4,"name":"Raja Mohammed"},{"id":"101664","parents":[],"children":[],"siblings":["101661","101673"],"spouses":["101687"],"level":4,"name":"Azima"},{"id":"101687","parents":[],"children":[],"siblings":[],"spouses":["101664"],"level":4,"name":"Habib Mohamed"},{"id":"101683","parents":[],"children":["101678","101677"],"siblings":["101661"],"spouses":["101682"],"level":4,"name":"Zakir Moideen"},{"id":"101682","parents":[],"children":[],"siblings":[],"spouses":["101683"],"level":4,"name":"Nasreen Banu"},{"id":"101680","parents":[],"children":[],"siblings":["101661","101673","101664","101683","101671"],"spouses":[],"level":4,"name":"Siraj"},{"id":"101671","parents":[],"children":[],"siblings":["101661","101673","101664","101683","101680"],"spouses":["101674"],"level":4,"name":"Maqdoom"},{"id":"101674","parents":[],"children":[],"siblings":[],"spouses":["101671"],"level":4,"name":"Nisha Mac"}];
//var array = [{"id":"101660","parents":[],"children":[],"siblings":[],"spouses":["101659"],"level":3,"name":"Shireen Fatima"},{"id":"101659","parents":["101661","101686"],"children":[],"siblings":["101676","101667"],"spouses":["101660"],"level":3,"name":"Saifudeen Khan"},{"id":"101676","parents":["101661","101686"],"children":[],"siblings":["101659","101667"],"spouses":["101663"],"level":3,"name":"Rafiudeen Khan"},{"id":"101663","parents":[],"children":[],"siblings":[],"spouses":["101676"],"level":3,"name":"Asima Rafi"},{"id":"101667","parents":["101661","101686"],"children":[],"siblings":["101676","101659"],"spouses":["101685"],"level":3,"name":"Moinudeen Khan"},{"id":"101685","parents":[],"children":[],"siblings":[],"spouses":["101667"],"level":3,"name":"Yasmeen"},{"id":"101661","parents":[],"children":["101676","101667","101659"],"siblings":["101673","101664","101683","101680","101671"],"spouses":["101686"],"level":4,"name":"Najma Banu"},{"id":"101686","parents":[],"children":["101676","101667","101659"],"siblings":[],"spouses":["101661"],"level":4,"name":"Zaheerudeen Khan"},{"id":"101673","parents":[],"children":[],"siblings":["101661"],"spouses":[],"level":4,"name":"Naseema Banu"},{"id":"101664","parents":[],"children":[],"siblings":["101661"],"spouses":[],"level":4,"name":"Azima"},{"id":"101683","parents":[],"children":[],"siblings":["101661"],"spouses":[],"level":4,"name":"Zakir Moideen"},{"id":"101680","parents":[],"children":[],"siblings":["101661"],"spouses":[],"level":4,"name":"Siraj"},{"id":"101671","parents":[],"children":[],"siblings":["101661"],"spouses":[],"level":4,"name":"Maqdoom"}];
//var array = [{"id":"101572","parents":["101571"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sukanya"},{"id":"101573","parents":["101571"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Vinayak"},{"id":"101584","parents":["101581"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Amrita"},{"id":"101583","parents":["101581"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Divya"},{"id":"101580","parents":["101578"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Leela"},{"id":"101576","parents":["101574"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Supriya"},{"id":"101577","parents":["101574"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Devadatta"},{"id":"101570","parents":["101567"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sanjay"},{"id":"101569","parents":["101567"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Anand"},{"id":"101564","parents":["101560"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Sonali"},{"id":"101563","parents":["101560"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Krishna"},{"id":"101562","parents":["101560"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Ram"},{"id":"101559","parents":["101553"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Arti"},{"id":"101557","parents":["101553"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Asha"},{"id":"101558","parents":["101553"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Ashish"},{"id":"101556","parents":["101553"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Kush"},{"id":"101555","parents":["101553"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Kedar"},{"id":"101550","parents":["101546"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Keerti"},{"id":"101549","parents":["101546"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Uma"},{"id":"101548","parents":["101546"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Satish"},{"id":"101586","parents":["101551"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Archana"},{"id":"101585","parents":["101551"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Tushar"},{"id":"101541","parents":["101539"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Urmi"},{"id":"101519","parents":["101488"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Navmalika"},{"id":"101511","parents":["101488"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Nilam"},{"id":"101490","parents":["101488"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Anusuya"},{"id":"101501","parents":["101488"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Prabhodh"},{"id":"101530","parents":["101526"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Pradip"},{"id":"101528","parents":["101526"],"children":[],"siblings":[],"spouses":[],"level":0,"name":"Shantikumar"},{"id":"101571","parents":["101486","101487"],"children":["101572","101573"],"siblings":["101581","101578","101574"],"spouses":[],"level":1,"name":"Tara"},{"id":"101581","parents":["101486","101487"],"children":["101584","101583"],"siblings":["101571","101578","101574"],"spouses":["101582"],"level":1,"name":"Gopalakrishna"},{"id":"101582","parents":[],"children":[],"siblings":[],"spouses":["101581"],"level":1,"name":"Tara"},{"id":"101578","parents":["101486","101487"],"children":["101580"],"siblings":["101571","101581","101574"],"spouses":["101579"],"level":1,"name":"Ramachandra"},{"id":"101579","parents":[],"children":[],"siblings":[],"spouses":["101578"],"level":1,"name":"Indu"},{"id":"101574","parents":["101486","101487"],"children":["101576","101577"],"siblings":["101571","101581","101578"],"spouses":["101575"],"level":1,"name":"Rajmohan"},{"id":"101575","parents":[],"children":[],"siblings":[],"spouses":["101574"],"level":1,"name":"Usha"},{"id":"101567","parents":["101484","101485"],"children":["101570","101569"],"siblings":["101560","101565"],"spouses":["101568"],"level":1,"name":"Usha"},{"id":"101568","parents":[],"children":[],"siblings":[],"spouses":["101567"],"level":1,"name":"Harish Gokani"},{"id":"101560","parents":["101484","101485"],"children":["101564","101563","101562"],"siblings":["101567","101565"],"spouses":["101561"],"level":1,"name":"Sumitra"},{"id":"101561","parents":[],"children":[],"siblings":[],"spouses":["101560"],"level":1,"name":"Gajanan"},{"id":"101565","parents":["101484","101485"],"children":[],"siblings":["101567","101560"],"spouses":["101566"],"level":1,"name":"Kahandas"},{"id":"101566","parents":[],"children":[],"siblings":[],"spouses":["101565"],"level":1,"name":"Shivalaxmi"},{"id":"101553","parents":["101482","101483"],"children":["101559","101557","101558","101556","101555"],"siblings":["101546","101551"],"spouses":["101554"],"level":1,"name":"Ila"},{"id":"101554","parents":[],"children":[],"siblings":[],"spouses":["101553"],"level":1,"name":"Ramgovin Mevalal"},{"id":"101546","parents":["101482","101483"],"children":["101550","101549","101548"],"siblings":["101553","101551"],"spouses":["101547"],"level":1,"name":"Sita"},{"id":"101547","parents":[],"children":[],"siblings":[],"spouses":["101546"],"level":1,"name":"Shashikant"},{"id":"101551","parents":["101482","101483"],"children":["101586","101585"],"siblings":["101553","101546"],"spouses":["101552"],"level":1,"name":"Arun"},{"id":"101552","parents":[],"children":[],"siblings":[],"spouses":["101551"],"level":1,"name":"Sunanda"},{"id":"101539","parents":["101480","101481"],"children":["101541"],"siblings":["101538","101488","101526"],"spouses":["101540"],"level":1,"name":"Manuben"},{"id":"101540","parents":[],"children":[],"siblings":[],"spouses":["101539"],"level":1,"name":"Surendra"},{"id":"101538","parents":["101480","101481"],"children":[],"siblings":["101488","101539","101526"],"spouses":[],"level":1,"name":"Rasik"},{"id":"101488","parents":["101480","101481"],"children":["101519","101511","101490","101501"],"siblings":["101539","101538","101526"],"spouses":["101489"],"level":1,"name":"Rami"},{"id":"101489","parents":[],"children":[],"siblings":[],"spouses":["101488"],"level":1,"name":"Kunverji Parekh"},{"id":"101526","parents":["101480","101481"],"children":["101530","101528"],"siblings":["101539","101538","101488"],"spouses":["101527"],"level":1,"name":"Kantilal"},{"id":"101527","parents":[],"children":[],"siblings":[],"spouses":["101526"],"level":1,"name":"Saraswati"},{"id":"101486","parents":["101457","101458"],"children":["101571","101581","101578","101574"],"siblings":["101480","101484","101482"],"spouses":["101487"],"level":2,"name":"Devdas"},{"id":"101487","parents":[],"children":["101571","101581","101578","101574"],"siblings":[],"spouses":["101486"],"level":2,"name":"Lakshmi"},{"id":"101480","parents":["101458","101457"],"children":["101539","101538","101488","101526"],"siblings":["101486","101484","101482"],"spouses":["101481"],"level":2,"name":"Harilal"},{"id":"101481","parents":[],"children":["101539","101538","101488","101526"],"siblings":[],"spouses":["101480"],"level":2,"name":"Gulabben"},{"id":"101484","parents":["101458","101457"],"children":["101567","101560","101565"],"siblings":["101486","101480","101482"],"spouses":["101485"],"level":2,"name":"Ramdas"},{"id":"101485","parents":[],"children":["101567","101560","101565"],"siblings":[],"spouses":["101484"],"level":2,"name":"Nirmalaben"},{"id":"101482","parents":["101458","101457"],"children":["101553","101546","101551"],"siblings":["101486","101484","101480"],"spouses":["101483"],"level":2,"name":"Manilal"},{"id":"101483","parents":[],"children":["101553","101546","101551"],"siblings":[],"spouses":["101482"],"level":2,"name":"Sushilaben"},{"id":"101458","parents":[],"children":["101486","101484","101482","101480"],"siblings":[],"spouses":["101457"],"level":3,"name":"Kasturba"},{"id":"101457","parents":["101463","101462"],"children":["101486","101484","101482","101480"],"siblings":["101460","101461","101459"],"spouses":["101458"],"level":3,"name":"Bapu"},{"id":"101460","parents":["101463","101462"],"children":[],"siblings":["101457","101461","101459"],"spouses":[],"level":3,"name":"Raliatben"},{"id":"101461","parents":["101463","101462"],"children":[],"siblings":["101460","101459","101457"],"spouses":[],"level":3,"name":"Karsandas"},{"id":"101459","parents":["101463","101462"],"children":[],"siblings":["101460","101461","101457"],"spouses":[],"level":3,"name":"Laxmidas"},{"id":"101463","parents":[],"children":["101460","101461","101459","101457"],"siblings":[],"spouses":["101462"],"level":4,"name":"Putlima"},{"id":"101462","parents":["101465","101464"],"children":["101460","101461","101459","101457"],"siblings":["101466"],"spouses":["101468","101467","101463"],"level":4,"name":"Karamchand"},{"id":"101467","parents":[],"children":[],"siblings":[],"spouses":["101462"],"level":4,"name":"Muliben"},{"id":"101468","parents":[],"children":[],"siblings":[],"spouses":["101462"],"level":4,"name":"Pankunvarben"},{"id":"101466","parents":[],"children":[],"siblings":["101462"],"spouses":[],"level":4,"name":"Tulsidas"},{"id":"101465","parents":[],"children":["101462"],"siblings":[],"spouses":[],"level":5,"name":"Laxmima"},{"id":"101464","parents":[],"children":["101462"],"siblings":[],"spouses":[],"level":5,"name":"Uttam"}];
//var array = [{"id":"128259","parents":["128258","128257"],"children":[],"siblings":["128260"],"spouses":[],"level":3,"name":"Deepti"},{"id":"128260","parents":["128258","128257"],"children":[],"siblings":["128259"],"spouses":[],"level":3,"name":"Chandan"},{"id":"128255","parents":["128254","128253"],"children":[],"siblings":["128256"],"spouses":[],"level":3,"name":"Manasa"},{"id":"128256","parents":["128253","128254"],"children":[],"siblings":["128255"],"spouses":[],"level":3,"name":"Srinath"},{"id":"128252","parents":["128250","128249"],"children":[],"siblings":["128251"],"spouses":[],"level":3,"name":"Ramya"},{"id":"128251","parents":["128250","128249"],"children":[],"siblings":["128252"],"spouses":[],"level":3,"name":"Divya"},{"id":"128264","parents":["128261","128262"],"children":[],"siblings":["128263"],"spouses":[],"level":3,"name":"Shreya"},{"id":"128263","parents":["128261","128262"],"children":[],"siblings":["128264"],"spouses":[],"level":3,"name":"Shreshta"},{"id":"128292","parents":["128289"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Akshara"},{"id":"128291","parents":["128289"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Sohit"},{"id":"128295","parents":["128293"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Deeksha"},{"id":"128271","parents":["128269"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Padma"},{"id":"128272","parents":["128269"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Sivaram"},{"id":"128276","parents":["128273"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Pranavi"},{"id":"128275","parents":["128273"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Anil"},{"id":"128268","parents":["128265"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Varshini"},{"id":"128267","parents":["128265"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Vaishnavi"},{"id":"128288","parents":["128285"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Srinitya"},{"id":"128287","parents":["128285"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Srikruti"},{"id":"128284","parents":["128281"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Risha"},{"id":"128283","parents":["128281"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Ridhi"},{"id":"128280","parents":["128277"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Sruthika"},{"id":"128279","parents":["128277"],"children":[],"siblings":[],"spouses":[],"level":3,"name":"Saranya"},{"id":"128257","parents":["128235","128236"],"children":["128259","128260"],"siblings":["128253","128249","128261"],"spouses":["128258"],"level":4,"name":"Yz"},{"id":"128258","parents":[],"children":["128259","128260"],"siblings":[],"spouses":["128257"],"level":4,"name":"Sreelatha"},{"id":"128253","parents":["128235","128236"],"children":["128255","128256"],"siblings":["128257","128249","128261"],"spouses":["128254"],"level":4,"name":"Neeraja"},{"id":"128254","parents":[],"children":["128255","128256"],"siblings":[],"spouses":["128253"],"level":4,"name":"Madhav"},{"id":"128249","parents":["128235","128236"],"children":["128252","128251"],"siblings":["128253","128261","128257"],"spouses":["128250"],"level":4,"name":"Sailaja"},{"id":"128250","parents":[],"children":["128252","128251"],"siblings":[],"spouses":["128249"],"level":4,"name":"Sudhakar"},{"id":"128261","parents":["128235","128236"],"children":["128264","128263"],"siblings":["128249","128253","128257"],"spouses":["128262"],"level":4,"name":"Premnath"},{"id":"128262","parents":[],"children":["128264","128263"],"siblings":[],"spouses":["128261"],"level":4,"name":"Sridurga"},{"id":"128289","parents":["128241","128242"],"children":["128292","128291"],"siblings":["128293"],"spouses":["128290"],"level":4,"name":"Aparna"},{"id":"128290","parents":[],"children":[],"siblings":[],"spouses":["128289"],"level":4,"name":"Ravikumar"},{"id":"128293","parents":["128241","128242"],"children":["128295"],"siblings":["128289"],"spouses":["128294"],"level":4,"name":"Satya"},{"id":"128294","parents":[],"children":[],"siblings":[],"spouses":["128293"],"level":4,"name":"Avanti"},{"id":"128269","parents":["128237","128238"],"children":["128271","128272"],"siblings":["128273","128265"],"spouses":["128270"],"level":4,"name":"Poornima"},{"id":"128270","parents":[],"children":[],"siblings":[],"spouses":["128269"],"level":4,"name":"Chakravarthy"},{"id":"128273","parents":["128237","128238"],"children":["128276","128275"],"siblings":["128269","128265"],"spouses":["128274"],"level":4,"name":"Raghu"},{"id":"128274","parents":[],"children":[],"siblings":[],"spouses":["128273"],"level":4,"name":"Vidya"},{"id":"128265","parents":["128237","128238"],"children":["128268","128267"],"siblings":["128269","128273"],"spouses":["128266"],"level":4,"name":"Panini"},{"id":"128266","parents":[],"children":[],"siblings":[],"spouses":["128265"],"level":4,"name":"Sunitha"},{"id":"128299","parents":["128247","128248"],"children":[],"siblings":["128298"],"spouses":[],"level":4,"name":"Akhil"},{"id":"128298","parents":["128247","128248"],"children":[],"siblings":["128299"],"spouses":[],"level":4,"name":"Ashwin"},{"id":"128297","parents":["128245","128246"],"children":[],"siblings":[],"spouses":[],"level":4,"name":"Pranav"},{"id":"128296","parents":["128243","128244"],"children":[],"siblings":[],"spouses":["128300"],"level":4,"name":"SriRajiv"},{"id":"128300","parents":[],"children":[],"siblings":[],"spouses":["128296"],"level":4,"name":"Sarojini"},{"id":"128285","parents":["128239","128240"],"children":["128288","128287"],"siblings":["128281","128277"],"spouses":["128286"],"level":4,"name":"Annapurna"},{"id":"128286","parents":[],"children":[],"siblings":[],"spouses":["128285"],"level":4,"name":"SatyaPhani"},{"id":"128281","parents":["128239","128240"],"children":["128284","128283"],"siblings":["128285","128277"],"spouses":["128282"],"level":4,"name":"Kalyan"},{"id":"128282","parents":[],"children":[],"siblings":[],"spouses":["128281"],"level":4,"name":"Parvatidevi"},{"id":"128277","parents":["128239","128240"],"children":["128280","128279"],"siblings":["128285","128281"],"spouses":["128278"],"level":4,"name":"Phaneendra"},{"id":"128278","parents":[],"children":[],"siblings":[],"spouses":["128277"],"level":4,"name":"Himabindu"},{"id":"128235","parents":["128234","128233"],"children":["128261","128253","128249","128257"],"siblings":["128241","128237","128247","128245","128243","128239"],"spouses":["128236"],"level":5,"name":"Annapurna"},{"id":"128236","parents":[],"children":["128261","128253","128249","128257"],"siblings":[],"spouses":["128235"],"level":5,"name":"Murty"},{"id":"128241","parents":["128234","128233"],"children":["128289","128293"],"siblings":["128235","128237","128247","128245","128243","128239"],"spouses":["128242"],"level":5,"name":"Vijaya"},{"id":"128242","parents":[],"children":["128289","128293"],"siblings":[],"spouses":["128241"],"level":5,"name":"Somayajulu"},{"id":"128237","parents":["128234","128233"],"children":["128269","128273","128265"],"siblings":["128235","128241","128247","128245","128243","128239"],"spouses":["128238"],"level":5,"name":"Saroja"},{"id":"128238","parents":[],"children":["128269","128273","128265"],"siblings":[],"spouses":["128237"],"level":5,"name":"Subbarayan"},{"id":"128247","parents":["128234","128233"],"children":["128299","128298"],"siblings":["128235","128241","128237","128245","128243","128239"],"spouses":["128248"],"level":5,"name":"Srinivas"},{"id":"128248","parents":[],"children":["128299","128298"],"siblings":[],"spouses":["128247"],"level":5,"name":"Sridevi"},{"id":"128245","parents":["128234","128233"],"children":["128297"],"siblings":["128235","128241","128237","128247","128243","128239"],"spouses":["128246"],"level":5,"name":"Murali Sudhakar"},{"id":"128246","parents":[],"children":["128297"],"siblings":[],"spouses":["128245"],"level":5,"name":"Sridevi"},{"id":"128243","parents":["128234","128233"],"children":["128296"],"siblings":["128235","128241","128237","128247","128245","128239"],"spouses":["128244"],"level":5,"name":"Vinayak"},{"id":"128244","parents":[],"children":["128296"],"siblings":[],"spouses":["128243"],"level":5,"name":"Balatripura"},{"id":"128239","parents":["128234","128233"],"children":["128285","128281","128277"],"siblings":["128235","128241","128237","128247","128245","128243"],"spouses":["128240"],"level":5,"name":"Ramachandra"},{"id":"128240","parents":[],"children":["128285","128281","128277"],"siblings":[],"spouses":["128239"],"level":5,"name":"Subbalaxmi"},{"id":"128234","parents":[],"children":["128235","128241","128237","128247","128245","128243","128239"],"siblings":[],"spouses":["128233"],"level":6,"name":"Annapurna"},{"id":"128233","parents":["128232","128231"],"children":["128235","128241","128237","128247","128245","128243","128239"],"siblings":[],"spouses":["128234"],"level":6,"name":"Subbarao"},{"id":"128232","parents":[],"children":["128233"],"siblings":[],"spouses":["128231"],"level":7,"name":"Subbalaxmi"},{"id":"128231","parents":["128230","128229"],"children":["128233"],"siblings":[],"spouses":["128232"],"level":7,"name":"Balakrishna"},{"id":"128230","parents":[],"children":["128231"],"siblings":[],"spouses":[],"level":8,"name":"Ramalaxmi"},{"id":"128229","parents":[],"children":["128231"],"siblings":[],"spouses":[],"level":8,"name":"Jagannatham"}]
//loadNetworkCallBack(array);
}

function loadNetworkCallBack(array) {
    networkdata = array;
    networkdata.forEach(function(elem) {
        if (levels.indexOf(elem.level) < 0) { levels.push(elem.level); }
    });
    levels.sort(function(a, b) { return a - b; });

    levels.forEach(function(level) {
        var startAt = networkdata.filter(function(person) {
            return person.level == level;
        });
        startAt.forEach(function(start) {
            var person = findPerson(start.id);
            plotNode(person, 'self');
            plotParents(person);
        });

    });
    connectParents();
    adjustNegatives();
    connectSpouses();
    applyCssForLines();
}

function plotParents(start) {
	if (! start) { return; }
	start.parents.reduce(function(previousId, currentId) {
		var previousParent = findPerson(previousId),
			currentParent = findPerson(currentId);
		plotNode(currentParent, 'parents', start, start.parents.length);
//		if (previousParent) { plotConnector(previousParent, currentParent, 'spouses'); }
//		plotConnector(start, currentParent, 'parents');
		plotParents(currentParent);
		return currentId;
	}, 0);
}

function plotNode() {
	var person = arguments[0], personType = arguments[1], relative = arguments[2], numberOfParents = arguments[3],
		node = get(person.id), relativeNode, element = {}, thisLevel, exists;
	if (node) { return; }
	node = document.createElement('div');
	node.id = person.id;
	node.classList.add('node'); node.classList.add('asset');
	node.textContent = person.name;
	node.setAttribute('data-level', person.level);

	thisLevel = findLevel(person.level);
	if (! thisLevel) {
		thisLevel = { 'level': person.level, 'top': startTop };
		levelMap.push(thisLevel);
	}

	if (personType == 'self') {
		node.style.left = startLeft + 'px';
		node.style.top = thisLevel.top + 'px';
	} else {
		relativeNode = get(relative.id);
	}
	if (personType == 'spouses') {
		node.style.left = (parseFloat(relativeNode.style.left) + size + (gap * 2)) + 'px';
		node.style.top = parseFloat(relativeNode.style.top) + 'px';
	}
	if (personType == 'children') {
		node.style.left = (parseFloat(relativeNode.style.left) - size) + 'px';
		node.style.top = (parseFloat(relativeNode.style.top) + size + gap) + 'px';
	}
	if (personType == 'parents') {
		if (numberOfParents == 1) {
			node.style.left = parseFloat(relativeNode.style.left) + 'px';
			node.style.top = (parseFloat(relativeNode.style.top) - gap - size) + 'px';
		} else {
			node.style.left = (parseFloat(relativeNode.style.left) - size) + 'px';
			node.style.top = (parseFloat(relativeNode.style.top) - gap - size) + 'px';
		}
	}
//	while (exists = detectCollision2(node)) {
        var exists = detectCollision(node);
        if(exists != undefined){
		    node.style.left = (exists.left + size + (gap * 2)) + 'px';
		}
//	}

	if (thisLevel.top > parseFloat(node.style.top)) {
		updateLevel(person.level, 'top', parseFloat(node.style.top));
	}

	element.id = parseFloat(node.id); element.left = parseFloat(node.style.left); element.top = parseFloat(node.style.top);
	elements.push(element);
	networktree.appendChild(node);

        if(person.spouses!=undefined && person.spouses!=null && person.spouses.length != 0){

            person.spouses.forEach(function(partnerId) {

                var partner = $.grep(networkdata, function(e){ return e.id == partnerId; });

    			plotNode( partner[0], 'spouses', person, 1);
            });

        }

}

/* Helper Functions */
function get(id) { return document.getElementById(id); }

function findPerson(id) {
	var element = networkdata.filter(function(elem) {
		return elem.id == id;
	});
	return element.pop();
}

function findLevel(level) {
	var element = levelMap.filter(function(elem) {
		return elem.level == level;
	});
	return element.pop();
}

function updateLevel(id, key, value) {
	levelMap.forEach(function(level) {
		if (level.level === id) {
			level[key] = value;
		}
	});
}

function detectCollision(node) {
    var currentLeft = 0, currentElement;
    for(var k = 0 ; k < elements.length ; k++){
		var left = parseFloat(node.style.left), elem = elements[k];
		if(elem.top == parseFloat(node.style.top)){
		    if(currentLeft == 0 || elem.left > currentLeft){
		        currentLeft = elem.left;
		        currentElement = elem;
		    }
		}
	}
	return currentElement;
}

function detectCollision1(node) {
	var element = elements.filter(function(elem) {
		var left = parseFloat(node.style.left);
		return ((elem.left == left || (elem.left < left && left < (elem.left + size + gap))) && elem.top == parseFloat(node.style.top));
	});
	return element.pop();
}

function adjustNegatives() {
	var allNodes = document.querySelectorAll('div.asset'),
		minTop = startTop, diff = 0;
	for (var i=0; i < allNodes.length; i++) {
		if (parseFloat(allNodes[i].style.top) < minTop) { minTop = parseFloat(allNodes[i].style.top); }
	};
	if (minTop < startTop) {
		diff = Math.abs(minTop) + gap;
		for (var i=0; i < allNodes.length; i++) {
			allNodes[i].style.top = parseFloat(allNodes[i].style.top) + diff + 'px';
		};
	}

    allNodes = document.querySelectorAll('div.asset'),
        minLeft = startLeft, diff = 0;
    for (var i=0; i < allNodes.length; i++) {
        if (parseFloat(allNodes[i].style.left) < minLeft) { minLeft = parseFloat(allNodes[i].style.left); }
    };
    if (minLeft < startLeft) {
        diff = Math.abs(minLeft) + gap;
        for (var i=0; i < allNodes.length; i++) {
            allNodes[i].style.left = parseFloat(allNodes[i].style.left) + diff + 'px';
        };
    }
}

function plotConnector(source, destination, relation) {
	var connector = document.createElement('div'),
  		orientation, comboId, comboIdInverse, start, stop,
			x1, y1, x2, y2, length, angle, transform;
  // We do not plot a connector if already present
  comboId = source.id + '_' + destination.id;
  comboIdInverse = destination.id + '_' + source.id;
  if (document.getElementById(comboId)) { return; }
  if (document.getElementById(comboIdInverse)) { return; }

  connector.id = comboId;
	orientation = relation == 'spouses' ? 'h' : 'v';
	connector.classList.add('asset');
	connector.classList.add('connector');
	connector.classList.add(orientation);
	start = get(source.id); stop = get(destination.id);
	if (relation == 'spouses') {

		x1 = parseFloat(start.style.left) + size; y1 = parseFloat(start.style.top) + (size/2);
		x2 = parseFloat(stop.style.left); y2 = parseFloat(stop.style.top);

		if(x2 > x1){
		    length = (x2 - x1) + 'px';
    		connector.style.left = x1 + 'px';
        }else {
            length = (x1 - x2) + 'px';
    		connector.style.left = x2 + 'px';
        }
		connector.style.width = length;
		connector.style.top = y1 + 'px';
        // Avoid collision moving down
        while (exists = detectConnectorCollision(connector)) {
          connector.style.top = (parseFloat(exists.style.top) + 4) + 'px';
        }
	}
	if (relation == 'parents') {
		x1 = parseFloat(start.style.left) + (size/2); y1 = parseFloat(start.style.top);
		x2 = parseFloat(stop.style.left) + (size/2); y2 = parseFloat(stop.style.top) + (size - 2);

		length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
		transform = 'rotate(' + angle + 'deg)';

		connector.style.width = length + 'px';
		connector.style.left = x1 + 'px';
		connector.style.top = y1 + 'px';
		connector.style.transform = transform;
	}
	networktree.appendChild(connector);
}

function detectConnectorCollision(connector) {
  var connectors = [].slice.call(document.querySelectorAll('div.connector.h'));
  var element = connectors.filter(function(elem) {
    return ((elem.style.left == connector.style.left) && (elem.style.top == connector.style.top))
  });
  return element.pop();
}

function detectConnectorCollisionV(connector) {
  var connectors = [].slice.call(document.querySelectorAll('div.connector.vertical'));

  for(var k = 0 ; k < connectors.length ; k++){
    var elem = connectors[k];
    var elemIds = elem.id.split("-")[0].split(","), elemId0 = elem.id.split("-")[0];
    if(parseFloat(elemIds[0]) > parseFloat(elemIds[1])){
        elemId0 = elemIds[1]+ "," + elemIds[0];
    }
    var connectorIds = connector.id.split("-")[0].split(","), connectorId0 = connector.id.split("-")[0];
    if(parseFloat(connectorIds[0]) > parseFloat(connectorIds[1])){
        connectorId0 = connectorIds[1]+ "," + connectorIds[0];
    }

    if(( elemId0 != connectorId0) && (elem.style.left == connector.style.left)){
        var elemTop = parseFloat(elem.style.top.replace('px')), elemBottom = elemTop + parseFloat(elem.style.width.replace('px'));
        var connectorTop = parseFloat(connector.style.top.replace('px')), connectorBottom = connectorTop + parseFloat(connector.style.width.replace('px'));

        if(elemTop <= connectorBottom && connectorTop <= elemBottom){
            return true;
        }
        console.log("detectConnectorCollisionV = " + elemTop + ' and ' + elemBottom + ' and ' + connectorTop + ' and ' + connectorBottom);
    }
  }
    return false;
}

function detectConnectorCollisionH(connector) {
  var connectors = [].slice.call(document.querySelectorAll('div.connector.l2'));
  var element = connectors.filter(function(elem) {
//    console.log("detectConnectorCollisionH = " + elem.id.split("-")[0] + ' and ' + connector.id.split("-")[0] + ' and ' + elem.style.top + ' and ' + connector.style.top);
//    return ((elem.style.left == connector.style.left) && (elem.style.top == connector.style.top));
    var elemIds = elem.id.split("-")[0].split(","), elemId0 = elem.id.split("-")[0];
    if(parseFloat(elemIds[0]) > parseFloat(elemIds[1])){
        elemId0 = elemIds[1]+ "," + elemIds[0];
    }
    var connectorIds = connector.id.split("-")[0].split(","), connectorId0 = connector.id.split("-")[0];
    if(parseFloat(connectorIds[0]) > parseFloat(connectorIds[1])){
        connectorId0 = connectorIds[1]+ "," + connectorIds[0];
    }

    if(( elemId0 != connectorId0) && (elem.style.top == connector.style.top)){
        var elemLeft = parseFloat(elem.style.left.replace('px')), elemRight = elemLeft + parseFloat(elem.style.width.replace('px'));
        var connectorLeft = parseFloat(connector.style.left.replace('px')), connectorRight = connectorLeft + parseFloat(connector.style.width.replace('px'));
//        if(elemLeft >= connectorLeft && elemRight >= connectorLeft){
//            return false
//        }

        if(elemLeft <= connectorLeft){
            if(elemRight <= connectorLeft)
             return false;
        }
        console.log("detectConnectorCollisionH = " + elemLeft + ' and ' + elemRight + ' and ' + connectorLeft + ' and ' + connectorRight);
        return true;
    }
    else {
        return false;
    }
  });
  return element.pop();
}

function connectParents(){
    console.log('elements = ' + JSON.stringify(elements));
    networkdata.forEach(function(elem) {
        console.log('element = ' + JSON.stringify(elem));
        if(elem.parents.length != 0){
            var fatherId = elem.parents[0];
            var motherId = elem.parents[1];
            console.log('father = ' + fatherId + " and " + motherId);

            var father = $.grep(elements, function(e){ return e.id == fatherId; });
            var mother = $.grep(elements, function(e){ return e.id == motherId; });
            var currentPerson = $.grep(elements, function(e){ return e.id == elem.id; });
            console.log('father = ' + JSON.stringify(father) + " and " + JSON.stringify(mother) + " and " + JSON.stringify(currentPerson));
            drawVerticalLine(father[0], mother[0], currentPerson[0]);
        }
    });
}

function drawVerticalLine(source1, source2, destination) {
	var connector = document.createElement('div'),
  		orientation, comboId, comboIdInverse, start, stop,
			x1, y1, x2, y2, length, angle, transform;
  // We do not plot a connector if already present

  if(source2 == undefined){
    comboId = source1.id + '_' + destination.id + '-l1';
    comboIdInverse = destination.id + '_' + source1.id + '-l1';
  }else{
    comboId = source1.id + '_' + source2.id + '-' + destination.id + '-l1';
    comboIdInverse = destination.id + '-' + source1.id + '_' + source2.id + '-l1';
  }
  if (document.getElementById(comboId)) { return; }
  if (document.getElementById(comboIdInverse)) { return; }

    connector.id = comboId;
	orientation = 'v';
	connector.classList.add('asset');
	connector.classList.add('connector');
	connector.classList.add(orientation);
	connector.classList.add('vertical');

  if(source2 != undefined){
    x1 = (source1.left + size + source2.left) / 2;
    y1 = parseFloat(source1.top) + (size/2);
  }else{
    x1 = source1.left + (size/2);
    y1 = parseFloat(source1.top) + (size/2);
  }
    x2 = parseFloat(destination.left) + (size/2);
    y2 = parseFloat(destination.top);

		length = (y2 - y1) + 'px';

        if(x1 != x2){
            length = (y2 - y1) * (2/3) + 'px';
        }

		connector.style.width = length;
		connector.style.left = x1 + 'px';
		connector.style.top = y1 + 'px';

        transform = 'rotate(90deg)';
        connector.style.transform = transform;


//        Avoid collision moving down

        if (detectConnectorCollisionV(connector)) {
          connector.style.left = (parseFloat(connector.style.left) + 4) + 'px';
        }
    	networktree.appendChild(connector);

        if(x1 != x2){
//            if(source2 != undefined){
                drawHorizontalLine(source1, source2, destination, connector);
//            }
        }
}

function drawHorizontalLine(source1, source2, destination, prevConnector) {
	var connector = document.createElement('div'),
  		orientation, comboId, comboIdInverse, start, stop,
			x1, y1, x2, y2, length, angle, transform;
  // We do not plot a connector if already present

  if(source2 == undefined){
    comboId = source1.id + '_' + destination.id + '-l2';
    comboIdInverse = destination.id + '_' + source1.id + '-l2';
  }else{
    comboId = source1.id + '_' + source2.id + '-' + destination.id + '-l2';
    comboIdInverse = destination.id + '-' + source1.id + '_' + source2.id + '-l2';
  }

  if (document.getElementById(comboId)) { return; }
  if (document.getElementById(comboIdInverse)) { return; }

    connector.id = comboId;
	orientation = 'v';
	connector.classList.add('asset');
	connector.classList.add('connector');
	connector.classList.add('l2');
	connector.classList.add(orientation);

  if(source2 != undefined){
    x1 = parseFloat(prevConnector.style.left);
    y1 = parseFloat(source1.top) + (size/2) + (size * (2/3));
  }else{
    x1 = parseFloat(prevConnector.style.left);
    y1 = parseFloat(source1.top) + (size/2) + (size * (2/3));
  }

    x2 = parseFloat(destination.left) + (size/2);
    y2 = parseFloat(source1.top) + (size/2) + (size * (2/3));

    if(x1 > x2){
        length = ((x1 - x2)) + 'px';
		connector.style.left = x2 + 'px';
    }else{
        length = ((x2 - x1)) + 'px';
		connector.style.left = (x1) + 'px';
    }
		connector.style.width = length;
		connector.style.top = (y1+1) + 'px';
        var isCollision = false;

//        Avoid collision moving down
        while (exists = detectConnectorCollisionH(connector)) {
          connector.style.top = (parseFloat(exists.style.top) - 4) + 'px';
          y2 = y2 - 4 ;
//          prevConnector.style.top = (parseFloat(prevConnector.style.top.replace('px')) - 5) + 'px';
          prevConnector.style.width = (parseFloat(prevConnector.style.width.replace('px')) - 4) + 'px';
          connector.classList.remove('v');
          connector.classList.add('v');
          prevConnector.classList.remove('v');
          prevConnector.classList.add('v');
          isCollision = true;
        }

	networktree.appendChild(connector);
    drawVerticalLine2(source1, source2, destination, x2, y2, isCollision, connector);
}

function drawVerticalLine2(source1, source2, destination, x2, y2, isCollision, prevConnector) {
	var connector = document.createElement('div'),
  		orientation, comboId, comboIdInverse, start, stop,
			x1, y1, length, angle, transform;
  // We do not plot a connector if already present

      if(source2 == undefined){
        comboId = source1.id + '_' + destination.id + '-l3';
        comboIdInverse = destination.id + '_' + source1.id + '-l3';
      }else{
        comboId = source1.id + '_' + source2.id + '-' + destination.id + '-l3';
        comboIdInverse = destination.id + '-' + source1.id + '_' + source2.id + '-l3';
      }

  if (document.getElementById(comboId)) { return; }
  if (document.getElementById(comboIdInverse)) { return; }

    connector.id = comboId;
    if(isCollision)
	    orientation = 'v';
	else
        orientation = 'v';

	connector.classList.add('asset');
	connector.classList.add('connector');
	connector.classList.add(orientation);
	connector.classList.add('vertical');

    x1 = x2;
    y1 = parseFloat(destination.top);

    if(y1 > y2){
       length = (y1 - y2) + 'px';
    }else{
       length = (y2 - y1) + 'px';
    }
		connector.style.width = length;
		connector.style.left = x1 + 'px';
		connector.style.top = y2 + 'px';

        transform = 'rotate(90deg)';
        connector.style.transform = transform;

//      Avoid collision moving down
        if (detectConnectorCollisionV(connector)) {
          connector.style.left = (parseFloat(connector.style.left) + 4) + 'px';
          //var horizontalLineId = comboId.split("-")[0] + "-" + comboId.split("-")[1] + '-l2';
          //var horizontalLine = $('#'+horizontalLineId)[0];
          prevConnector.style.width = (parseFloat(prevConnector.style.width) + 4) + 'px';
        }

	networktree.appendChild(connector);
}

function connectSpouses(){
    console.log('elements = ' + JSON.stringify(elements));
    networkdata.forEach(function(elem) {
        console.log('element = ' + JSON.stringify(elem));
        if(elem.spouses!=undefined && elem.spouses!=null && elem.spouses.length != 0){

            elem.spouses.forEach(function(partnerId) {

                var partner = $.grep(networkdata, function(e){ return e.id == partnerId; });
		        plotConnector(elem, partner[0], 'spouses');

            });

        }
        if(elem.parents!=undefined && elem.parents!=null && elem.parents.length > 1){
            var partner1 = $.grep(networkdata, function(e){ return e.id == elem.parents[0]; });
            var partner2 = $.grep(networkdata, function(e){ return e.id == elem.parents[1]; });
            plotConnector(partner1[0], partner2[0], 'spouses');
        }

    });
}

function applyCssForLines() {
  var idJson = {}, groupIdJson = {};
  var connectors = [].slice.call(document.querySelectorAll('div.connector'));
  var element = connectors.filter(function(elem) {
    var elemId = elem.id, groupElemId1,groupElemId2;
    if(elemId.indexOf('_')!=-1){
        elemId = elemId.split('-')[0];
        groupElemId1 = elemId.split('_')[0];
        groupElemId2 = elemId.split('_')[1];
        console.log(groupElemId1 +" and "+groupElemId2 +" and "+elemId +" and "+elem.id );
    }

    if(!idJson.hasOwnProperty(elemId)){
        idJson[elemId] = [];
    }

    idJson[elemId].push(elem.id);

    if(elem.id.indexOf('l') != -1){
        if(!groupIdJson.hasOwnProperty(groupElemId1)){
            groupIdJson[groupElemId1] = {};
        }

            if(!groupIdJson[groupElemId1].hasOwnProperty(elemId)){
                groupIdJson[groupElemId1][elemId] = {};
            }
                    var innerElemId = elem.id.replace('-l1','').replace('-l2','').replace('-l3','');
                    if(!groupIdJson[groupElemId1][elemId].hasOwnProperty(innerElemId)){
                        groupIdJson[groupElemId1][elemId][innerElemId] = [];
                    }
                    groupIdJson[groupElemId1][elemId][innerElemId].push(elem.id)

        if(!groupIdJson.hasOwnProperty(groupElemId2)){
            groupIdJson[groupElemId2] = {};
        }

            if(!groupIdJson[groupElemId2].hasOwnProperty(elemId)){
                groupIdJson[groupElemId2][elemId] = {};
            }
                    var innerElemId = elem.id.replace('-l1','').replace('-l2','').replace('-l3','');
                    if(!groupIdJson[groupElemId2][elemId].hasOwnProperty(innerElemId)){
                        groupIdJson[groupElemId2][elemId][innerElemId] = [];
                    }
                    groupIdJson[groupElemId2][elemId][innerElemId].push(elem.id)

                }
  });
      console.log(JSON.stringify(groupIdJson));
      var j = 0;
$.each(idJson, function(i, arrayIds) {
    var k = (j++)%7;
    arrayIds.forEach(function(item) {
        item = item +'';
        jQuery('#'+item).addClass('v'+k).removeClass('v').removeClass('h');
    });
});

$.each(groupIdJson, function(i, spouseJson) {
    var l = 0;
    var spouseJsonLength = Object.keys(spouseJson).length;
    if(spouseJsonLength>1){

        $.each(spouseJson, function(j, childLinesJson) {
            if(l == 0)
                l = 3;
            else if(l == 3)
                l = 6;
            else if(l == 6)
                l = 0;
            $.each(childLinesJson, function(k, arrayIds) {
                arrayIds.forEach(function(item) {
                    item = item +'';
                    var item2 = item.split('-')[0];

                    jQuery('#'+item).removeClass('v0').removeClass('v1').removeClass('v2').removeClass('v3').removeClass('v4').removeClass('v5').removeClass('v6').removeClass('v');
                    jQuery('#'+item).addClass('v'+l).removeClass('v').removeClass('h');
                    jQuery('#'+item2).removeClass('v0').removeClass('v1').removeClass('v2').removeClass('v3').removeClass('v4').removeClass('v5').removeClass('v6').removeClass('v');
                    jQuery('#'+item2).addClass('v'+l).removeClass('v').removeClass('h');
                });
            });
        });
    }
});
}